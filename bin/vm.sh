#!/usr/bin/env bash

# A script to setup dev/test env for magnus

wget http://mirrors.163.com/.help/sources.list.trusty -O /etc/apt/sources.list

apt-get update
apt-get install -y git expect zsh python-pip python-dev ruby ruby-dev htop libpq-dev  python-flake8
# pg client
pip install pgcli 
# git rebase tool
gem sources --add https://ruby.taobao.org/ --remove https://rubygems.org/
gem install git-up

# cd / && git clone https://hangyan@bitbucket.org/mathildetech/magnus.git
git config --local user.email hangyan@alauda.io \
  && git config --local user.name "Hang Yan" \
  && git config --global core.editor "vim" \
  && git config --global push.default current \
  && pip install -r requirements.txt \
  && mkdir -p /var/log/mathilde
  

# simple vim configure
git clone git://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_basic_vimrc.sh



# oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s /usr/bin/zsh

