#!/usr/bin/env bash
SERVER='54.223.32.47:8080'
URL='http://'$SERVER'/v1'

USER='yayu'
SERVICE='lash'
REGION='pri-aws-yayu-cn-north-1-1985922602955064'
APP='app'

# get token 
TOKEN=$(http -b POST $URL/api-token-auth/ username=foo password=bar | jq .token | tr -d '"')
HEADER="Authorization: JWT $TOKEN"

# delete service
http -b DELETE $URL/services/$USER/$SERVICE?application=app "$HEADER" 
echo "Delete service. |  $SERVICE."

# delete region
http -b DELETE $URL/regions/$USER/$REGION "$HEADER"
echo "Delete region. |  $REGION."

