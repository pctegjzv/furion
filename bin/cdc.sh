#!/usr/bin/env bash

# crontab example:
# */5 * * * * /root/cdc.sh >> /tmp/furion.log

docker pull index.alauda.cn/mathildedev/furion:release | grep -q "Status: Image is up to date for index.alauda.cn/mathildedev/furion:release"

[ $? -eq 0 ] && exit 0

echo "-----------------"
date +%Y-%m-%d:%H:%M:%S
echo "Update furion ..."

docker rm -f furion

docker run -d --net=host --name=furion \
    -e LOG_LEVEL=DEBUG \
    -e LOG_HANDLER="debug,info,error,color" \
    -e LOG_PATH=/var/log/mathilde/ \
    -e DB_ENGINE="postgresql"  \
    -e DB_USER=mathilde  \
    -e DB_NAME=furiondb  \
    -e DB_HOST="prod-alaudacn.coc6eeslp1iv.rds.cn-north-1.amazonaws.com.cn" \
    -e DB_PASSWORD="Q*PRDF7nF4&M5&xsMK7^YDn5" \
    -v /var/log/mathilde:/var/log/mathilde \
    index.alauda.cn/mathildedev/furion:release

