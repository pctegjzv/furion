#!/usr/bin/env bash


docker run -d --net=host --name=furion \
    -e LOG_LEVEL=DEBUG \
    -e LOG_HANDLER="debug,info,error,color" \
    -e LOG_PATH=/var/log/mathilde/ \
    -e DB_ENGINE="postgresql"  \
    -e DB_USER=mathilde  \
    -e DB_NAME=furiondb  \
    -e DB_HOST="172.31.24.201" \
    -e DB_PASSWORD="07Apples" \
    -v /var/log/mathilde:/var/log/mathilde \
    index.alauda.cn/mathildedev/furion

