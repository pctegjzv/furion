#!/usr/bin/env bash

set_env()
{
  export LOG_LEVEL=DEBUG
  export LOG_HANDLER="debug,info,error,color"
  export LOG_PATH=/var/log/mathilde/

  export DB_ENGINE="postgresql"
  export DB_USER=mathilde
  export DB_NAME=furiondb
  export DB_HOST="prod-alaudacn.coc6eeslp1iv.rds.cn-north-1.amazonaws.com.cn"
  export DB_PASSWORD="Q*PRDF7nF4&M5&xsMK7^YDn5"
}

main()
{
  set_env

  while [[ $# > 0 ]]
  do
    key="$1"
    case $key in
      -r|--run)
        python manage.py runserver 0.0.0.0:8080
        exit
        ;;
      -m|--migration)
        python manage.py makemigrations
        python manage.py migrate
        exit
        ;;
      -s|--shell)
        ./manage.py shell
        exit
        ;;
      -c|--cron)
        FOR_CRON=true
        shift
        ;;
      *)

        ;;
    esac
  done

}

main "$@"
