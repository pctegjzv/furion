#!/usr/bin/env bash



set_env()
{
  export LOG_LEVEL=DEBUG
  export LOG_HANDLER="debug,info,error,color"
  export LOG_PATH=/var/log/mathilde/

  export DB_HOST=123.57.29.83
  export DB_USER=mathilde
  export DB_PASSWORD=07Apples
  export DB_PORT=5432
  export DB_ENGINE=postgresql
  export DB_NAME=furiondb

  export PHOENIX_ENDPOINT=http://localhost:18080
  export PHOENIX_TOKEN=a55330ad2d845dbd04cb370f5963b062d15e364c
  export PHOENIX_PASSWORD=07Apples
  export PHOENIX_USERNAME=alauda

  export TINY_ENDPOINT=http://tiny-int.alauda.cn:8080/v2


}

main()
{
  set_env

  while [[ $# > 0 ]]
  do
    key="$1"
    case $key in
      -r|--run)
        python manage.py runserver 0.0.0.0:8080
        exit
        ;;
      -m|--migration)
        python manage.py migrate
        exit
        ;;
      -s|--shell)
        ./manage.py shell
        exit
        ;;
      *)

        ;;
    esac
  done

}

main "$@"
