#!/usr/bin/env bash

set_env()
{

  export $(cat ./furion.env | xargs)

}

set_env_cluster ()
{
  export REDIS_KEY_PREFIX_READER=docker_
  export REDIS_TYPE_READER=cluster
  export REDIS_STARTUP_NODES_READER=123.123.123.123:30001,123.123.123.123:30002,123.123.123.123:30003,123.123.123.123:30004,123.123.123.123:30005,123.123.123.123:30006
  export CELERY_BROKER=redis-cluster://123.123.123.123:30001/0?alts=123.123.123.123:30002,123.123.123.123:30003,123.123.123.123:30004,123.123.123.123:30005,123.123.123.123:30006
  export CELERY_RESULT_BACKEND=db+mysql://admin:udNMnPQe88Pd@123.123.123.123/test
}

main()
{
  set_env

  while [[ $# > 0 ]]
  do
    key="$1"
    case $key in
      -rc|--redis-cluster)
        echo "Using redis cluster"
        set_env_cluster
        shift
        ;;
      -r|--run)
        python manage.py runserver 0.0.0.0:8080
        exit
        ;;
      -m|--migration)
        python manage.py migrate
        exit
        ;;
      -s|--shell)
        ./manage.py shell
        exit
        ;;
      -d|--db)
        psql -h 127.0.0.1 -U mathilde -W furiondb
        exit
        ;;
      -w|--worker)
        celery -A furion worker --concurrency=5 -l debug
        exit
        ;;
      -c|--celery)
        shift
        celery -A furion $@
        exit
        ;;
      *)
        exit
        ;;
    esac
  done

}

main "$@"
