#CHANGELOG

## 2017-02-24
# Suppport mount-points as default feature

## 2017-02-15
# Support add node and update node to worker for swarm cluster

## 2017-02-14
# Support create registry endpoint with URL format is <region_name>-<namespace>.suffix
# Since docker does't support registry endpoint contain _ and : at the same time, so replace _ with - in region_name.

## 2017-01-22
# Support upgrade specify components.
# Fix AWS region bug
# Use root account in region

## 2017-01-17
* Support transfer user data to alchemist format.
* Support valid data

## 2017-01-03
* Support add swarm worker
* Update alchemist

## 2016-12-28
* Tune update node API

## 2016-12-27
* Support swarm metrics

## 2016-12-04
* Add worker mechanism in Furion.
* Make update region/node state more reasonable.
* Change the mechanism of creating node.
* Improve cronjob.
* Add CRITICAL state for region/node
* Add enable/disable feature mechanism.

## 2016-12-05
* Update alchemist version

## 2016-12-01
* Support swarm_poc mode for acmp
* Support Swarm as container manager

## 2016-11-08
* Support tunnel port recycle

## 2016-10-24
* Support self-service region.
* Support user create a region via furion.
* Support user to make an empty region to ACMP(Alauda Container Manager Platform) region.
* Support user to make an ACMP region to empty region.
* Support user add empty/mesos-slave/mesos-slave-attribute node

## 2016-10-24
* Fix: When update region with both pipeline and tunnel, furion cannot get right port mapping

## 2016-10-18
* Add back cron job for django_cron can't init with django settings with conditions.
* Add custom user_data for adding nodes

## 2016-10-12
* Divide cron job into another service to avoid potential race conditions
  between multi instances.

## 2016-10-09
* Support pipeline

## 2016-09-27
* Update alchemist version

## 2016-09-21
* Support add node for Qingcloud
* Support register volume info with tunnel when create or update region

## 2016-09-20
* Support add slave which needs attribute
* update RegionSettings data when remove a node
* Fix: When add more than one node, all node will deploy all resource.

## 2016-09-19
* Remove node from phoenix when remove node in furion

## 2016-09-09
* Ignore region info if an env not belong any regions when alarm unhealthy node.
* Since customers may add node by themself, so, will not update mesos-slave info in RegionSetting model.
* Add lock in job. So only one container can execute the job each time
* Rename some unfit variable
* Add block and unblock for upgrade region

## 2016-09-04
* Support alarm when node step into unhealthy state

## 2016-08-30
* Support update cluster
* Support query current verions
* Support query default versions
* Support query region settings

## 2016-08-16
* Support add node via phoenix.

## 2016-08-11
* Add more encrypt region secret keys

## 2016-08-01
* Support encrypt region secret keys

## 2016-07-28
* Support max-disk-usage metrics

## 2016-06-30
* Support create region with tunnel

## 2016-06-07
* Region / Node comps
* Region / Node comps metrics

## 2016-06-02
* Use sange(logs,response,middleware...)
* Add api for components(not complete yet)
* Add --region-name to node scripts
* Change node metrics resource_type to node

## 0.2.2 (2016-05-24)
* Node/Region Dashboard
* Separate utils to a new common module(sange)

## 0.2.1 (2016-05-23)
* Remove auth
* Add logs for uwsgi

## 0.2.0 (2016-05-03)
* Node scaling
* Add cron job to delete failed node
* Add key-pair list api


## 0.1.3 (2016-04-19)
* Deploy scripts support insecure registry
* Support tag mesos slaves

## 0.1.2 (2016-04-07)
* Add api for deploy scripts
* Optimize node list api

## 0.1.1 (2016-04-02)
* Support get IAAS related config for region
* Support show various node state on page
* Sort node list by state
* Support get node detail


## 0.1.0 (2016-03-23)
* Support List/Get Regions info
* Support display region mesos metrics
* Support list nodes in region (mesos-slaves)
* Support Add/Start/Stop/Delete Node in region
