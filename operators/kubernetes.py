from __future__ import absolute_import
import base64
import logging
from operators.resource import feature_clusterrolebinding
from urllib3.exceptions import MaxRetryError

from kubernetes.client import (Configuration, ApiClient, RbacAuthorizationV1Api,
                               CoreV1Api, VersionApi, V1ConfigMap,
                               V1Secret, V1ObjectMeta, V1Namespace)
from kubernetes.client.rest import ApiException

from app_v2.region.entities import NodeListData


logger = logging.getLogger(__name__)

# define call apiserver timeout.
TIMEOUT = 5


class KubernetesApiError(Exception):

    def __init__(self, reason):
        self.reason = reason

    def __str__(self):
        return self.reason


class KubernetesUnauthorized(KubernetesApiError):

    def __init__(self):
        self.reason = "Current user's certificate is invalid."


class KubernetesForbidden(KubernetesApiError):

    def __init__(self):
        self.reason = "Current user does not have permission to perform this action."


class KubernetesResNotFound(KubernetesApiError):

    def __init__(self):
        self.reason = "Specified resource not found."


class KubernetesManager(object):
    def __init__(self, endpoint, token):
        """Initialize a kubernetes api operator. """
        config = Configuration()
        config.verify_ssl = False
        config.api_key_prefix = {'authorization': 'Bearer'}
        config.api_key = {'authorization': token}
        config.host = endpoint
        self.client = ApiClient(configuration=config)
        self.core_client = CoreV1Api(api_client=self.client)
        self.rbac_client = RbacAuthorizationV1Api(api_client=self.client)
        self.version_client = VersionApi(api_client=self.client)

    def get_version(self):
        res = self._call(self.version_client, "get_code")
        return res.git_version[1:]

    def list_nodes(self):
        res = self._call(self.core_client, "list_node").to_dict()
        return NodeListData(data=res)
    
    def create_namespace(self, namespace):
        body = {"metadata": {"name": namespace}}
        return self._call(self.core_client, "create_namespace", body).to_dict()
    
    def check_namespace(self, namespace):
        namespace_list = self._call(self.core_client, "list_namespace").to_dict()
        for n in namespace_list['items']:
            if n["metadata"]["name"] == namespace:
                return True
        return False

    def check_cluster_role_binding(self):
        try:
            self._call(self.rbac_client,
                       "read_cluster_role_binding",
                       feature_clusterrolebinding['metadata']['name'])
        except KubernetesResNotFound:
            return False
        return True

    def create_namespace_binding(self, namespace):
        feature_clusterrolebinding["subjects"][0]["namespace"] = namespace
        body = feature_clusterrolebinding
        return self._call(self.rbac_client, "create_cluster_role_binding", body).to_dict()

    def add_namespace_label(self, namespace, label):
        body = {"metadata": {"labels": label}}
        return self._call(self.core_client, "patch_namespace", namespace, body).to_dict()

    def check_namespace_label(self, namespace):
        body = self._call(self.core_client, "read_namespace", namespace)
        if body.metadata.labels is not None and "alauda-registered-region" in body.metadata.labels:
            return True, body.metadata.labels["alauda-registered-region"]
        return False, None

    def delete_namespace_label(self, namespace):
        body = self._call(self.core_client, "read_namespace", namespace)
        if body.metadata.labels is not None and "alauda-registered-region" in body.metadata.labels:
            del body.metadata.labels['alauda-registered-region']
        return self._call(self.core_client, "replace_namespace", namespace, body).to_dict()

    def get_configmap_data(self, namespace, name):
        try:
            body = self._call(self.core_client, "read_namespaced_config_map", name, namespace)
        except KubernetesResNotFound:
            return {}
        else:
            return body.data

    def get_secret_data(self, namespace, name):
        try:
            body = self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            return {}
        else:
            return self._decode_secret_data(body.data)

    def get_dockercfg_secret(self, namespace, name):
        try:
            body = self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            return None
        else:
            return body.data.get(".dockerconfigjson", None)

    def apply_configmap_data(self, namespace, name, data):
        self.assure_namespace_exist(namespace)
        try:
            self._call(self.core_client, "read_namespaced_config_map", name, namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespaced_config_map", namespace,
                       V1ConfigMap(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                   kind="ConfigMap", data=data))
        else:
            self._call(self.core_client, "patch_namespaced_config_map", name, namespace,
                       V1ConfigMap(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                   kind="ConfigMap", data=data))

    def apply_secret_data(self, namespace, name, data):
        self.assure_namespace_exist(namespace)
        try:
            self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespaced_secret", namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", data=self._encode_secret_data(data)))
        else:
            self._call(self.core_client, "patch_namespaced_secret", name, namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", data=self._encode_secret_data(data)))

    def apply_dockercfg_secret(self, namespace, name, data):
        self.assure_namespace_exist(namespace)
        try:
            self._call(self.core_client, "read_namespaced_secret", name, namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespaced_secret", namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", type="kubernetes.io/dockerconfigjson",
                                data={".dockerconfigjson": data}))
        else:
            self._call(self.core_client, "patch_namespaced_secret", name, namespace,
                       V1Secret(metadata=V1ObjectMeta(name=name, namespace=namespace),
                                kind="Secret", type="kubernetes.io/dockerconfigjson",
                                data={".dockerconfigjson": data}))

    def assure_namespace_exist(self, namespace):
        try:
            self._call(self.core_client, "read_namespace", namespace)
        except KubernetesResNotFound:
            self._call(self.core_client, "create_namespace",
                       V1Namespace(metadata=V1ObjectMeta(name=namespace), kind="Namespace"))

    def _encode_secret_data(self, data):
        return {
            k: base64.b64encode(v)
            for k, v in data.items()
        }

    def _decode_secret_data(self, data):
        return {
            k: base64.b64decode(v)
            for k, v in data.items()
        }

    def _call(self, client, func_name, *args, **kwargs):
        kwargs["_request_timeout"] = TIMEOUT
        func = getattr(client, func_name)
        try:
            return func(*args, **kwargs)
        except ApiException as err:
            if err.status == 401:
                raise KubernetesUnauthorized()
            elif err.status == 403:
                raise KubernetesForbidden()
            elif err.status == 404:
                raise KubernetesResNotFound()
            raise KubernetesApiError("{}: {}".format(err.reason, err.body))
        except MaxRetryError:
            msg = "can not connect to kubernetes api server."
            raise KubernetesApiError(msg)
