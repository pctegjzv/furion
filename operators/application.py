from __future__ import absolute_import

from django.conf import settings

from mekansm.request import MekRequest
from mekansm.exceptions import MekServiceException

from app_v2.region.entities import ApplicationInfo


class ApplicationNotFound(Exception):

    def __init__(self, id):
        self.id = id

    def __str__(self):
        return "application {} not found".format(self.id)


class ApplicationOperator:

    @staticmethod
    def get_template(*args, **kwargs):
        return Chen2Operator.get_template(*args, **kwargs)

    @staticmethod
    def list_public_templates(*args, **kwargs):
        return Chen2Operator.list_public_templates(*args, **kwargs)

    @staticmethod
    def create_template_app(*args, **kwargs):
        return Chen2Operator.create_template_app(*args, **kwargs)

    @staticmethod
    def get_application_instance(*args, **kwargs):
        try:
            return JakiroOperator.get_application_instance(*args, **kwargs)
        except MekServiceException as err:
            if err.status_code == 404:
                raise ApplicationNotFound(id)
            raise err

    @staticmethod
    def check_application_instance(*args, **kwargs):
        try:
            return JakiroOperator.check_application_instance(*args, **kwargs)
        except MekServiceException as err:
            if err.status_code == 404:
                raise ApplicationNotFound(id)
            raise err

    @staticmethod
    def list_application_instances(*args, **kwargs):
        return JakiroOperator.list_application_instances(*args, **kwargs)

    @staticmethod
    def delete_application_instance(*args, **kwargs):
        return JakiroOperator.delete_application_instance(*args, **kwargs)


class Chen2Operator(MekRequest):
    endpoint = settings.CHEN2_ENDPOINT

    @classmethod
    def get_template(cls, template_id):
        return cls.send("/v1/catalog/templates/{}".format(template_id), method="GET")["data"]

    @classmethod
    def list_public_templates(cls, template_type=None):
        params = {
            "name": "alauda_public_rp"
        }
        if template_type:
            params["template_type"] = template_type
        res = cls.send("/v1/catalog/template_public_repositories", method="GET", params=params)["data"]
        if len(res) <= 0:
            return {}
        return {
            t["name"]: t
            for t in res[0]["templates"] if t['is_active']
        }

    @classmethod
    def create_template_app(cls, region_id, region_name, namespace, name, template, values_yaml_content, token):
        post_data = {
            "name": name,
            "values_yaml_content": values_yaml_content,
            "template": {
                "uuid": template["uuid"],
                "name": template["name"],
                "version": {
                    "uuid": template["versions"][0]["uuid"]
                }
            },
            "namespace": {"name": namespace},
            "cluster": {"uuid": region_id, "name": region_name},
            "user_token": token
        }
        res = cls.send("/v1/catalog/applications", method="POST", data=post_data)["data"]
        return ApplicationInfo(data={
            "uuid": res["resource"]["uuid"],
            "name": res["resource"]["name"],
            "status": res["resource"].get("status", "")
        })


class KrobelusOperator(MekRequest):
    endpoint = settings.KROBELUS_ENDPOINT


class JakiroOperator(MekRequest):
    endpoint = settings.JAKIRO_ENDPOINT

    @classmethod
    def delete_application_instance(cls, uuid, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        cls.send("/v2/apps/{}".format(uuid), method="DELETE", headers=headers)

    @classmethod
    def get_application_instance(cls, uuid, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        data = cls.send("/v2/apps/{}".format(uuid), method="GET", headers=headers)["data"]
        return ApplicationInfo(data={
            "uuid": data["resource"]["uuid"],
            "name": data["resource"]["name"],
            "status": data["resource"]["status"],
            "version": data["catalog_info"]["template"]["version"]["uuid"]
        })

    @classmethod
    def check_application_instance(cls, uuid, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        cls.send("/v2/apps/{}".format(uuid), method="GET", headers=headers)["data"]

    @classmethod
    def list_application_instances(cls, uuids, token):
        headers = {
            "Authorization": "Token {}".format(token)
        }
        apps = []
        # Since jakiro not support /v2/apps with uuid. We use for loop.
        for uuid in uuids:
            data = cls.send("/v2/apps/{}".format(uuid), method="GET", headers=headers)["data"]
            apps.append(ApplicationInfo(data={
                "uuid": data["resource"]["uuid"],
                "name": data["resource"]["name"],
                "status": data["resource"]["status"],
                "version": data["catalog_info"]["template"]["version"]["uuid"]
            }))
        return apps
