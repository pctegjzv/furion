feature_clusterrolebinding = {
        "apiVersion": "rbac.authorization.k8s.io/v1",
        "kind": "ClusterRoleBinding",
        "metadata": {
                "annotations": {
                        "rbac.authorization.kubernetes.io/autoupdate": "true"},
                "labels": {
                        "kubernetes.io/bootstrapping": "rbac-defaults"},
                "name": "system:controller:clusterrole-aggregation-controller-alauda"
        },
        "roleRef": {
                "apiGroup": "rbac.authorization.k8s.io",
                "kind": "ClusterRole",
                "name": "system:controller:clusterrole-aggregation-controller"
        },
        "subjects": [
                {
                        "kind": "ServiceAccount",
                        "name": "default",
                        "namespace": "alaudaee"
                }
        ]
}
