#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import Http404
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from schema import Schema
from controls.node import NodeControl, NodeActionControl
from controls.region import RegionControl
from controls.stats import StatsControl
from controls.tasks import NodeControlAsync  # NOQA
from furion.exception import FurionException
from util import const
from alauda.common.web import empty_response, json_response
from django.conf import settings
from util.phoenix import connection_validation
from alauda.tracing import tracing_response_time

from util.validation import validate

__author__ = 'Hang Yan'


def _get_region(region_id):
    return RegionControl.get_object(id=region_id)


@api_view(['PUT'])
@connection_validation
@tracing_response_time(module='nodes')
def start_node(request, region_id, private_ip):
    NodeActionControl.start(_get_region(region_id), private_ip)
    return empty_response()


@api_view(['PUT'])
@connection_validation
@tracing_response_time(module='nodes')
def stop_node(request, region_id, private_ip):
    NodeActionControl.stop(_get_region(region_id), private_ip)
    return empty_response()


@api_view(['PUT'])
@connection_validation
@tracing_response_time(module='nodes')
def do_actions(request, region_id, private_ip):
    validate(Schema({'action': lambda x: x in const.NODE_ACTIONS}), request.data)
    NodeActionControl.do_actions(_get_region(region_id), private_ip, request.data['action'])
    return empty_response()


@api_view(['GET'])
@tracing_response_time(module='nodes')
def get_stats(request, region_id, private_ip):
    data = StatsControl.get_object(region__id=region_id).data
    data = [x for x in data['nodes'] if x['private_ip'] == private_ip]
    if data:
        return json_response({k: data[0][k] for k in data[0].keys() if k not in ['ports_allocated', 'ports_total']})
    raise Http404()


@api_view(['PUT'])
@connection_validation
@tracing_response_time(module='nodes')
def update_type(request, region_id, private_ip):
    if 'type' not in request.data:
        raise FurionException('insufficient_data', message_params=['type not provided.'])
    new_type = request.data['type']
    if new_type not in const.PHOENIX_NODE_TYPES:
        message = "Type {} is not valid.".format(new_type)
        raise FurionException('invalid_node_type', message_params=[message])
    method = NodeControl
    if settings.METHOD_NAME:
        method = NodeControlAsync
    method.update_type(_get_region(region_id), private_ip, new_type)
    return empty_response()


# PUT /regions/<region_id>/nodes/<node_ip>/labels
@api_view(['PUT'])
@tracing_response_time(module='nodes')
def update_labels(request, region_id, private_ip):
    region = _get_region(region_id)
    NodeControl.update_labels(region, private_ip, request.data)
    return empty_response()


class NodesView(APIView):
    @tracing_response_time(module='nodes')
    def get(self, request, region_id):
        return json_response(NodeControl.get_nodes_info(_get_region(region_id)))

    @connection_validation
    @tracing_response_time(module='nodes')
    def post(self, request, region_id):
        # TODO:SS_Milestone2 - Currently we use the default node type as mesos-slave.
        # In next milestone, Rubick will always provide the node type and so this
        # method should fail if no node_type is provided.
        node_type = request.data.get('node_type', const.PHOENIX_NODE_TYPE_EMPTY)
        user_data = RegionControl.get_user_data(_get_region(region_id), node_type)
        request.data['node_type'] = node_type
        NodeControl.create_nodes(_get_region(region_id), request.data, user_data=user_data)
        return empty_response()


class NodeView(APIView):
    @staticmethod
    @connection_validation
    @tracing_response_time(module='node')
    def delete(request, region_id, private_ip):
        NodeControl.delete(_get_region(region_id), private_ip)
        return empty_response()

    @staticmethod
    @tracing_response_time(module='node')
    def get(request, region_id, private_ip):
        return json_response(NodeControl.get_info(region__id=region_id, private_ip=private_ip))

    @staticmethod
    @tracing_response_time(module='node')
    def put(request, region_id, private_ip):
        NodeControl.update(region_id, private_ip, request.data)
        return empty_response()
