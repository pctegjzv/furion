from __future__ import absolute_import

from alauda.clients.furion import FurionClient
from rest_framework.decorators import api_view
from rest_framework.views import APIView

from controls.node import NodeControl
from controls.stats import StatsControl
from controls.region import RegionControl, get_docker_version
from controls.component_version import ComponentVersionControl
from controls.region_settings import RegionSettingControl
from controls.tasks import RegionControlAsync  # NOQA
from furion.exception import FurionException
from util import const
from alauda.common.web import json_response, empty_response
from django.conf import settings
from controls.features import get_class
from util.phoenix import connection_validation
from alauda.tracing import tracing_response_time

__author__ = 'Hang Yan'


class RegionsView(APIView):
    @connection_validation
    @tracing_response_time(module='regions', action='save')
    def post(self, request):
        return json_response(RegionControl.save(request.data))

    @tracing_response_time(module='regions')
    def get(self, request):
        regions = RegionControl.list(**request.query_params)
        result = []
        for x in regions:
            if x['platform_version'] != const.KUBERNETES_PLATFORM_VERSION_V4:
                result.append(FurionClient(region=x).merge_region())
                continue
            result.append(x)
        return json_response(result)


class RegionView(APIView):
    @connection_validation
    @tracing_response_time(module='region')
    def delete(self, request, region_id):
        RegionControl.delete(id=region_id)
        return empty_response()

    @tracing_response_time(module='region')
    def get(self, request, region_id):
        data = RegionControl.get_info(id=region_id)
        merged = data
        if data['platform_version'] != const.KUBERNETES_PLATFORM_VERSION_V4:
            merged = FurionClient(region=data).merge_region()
        return json_response(merged)

    @tracing_response_time(module='region')
    @connection_validation
    def put(self, request, region_id):
        method = RegionControl
        if settings.METHOD_NAME:
            method = RegionControlAsync
        method.update(request.data, id=region_id)
        return empty_response()


class UpgradeView(APIView):
    @tracing_response_time(module='upgrade')
    def get(self, request, region_id):
        return json_response(RegionControl.get_upgrade(region_id))

    @tracing_response_time(module='upgrade')
    @connection_validation
    def put(self, request, region_id):
        region = RegionControl.get_object(id=region_id)
        rs_obj = RegionSettingControl.get_object(**{'region_id': region.id})

        components = request.data.get('components_upgrade')
        if components:
            ComponentVersionControl.upgrade_components(region, rs_obj, components)

        spectre = request.data.get('spectre_upgrade')
        if spectre:
            ComponentVersionControl.upgrade_spectre(region, rs_obj, spectre)

        return empty_response()


class StatsView(APIView):
    @tracing_response_time(module='stats')
    def get(self, request, region_id):
        container_manager = RegionControl.get_object(id=region_id).container_manager
        metrics_type = const.REGION_METRICS_MAP.get(container_manager, const.STAT_MESOS_METRICS)
        data = StatsControl.get_object(region__id=region_id, type=metrics_type).data
        data = data['region']
        data = {k: data[k] for k in data.keys() if k not in ['available_ports_count']}
        return json_response(data)

    @tracing_response_time(module='stats')
    def put(self, request, region_id):
        region = RegionControl.get_object(id=region_id)
        args = request.data.copy()
        stats_type = args.pop('type')
        StatsControl.create_or_update(region, stats_type, args)
        return empty_response()


class LogSourceView(APIView):
    @tracing_response_time(module='stats')
    def get(self, request, region_id):
        features = RegionControl.get_object(id=region_id).features
        return json_response(RegionControl.get_log_source(features))

    @tracing_response_time(module='stats')
    def put(self, request, region_id):
        region = RegionControl.get_object(id=region_id)

        features = region.features
        if 'log' not in features:
            features['logs'] = {}
        if 'storage' not in features['logs']:
            features['logs']['storage'] = {}
        features['logs']['storage']['write_log_source'] = request.data['write_log_source']
        features['logs']['storage']['read_log_source'] = request.data['read_log_source']
        region.features = features
        RegionControl.update(region.__dict__, id=region_id)
        return empty_response()


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_support_docker_version(request):
    return json_response(get_docker_version())


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_region_config(request, region_id):
    region = RegionControl.get_object(id=region_id)
    cloud_name = region.attr.get('cloud', {}).get('name')
    if cloud_name:
        return json_response(RegionControl.get_cloud(region).get_instance_types())
    raise FurionException('region_data_incorrect',
                          message_params=[region_id, 'region.attr.cloud.name'])


@api_view(['PUT'])
@tracing_response_time(module='regions')
def check_region_features(request, region_id):
    """ Check whether region contains the features in request body

        Args:
            features(list): ['f1', 'f2' ...]

        Returns:
            200: valid
            400: invalid, error message included
    """

    return json_response(
        RegionControl.check_region_features(
            region_id, request.data.get('features'))
    )


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_labels(request, region_id):
    return json_response(RegionControl.get_labels(region_id))


@api_view(['GET'])
@tracing_response_time(module='regions')
def select_node(request, region_id):
    params = request.query_params
    labels = params.get('labels')
    return json_response(NodeControl.select_node_by_labels(region_id, labels))


@api_view(['GET'])
@connection_validation
@tracing_response_time(module='regions')
def get_node_scripts(request, region_id):
    region = RegionControl.get_object(id=region_id)
    return json_response(RegionControl.get_node_scripts(region, request.query_params))


@api_view(['PUT'])
@tracing_response_time(module='regions')
def node_scale_settings(request, region_id):
    region = RegionControl.get_object(id=region_id)
    RegionControl.update_node_scale_settings(region, request.data)
    return empty_response()


@api_view(['GET'])
@tracing_response_time(module='regions')
def key_pairs(request, region_id):
    region = RegionControl.get_object(id=region_id)
    return json_response(RegionControl.get_key_pairs(region))


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_comps(request, region_id):
    return json_response(NodeControl.get_sys_comps(region_id))


@api_view(['PUT'])
@tracing_response_time(module='regions')
def block(request, region_id):
    rs_obj = RegionSettingControl.get_object(**{'region_id': region_id})
    if not rs_obj.is_update_blocked:
        rs_obj.is_update_blocked = True
        rs_obj.save()
    return empty_response()


@api_view(['PUT'])
@tracing_response_time(module='regions')
def unblock(request, region_id):
    rs_obj = RegionSettingControl.get_object(**{'region_id': region_id})
    if rs_obj.is_update_blocked:
        rs_obj.is_update_blocked = False
        rs_obj.save()
    return empty_response()


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_region_settings(request, region_id):
    region = RegionControl.get_object(id=region_id)
    return json_response(RegionSettingControl.get_settings(region))


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_current_version(request, region_id):
    region = RegionControl.get_object(id=region_id)
    region_settings = RegionSettingControl.get_object(region=region)
    return json_response(ComponentVersionControl.get_current_version(region_settings))


@api_view(['GET'])
@tracing_response_time(module='regions')
def get_default_version(request, region_id):
    region = RegionControl.get_object(id=region_id)
    region_settings = RegionSettingControl.get_object(region=region)
    return json_response(ComponentVersionControl.get_default_version(region_settings))


@api_view(['PUT'])
@tracing_response_time(module='regions')
def update_component_version(request, region_id, component_name):
    region = RegionControl.get_object(id=region_id)
    region_setting = RegionSettingControl.get_object(region=region)
    ComponentVersionControl.update_version(request.data, region_setting, component_name)
    return empty_response()


@api_view(['PUT'])
@connection_validation
@tracing_response_time(module='regions')
def enable(request, region_id, feature):
    region = RegionControl.get_object(id=region_id)
    method = get_class('Enable', feature)
    obj = method(region, request.data)
    obj.enable()
    return empty_response()


@api_view(['PUT'])
@connection_validation
@tracing_response_time(module='regions')
def disable(request, region_id, feature):
    region = RegionControl.get_object(id=region_id)
    method = get_class('Disable', feature)
    obj = method(region, request.data)
    obj.disable()
    return empty_response()
