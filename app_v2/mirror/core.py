from mekansm.exceptions import MekAPIException


class MirrorException(MekAPIException):
    errors_map = {
        "invalid_regions": {
            "message": "Regions should not belong to other mirrors"
        }
    }


def format_region_data(data):
    return {
        "name": data['name'],
        "display_name": data['display_name'],
        "id": data["id"],
        "created_at": data["created_at"]
    }


def create_mirror(db_gw, cache_gw, mirror):
    mirror_data = mirror.data
    regions = mirror_data.pop('regions', [])
    for region in regions:
        r = db_gw.get_region(region["id"])
        if r.data['mirror']:
            raise MirrorException(code="invalid_regions")
    obj = db_gw.create_mirror(mirror_data)
    ids = [reg["id"] for reg in regions]
    db_gw.update_region(ids, **{"mirror_id": obj.data['id']})
    obj_data = obj.data
    mirror_regions = db_gw.get_mirror_regions(obj.data['id'])
    obj_data['regions'] = [format_region_data(reg.data) for reg in mirror_regions]
    return obj_data


def update_mirror(db_gw, cache_gw, mirror, mirror_id):
    db_gw.update_mirror(mirror_id, **mirror.data)


def list_mirrors(db_gw, cache_gw, mirror_ids):
    mirror_list, mirror_regions = db_gw.list_mirrors(mirror_ids)
    for mirror in mirror_list:
        mirror['regions'] = [format_region_data(r.data) for r in mirror_regions[mirror["id"]]]
    return mirror_list


def get_mirror(db_gw, cache_gw, mirror_id):
    mirror, regions = db_gw.get_mirror(mirror_id)
    mirror_data = mirror.data
    mirror_data['regions'] = [format_region_data(r.data) for r in regions]
    return mirror_data


def delete_mirror(db_gw, cache_gw, mirror_id):
    db_gw.delete_mirror(mirror_id)
