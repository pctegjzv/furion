from django.conf.urls import url
from mekansm.exceptions import MekAPIException
from mekansm.errors import Types as APIErrorType
from rest_framework import status, viewsets
from rest_framework.response import Response

from gateway.database import MirrorDBGateway
from gateway.cache import CacheGateway

from .entities import MirrorDataWithRegion, UpdateMirrorData
from . import core


UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
NAME_PATTERN = "[A-Za-z0-9-_.]+"


class MirrorError(MekAPIException):

    def __init__(self, code, error_type, message):
        self.code = code
        self._error_type = error_type
        self._message = message

    @property
    def error_type(self):
        return self._error_type

    @property
    def message(self):
        return self._message


def handle_errors(errors):
    def error_decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as exc:
                for err in errors:
                    if exc.__class__.__name__ != err["name"]:
                        continue
                    else:
                        code = err.get("code", err["name"])
                        error_type = err.get("error_type", APIErrorType.SERVER_ERROR)
                        message = err.get("message", str(exc))
                        raise MirrorError(code, error_type, message)
                raise exc
        return wrapper
    return error_decorator


class MirrorViewSet(viewsets.ViewSet):

    def create_mirror(self, request):
        mirror = MirrorDataWithRegion(data=request.data)
        mirror = core.create_mirror(MirrorDBGateway, CacheGateway, mirror)
        return Response(mirror, status=status.HTTP_201_CREATED)

    def update_mirror(self, request, mirror_id):
        mirror = UpdateMirrorData(data=request.data)
        core.update_mirror(MirrorDBGateway, CacheGateway, mirror, mirror_id)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    def list_mirrors(self, request):
        mirror_ids = request.query_params.get("uuids", "").split(",")
        mirror_ids = [x for x in mirror_ids if x]
        mirrors = core.list_mirrors(MirrorDBGateway, CacheGateway, mirror_ids)
        return Response(mirrors)

    def get_mirror(self, request, mirror_id):
        mirror = core.get_mirror(MirrorDBGateway, CacheGateway, mirror_id)
        return Response(mirror, status=status.HTTP_200_OK)

    def delete_mirror(self, request, mirror_id):
        core.delete_mirror(MirrorDBGateway, CacheGateway, mirror_id)
        return Response(None, status=status.HTTP_204_NO_CONTENT)


urlpatterns = [
    url(r"^/?$",
        MirrorViewSet.as_view({
            "post": "create_mirror",
            "get": "list_mirrors"
        })),
    url(r"^/(?P<mirror_id>{})/?$".format(UUID_PATTERN),
        MirrorViewSet.as_view({
            "get": "get_mirror",
            "delete": "delete_mirror",
            "put": "update_mirror"
        })),
]
