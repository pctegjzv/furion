from rest_framework import serializers

from .data_type import DataType
from .regions import MirrorRegionData


class MirrorData(DataType):
    id = serializers.UUIDField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    name = serializers.CharField(max_length=36, allow_blank=True, default="")
    display_name = serializers.CharField()
    description = serializers.CharField(max_length=1024, allow_blank=True, default='')
    flag = serializers.CharField()


class UpdateMirrorData(DataType):
    display_name = serializers.CharField()
    description = serializers.CharField(max_length=1024, allow_blank=True, default='')
    flag = serializers.CharField()


class MirrorDataWithRegion(MirrorData):
    regions = serializers.ListField(child=MirrorRegionData())
