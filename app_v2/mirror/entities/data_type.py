from rest_framework import serializers


class DataType(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        super(DataType, self).__init__(*args, **kwargs)
        if hasattr(self, "initial_data"):
            self.is_valid(raise_exception=True)

    @property
    def data(self):
        if hasattr(self, "_data"):
            delattr(self, "_data")
        return super(DataType, self).data
