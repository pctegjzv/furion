from rest_framework import serializers

from .data_type import DataType


class BasicFeature(DataType):
    application_uuid = serializers.UUIDField(required=False)
    integration_uuid = serializers.UUIDField(required=False)


class PipelineFeature(BasicFeature):
    type = serializers.ChoiceField(choices=(("official", "Official"),))


class LoadBalancerFeature(BasicFeature):
    type = serializers.ChoiceField(choices=(("official", "Official"),))


class RegistryFeature(BasicFeature):
    type = serializers.ChoiceField(choices=(("official", "Official"),))


class MetricFeature(BasicFeature):
    type = serializers.ChoiceField(choices=(("official", "Official"),
                                            ("prometheus", "Prometheus")))


class LogStorage(DataType):
    read_log_source = serializers.CharField(default="default")
    write_log_source = serializers.CharField(default="default")


class LogFeature(BasicFeature):
    type = serializers.ChoiceField(choices=(("official", "Official"),))
    storage = LogStorage()


class VolumeDriver(DataType):
    desc = serializers.CharField()
    driver_name = serializers.CharField()
    disp_name = serializers.CharField()


class VolumeAgent(DataType):
    endpoint = serializers.URLField()


class VolumeFeature(BasicFeature):
    type = serializers.ChoiceField(choices=(("official", "Official"),))
    drivers = serializers.ListSerializer(child=VolumeDriver())
    features = serializers.ListSerializer(child=serializers.CharField())
    agent = VolumeAgent()
