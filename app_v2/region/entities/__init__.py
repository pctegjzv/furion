from rest_framework import serializers

from .data_type import DataType
from .region_attrs import *  # flake8:noqa
from .region_features import *  # flake8:noqa
from util.const import KUBERNETES_PLATFORM_VERSION_V4, CONTAINER_MANAGER_K8S
from django.conf import settings


class RegionAttr(DataType):
    cluster = ClusterInfo()
    cloud = CloudInfo()
    docker = DockerInfo()
    kubernetes = KubernetesInfo()
    feature_namespace = serializers.CharField(required=False)

    def to_representation(self, instance):
        ret = super(RegionAttr, self).to_representation(instance)
        if not ret.get("feature_namespace"):
            ret["feature_namespace"] = settings.FEATURE_REGION_CONF['namespace']
        return ret


class RegionFeature(DataType):
    pipeline = PipelineFeature(required=False)
    volume = VolumeFeature(required=False)
    lb = LoadBalancerFeature(required=False)
    registry = RegistryFeature(required=False)
    log = LogFeature(required=False)
    metric = MetricFeature(required=False)
    customized = serializers.DictField(required=False)


class AutoValue(DataType):
    kind = serializers.ChoiceField(choices=(("configmap", "ConfigMap"), ("secret", "Secret"),
                                            ("docker-config-secret", "Docker Config Secret")))
    name = serializers.CharField()
    value = serializers.CharField()


class IntegrationInfo(DataType):
    uuid = serializers.UUIDField()
    type = serializers.CharField()
    enabled = serializers.BooleanField()


class ApplicationInfo(DataType):
    uuid = serializers.UUIDField()
    name = serializers.CharField()
    status = serializers.CharField(allow_blank=True)
    version = serializers.UUIDField(required=False)


class FeatureCreateData(DataType):
    auto_values = serializers.ListField(child=AutoValue())
    values_yaml_content = serializers.CharField(allow_blank=True, required=False)
    token = serializers.CharField()


class LogFeatureCreateData(FeatureCreateData):
    config = LogFeature()


class MetricFeatureCreateData(FeatureCreateData):
    config = MetricFeature()


class LogFeatureResp(DataType):
    config = LogFeature()
    application_info = ApplicationInfo()


class MetricFeatureResp(DataType):
    config = MetricFeature()
    application_info = ApplicationInfo()


class MetricFeatureIntegrationResp(DataType):
    config = MetricFeature()
    integration_info = IntegrationInfo()


FEATURES_MAP = {
    "log": {
        "support_integration": False,
        "feature_create_cls": LogFeatureCreateData,
        "official_response_cls": LogFeatureResp
    },
    "metric": {
        "support_integration": True,
        "feature_create_cls": MetricFeatureCreateData,
        "official_response_cls": MetricFeatureResp,
        "integration_response_cls": MetricFeatureIntegrationResp
    }
}


class RegionCheckData(DataType):
    endpoint = serializers.URLField()
    token = serializers.CharField()


class SimpleRegion(DataType):
    id = serializers.UUIDField(read_only=True)
    name = serializers.CharField()
    display_name = serializers.CharField()
    created_at = serializers.DateTimeField(read_only=True)


class Mirror(DataType):
    id = serializers.UUIDField(read_only=True)
    name = serializers.CharField()
    display_name = serializers.CharField()
    flag = serializers.CharField()

    def to_representation(self, instance):
        ret = super(Mirror, self).to_representation(instance)
        ret["regions"] = [SimpleRegion(r).data for r in instance.region_set.all()]
        return ret


class RegionData(DataType):
    id = serializers.UUIDField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    state = serializers.CharField(read_only=True)
    name = serializers.CharField(max_length=36, allow_blank=True, default="")
    namespace = serializers.CharField(max_length=36, allow_blank=True, default="")
    display_name = serializers.CharField()
    attr = RegionAttr()
    features = RegionFeature(default={}, read_only=True)
    container_manager = serializers.CharField(required=False, default=CONTAINER_MANAGER_K8S)
    platform_version = serializers.CharField(required=False,
                                             default=KUBERNETES_PLATFORM_VERSION_V4)
    mirror = Mirror(read_only=True)

    def to_representation(self, instance):
        ret = super(RegionData, self).to_representation(instance)
        if not ret.get("mirror"):
            ret["mirror"] = {}
        return ret

    def validate_container_manager(self, value):
        return "KUBERNETES"

    def validate_platform_version(self, value):
        return "v4"


class NodeListData(DataType):
    items = serializers.ListField(child=serializers.DictField())
    kind = serializers.CharField()
    api_version = serializers.CharField()
    metadata = serializers.DictField()