from django.conf import settings
from rest_framework import serializers

from .data_type import DataType
from operators import utils


class ClusterInfo(DataType):
    nic = serializers.CharField()


class CloudInfo(DataType):
    name = serializers.CharField(default="PRIVATE")
    access_key = serializers.CharField(required=False)
    secret_key = serializers.CharField(required=False)


class DockerInfo(DataType):
    version = serializers.CharField(default="1.12.6")
    path = serializers.CharField(default="/var/lib/docker")


class KubernetesCNIInfo(DataType):
    network_policy = serializers.CharField(allow_blank=True, default="")
    cidr = serializers.CharField(required=False)
    type = serializers.CharField()
    backend = serializers.CharField(required=False)


class KubernetesInfo(DataType):
    type = serializers.ChoiceField(choices=["original", "openshift"])
    cni = KubernetesCNIInfo()
    version = serializers.CharField()
    endpoint = serializers.URLField()
    token = serializers.CharField()

    def validate_version(self, value):
        for v in settings.SUPPORTED_KUBERNETES_VERSIONS:
            if utils.check_versions_matched(value, v):
                return value
        else:
            raise serializers.ValidationError("version {} is not supported.".format(value))
