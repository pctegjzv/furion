from django.utils.decorators import method_decorator
from django.conf.urls import url
from django.http import Http404

from mekansm.exceptions import MekAPIException
from mekansm.errors import Types as APIErrorType
from rest_framework import status, viewsets
from rest_framework.response import Response

from gateway.database import RegionDBGateway
from gateway.cache import CacheGateway

from .entities import (RegionCheckData, RegionData, FEATURES_MAP)
from . import core


UUID_PATTERN = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
NAME_PATTERN = "[A-Za-z0-9-_.]+"


class RegionError(MekAPIException):

    def __init__(self, code, error_type, message):
        self.code = code
        self._error_type = error_type
        self._message = message

    @property
    def error_type(self):
        return self._error_type

    @property
    def message(self):
        return self._message


def handle_errors(errors):
    def error_decorator(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as exc:
                for err in errors:
                    if exc.__class__.__name__ != err["name"]:
                        continue
                    else:
                        code = err.get("code", err["name"])
                        error_type = err.get("error_type", APIErrorType.SERVER_ERROR)
                        message = err.get("message", str(exc))
                        raise RegionError(code, error_type, message)
                raise exc
        return wrapper
    return error_decorator


class RegionViewSet(viewsets.ViewSet):

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def check_region(self, request):
        check_data = RegionCheckData(data=request.data)
        version = core.check_region(CacheGateway, check_data)
        return Response({"version": version})

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "VersionNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "RegionRegistered", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def create_region(self, request):
        region = RegionData(data=request.data)
        region = core.create_region(RegionDBGateway, CacheGateway, region)
        return Response(region.data,
                        status=status.HTTP_201_CREATED)

    def list_regions(self, request):
        region_ids = request.query_params.get("uuids", "").split(",")
        regions = core.list_regions(RegionDBGateway, CacheGateway, region_ids)
        return Response([r.data for r in regions])

    @method_decorator(handle_errors([
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST}
    ]))
    def get_region(self, request, region_id):
        region = core.get_region(RegionDBGateway, CacheGateway, region_id)
        return Response(region.data)

    @method_decorator(handle_errors([
        {"name": "DoesNotExist", "error_type": APIErrorType.NOT_FOUND},
    ]))
    def delete_region(self, request, region_id):
        core.delete_region(RegionDBGateway, CacheGateway, region_id)
        return Response(None, status=status.HTTP_204_NO_CONTENT)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def list_nodes(self, request, region_id):
        node_list = core.list_nodes(RegionDBGateway, CacheGateway, region_id)
        return Response(node_list.data) 
    
    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def list_nodes_labels(self, request, region_id):
        nodes_label = core.list_nodes_labels(RegionDBGateway, CacheGateway, region_id)
        return Response(nodes_label) 

    def list_features(self, request, region_id):
        token = request.query_params.get("token", "")
        features = core.list_features(RegionDBGateway, CacheGateway, region_id, token)
        return Response(features)

    @method_decorator(handle_errors([
        {"name": "KubernetesUnauthorized", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesForbidden", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "KubernetesApiError", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "FeatureNotSupported", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "FeatureNotSupportIntegration", "error_type": APIErrorType.BAD_REQUEST},
        {"name": "FeatureAlreadyAdded", "error_type": APIErrorType.BAD_REQUEST},
    ]))
    def add_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        feature_create_data = FEATURES_MAP[feature_name]["feature_create_cls"](data=request.data)
        feature = core.add_feature(RegionDBGateway, CacheGateway, region_id, feature_name, feature_create_data)
        return Response(feature.data)

    def get_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        token = request.query_params.get("token", "")
        feature = core.get_feature(RegionDBGateway, CacheGateway, region_id, feature_name, token)
        return Response(feature)

    def update_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        feature_create_data = FEATURES_MAP[feature_name]["feature_create_cls"](data=request.data)
        feature = core.update_feature(RegionDBGateway, CacheGateway, region_id,
                                      feature_name, feature_create_data)
        return Response(feature.data)

    def delete_feature(self, request, region_id, feature_name):
        if feature_name not in FEATURES_MAP:
            raise Http404
        token = request.query_params.get("token", "")
        core.delete_feature(RegionDBGateway, CacheGateway, token, region_id, feature_name)
        return Response(None, status=status.HTTP_204_NO_CONTENT)


urlpatterns = [
    url(r"^/version-check/?$",
        RegionViewSet.as_view({
            "post": "check_region"
        })),
    url(r"^/?$",
        RegionViewSet.as_view({
            "post": "create_region",
            "get": "list_regions"
        })),
    url(r"^/(?P<region_id>{})/?$".format(UUID_PATTERN),
        RegionViewSet.as_view({
            "get": "get_region",
            "delete": "delete_region"
        })),
    url(r"^/(?P<region_id>{})/nodes/?$".format(UUID_PATTERN),
        RegionViewSet.as_view({
            "get": "list_nodes"
        })),
    url(r"^/(?P<region_id>{})/labels/?$".format(UUID_PATTERN),
        RegionViewSet.as_view({
            "get": "list_nodes_labels"
        })),
    url(r"^/(?P<region_id>{})/features/?$".format(UUID_PATTERN),
        RegionViewSet.as_view({
            "get": "list_features"
        })),
    url(r"^/(?P<region_id>{})/features/(?P<feature_name>{})?$".format(UUID_PATTERN, NAME_PATTERN),
        RegionViewSet.as_view({
            "post": "add_feature",
            "get": "get_feature",
            "put": "update_feature",
            "delete": "delete_feature"
        })),
]
