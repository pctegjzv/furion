import logging
import time
from django.conf import settings

from operators import utils
from operators.kubernetes import KubernetesManager, KubernetesApiError
from operators.integration import IntegrationOperator, IntegrationNotFound
from operators.application import ApplicationOperator, ApplicationNotFound

from .entities import FEATURES_MAP, RegionData, RegionCheckData, ApplicationInfo, IntegrationInfo

LOG = logging.getLogger(__name__)


cache_key_map = {
    "region_version": (
        lambda endpoint, token: utils.md5sum("{}_{}".format(endpoint, token))
    ),
    "region_data": (
        lambda region_id: "region_data_{}".format(region_id)
    ),
    "region_list": (
        lambda region_ids: "region_list_{}".format(
            utils.md5sum("_".join(region_ids)))
    ),
    "node_list": (
        lambda region_id: "node_list_{}".format(region_id)
    ),
    "feature_template": (
        lambda feature_name: "feature_template_{}".format(feature_name)
    ),
    "feature_template_list": "feature_templates"
}


class VersionNotSupported(Exception):

    def __init__(self, version):
        self.version = version
    
    def __str__(self):
        return "version {} is not supported.".format(self.version)


class RegionRegistered(Exception):

    def __init__(self, name):
        self.name = name
    
    def __str__(self):
        return "This cluster is already registered,cluster name is {}.".format(self.name)


class FeatureNotSupported(Exception):

    def __init__(self, feature_name):
        self.feature_name = feature_name

    def __str__(self):
        return "The feature {} is not supported yet.".format(self.feature_name)


class FeatureNotSupportIntegration(Exception):

    def __init__(self, feature_name):
        self.feature_name = feature_name

    def __str__(self):
        return "Feature {} does not support integration.".format(self.feature_name)


class FeatureAlreadyAdded(Exception):

    def __init__(self, region_id, feature_name):
        self.region_id = region_id
        self.feature_name = feature_name

    def __str__(self):
        return ("Feature {} already added in region {}, "
                "can not add it again.").format(self.feature_name, self.region_id)


def check_region(cache_gw, check):
    cache_key = cache_key_map["region_version"](check.data["endpoint"],
                                                check.data["token"])
    cached, k8s_version = cache_gw.get(cache_key)
    if not cached:
        k8s_operator = KubernetesManager(endpoint=check.data["endpoint"],
                                         token=check.data["token"])
        k8s_version = k8s_operator.get_version()
        for v in settings.SUPPORTED_KUBERNETES_VERSIONS:
            if utils.check_versions_matched(k8s_version, v):
                break
        else:
            raise VersionNotSupported(k8s_version)
        cache_gw.set(cache_key, k8s_version)
    return k8s_version


def create_region(db_gw, cache_gw, region):
    check_region(cache_gw, RegionCheckData(region.data["attr"]["kubernetes"]))
    k8s_operator = KubernetesManager(endpoint=region.data["attr"]["kubernetes"]["endpoint"],
                                     token=region.data["attr"]["kubernetes"]["token"])
    check, name = k8s_operator.check_namespace_label("default")
    if check:
        raise RegionRegistered(name)
    label = {"alauda-registered-region": region.data["name"]}
    k8s_operator.add_namespace_label("default", label)
    if not k8s_operator.check_namespace(settings.FEATURE_REGION_CONF['namespace']):
        k8s_operator.create_namespace(settings.FEATURE_REGION_CONF['namespace'])
    if not k8s_operator.check_cluster_role_binding():
        k8s_operator.create_namespace_binding(settings.FEATURE_REGION_CONF['namespace'])
    region = db_gw.create_region(region)
    cache_key = cache_key_map["region_data"](region.data["id"])
    cache_gw.set(cache_key, region.data)
    return region


def list_regions(db_gw, cache_gw, region_ids):
    cache_key = cache_key_map["region_list"](region_ids)
    cached, region_list_data = cache_gw.get(cache_key)
    if cached:
        return [RegionData(data=d) for d in region_list_data]

    region_list = db_gw.list_regions(region_ids)
    cache_gw.set(cache_key, [r.data for r in region_list])
    return region_list


def get_region(db_gw, cache_gw, region_id):
    cache_key = cache_key_map["region_data"](region_id)
    cached, region_data = cache_gw.get(cache_key)
    if cached:
        return RegionData(data=region_data, need_validate=False)

    region = db_gw.get_region(region_id)
    k8s_operator = KubernetesManager(endpoint=region.data["attr"]["kubernetes"]["endpoint"],
                                     token=region.data["attr"]["kubernetes"]["token"])
    updated_fileds = {}
    try:
        k8s_version = k8s_operator.get_version()
    except KubernetesApiError as e:
        if region.data["state"] != "ERROR":
            region.instance.state = "ERROR"
            updated_fileds["state"] = "ERROR"
        LOG.warning('error happens while get kubernetes version | {}'.format(e))
    else:
        if k8s_version != region.data["attr"]["kubernetes"]["version"]:
            region.instance.attr["kubernetes"]["version"] = k8s_version
        updated_fileds["attr"] = region.data["attr"]
        if region.data["state"] != "RUNNING":
            region.instance.state = "RUNNING"
            updated_fileds["state"] = "RUNNING"
    if updated_fileds:
        db_gw.update_region(region.data["id"], **updated_fileds)
    cache_gw.set(cache_key, region.data)
    return region


def delete_region(db_gw, cache_gw, region_id):
    region = db_gw.get_region(region_id)
    k8s_operator = KubernetesManager(endpoint=region.data["attr"]["kubernetes"]["endpoint"],
                                     token=region.data["attr"]["kubernetes"]["token"])
    k8s_operator.delete_namespace_label("default")
    cache_key = cache_key_map["region_data"](region_id)
    cache_gw.delete(cache_key)
    db_gw.delete_region(region_id)


def list_nodes(db_gw, cache_gw, region_id):
    cache_key = cache_key_map["node_list"](region_id)
    cached, node_list_data = cache_gw.get(cache_key)
    if cached:
        return RegionData(data=node_list_data)

    region = db_gw.get_region(region_id)
    k8s_operator = KubernetesManager(endpoint=region.data["attr"]["kubernetes"]["endpoint"],
                                     token=region.data["attr"]["kubernetes"]["token"])
    try:
        node_list = k8s_operator.list_nodes()
    except KubernetesApiError as e:
        raise e 
    cache_gw.set(cache_key, node_list.data)
    return node_list


def list_nodes_labels(db_gw, cache_gw, region_id):
    node_list = list_nodes(db_gw, cache_gw, region_id)
    labels_list = {
        node["status"]["addresses"][0]["address"]: [
            dict(key=k, value=v)
            for k, v in node["metadata"]["labels"].items()
        ]
        for node in node_list.data["items"]
        if node["spec"]["taints"] is None and node["spec"]["unschedulable"] is None
    }  
    return labels_list


def add_feature(db_gw, cache_gw, region_id, feature_name, feature_create_data):
    if feature_name not in FEATURES_MAP:
        raise FeatureNotSupported(feature_name)
    feature_info = FEATURES_MAP[feature_name]
    region = db_gw.get_region(region_id)
    _check_feature_status(region, feature_name)
    if feature_info["support_integration"] and "integration_uuid" in feature_create_data.data["config"]:
        feature_config, integration_ins = _add_integration_feature(db_gw, region_id, feature_name, feature_create_data)
        return feature_info["integration_response_cls"](data={
            "config": feature_config,
            "integration_info": integration_ins.data
        })
    elif not feature_info["support_integration"] and "integration_uuid" in feature_create_data.data["config"]:
        raise FeatureNotSupportIntegration(feature_name)
    else:
        feature_config, app_ins = _add_official_feature(db_gw, cache_gw, region, feature_name,
                                                        feature_create_data)
        return feature_info["official_response_cls"](data={
            "config": feature_config,
            "application_info": app_ins.data
        })


def get_feature(db_gw, cache_gw, region_id, feature_name, token):
    if feature_name not in FEATURES_MAP:
        raise FeatureNotSupported(feature_name)
    feature_config = db_gw.get_feature_config(region_id, feature_name)
    feature_template = _get_feature_template(cache_gw, feature_name)
    res = {
        "config": feature_config,
        "template": feature_template,
    }

    if feature_config.get("integration_uuid"):
        try:
            integration_ins = IntegrationOperator.get_integration_instance(
                feature_config["integration_uuid"]
            )
        except IntegrationNotFound:
            integration_ins = IntegrationInfo(data={
                "uuid": feature_config["integration_uuid"], "name": None, "enabled": None
            })
        res["integration_info"] = integration_ins.data
    elif feature_config.get("application_uuid"):
        try:
            app_ins = ApplicationOperator.get_application_instance(
                feature_config["application_uuid"], token
            )
        except ApplicationNotFound:
            app_ins = ApplicationInfo(data={
                "uuid": feature_config["application_uuid"], "name": None, "status": None
            })
        res["application_info"] = app_ins.data
        res["new_version_avaliable"] = False
        if app_ins.data["version"] != feature_template["versions"][0]["uuid"]:
            res["new_version_avaliable"] = True

    return res


def list_features(db_gw, cache_gw, region_id, token):
    res = {
        feature_name: {"config": {}, "template": _get_feature_template(cache_gw, feature_name)}
        for feature_name in FEATURES_MAP
    }

    feature_config_list = db_gw.get_feature_config_list(region_id)
    for name, config in feature_config_list.items():
        if name in res:
            res[name]["config"] = config

    integration_uuids = {
        v["integration_uuid"]: k
        for k, v in feature_config_list.items()
        if v.get("integration_uuid")
    }
    if integration_uuids:
        integration_ins_list = IntegrationOperator.list_integration_instances(uuids=[
            i for i in integration_uuids
        ])
        for integration_ins in integration_ins_list:
            feature_name = integration_uuids[integration_ins.data["uuid"]]
            res[feature_name]["integration_info"] = integration_ins.data

    application_uuids = {
        v["application_uuid"]: k
        for k, v in feature_config_list.items()
        if v.get("application_uuid")
    }
    if application_uuids:
        application_ins_list = ApplicationOperator.list_application_instances(uuids=[
            i for i in application_uuids
        ], token=token)
        for application_ins in application_ins_list:
            feature_name = application_uuids[application_ins.data["uuid"]]
            res[feature_name]["application_info"] = application_ins.data
            res[feature_name]["new_version_avaliable"] = False
            if application_ins.data["version"] != res[feature_name]["template"]["versions"][0]["uuid"]:
                res[feature_name]["new_version_avaliable"] = True

    return res


def delete_feature(db_gw, cache_gw, token, region_id, feature_name):
    feature_config = db_gw.get_feature_config(region_id, feature_name)

    app_uuid = feature_config.get("application_uuid")
    if app_uuid:
        ApplicationOperator.delete_application_instance(feature_config["application_uuid"], token)
    db_gw.delete_feature_config(region_id, feature_name)
    return app_uuid


def wait_app_deleted(uuid, token):
    time.sleep(8)


def update_feature(db_gw, cache_gw, region_id, feature_name, feature_create_data):
    app_uuid = delete_feature(db_gw, cache_gw, feature_create_data.data["token"], region_id, feature_name)
    if app_uuid:
        wait_app_deleted(app_uuid, feature_create_data.data["token"])
    return add_feature(db_gw, cache_gw, region_id, feature_name, feature_create_data)


def _check_feature_status(region, feature_name):
    if (feature_name in region.data["features"] and (
           region.data["features"][feature_name].get("application_uuid") or
           region.data["features"][feature_name].get("integration_uuid"))):
        raise FeatureAlreadyAdded(region.data["id"], feature_name)


def _add_integration_feature(db_gw, region_id, feature_name, feature_create_data):
    integration_ins = IntegrationOperator.get_integration_instance(
        feature_create_data.data["config"]["integration_uuid"]
    )
    feature_config = db_gw.save_feature_config(region_id, feature_name, feature_create_data.data["config"])
    return feature_config, integration_ins


def _add_official_feature(db_gw, cache_gw, region, feature_name, feature_create_data):
    cluster_conf = settings.FEATURE_REGION_CONF.copy()
    if region.data['attr'].get('feature_namespace'):
        cluster_conf['namespace'] = region.data['attr']['feature_namespace']
    feature_app_name = settings.REGION_FEATURE_TEMPLATE_MAP[feature_name]["app_name"]

    feature_template = _get_feature_template(cache_gw, feature_name)

    k8s_operator = KubernetesManager(endpoint=region.data["attr"]["kubernetes"]["endpoint"],
                                     token=region.data["attr"]["kubernetes"]["token"])
    secret = k8s_operator.get_secret_data(cluster_conf["namespace"], cluster_conf["secret_name"])
    configmap = k8s_operator.get_configmap_data(cluster_conf["namespace"], cluster_conf["configmap_name"])
    dockercfg_secret = k8s_operator.get_dockercfg_secret(cluster_conf["namespace"],
                                                         cluster_conf["dockercfg_secret_name"])
    changed_secret = {}
    changed_configmap = {}
    changed_dockercfg_secret = None
    for v in feature_create_data.data["auto_values"]:
        if v["kind"] == "configmap" and v["value"] != configmap.get(v["name"]):
            changed_configmap[v["name"]] = v["value"]
        elif v["kind"] == "secret" and v["value"] != secret.get(v["name"]):
            changed_secret[v["name"]] = v["value"]
        # there should only one docker-config-secret type auto-value, if more than one
        # the last will be applyed.
        elif v["kind"] == "docker-config-secret" and v["value"] != dockercfg_secret:
            changed_dockercfg_secret = v["value"]
    if changed_configmap:
        k8s_operator.apply_configmap_data(cluster_conf["namespace"], cluster_conf["configmap_name"],
                                          utils.merge_dicts(configmap, changed_configmap))
    if changed_secret:
        k8s_operator.apply_secret_data(cluster_conf["namespace"], cluster_conf["secret_name"],
                                       utils.merge_dicts(secret, changed_secret))
    if changed_dockercfg_secret:
        k8s_operator.apply_dockercfg_secret(cluster_conf["namespace"], cluster_conf["dockercfg_secret_name"],
                                            changed_dockercfg_secret)

    app_ins = ApplicationOperator.create_template_app(
        region.data["id"], region.data["name"], cluster_conf["namespace"],
        feature_app_name,
        feature_template,
        feature_create_data.data["values_yaml_content"],
        feature_create_data.data["token"])
    config = feature_create_data.data["config"].copy()
    config["application_uuid"] = app_ins.data["uuid"]
    # FIXME: data model is broken.
    feature_config = db_gw.save_feature_config(region.data["id"], feature_name, config)
    return feature_config, app_ins


def _get_feature_template(cache_gw, feature_name):
    feature_template_name = settings.REGION_FEATURE_TEMPLATE_MAP[feature_name]["template_name"]

    cached, feature_template = cache_gw.get(cache_key_map["feature_template"](feature_name))
    if not cached:
        tplst_cached, feature_template_list = cache_gw.get(
            cache_key_map["feature_template_list"])
        if not tplst_cached:
            feature_template_list = ApplicationOperator.list_public_templates(
                template_type=settings.REGION_FEATURE_TEMPLATE_TYPE)
            cache_gw.set(cache_key_map["feature_template_list"], feature_template_list)
        if feature_template_list.get(feature_template_name):
            feature_template = ApplicationOperator.get_template(
                feature_template_list[feature_template_name]["uuid"])
        else:
            feature_template = {}
        cache_gw.set(cache_key_map["feature_template"](feature_name), feature_template)
    return feature_template
