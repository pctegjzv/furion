#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Operations about kubernetes.

import logging
from kubernetes.client import Configuration, ApiClient, ExtensionsV1beta1Api, CoreV1Api, V1DeleteOptions

from furion.exception import FurionException, ErrCodes

LOG = logging.getLogger(__name__)


class KubernetesManager(object):
    def __init__(self, endpoint, token):
        """Initialize the SchedulerClient object, create a k8s Client instance. """
        config = Configuration()
        config.verify_ssl = False
        config.api_key_prefix = {'authorization': 'Bearer'}
        config.api_key = {'authorization': token}
        config.host = endpoint
        self.client = ApiClient(configuration=config)
        self.extensions_client = ExtensionsV1beta1Api(api_client=self.client)
        self.core_client = CoreV1Api(api_client=self.client)
        self.delete_option = V1DeleteOptions()

    def list_node(self):
        return self.core_client.list_node().items

    def get_node(self, private_ip, raise_ex=False):
        """Get kubernetes node by private ip.

        Args:
            private_ip(str): nodes' private_ip
            raise_ex: if true, raise exception if no kubernetes node found, else return None
        Returns:
            V1Node: 
            None: not found        
        Raises:
            FurionException
        """
        nodes = self.list_node()
        if nodes:
            for n in nodes:
                addr = n.status.addresses
                ip = [x for x in addr if x.type == 'InternalIP'][0].address
                if ip == private_ip:
                    return n
        if raise_ex:
            raise FurionException(ErrCodes.KUBERNETES_RESOURCE_ERROR, message_params=['Node', private_ip])

    def update_node_schedulable_state(self, private_ip, schedulable=False):
        """Cordon/Uncordon a node.(mark it un-schedulable/schedulable).
        
        Args:
            private_ip(str): node's private_ip
            schedulable(bool): node's target schedulable state
        Raises:
            FurionException: kubernetes_resource_error
        """
        node = self.get_node(private_ip, raise_ex=True)
        node.spec.unschedulable = not schedulable
        try:
            self.core_client.replace_node(name=node.metadata.name, body=node)
        except Exception as e:
            LOG.error("Update kubernetes node schedulable state error: {}".format(e))
            raise FurionException(ErrCodes.KUBERNETES_RESOURCE_ERROR, message=e)

    def delete_node(self, name):
        try:
            LOG.info("Delete k8s node: {}".format(name))
            self.core_client.delete_node(name, self.delete_option)
        except Exception as e:
            LOG.error("Delete k8s node: {} fail. error: {}".format(name, e))
