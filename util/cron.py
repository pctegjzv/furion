#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
import time
import datetime
from alauda_redis_client import RedisClientFactory

from util.phoenix import NodeManager


class JobLock(object):
    def __init__(self, key_name):
        self.handler = RedisClientFactory.get_client_by_key("READER")
        self.key = key_name

    def is_locked(self, timeout):
        return self.handler.set(self.key, "For the Horde!", ex=timeout, nx=True)

    def random_sleep(self):
        time.sleep(random.randint(5, 10))


def stdout(content):
    print '[{}] {}'.format(datetime.datetime.now(), content)


def is_task_running(key, expiration):
    """
    This method returns True if a key unique to this task is set in the
    database indicating that another instance of this task is currently
    running.
    """
    rds = JobLock(key)
    rds.random_sleep()
    value = rds.is_locked(expiration)
    if not value:
        stdout("Another Task is running. set key:{} {}.".format(key, value))
        return True
    return False


def get_phoenix_nodes_by_type(node_type):
    """
    This method retrieves nodes of the given type from Phoenix.

    Args:
        node_type(str): node type
    Returns:
        list[dict]: list of nodes. empty list if nothing found
    """

    filter_string = 'node_type={}&goal_incarnation=1'.format(node_type)
    return get_phoenix_nodes(filter_string)


def get_phoenix_nodes_by_ip(private_ip):
    """
    This method retrieves nodes of the given region_id from Phoenix.

    Args:
        private_ip(str): node ip
    Returns:
        list[dict]: list of nodes. empty list if nothing found
    """

    filter_string = 'ip_address={}&goal_incarnation=1'.format(private_ip)
    return get_phoenix_nodes(filter_string)


def get_phoenix_nodes(filter_string):
    """
    This method retrieves nodes using the given filter from Phoenix.
    
    Args:
        filter_string(str): example: '<key>=<value>&<key>=<value>'
    Returns:
        list[dict]: list of nodes. empty list if nothing found
    """
    phoenix_nodes = []
    try:
        phoenix_nodes = NodeManager.filter(filter_string)
        stdout('Phoenix returned {} nodes for filter {}.'.format(len(phoenix_nodes), filter_string))
    except Exception as e:
        stdout('Getting nodes from Phoenix using filter {} failed. Exception: {}'.format(filter_string, e))
    return phoenix_nodes
