#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Validate various json input. """

import logging

from schema import SchemaError

from furion.exception import InvalidArgsException

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


def validate(schema, data, message=None):
    """Validate data by schema.
    
    Args:
        schema(Schema): 
        data(dict/list...): 
        message(str): Optional error message
    Returns:
        None
    Raises:
        InvalidArgsException
    """
    try:
        schema.validate(data)
    except SchemaError as ex:
        LOG.error("Schema validate error ! | {} {}".format(data, ex))
        err_msg = message if message else str(ex)
        raise InvalidArgsException(message=err_msg)
