#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import uuid

from django.conf import settings
import const
from alauda.alchemist.environments import Environment
from alauda.alchemist.regions import Region
from alauda.alchemist.cluster_templates import ClusterTemplate
from alauda.alchemist.resources import Resource
from alauda.alchemist.nodes import Node
from alauda.alchemist.spectre import Spectre
from alauda.alchemist.versions import ComponentVersion
from alauda.alchemist.components import Component
from alauda.alchemist.auth import get_auth_token
from alauda.alchemist.utils import health_check
import traceback
import copy
from furion.exception import FurionException

LOG = logging.getLogger(__name__)


def get_token():
    if hasattr(settings, 'UNIT_TEST'):
        return 'unit_test'
    token = settings.PHOENIX_SETTINGS['TOKEN']
    if token:
        return token
    token = get_auth_token(settings.PHOENIX_SETTINGS['ENDPOINT'],
                           settings.PHOENIX_SETTINGS['USERNAME'],
                           settings.PHOENIX_SETTINGS['PASSWORD'])
    return token


class ResourceUtil(object):
    """Various resource utils."""

    @classmethod
    def add_drain_node_resource(cls, env_uuid, node_name, node):
        """Generate drain node task resource.
        Only supported in kubernetes regions.
        
        Args:
            env_uuid(str): region's env uuid
            node_name(str): target node's name (in kubernetes)
            node(dict): node info dict. The drain operation should be executed on this node. For now it's the same 
                        node as `private_ip`.
        """
        version = EnvironmentManager.get_spectre_version(env_uuid)
        if version < const.SPECTRE_OLD_VERSION:
            return
        data = copy.deepcopy(const.DRAIN_NODE_RESOURCE)
        data['identifier']['command'][0] = data['identifier']['command'][0].format(node_name, str(uuid.uuid4()))
        resource = ResourceManager.create(env_uuid, data)
        ResourceManager.add_node(resource['resource_uuid'], node['node_uuid'])

    @classmethod
    def add_cleanup_resource(cls, env_uuid, cm, nodes=None):
        """Add clean up node resource.
        
        Args:
            env_uuid(str): env uuid
            cm(str): container manager
            nodes(list[dict]): node list
        """
        version = EnvironmentManager.get_spectre_version(env_uuid)
        if version < const.SPECTRE_OLD_VERSION:
            return

        cleanup_resource = "{}_CLEAN_UP_RESOURCE".format(cm)
        resource = copy.deepcopy(
            getattr(const, cleanup_resource, const.MESOS_CLEAN_UP_RESOURCE)
        )

        resource = ResourceManager.create(env_uuid, resource)
        if nodes is None:
            nodes = NodeManager.list_nodes(env_uuid)
        for node in nodes:
            ResourceManager.add_node(resource['resource_uuid'], node['node_uuid'])


def connection_validation(method):
    def ping(*args, **kwargs):
        if not hasattr(settings, 'UNIT_TEST'):
            try:
                health_check(settings.PHOENIX_SETTINGS['ENDPOINT'], 'internalapi')
            except Exception as e:
                LOG.error("Ping phoenix fail.err: {} stack: {}".format(e, traceback.format_exc()))
                raise FurionException('internal_error', message="Communicate with Phoenix fail!")
        return method(*args, **kwargs)

    return ping


class NodeManager(object):
    node = Node(settings.PHOENIX_SETTINGS['ENDPOINT'],
                settings.PHOENIX_SETTINGS['TOKEN'],
                settings.PHOENIX_SETTINGS['USERNAME'],
                settings.PHOENIX_SETTINGS['PASSWORD'])

    @classmethod
    def copy_resources_to_node(cls, env_uuid, src_ip, dest_ip):
        LOG.info("NodeManager: Copy resource from {} to {} in Region: {}".format(src_ip, dest_ip, env_uuid))
        cls.node.copy_resource_with_ip(env_uuid, src_ip, dest_ip)

    @classmethod
    def delete_all_resources(cls, env_uuid):
        LOG.info('NodeManager: Delete all resources in Region {}.'.format(env_uuid))
        nodes = cls.node.list(env_uuid)
        for n in nodes:
            cls.node.delete_all_resources(env_uuid, n['node_uuid'])

    @classmethod
    def list_nodes(cls, env_uuid):
        return cls.node.list(env_uuid)

    @classmethod
    def filter(cls, filter_string):
        LOG.info('NodeManager: Get nodes with filter string {}.'.format(filter_string))
        return cls.node.filter(filter_string)

    @classmethod
    def delete(cls, env_uuid, private_ip):
        LOG.info('NodeManager: Delete Node: {}, Region: {}.'.format(private_ip, env_uuid))
        nodes = cls.node.list(env_uuid)
        for node in nodes:
            if node['ip_address'] == private_ip:
                cls.node.delete_all_resources(env_uuid, node['node_uuid'])
                break
        return cls.node.delete(env_uuid, private_ip)

    @classmethod
    def delete_resources(cls, env_uuid, phoenix_node):
        LOG.info('NodeManager: Delete resources for Node - {}:{}, Region: {}.'.format(
            phoenix_node['node_uuid'],
            phoenix_node['ip_address'],
            env_uuid))
        cls.node.delete_all_resources(env_uuid, phoenix_node['node_uuid'])

    @classmethod
    def update_type(cls, env_uuid, private_ip, node_type):
        LOG.info('NodeManager: Update type - Node: {}, Region: {}, Type: {}.'.format(
            private_ip,
            env_uuid,
            node_type))
        phoenix_node = cls.node.retrieve(env_uuid, private_ip)
        cls.node.update(env_uuid, phoenix_node['node_uuid'], node_type=node_type)

    @classmethod
    def clean_up(cls, env_uuid):
        LOG.info('NodeManager: Delete all nodes in Region: {}.'.format(env_uuid))
        cls.node.delete_all_nodes(env_uuid)

    @classmethod
    def retrieve(cls, env_uuid, private_ip):
        LOG.info('NodeManager: Retrieve Node: {}, Region: {}.'.format(private_ip, env_uuid))
        phoenix_node = cls.node.retrieve(env_uuid, private_ip)
        return phoenix_node

    @classmethod
    def clean_up_one_node(cls, env_uuid, private_ip):
        LOG.info('NodeManager: Cleanup Node: {}, Region: {}.'.format(private_ip, env_uuid))
        phoenix_node = cls.node.retrieve(env_uuid, private_ip)
        cls.node.delete_all_resources(env_uuid, phoenix_node['node_uuid'])

    @classmethod
    def activate(cls, env_uuid, private_ip, activate=True):
        LOG.info("NodeManager: Active node - Node: {} Region: {}".format(private_ip, env_uuid))
        phoenix_node = cls.node.retrieve(env_uuid, private_ip)
        if activate:
            cls.node.activate(env_uuid, phoenix_node['node_uuid'])
            return
        cls.node.deactivate(env_uuid, phoenix_node['node_uuid'])


class ResourceManager(object):
    resource = Resource(settings.PHOENIX_SETTINGS['ENDPOINT'],
                        settings.PHOENIX_SETTINGS['TOKEN'],
                        settings.PHOENIX_SETTINGS['USERNAME'],
                        settings.PHOENIX_SETTINGS['PASSWORD'])

    @classmethod
    def create(cls, env_uuid, data):
        LOG.info("ResourceManager: Create resource for env: {} data: {}".format(env_uuid, data))
        return cls.resource.create(env_uuid, data)

    @classmethod
    def clean_up(cls, env_uuid):
        LOG.info('ResourceManager: Remove all resources for Region: {}.'.format(env_uuid))
        cls.resource.remove_all_resources(env_uuid)

    @classmethod
    def list_resources(cls, env_uuid):
        LOG.info("ResourceManager: Resource list for Region: {}".format(env_uuid))
        return cls.resource.list(env_uuid)

    @classmethod
    def add_node(cls, resource_id, id):
        LOG.info("ResourceManager: Add node resource: {} ip: {}".format(resource_id, id))
        return cls.resource.add_node(resource_id, id)

    @classmethod
    def update(cls, env_uuid, resource_id, data):
        LOG.info("ResourceManager: Update resource: {} data: {} for env: {}".format(resource_id,
                                                                                    data,
                                                                                    env_uuid))
        return cls.resource.update(env_uuid, resource_id, data)

    @classmethod
    def retrieve_by_name(cls, env_uuid, resource_name):
        LOG.info("ResourceManager: Retrieve resource {} from {}".format(resource_name, env_uuid))
        return cls.resource.retrieve_by_name(env_uuid, resource_name)


class EnvironmentManager(object):
    env = Environment(settings.PHOENIX_SETTINGS['ENDPOINT'],
                      settings.PHOENIX_SETTINGS['TOKEN'],
                      settings.PHOENIX_SETTINGS['USERNAME'],
                      settings.PHOENIX_SETTINGS['PASSWORD'])
    spectre = Spectre(settings.PHOENIX_SETTINGS['ENDPOINT'],
                      settings.PHOENIX_SETTINGS['TOKEN'],
                      settings.PHOENIX_SETTINGS['USERNAME'],
                      settings.PHOENIX_SETTINGS['PASSWORD'])

    @classmethod
    def create(cls, region_name):
        LOG.info('EnvironmentManager: Create environment with name {}.'.format(region_name))
        description = '{} from web'.format(region_name)
        ret = cls.env.create(description=description)
        LOG.info('EnvironmentManager: Successfully created environment {} - {}.'.format(
            region_name,
            ret['env_uuid']))
        return ret['env_uuid']

    @classmethod
    def delete(cls, env_uuid):
        LOG.info('EnvironmentManager: Delete environment {}.'.format(env_uuid))
        cls.env.delete(env_uuid)

    @classmethod
    def get_command(cls, env_uuid):
        LOG.info('EnvironmentManager: Get command for environment {}.'.format(env_uuid))
        if env_uuid == '9a37a583-ec1b-4678-bb92-cc5967941ae0':
            return 'http://test'
        return cls.env.get_register_cmd(env_uuid)

    @classmethod
    def retrieve(cls, env_uuid):
        LOG.info("EnvironmentManager: Retrieve environment: {}".format(env_uuid))
        return cls.env.retrieve(env_uuid)

    @classmethod
    def get_spectre_version(cls, env_uuid):
        LOG.info("EnvironmentManager: Get spectre version for environment: {}".format(env_uuid))
        env_info = cls.retrieve(env_uuid)
        spectre_id = env_info['spectre']
        if not spectre_id:
            return None
        spectre_info = cls.spectre.get(spectre_id)
        return float(spectre_info['version'])

    @classmethod
    def get_spectre_latest_version(cls):
        LOG.info("EnvironmentManager: Get spectre latest version")
        latest_version = cls.spectre.get_latest().get('version')
        return float(latest_version)

    @classmethod
    def upgrade_spectre_to_latest_version(cls, env_uuid):
        LOG.info("EnvironmentManager: Upgrade spectre to latest version")
        latest_version_uuid = cls.spectre.get_latest().get('version_uuid')
        cls.env.update(env_uuid, None, latest_version_uuid)


class ComponentManager(object):
    comp = Component()

    @classmethod
    def get_component_envs(cls, comp_name):
        LOG.info("EnvironmentManager: Get latest env list")
        return cls.comp.get_component_envs(comp_name)


class RegionManager(object):
    region = Region(settings.PHOENIX_SETTINGS['ENDPOINT'],
                    settings.PHOENIX_SETTINGS['TOKEN'],
                    settings.PHOENIX_SETTINGS['USERNAME'],
                    settings.PHOENIX_SETTINGS['PASSWORD'])

    @classmethod
    def create(cls, env_uuid, cusomer_settings, template_name, components=None, update=False):
        LOG.info('RegionManager: Create Region {} in Environment {}.'.format(
            template_name,
            env_uuid))
        return cls.region.create(env_uuid, cusomer_settings, template_name, components, update)

    @classmethod
    def update_region(cls, env_uuid, user_settings, template_type, component_versions):
        LOG.info('RegionManager: Update Region {}, Template: {}, Components: {}'.format(
            env_uuid,
            template_type,
            component_versions))
        cls.region.update(env_uuid,
                          user_settings,
                          template_type,
                          component_versions)

    @classmethod
    def add_slave_to_region(cls, env_uuid, phoenix_settings, template_type="slave", update=False):
        LOG.info('RegionManager: Add slave to Region {}.'.format(env_uuid))
        cls.region.add_slave_to_region(env_uuid, phoenix_settings, update, template_type)

    @classmethod
    def remove_feature(cls, env_uuid, user_settings, template_type, components):
        LOG.info('RegionManager: Disable feature {}, Region: {}, Template: {}'.format(
            components,
            env_uuid,
            template_type))
        return cls.region.remove_feature(env_uuid, user_settings, template_type, components)


class ClusterTemplateManger(object):
    cluster_template = ClusterTemplate()

    @classmethod
    def get_region_settings(cls, customer_settings, template_name, components=None):
        data = cls.cluster_template.get_region_settings(customer_settings, template_name, components)
        data.pop('register_info', {})
        return data


class ComponentVersionManager(object):
    component_version = ComponentVersion()

    @classmethod
    def get_default_version(cls, component_name):
        return cls.component_version.get_default_version(component_name)
