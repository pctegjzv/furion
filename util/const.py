#!/usr/bin/env python
# -*- coding: utf-8 -*-


__author__ = 'Hang Yan'

STATE_UNKNOWN = 'UNKNOWN'
STATE_DEPLOYING = 'DEPLOYING'
STATE_BUILD = 'BUILD'
STATE_PREPARING = 'PREPARING'
STATE_DESTROYING = 'DESTROYING'
STATE_RUNNING = 'RUNNING'
STATE_ACTIVE = 'ACTIVE'
STATE_STOPPING = 'STOPPING'
STATE_STOPPED = 'STOPPED'
STATE_ERROR = 'ERROR'
STATE_CRITICAL = 'CRITICAL'
STATE_SHUTTING_DOWN = 'SHUTTING_DOWN'
STATE_SHUTOFF = 'SHUTOFF'
STATE_REMOVED = 'REMOVED'
STATE_OFFLINE = 'OFFLINE'
STATE_DRAINING = 'DRAINING'  # kubernetes node only

REGION_STATE = (
    (STATE_PREPARING, 'Preparing'),
    (STATE_DEPLOYING, 'Deploying'),
    (STATE_RUNNING, 'Running'),
    (STATE_STOPPED, 'Stopped'),
    (STATE_ERROR, 'Error'),
    (STATE_CRITICAL, 'Critical'),
)

# region types
CONTAINER_MANAGER_NONE = 'NONE'
CONTAINER_MANAGER_MESOS = 'MESOS'
CONTAINER_MANAGER_ACMP = 'MESOS'
CONTAINER_MANAGER_SWARM = 'SWARM'
CONTAINER_MANAGER_K8S = 'KUBERNETES'
CONTAINER_MANAGER_LEGACY = 'LEGACY'

CONTAINER_MANAGER_LIST = [CONTAINER_MANAGER_SWARM, CONTAINER_MANAGER_MESOS,
                          CONTAINER_MANAGER_K8S, CONTAINER_MANAGER_NONE,
                          CONTAINER_MANAGER_LEGACY]
CONTAINER_MANAGER_NOT_NONE_LIST = [CONTAINER_MANAGER_SWARM, CONTAINER_MANAGER_MESOS,
                                   CONTAINER_MANAGER_K8S, CONTAINER_MANAGER_LEGACY]

CONTAINER_MANAGER = (
    (CONTAINER_MANAGER_NONE, 'NONE'),
    (CONTAINER_MANAGER_MESOS, 'MESOS'),
    (CONTAINER_MANAGER_SWARM, 'SWARM'),
    (CONTAINER_MANAGER_LEGACY, 'LEGACY'),
    (CONTAINER_MANAGER_K8S, 'Kubernetes')
)

#
KUBERNETES_PLATFORM_VERSION_V2 = 'v2'
KUBERNETES_PLATFORM_VERSION_V3 = 'v3'
KUBERNETES_PLATFORM_VERSION_V4 = 'v4'

# network modes
NM_BRIDGE = "bridge"
NM_HOST = "host-network"
NM_STATIC_IP = "static-ip"
NM_MACVLAN = "macvlan"
NM_FLANNEL = "flannel"
NM_CALICO = "calico"

ALL_NMS = [
    NM_BRIDGE,
    NM_HOST,
    NM_STATIC_IP,
    NM_FLANNEL,
    NM_MACVLAN,
    NM_CALICO,
]

NODE_STATE = (
    (STATE_DEPLOYING, 'Deploying'),
    (STATE_RUNNING, 'Running'),
    (STATE_BUILD, 'BUILD'),
    (STATE_ACTIVE, 'ACTIVE'),
    (STATE_SHUTOFF, 'SHUTOFF'),
    (STATE_SHUTTING_DOWN, 'Shutting Down'),
    (STATE_STOPPED, 'Stopped'),
    (STATE_STOPPING, 'Stopping'),
    (STATE_REMOVED, 'Removed'),
    (STATE_UNKNOWN, 'Unknown'),
    (STATE_OFFLINE, 'Offline'),
    (STATE_ERROR, 'Error'),
    (STATE_CRITICAL, 'Critical'),
    (STATE_PREPARING, 'Preparing'),
    (STATE_DRAINING, 'Draining')
)

NODE_STATE_MAP = {
    'pending': STATE_DEPLOYING,
    'preparing': STATE_PREPARING,
    'running': STATE_RUNNING,
    'shutting-down': STATE_SHUTTING_DOWN,
    'BUILD': STATE_DEPLOYING,
    'ACTIVE': STATE_RUNNING,
    'SHUTOFF': STATE_SHUTTING_DOWN,
    'terminated': STATE_REMOVED,
    'stopping': STATE_STOPPING,
    'stopped': STATE_STOPPED,
    'suspended': STATE_STOPPED,
    'ceased': STATE_REMOVED,
    'offline': STATE_OFFLINE,
    'error': STATE_ERROR,
    'critical': STATE_CRITICAL,
    'draining': STATE_DRAINING
}

STAT_MESOS_METRICS = 'MESOS_METRICS'
STAT_SWARM_METRICS = 'SWARM_METRICS'
STAT_K8S_METRICS = 'KUBERNETES_METRICS'

STATS_TYPES = (
    (STAT_MESOS_METRICS, 'Mesos Metrics'),
    (STAT_SWARM_METRICS, 'Swarm Metrics'),
    (STAT_K8S_METRICS, 'Kubernetes Metrics')
)

REGION_METRICS_MAP = {
    CONTAINER_MANAGER_SWARM: STAT_SWARM_METRICS,
    CONTAINER_MANAGER_MESOS: STAT_MESOS_METRICS,
    CONTAINER_MANAGER_LEGACY: STAT_MESOS_METRICS,
    CONTAINER_MANAGER_NONE: STAT_MESOS_METRICS,
    CONTAINER_MANAGER_K8S: STAT_K8S_METRICS
}

NODE_SOURCE_API_CREATE = 'api-create'
NODE_SOURCE_PRE_EXISTING = 'pre-existing'
NODE_SOURCE_SCALE_UP = 'scale-up'

FAILED_NODE_INTERVAL = 60 * 19

SLAVE_NODE_TYPE = 'SLAVE'
MASTER_NODE_TYPE = 'SYS'
MASTER_AND_SLAVE_NODE_TYPE = "SYSLAVE"

NODE_TYPE_COMPUTE = SLAVE_NODE_TYPE  # mesos-slave
NODE_TYPE_CONTROLLER = MASTER_NODE_TYPE  # sys
NODE_TYPE_SWARM_WORKER = SLAVE_NODE_TYPE
NODE_TYPE_SWARM_MANAGER = MASTER_NODE_TYPE
NODE_TYPE_K8S_MASTER = MASTER_NODE_TYPE
NODE_TYPE_K8S_SLAVE = SLAVE_NODE_TYPE
NODE_TYPE_K8S_SYSLAVE = MASTER_AND_SLAVE_NODE_TYPE
NODE_TYPE_EMPTY = 'EMPTY'  # empty node
NODE_TYPE_MANAGER = 'MANAGER'  # marathon, mesos
NODE_TYPE_LB = 'LB'  # haproxy
NODE_TYPE_LOG = 'LOG'  # tiny es
NODE_TYPE_EXEC = 'EXEC'  # doom
NODE_TYPE_SCALE = 'SCALE'  # meepo

NODE_SYS_TYPES = [
    NODE_TYPE_MANAGER,
    NODE_TYPE_EXEC,
    NODE_TYPE_LOG,
    NODE_TYPE_SCALE,
    NODE_TYPE_LB,
    NODE_TYPE_SWARM_MANAGER,
    NODE_TYPE_K8S_MASTER,
]

NODE_TYPES = (
    (NODE_TYPE_COMPUTE, 'Slave'),
    (NODE_TYPE_CONTROLLER, 'Sys'),
    (NODE_TYPE_EMPTY, 'Empty'),
    (NODE_TYPE_SWARM_MANAGER, 'Sys'),
    (NODE_TYPE_SWARM_WORKER, 'Slave'),
    (NODE_TYPE_K8S_MASTER, 'Sys'),
    (NODE_TYPE_K8S_SLAVE, 'Slave')
)

PHOENIX_NODE_TYPE_CONTROLLER = 'controller'
PHOENIX_NODE_TYPE_SWARM_MANAGER = 'controller'
PHOENIX_NODE_TYPE_K8S_MASTER = 'controller'
PHOENIX_NODE_TYPE_SWARM_WORKER = 'swarm_worker'
PHOENIX_NODE_TYPE_COMPUTE = 'mesos-slave'
PHOENIX_NODE_TYPE_K8S_SLAVE = 'k8s-slave'
PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE = 'mesos-slave-attribute'
PHOENIX_NODE_TYPE_EMPTY = 'empty'

PHOENIX_NODE_TYPES = [
    PHOENIX_NODE_TYPE_CONTROLLER,
    PHOENIX_NODE_TYPE_COMPUTE,
    PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE,
    PHOENIX_NODE_TYPE_EMPTY,
    PHOENIX_NODE_TYPE_SWARM_MANAGER,
    PHOENIX_NODE_TYPE_SWARM_WORKER,
    PHOENIX_NODE_TYPE_K8S_MASTER,
    PHOENIX_NODE_TYPE_K8S_SLAVE
]

PHOENIX_NODE_TYPE_MAP = {
    PHOENIX_NODE_TYPE_CONTROLLER: NODE_TYPE_CONTROLLER,
    PHOENIX_NODE_TYPE_COMPUTE: NODE_TYPE_COMPUTE,
    PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE: NODE_TYPE_COMPUTE,
    PHOENIX_NODE_TYPE_EMPTY: NODE_TYPE_EMPTY,
    PHOENIX_NODE_TYPE_SWARM_MANAGER: NODE_TYPE_CONTROLLER,
    PHOENIX_NODE_TYPE_SWARM_WORKER: NODE_TYPE_COMPUTE,
    PHOENIX_NODE_TYPE_K8S_SLAVE: NODE_TYPE_COMPUTE,
    PHOENIX_NODE_TYPE_K8S_MASTER: NODE_TYPE_CONTROLLER

}

# Node actions list
NODE_ACTION_CORDON = 'cordon'
NODE_ACTION_UNCORDON = 'uncordon'
NODE_ACTION_DRAIN = 'drain'

NODE_ACTIONS = [
    NODE_ACTION_CORDON,
    NODE_ACTION_DRAIN,
    NODE_ACTION_UNCORDON
]

SSH = 'SSH'
MAPPING = 'Mapping'
PORT_TYPE = (
    (SSH, 'SSH'),
    (MAPPING, 'Mapping')
)

# TODO: remove
LB_TYPE = [
    'elb',
    'slb',
    'ulb',
    'clb',
    'blb'
]

QING_PRIVATE_IP_PREFIX = 'unallocated'
# make sure the length be limited in 10
KINGSOFT_PRIVATE_IP_PREFIX = 'temp_'

FEATURE_EXEC = 'exec'
FEATURE_HAPROXY = 'haproxy'
FEATURE_INTERNAL_HAPROXY = 'internal-haproxy'
FEATURE_REGISTRY = 'registry'
FEATURE_CHRONOS = 'chronos'

COMPUTE_RESOURCE_MESOS = ['mesos_tmp_dir', 'mesos_log_dir', 'ssh_folder', 'alauda_public_key',
                          'nevermore', 'xin_driver', 'dd_agent'
                          'docker-daemon-proxy']
COMPUTE_RESOURCE_SWARM = ['ssh_folder', 'alauda_public_key', 'dd_agent',
                          'nevermore', 'docker-daemon-proxy', 'swarm']
COMPUTE_RESOURCE_K8S = ['dd_agent', 'kubernetes_compute', 'nevermore',
                        'command_kubeadm_compute', 'pudge', 'ssh_folder', 'alauda_public_key']

SPECTRE_OLD_VERSION = 2.8

COMMAND_HANDLER_SPECTRE = 'SPECTRE'
COMMAND_HANDLER_DIRGE = 'DIRGE'

VMWARE_CLONING_STATE_IP = '0.0.0.0'
CLOUD_VMWARE = 'VMWARE'
CLOUD_KINGSOFT = 'KINGSOFT'
# task resources

KUBERNETES_CLEAN_UP_RESOURCE = {
    "identifier": {
        "handler": COMMAND_HANDLER_DIRGE,
        "command": [
            "/alauda/cleanup.sh"
        ],
        "name": "command_clean_up"
    },
    "type": "COMMANDS",
    "name": "command_clean_up",
    "configuration": {
        "depends_on": []
    }
}

DRAIN_NODE_RESOURCE = {
    'identifier': {
        'handler': COMMAND_HANDLER_DIRGE,
        "command": [
            'kubectl --kubeconfig=/etc/kubernetes/kubelet.conf drain {}  --force=true '
            '--ignore-daemonsets=True --timeout=300s; echo {}'
        ],
        "name": "command_drain_node"
    },
    "type": "COMMANDS",
    "name": "command_drain_node",
    "configuration": {
        "depends_on": []
    }
}

SWARM_CLEAN_UP_RESOURCE = {
    "identifier": {
        "handler": COMMAND_HANDLER_DIRGE,
        "command": [
            "/alauda/daemon.py reset"
        ],
        "name": "command_swarm_clean_up"
    },
    "type": "COMMANDS",
    "name": "command_swarm_clean_up",
    "configuration": {
        "depends_on": []
    }
}

MESOS_CLEAN_UP_RESOURCE = {
    "identifier": {
        "handler": COMMAND_HANDLER_SPECTRE,
        "command": [
            "echo 'not k8s region'"
        ],
        "name": "command_clean_up"
    },
    "type": "COMMANDS",
    "name": "command_clean_up",
    "configuration": {
        "depends_on": []
    }
}
