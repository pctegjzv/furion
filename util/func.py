#!/usr/bin/env python
# -*- coding: utf-8 -*-
import binascii
import os

from django.conf import settings
from mekansm.exceptions import MekFieldValidateFailed
from furion.exception import FurionException
from alauda.common.func import encode_aes, decode_aes
from alauda.common.func import pad_str
import inspect
from Crypto.Cipher import AES

__author__ = 'Hang Yan'


def ensure_key(feature_key, attr_key, data, is_list=False):
    if feature_key not in data:
        data[feature_key] = {}
    if attr_key not in data[feature_key]:
        data[feature_key][attr_key] = [] if is_list else {}


def check_integer(schema_list):
    def _schema_validate():
        assert isinstance(schema_list, list)
        for it in schema_list:
            assert isinstance(it, dict)
            assert set(it) - set('validator'), 'k, v pairs is needed'

    _schema_validate()

    def _process_schema(schema):
        validator = schema.pop('validator', None)
        key_name, value = schema.popitem()
        try:
            value = int(value)
        except ValueError:
            raise MekFieldValidateFailed({
                key_name: 'Integer needed, get {}'.format(type(value))
            })

        if validator:
            enum = validator.get('enum')

            if validator.get('enum'):
                if value not in validator['enum']:
                    raise MekFieldValidateFailed({key_name: 'Not in valid set {}'.format(enum)})
                else:
                    return

            max_value = validator.get('max')
            min_value = validator.get('min')

            if None is max_value:
                max_value = value + 1
            if None is min_value:
                min_value = value - 1

            if min_value < value < max_value:
                return

            if (not validator.get('exclude_min') and value != min_value or
                    not validator.get('exclude_max') and value == max_value):
                return

            raise MekFieldValidateFailed({
                key_name: 'Not in valid range {} ~ {}'.format(min_value, max_value)
            })

        map(_process_schema(schema), schema_list)


def check_keys(keys, data, request_data=False):
    def _process_key(key):
        if key not in data:
            if request_data:
                raise MekFieldValidateFailed([{
                    key: ['Missing required key in data: {}'.format(key)]}])
            raise FurionException('region_data_incorrect',
                                  message='Missing required key in data: {''}'.format(key))

    map(_process_key, keys)


_prefix = '__encrypted:'


def wrapped_encode_aes(cipher, plain_text):
    return _prefix + encode_aes(cipher, plain_text)


def wrapped_decode_aes(cipher, encrypted_text):
    if encrypted_text.startswith(_prefix):
        encrypted_text = encrypted_text[len(_prefix):]
    return decode_aes(cipher, encrypted_text)


def encrypt(origin):
    assert origin and isinstance(origin, str)
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    encrypted = wrapped_encode_aes(cipher, origin)
    return encrypted


def decrypt(encrypted):
    assert encrypted and isinstance(encrypted, str)
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    decrypted = wrapped_decode_aes(cipher, encrypted)
    return decrypted


def gen_rand(i=10):
    """Generate random strings for passwords."""
    return binascii.hexlify(os.urandom(i)).decode()


def get_stack():
    cur_frame = inspect.currentframe()
    cal_frame = inspect.getouterframes(cur_frame, 2)
    return cal_frame[2][3], cal_frame
