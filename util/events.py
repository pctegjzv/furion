from mekansm.request import MekRequest
from django.conf import settings
import logging
import time
import const


LOG = logging.getLogger(__name__)

EVENT_LEVEL_TRIVIAL = 0
EVENT_LEVEL_ATTENTION = 1
EVENT_LEVEL_IMPORTANT = 2

EVENT_OPERATOR_ADD = 'add'
EVENT_OPERATOR_CREATE = 'create'
EVENT_OPERATOR_REMOVE = 'remove'
EVENT_OPERATOR_DELETE = 'delete'
EVENT_OPERATOR_UPDATE = 'update'
EVENT_OPERATOR_DESTROY = 'destroy'
EVENT_OPERATOR_CHANGE_STATUS = 'status_change'

EVENT_STATUS_STARTED = 'started'
EVENT_STATUS_RUNNING = 'running'
EVENT_STATUS_DEPLOYING = 'deploying'
EVENT_STATUS_STOPPED = 'stopped'
EVENT_STATUS_ERROR = 'error'
EVENT_STATUS_SUCCEEDED = 'succeeded'
EVENT_STATUS_FAILED = 'failed'
EVENT_STATUS_CRITICAL = ''

EVENT_RESOURCE_NODE = 'node'
EVENT_RESOURCE_REGION = 'region'

OPERATOR = 'Alauda System'

OPERATION_LEVEL_MAPPING = {
    EVENT_OPERATOR_ADD: EVENT_LEVEL_TRIVIAL,
    EVENT_OPERATOR_CREATE: EVENT_LEVEL_TRIVIAL,
    EVENT_OPERATOR_DELETE: EVENT_LEVEL_TRIVIAL,
    EVENT_OPERATOR_UPDATE: EVENT_LEVEL_ATTENTION,
    EVENT_OPERATOR_DESTROY: EVENT_LEVEL_ATTENTION,
    EVENT_OPERATOR_REMOVE: EVENT_LEVEL_ATTENTION,
}

STATUS_LEVEL_MAPPING = {
    const.STATE_UNKNOWN: EVENT_LEVEL_ATTENTION,
    const.STATE_DEPLOYING: EVENT_LEVEL_TRIVIAL,
    const.STATE_BUILD: EVENT_LEVEL_TRIVIAL,
    const.STATE_PREPARING: EVENT_LEVEL_TRIVIAL,
    const.STATE_RUNNING: EVENT_LEVEL_TRIVIAL,
    const.STATE_ACTIVE: EVENT_LEVEL_TRIVIAL,
    const.STATE_STOPPING: EVENT_LEVEL_ATTENTION,
    const.STATE_STOPPED: EVENT_LEVEL_TRIVIAL,
    const.STATE_ERROR: EVENT_LEVEL_IMPORTANT,
    const.STATE_CRITICAL: EVENT_LEVEL_IMPORTANT,
    const.STATE_SHUTTING_DOWN: EVENT_LEVEL_ATTENTION,
    const.STATE_SHUTOFF: EVENT_LEVEL_ATTENTION,
    const.STATE_REMOVED: EVENT_LEVEL_ATTENTION,
    const.STATE_OFFLINE: EVENT_LEVEL_IMPORTANT,
}


def mapping_operation_to_log_level(operation):
    if operation in OPERATION_LEVEL_MAPPING:
        return OPERATION_LEVEL_MAPPING[operation]
    return 0


class Event(MekRequest):
    endpoint = "{}/v1".format(settings.TINY_ENDPOINT)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    @classmethod
    def add_event(cls, resource, operation, namespace, uuid, name, detail, template_id, state=None):
        detail['operation'] = operation
        log_level = mapping_operation_to_log_level(operation)
        if state:
            log_level = STATUS_LEVEL_MAPPING[state]
        event = [{
            "time": int(time.time() * 1000000),
            "log_level": log_level,
            "namespace": namespace,
            "resource_type": resource,
            "resource_id": uuid,
            "resource_name": name,
            "template_id": template_id,
            "detail": detail
        }]
        LOG.info("Add event: {}".format(event))
        try:
            cls.send('logs/events/', method='POST', data=event)
        except Exception as e:  # NOQA
            LOG.error("Create event fail. event: {} error: {}".format(event, e))

    @classmethod
    def node_state_update(cls, region, node, new_state, detail=''):
        msg = "Update node {} state to {} in region: {}({})".format(node.private_ip,
                                                                    new_state,
                                                                    region.name,
                                                                    region.id)
        if detail:
            msg = msg + ' detail: {}'.format(detail)
        detail = {'message': msg,
                  'operator': OPERATOR,
                  'status': new_state,
                  'region_name': region.name
                  }
        cls.add_event(EVENT_RESOURCE_NODE, EVENT_OPERATOR_CHANGE_STATUS, region.namespace,
                      region.id, node.private_ip, detail, 'service_status', new_state)

    @classmethod
    def region_state_update(cls, region, new_state, detail=''):
        msg = "Updated region {}({}) state to {}.".format(region.name,
                                                          region.id,
                                                          new_state)
        if detail:
            msg = msg + ' detail: {}'.format(detail)
        detail = {'message': msg,
                  'operator': OPERATOR,
                  'status': new_state,
                  'region_name': region.name
                  }
        cls.add_event(EVENT_RESOURCE_REGION, EVENT_OPERATOR_UPDATE, region.namespace,
                      region.id, region.name, detail, 'service_status', new_state)

    @classmethod
    def node_add(cls, region, ip, node_type, state):
        detail = {
            'operator': OPERATOR,
            'type': node_type,
            'status': state,
            'region_name': region.name
        }
        cls.add_event(EVENT_RESOURCE_NODE, EVENT_OPERATOR_ADD, region.namespace,
                      region.id, ip, detail, 'generic', state)

    @classmethod
    def node_remove(cls, region, ip, node_type):
        detail = {
            'operator': OPERATOR,
            'type': node_type,
            'status': const.STATE_REMOVED,
            'region_name': region.name
        }
        cls.add_event(EVENT_RESOURCE_NODE, EVENT_OPERATOR_REMOVE, region.namespace,
                      region.id, ip, detail, 'generic', const.STATE_REMOVED)
