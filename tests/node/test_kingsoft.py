from rest_framework.test import APIClient
from tests.base import Base
from tests import helper
from mock.mock import patch
from controls.region import RegionControl
from util.phoenix import EnvironmentManager
import json
from util import const
import importlib
__autho__ = 'lingming xia <lmxia@alauda.io>'
helper.load_json('fixtures/kingsoft/create_nodes.json')

class NodeTest(Base):

    def _pre_kingsoft_region_info(self):
        region_config = helper.load_json('fixtures/kingsoft/create_region.json')
        return region_config

    @patch.object(EnvironmentManager, 'create')
    def setUp(self, create):
        self.client = APIClient()
        self.env_uuid = '9D255D3E-904F-40D5-8B67-7AFD58461077'
        create.return_value = self.env_uuid
        self.region = json.loads(
            self.api_create_region(payload=self._pre_kingsoft_region_info()).content
        )

    @patch.object(RegionControl, 'get_user_data')
    @patch('cloud.kingsoft.Manager.create_instances')
    def test_create_node(self, mock_create_instances, mock_get_user_data):
        mock_get_user_data.return_value = '#bin/bash\ncommand'
        mock_create_instances.return_value = [{
            'instance_id': 'instance_id',
            'private_ip': 'private_ip',
            'state': const.STATE_RUNNING,
            'attr': {
                'instance_type': 'instance_type',
            }
        }]
        response = self.api_create_node(
            self.region['id'],
            helper.load_json('fixtures/kingsoft/create_nodes.json')
        )
        self.assertEqual(204, response.status_code)
        args, num, volume_size = mock_create_instances.call_args[0]
        self.assertEqual(1, num)
        self.assertEqual('ima', args['image_id'])
        self.assertEqual('79', args['instance_type'])
        self.assertEqual('2b4bd4af-6f4c-49a4-84b0-270983d487b2', args['subnet_id'])
        self.assertTrue('user_data' in args)
        user_data = args['user_data'].split('\n')
        custom_user_data = user_data.pop()
        self.assertEqual('command', custom_user_data)

    @patch('cloud.kingsoft.Manager.delete_instance')
    def test_delete_node(self, mock_delete_instance):
        mock_delete_instance.return_value = None
        self.test_create_node()
        response = self.api_delete_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)

    @patch('util.phoenix.NodeManager.activate')
    @patch('cloud.kingsoft.Manager.start_instance')
    def test_start_node(self, mock_start_instance, mock_activate):
        mock_start_instance.return_value = None
        self.test_create_node()
        response = self.api_start_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)
        self.assertTrue('instance_id', mock_start_instance.call_args[0])

    @patch('util.phoenix.NodeManager.activate')
    @patch('cloud.kingsoft.Manager.stop_instance')
    def test_stop_node(self, mock_stop_instance, mock_activate):
        mock_stop_instance.return_value = None
        self.test_create_node()
        response = self.api_stop_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)
        self.assertTrue('instance_id', mock_stop_instance.call_args[0])