from rest_framework.test import APIClient
from tests.base import Base
from mock.mock import patch
from tests import helper
import json
from controls.region import RegionControl
from util.phoenix import EnvironmentManager
from util import const


class NodeTest(Base):

    def _pre_aws_region_info(self):
        region_config = helper.load_json('fixtures/aws/create_region.json')
        return region_config

    @patch.object(EnvironmentManager, 'create')
    def setUp(self, mock_create):
        self.client = APIClient()
        self.env_uuid = '9D255D3E-904F-40D5-8B67-7AFD58461077'
        mock_create.return_value = self.env_uuid
        self.region = json.loads(
            self.api_create_region(payload=self._pre_aws_region_info()).content
        )
        # patcher = patch('openstack.iaas.connect_to_zone')
        # connect_to_zone = patcher.start()
        # connect_to_zone.return_value = None
        # self.addCleanup(patcher.stop)

    @patch.object(RegionControl, 'get_user_data')
    @patch('cloud.aws.Manager.create_instances')
    def test_create_node(self, mock_create_instances, mock_get_user_data):
        mock_get_user_data.return_value = '#bin/bash\n command'
        mock_create_instances.return_value = [{
            'instance_id': 'instance_id',
            'private_ip': 'private_ip',
            'state': const.STATE_RUNNING,
            'attr': {
                'instance_type': 'instance_type',
            }
        }]
        response = self.api_create_node(
            self.region['id'],
            helper.load_json('fixtures/aws/create_nodes.json')
        )
        print response
        self.assertEqual(204, response.status_code)

