# noqa

AWS_CREATE_INSTANCE_PARAM = {
    "ImageId": 'ami-123456',
    "MinCount": 1,
    "MaxCount": 1,
    "InstanceType": 'm3.medium',
    "SecurityGroupIds": [
        "sg-12345678",
        "sg-123456789"
    ],
    "SubnetId": 'subnet-12345678',
    "UserData": '#!/bin/bash\ncurl http://get.alauda.cn/deploy/slave/ | sh -s -- --mesos-version 0.28.1-luna '
                '--marathon-url http://1.1.1.1:8080/v2/info --username sys_admin --token 1234567890 --zeus-version '
                '123456 --nevermore-version 123456 --log-token 1234567890 --cloud-name AWS --region-name test\ncustom '
                'user data',
    # noqa
}

AWS_BLOCK_MAPPINGS = {
    "BlockDeviceMappings": [
        {
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "VolumeSize": 100,
                "DeleteOnTermination": True,
                "VolumeType": "gp2",
            },
        }
    ]
}

CREATE_NODES_REQUEST = {
    "access_key": "123456",
    "ssh_key": "123456",
    "image_id": "ami-123456",
    "subnet_id": "subnet-12345678",
    "secret_access_key": "123456",
    "instance_type": "m3.medium",
    "security_groups": [
        "sg-12345678",
        "sg-123456789"
    ],
    'num': 1
}

AWS_INSTANCE = {
    'InstanceId': 'string',
    'ImageId': 'ami-123456',
    'InstanceType': 'm3.medium',
    'PrivateIpAddress': '1.1.1.2',
    'PublicIpAddress': '1.1.1.1',
}

UPDATE_NODE_MAX_DISK_USAGE_DATA = {
    'attr': {
        'max_disk_usage': 0.35,
        'components': []
    },
}

UPDATE_NODE_STAT = {
    "apps": [],
    "nodes": [
        {
            "containers_count": 0,
            "mem_allocated": 2097,
            "private_ip": "172.31.21.27",
            "cpus_utilization": 1.0,
            "ports_allocated": "",
            "mem_utilization": 1.0,
            "cpus_total": 1,
            "uptime": 80137,
            "ports_total": "",
            "hostname": "ip-172-31-21-27",
            "cpus_allocated": 1.0,
            "attributes": {
                "Status": {
                    "State": "ready"
                },
                "Description": {
                    "Engine": {
                        "Plugins": [
                            {
                                "Type": "Network",
                                "Name": "bridge"
                            },
                            {
                                "Type": "Network",
                                "Name": "host"
                            },
                            {
                                "Type": "Network",
                                "Name": "null"
                            },
                            {
                                "Type": "Network",
                                "Name": "overlay"
                            },
                            {
                                "Type": "Volume",
                                "Name": "local"
                            }
                        ],
                        "EngineVersion": "1.12.1"
                    },
                    "Platform": {
                        "OS": "linux",
                        "Architecture": "x86_64"
                    },
                    "Hostname": "ip-172-31-21-27",
                    "Resources": {
                        "MemoryBytes": 2097647616,
                        "NanoCPUs": 1000000000
                    }
                },
                "ID": "7rd3jibz7nplh42wv7efa48xk",
                "Version": {
                    "Index": 10
                },
                "ManagerStatus": {
                    "Reachability": "reachable",
                    "Leader": True,
                    "Addr": "172.31.21.27:2377"
                },
                "UpdatedAt": "2016-12-27T10:19:25.495154517Z",
                "Spec": {
                    "Role": "manager",
                    "Availability": "active"
                },
                "CreatedAt": "2016-12-27T10:19:25.472355775Z"
            },
            "mem_total": 2097
        },
        {
            "containers_count": 0,
            "mem_allocated": 2097,
            "private_ip": "172.31.19.2",
            "cpus_utilization": 1.0,
            "ports_allocated": "",
            "mem_utilization": 1.0,
            "cpus_total": 1,
            "uptime": 80130,
            "ports_total": "",
            "hostname": "ip-172-31-19-2",
            "cpus_allocated": 1.0,
            "attributes": {
                "Status": {
                    "State": "ready"
                },
                "Description": {
                    "Engine": {
                        "Plugins": [
                            {
                                "Type": "Network",
                                "Name": "bridge"
                            },
                            {
                                "Type": "Network",
                                "Name": "host"
                            },
                            {
                                "Type": "Network",
                                "Name": "null"
                            },
                            {
                                "Type": "Network",
                                "Name": "overlay"
                            },
                            {
                                "Type": "Volume",
                                "Name": "local"
                            }
                        ],
                        "EngineVersion": "1.12.1"
                    },
                    "Platform": {
                        "OS": "linux",
                        "Architecture": "x86_64"
                    },
                    "Hostname": "ip-172-31-19-2",
                    "Resources": {
                        "MemoryBytes": 2097647616,
                        "NanoCPUs": 1000000000
                    }
                },
                "ID": "9igg5zu3wxu1pg7hnyvbh6i8y",
                "Version": {
                    "Index": 18
                },
                "UpdatedAt": "2016-12-28T07:52:22.557202409Z",
                "Spec": {
                    "Role": "worker",
                    "Availability": "active"
                },
                "CreatedAt": "2016-12-27T10:19:32.904444917Z"
            },
            "mem_total": 2097
        }
    ],
    "type": "SWARM_METRICS",
    "region": {
        "cpus_total": 1,
        "cpus_allocated": 0,
        "containers_count": 0,
        "mem_total": 2097,
        "cpus_utilization": 0,
        "nodes_count": 2,
        "services_count": 0,
        "mem_allocated": 0,
        "mem_utilization": 0.0,
        "available_ports_count": 0
    }
}

KUBERNETES_METRICS = {
    'nodes': [
    ],
    'type': 'KUBERNETES_METRICS',
    'region': UPDATE_NODE_STAT['region']
}
