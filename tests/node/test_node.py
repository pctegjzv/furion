from rest_framework.test import APITestCase, APIClient
from django.core.urlresolvers import reverse
from app.models import Region, Node, Stats, Label
from tests import helper
from cloud.aws import Manager
from controls.region import RegionControl, _xcode_region_info
from controls.node import NodeControl
import mock
from botocore.stub import Stubber

from tests.helper import my_reverse
from . import constant
import json
import uuid
from util.misc import TinyRequest
from util import const


class NodeTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()

        self.feature = helper.load_json('fixtures/region-feature.json')
        self.attr = helper.load_json('fixtures/region-attr.json')
        _xcode_region_info('encode', self.feature, self.attr)
        self.region = Region.objects.create(
            name='test',
            display_name='test',
            namespace='',
            state='RUNNING',
            attr=self.attr,
            features=self.feature
        )

        k8s_region_json = helper.load_json('fixtures/region/k8s.json')
        self.k8s_region = Region.objects.create(
            name='test-k8s-region',
            display_name='hehe',
            namespace='',
            state='RUNNING',
            container_manager="KUBERNETES",
            attr=k8s_region_json['attr'],
            features=k8s_region_json['features']
        )

        self.k8s_node = Node.objects.create(
            id=str(uuid.uuid4()),
            private_ip='0.0.0.0',
            region=self.k8s_region,
            type=const.NODE_TYPE_COMPUTE
        )

        self.node = Node.objects.create(
            id=str(uuid.uuid4()),
            private_ip='1.1.1.1',
            region=self.region
        )

        self.label1 = Label.objects.create(
            key='a',
            value='a',
            editable=True,
            node=self.k8s_node,
        )
        self.label2 = Label.objects.create(
            key='b',
            value='b',
            editable=True,
            node=self.k8s_node,
        )
        self.label3 = Label.objects.create(
            key='c',
            value='c',
            editable=False,
            node=self.k8s_node,
        )

        Stats.objects.create(
            data={'nodes': [{'private_ip': self.node.private_ip}]},
            region=self.region,
            type='MESOS_METRICS'
        )

        self.instances = {'Instances': [constant.AWS_INSTANCE]}

        constant.CREATE_NODES_REQUEST.update({
            "region_id": self.region.id,
        })
        self.data = self.args = constant.CREATE_NODES_REQUEST

    def tearDown(self):
        APITestCase.tearDown(self)

    @mock.patch.object(RegionControl, 'get_user_data')
    @mock.patch('controls.region.RegionControl.get_cloud')
    def _create_node(self, data, create_excepted_params, mock_get_cloud, mock_get_user_data):
        mock_get_user_data.return_value = '#bin/bash\n command'
        mock_get_cloud.return_value = m = Manager(self.args)
        user_data = RegionControl.get_user_data(
            RegionControl.get_object(id=self.region.id),
            'mesos-slave'
        )
        user_data += '\n' + self.region.features.get('node',
                                                     {}).get('instance', {}).get('user_data')
        create_excepted_params.update({'UserData': user_data})
        stuber = Stubber(m.ec2_res.meta.client)
        stuber1 = Stubber(m.ec2_client)
        stuber.add_response(
            'run_instances',
            self.instances,
            create_excepted_params)
        stuber1.add_response('create_tags', {})
        stuber.activate(), stuber1.activate()
        url = reverse('regions/nodes/', kwargs={'region_id': self.region.id})
        response = self.client.post(url, data=data, format='json')
        stuber1.deactivate(), stuber.deactivate()
        return response

    def _test_create_node_base(self):
        data = self.data
        create_excepted_params = constant.AWS_CREATE_INSTANCE_PARAM.copy()
        response = self._create_node(data, create_excepted_params)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Node.objects.all().count(), 3)

    def _test_get_node_base(self):
        url = reverse(
            'regions/nodes/private-ip',
            kwargs={'region_id': self.region.id, 'private_ip': '1.1.1.2'}
        )
        response = self.client.get(url)
        node = json.loads(response.content)
        self.assertEqual(node['instance_id'], constant.AWS_INSTANCE['InstanceId'])
        self.assertEqual(node['private_ip'], constant.AWS_INSTANCE['PrivateIpAddress'])
        self.assertEqual(node['attr']['public_ip'], constant.AWS_INSTANCE['PublicIpAddress'])
        self.assertEqual(node['attr']['instance_type'], constant.AWS_INSTANCE['InstanceType'])
        self.assertEqual(node['attr']['source'], 'api-create')
        self.assertEqual(node['attr']['init_volume_size'], self.volume_size)

    def test_node_create_get_base_case(self):
        self.volume_size = ''
        self._test_create_node_base()
        self._test_get_node_base()

    def test_labels(self):
        url = reverse('regions/nodes/private-ip',
                      kwargs={'region_id': self.k8s_region.id, 'private_ip': '0.0.0.0'})
        response = self.client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data['labels']), 3)
        self.assertEqual(data['labels'][0]['key'], 'c')

        url = reverse('regions/region-id/labels',
                      kwargs={'region_id': self.k8s_region.id})

        response = self.client.get(url)
        labels = json.loads(response.content)['labels']
        self.assertEqual(response.status_code, 200)
        self.assertEqual(labels[0]['key'], 'a')
        self.assertEqual(labels[1]['key'], 'b')

    def test_node_select_by_label(self):
        url = my_reverse('regions/region-id/select-node',
                         kwargs={'region_id': self.k8s_region.id},
                         query_kwargs={"labels": "a:a,b:b"})
        response = self.client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['nodes'][0], '0.0.0.0')

    def test_list_nodes_with_labels(self):
        url = reverse('regions/nodes/', kwargs={'region_id': self.k8s_region.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)[0]['labels']
        self.assertEqual(len(data), 3)

    def _test_create_node_with_size(self):
        data = self.data
        self.volume_size = '100'
        data['volume_size'] = self.volume_size
        create_excepted_params = constant.AWS_CREATE_INSTANCE_PARAM.copy()
        create_excepted_params.update(constant.AWS_BLOCK_MAPPINGS)
        response = self._create_node(data, create_excepted_params)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Node.objects.all().count(), 3)

    def test_node_create_get_with_volume_case(self):
        self._test_create_node_with_size()
        self._test_get_node_base()

    @mock.patch.object(TinyRequest, 'send')
    def test_node_update_disk_metrics(self, mock_send):
        mock_send.return_value = None
        url = reverse(
            'regions/nodes/private-ip',
            kwargs={'region_id': self.region.id, 'private_ip': self.node.private_ip}
        )
        response = self.client.put(url, data=constant.UPDATE_NODE_MAX_DISK_USAGE_DATA,
                                   format='json')
        self.assertEqual(response.status_code, 204)
        nodes_stats = Stats.objects.get(region=self.region).data['nodes']
        stats = None
        for item in nodes_stats:
            if item.get('private_ip') == self.node.private_ip:
                stats = item
                break
        self.assertIsNotNone(stats)
        self.assertEqual(stats.get('max_disk_usage'), 0.35)

    def test_update_compute_node_stats(self):
        Node.objects.create(
            id=str(uuid.uuid4()),
            private_ip='1.1.1.2',
            region=self.region,
            type=const.NODE_TYPE_COMPUTE,
        )

        data = [{
            'private_ip': '1.1.1.2',
            'labels': {
                'ip': '1.1.1.2'
            },
            'cpus_total': 2,
            'mem_total': 1024,
            'cpus_allocated': 0.125,
            'mem_allocated': 512,
        }
        ]
        NodeControl.update_compute_nodes_stats(self.region, data)
        node = Node.objects.get(region=self.region, private_ip='1.1.1.2')
        self.assertEqual(node.attr['node_tag'], 'ip:1.1.1.2')

    @mock.patch.object(TinyRequest, 'send')
    def test_node_update_swarm_metrics(self, mock_send):
        mock_send.return_value = None
        url = reverse(
            'regions/region-id/stats',
            kwargs={'region_id': self.region.id}
        )

        response = self.client.put(url, data=constant.UPDATE_NODE_STAT, format='json')
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Stats.objects.all().count(), 2)

    def _create_node_in_region(self, region, ip, node_type):
        node = Node.objects.create(
            id=str(uuid.uuid4()),
            private_ip=ip,
            region=region,
            type=node_type)
        return node
