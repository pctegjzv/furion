from rest_framework.test import APIClient
from tests.base import Base
from tests import helper
from mock.mock import patch
from controls.region import RegionControl
import json
from util import const


__autho__ = 'Lei Gong <lgong@alauda.io>'


class NodeTest(Base):

    def _pre_qingcloud_region_info(self):
        region_config = helper.load_json('fixtures/region/create_region.json')
        region_config['attr'].update(helper.load_json('fixtures/qingcloud/region-attr.json'))
        region_config['features'].update(helper.load_json('fixtures/qingcloud/region-feature.json'))
        return region_config

    def setUp(self):
        self.client = APIClient()
        self.region = json.loads(
            self.api_create_region(payload=self._pre_qingcloud_region_info()).content
        )
        patcher = patch('qingcloud.iaas.connect_to_zone')
        connect_to_zone = patcher.start()
        connect_to_zone.return_value = None
        self.addCleanup(patcher.stop)

    @patch.object(RegionControl, 'get_user_data')
    @patch('cloud.qing.Manager.create_instances')
    def test_create_node(self, mock_create_instances, mock_get_user_data):
        mock_get_user_data.return_value = '#bin/bash\n command'
        mock_create_instances.return_value = [{
            'instance_id': 'instance_id',
            'private_ip': 'private_ip',
            'state': const.STATE_RUNNING,
            'attr': {
                'instance_type': 'instance_type',
                'public_ip': 'public_ip',
                'init_volume_size': 'init_volume_size'
            }
        }]
        response = self.api_create_node(
            self.region['id'],
            helper.load_json('fixtures/qingcloud/create_nodes.json')
        )
        self.assertEqual(204, response.status_code)
        args, num, volume_size = mock_create_instances.call_args[0]
        self.assertEqual('1', num)
        self.assertEqual(None, volume_size)
        self.assertEqual('ami', args['image_id'])
        self.assertEqual('keypair', args['login_mode'])
        self.assertEqual('c1m1', args['instance_type'])
        self.assertEqual('kp-iioo1rih', args['ssh_key_name'])
        self.assertEqual('sg', args['security_groups'])
        self.assertTrue('user_data' in args)
        user_data = args['user_data'].split('\n')
        custom_user_data = user_data.pop()
        self.assertEqual('custom user data', custom_user_data)

    def test_list_nodes(self, ):
        self.test_create_node()
        response = self.api_list_node(self.region['id'])
        self.assertEqual(200, response.status_code)
        nodes = json.loads(response.content)
        self.assertEqual(len(nodes), 1)
        self.assertTrue('state' in nodes[0])
        self.assertTrue('private_ip' in nodes[0])
        self.assertTrue('resources' in nodes[0])
        self.assertEqual(const.NODE_SOURCE_API_CREATE, nodes[0]['attr']['source'])

    @patch('util.phoenix.NodeManager.delete')
    @patch('util.phoenix.NodeManager.delete_resources')
    @patch('cloud.qing.Manager.delete_instance')
    def test_delete_node(self, mock_delete_instance, mock_delete_resources, mock_delete):
        mock_delete_instance.return_value = None
        self.test_create_node()
        response = self.api_delete_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)

    @patch('util.phoenix.NodeManager.activate')
    @patch('cloud.qing.Manager.start_instance')
    def test_start_node(self, mock_start_instance, mock_activate):
        mock_start_instance.return_value = None
        self.test_create_node()
        response = self.api_start_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)
        self.assertTrue('instance_id', mock_start_instance.call_args[0])

    @patch('util.phoenix.NodeManager.activate')
    @patch('cloud.qing.Manager.stop_instance')
    def test_stop_node(self, mock_stop_instance, mock_activate):
        mock_stop_instance.return_value = None
        self.test_create_node()
        response = self.api_stop_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)
        self.assertTrue('instance_id', mock_stop_instance.call_args[0])

    def test_get_node(self):
        self.test_create_node()
        self.api_get_node(self.region['id'], 'private_ip')
