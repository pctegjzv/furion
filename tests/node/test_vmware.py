from rest_framework.test import APIClient
from tests.base import Base
from tests import helper
from mock.mock import patch
from controls.region import RegionControl
from util.phoenix import EnvironmentManager
import json
from util import const
import importlib
__autho__ = 'lingming xia <lmxia@alauda.io>'


class NodeTest(Base):

    def _pre_vmware_region_info(self):
        region_config = helper.load_json('fixtures/region/vmware_region.json')
        return region_config

    @patch.object(EnvironmentManager, 'create')
    def setUp(self, create):
        self.client = APIClient()
        self.env_uuid = '9D255D3E-904F-40D5-8B67-7AFD58461077'
        create.return_value = self.env_uuid
        self.region = json.loads(
            self.api_create_region(payload=self._pre_vmware_region_info()).content
        )

    @patch.object(RegionControl, 'get_user_data')
    @patch.object(RegionControl, 'get_cloud')
    @patch('cloud.vmware.Manager.create_instances')
    def test_create_node(self, mock_create_instances, mock_get_cloud, mock_get_user_data):
        module = importlib.import_module('cloud.{}'.format('vmware'))
        mock_get_cloud.return_value = getattr(module, 'Manager')
        mock_get_user_data.return_value = '#bin/bash\n command'
        mock_create_instances.return_value = [{
            'instance_id': 'instance_id',
            'private_ip': 'private_ip',
            'state': const.STATE_RUNNING,
            'attr': {
                'instance_type': 'instance_type'
            }
        }]
        response = self.api_create_node(
            self.region['id'],
            helper.load_json('fixtures/vmware/create_nodes.json')
        )
        self.assertEqual(204, response.status_code)

    @patch.object(RegionControl, 'get_cloud')
    @patch('cloud.openstack.Manager.delete_instance')
    def test_delete_node(self, mock_delete_instance, mock_get_cloud):
        mock_delete_instance.return_value = None
        module = importlib.import_module('cloud.{}'.format('vmware'))
        mock_get_cloud.return_value = getattr(module, 'Manager')
        self.test_create_node()
        response = self.api_delete_node(self.region['id'], 'private_ip')
        self.assertEqual(204, response.status_code)
