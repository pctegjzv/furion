from django.test.testcases import TestCase
from cloud.qing import Manager
from tests import helper
from mock.mock import patch, MagicMock
from furion.exception import FurionException


class NodeTest(TestCase):

    def setUp(self):
        self.args = {
            'region_id': 'GD1',
            'access_key': 'MDFQKAPEDEWIMILHXWXM',
            'secret_access_key': 'rjdv9s29Zw3ZO0Onn3E0tvtLPZAUfafdTtFnUsCC'
        }
        self.m = Manager(self.args)
        patcher = patch('qingcloud.iaas.connection.APIConnection', autospec=True)
        mock_claas = patcher.start()
        self.instance = mock_claas.return_value
        self.addCleanup(patcher.stop)
        self.m.session = self.instance

    def test_create_instnace(self):
        self.instance.create_tag.return_value = {
            "action": "CreateTagResponse",
            "tag_id": "tag-axbkmf20",
            "ret_code": 0
        }
        self.instance.attach_tags.return_value = {
            "action": "AttachTagsResponse",
            "ret_code": 0
        }
        self.instance.run_instances.return_value = helper.load_json(
            'fixtures/qingcloud/run_instances.json')
        self.instance.describe_instances.return_value = helper.load_json(
            'fixtures/qingcloud/describe_instances.json')

        self.m._describe_instance_types = MagicMock(
            return_value=helper.load_json('fixtures/qingcloud/instance_type.json')
        )

        payload = helper.load_json('fixtures/qingcloud/create_instances.json')
        instances = self.m.create_instances(payload, 1)
        instance = helper.load_json('fixtures/qingcloud/describe_instances.json'
                                    )['instance_set'][0]
        self.assertEqual(len(instances), 1)
        self.assertEqual(instances[0]['instance_id'], instance['instance_id'])
        self.assertEqual(instances[0]['private_ip'], instance['vxnets'][0]['private_ip'])
        self.assertEqual(instances[0]['state'].lower(), instance['status'])
        self.assertEqual(instances[0]['attr']['instance_type'], instance['instance_type'])
        self.assertEqual(instances[0]['attr']['public_ip'], instance['eip']['eip_addr'])
        self.assertTrue('init_volume_size' in instances[0]['attr'])

    def test_stop_instance(self):
        self.instance.describe_instances.return_value = helper.load_json(
            'fixtures/qingcloud/describe_instances.json')
        self.instance.stop_instances.return_value = {
            "action": "StopInstancesResponse",
            "job_id": "j-ybnoeitr",
            "ret_code": 0
        }

        self.m.stop_instance('inst_id')

    def test_start_instance(self):
        self.instance.describe_instances.return_value = helper.load_json(
            'fixtures/qingcloud/describe_instances.json')
        self.instance.start_instances.return_value = {
            "action": "StartInstancesResponse",
            "job_id": "j-ybnoeitr",
            "ret_code": 0
        }
        with self.assertRaises(FurionException) as cm:
            self.m.start_instance('inst_id')

        self.assertEqual('Instance is not STOPPED, cannot start it', cm.exception.message)

    def test_delete_instance(self):
        self.instance.terminate_instances.return_value = {
            "message": "ResourceNotFound, resource [['123']] not found",
            "ret_code": 2100
        }
        self.m.delete_instance('inst_id')

    def test_list_key_pairs(self):
        self.instance.describe_key_pairs.return_value = helper.load_json(
            'fixtures/qingcloud/desc_keypairs.json')

        res = self.m.list_key_pairs()
        self.assertEqual(res['key_pairs'], ["kp-bn2n77ow"])

    def test_get_instance_info(self):
        self.instance.describe_instances.return_value = helper.load_json(
            'fixtures/qingcloud/describe_instances.json')
        instance = helper.load_json('fixtures/qingcloud/describe_instances.json'
                                    )['instance_set'][0]
        res = self.m.get_instance_info(instance['vxnets'][0]['private_ip'])
        self.assertEqual(res['instance_id'], instance['instance_id'])
        self.assertEqual(res['private_ip'], instance['vxnets'][0]['private_ip'])
        self.assertEqual(res['state'].lower(), instance['status'])
        self.assertEqual(res['attr']['instance_type'], instance['instance_type'])
        self.assertEqual(res['attr']['public_ip'], instance['eip']['eip_addr'])

    def test_get_instance_types(self):
        self.m.session.send_request = MagicMock(
            return_value=helper.load_json('fixtures/qingcloud/instance_type.json'),
            autospec=True
        )
        self.assertEqual(self.m.get_instance_types(), {"instance_types": ["c1m1"]})
