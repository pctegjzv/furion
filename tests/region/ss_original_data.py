BASE_REGION = {
    "container_manager": "MESOS",
    "template_type": "poc",
    "username": "claasv1/xdzhang",
    "token": "20cefd49c0ed220b756070683163dabf34bd3c45",
    "master": {
        "public_ip": "139.219.234.191",
        "private_ip": "10.1.0.13"
    },
    "features": [],
    "over_commit": {"cpu": 1,
                    "memory": 2},
    "slave_with_tag": [
        "10.1.0.16"
    ],
    "slave": [
        "10.1.0.14",
        "10.1.0.16"
    ],
    "slave_username": "ubuntu",
    "root_token": '31469fa01be47611376bd82eea8dbbbaa3e88e78'
}

PROD_MASTER = ['10.1.0.10', '10.1.0.11', '10.1.0.12']

DOOM_NODE = {
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
}

HAPROXY = {
    "public_ip": "139.219.234.191",
    "private_ip": "10.1.0.13"
}

HAPROXY_HA = {
    "public_ip": "this-is-url-example-public",
    "private_ip": "this-is-url-example-private",
    "nodes": ['10.3.0.1', '10.3.0.2']
}

INTERNAL_HAPROXY = '10.1.0.17'
INTERNAL_HAPROXY_HA = {
    'nodes': ['10.2.0.3', '10.2.0.4'],
    'floating_ip': '10.3.0.1',
}

REGISTRY = {
    "node": "10.1.0.15",
    "registry_settings": {
        "name": "all_test_registry",
        "storage": "local",
        "local_hostdir": "/home/ubuntu",
        "local_rootdir": "/root"
    }
}

VOLUME = {
    'volume_settings': {
        "glusterfs": {
            "replica": 1,
            "nodes": {
                '10.1.0.20': [
                    "/dev/sda1"
                ],
                '10.1.0.21': [
                    "/dev/sda1"
                ],
                '10.1.0.22': [
                    "/dev/sda1"
                ]
            }
        },
        'ebs': {
            'aws_access_key': 'aaaaa',
            'aws_secret_key': 'bbbbb',
            'zone': 'cn-north-1a'
        }
    },
    "storageclasses": [
        {
            "name": "ebs",
            "default": True,
        },
        {
            "name": "glusterfs",
            "default": False,
        }
    ]
}

CONTROLLER_HAPROXY = {
    'controller_haproxy': {
        'nodes': ['10.1.0.23', '10.1.0.24'],
        'floating_ip': '192.168.1.1',
        'monitor_ip': '114.114.114.114'
    }
}

K8S_CNI = 'flannel'

K8S_CIDR = '10.1.0.0/16'

K8S_CNI_FLANNEL = {
    'type': 'flannel',
    'flannel': {
        'type': 'vxlan'
    }
}

K8S_CNI_MACVLAN = {
    'type': 'macvlan'
}

K8S_CNI_CALICO = {
    'type': 'calico'
}

MESOS_CUSTOMER_LBS = {
    'puck_public_lb': 'http://puck_public_lb:58080',
    'puck_private_lb': 'http://puck_private_lb:58080',
    'marathon_lb': 'http://marathon_lb:58081',
    'registry_lb': 'http://registry_lb:55000',
    'chronos_public_lb': 'http://chronos_public_lb:54400',
    'chronos_private_lb': 'http://chronos_private_lb:54400',
    'xin_agent_lb': 'http://xin_agent_lb:58090',
    'doom_lb': 'tcp://doom_lb'
}

K8S_CUSTOMER_LBS = {
    'puck_public_lb': 'http://puck_public_lb:58080',
    'puck_private_lb': 'http://puck_private_lb:58080',
    'registry_lb': 'http://registry_lb:55000',
    'xin_agent_lb': 'http://xin_agent_lb:58090',
    'doom_lb': 'tcp://doom_lb',
    'advertise_address': 'customer_advertise_address',
    'advertise_public_address': 'customer_advertise_public_address'
}
