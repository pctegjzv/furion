from rest_framework import status


PORT_POOL_PUBLIC_IP = '1.1.1.1'
PORT_POOL_PRIVATE_IP = '2.2.2.2'
PORT_POOL_SSH_PORT = [22]
PORT_POOL_MAPPING_PORT = range(5)
CREATE_REQUEST = {
    u"name": u"claasv1-integration-region",
    u"display_name": u"test-region",
    u"namespace": u"test",
    u'platform_version': u'v2',
    u"state": u"RUNNING",
    u"attr": {
        u"insecure_registry": [
            u"index.int.alauda.io"
        ],
        u"mesos": {
            u"marathon_endpoint": u"http://52.26.219.200:8080",
            u"auth": {
                u"username": u"sys_admin",
                u"token": u"*******",
                u"log_token": u"*******"
            },
            u"versions": {
                u"nevermore": u"ccff572362ffe72b1fe1efe45b590bf1b10c3df9",
                u"zeus": u"90973afb4fba18479daea298c66054728982a602",
                u"mesos": u"0.24.1-1"
            }
        },
        u"mesos_endpoint": u"http://52.37.87.50:8080",
        u"cloud": {
            u"region_id": u"us-west-2",
            u"name": u"AWS"
        },
        u"components": {
            u"meepo": {
                u"endpoint": u"0.0.0.0:0"
            }
        },
        u"node_scaling": {
            u"enable_scale": False,
            u"scale_down": {
                u"min_mem_utilization": 0.2,
                u"decrease_delta": 1,
                u"max_available_ports_count": 2,
                u"min_scale_num": 1,
                u"forbid_delete_occupied_node": False,
                u"min_cpus_utilization": 0.2
            },
            u"scale_up": {
                u"increase_delta": 1,
                u"max_mem_utilization": 0.91,
                u"min_available_ports_count": 0,
                u"instance_type": u"t2.medium",
                u"ssh_key_name": u"mathilde",
                u"max_cpus_utilization": 0.95,
                u"max_scale_num": 7
            }
        }
    },
    u"features": {
        u"node": {
            u"instance": {
                u"access_key": u"*******",
                u"instance_type": u"t2.micro",
                u"subnet_id": u"subnet-000",
                u"secret_access_key": u"*******",
                u"ssh_key": u"*******",
                u"image_id": u"ami-000",
                u"security_groups": [
                    u"sg-4955ac2d"
                ]
            },
            u"features": [
                u"instance",
                u"auto-scaling"
            ]
        },
        u"service": {
            u"haproxy": {
                u"cloud_name": u"AWS",
                u"lb_tag": u"ha",
                u"ipaddress": u"0.0.0.0",
                u"private_ip": u"1.1.1.1",
                u"region_name": u"BEIJING1"
            },
            u"features": [
                u"host-network",
                u"elb",
                u"raw-container",
                u"haproxy",
                u"internal-haproxy",
                u"application"
            ],
            u"elb": {
                u"access_key": u"*******",
                u"subnets": [
                    u"subnet-000",
                    u"subnet-001"
                ],
                u"secret_access_key": u"*******",
                u"security_groups": [
                    u"sg-4955ac2d"
                ]
            },
            u"metrics": {
                u"features": [
                    u"aws-metric"
                ]
            },
            u"manager": {
                u"host_port_range": [
                    31000,
                    32000
                ],
                u"endpoint": u"http://0.0.0.1:10000/marathon",
                u"port_resources": [
                    [
                        31000,
                        32000
                    ]
                ],
                u"user": u"oilbeater",
                u"docker_resource_ratio": {
                    u"mem": 2,
                    u"cpu": 2
                },
                u"password": u"hehehehe"
            },
            u"internal_haproxy": {
                u"cloud_name": u"AWS",
                u"private_ip": u"0.0.0.0",
                u"region_name": u"BEIJING1",
                u"lb_tag": u"ha"
            },
            u"volume": {
                u"drivers": [{
                    u"driver_name": u"ebs",
                    u"disp_name": u"AWS EBS",
                    u"desc": u"AWS EBS"
                }],
                u"agent": {
                    u"username": u"oilbeather",
                    u"password": u"hehehe",
                    u"endpoint": u"http://1.1.1.1:1000/volume_driver"
                }
            }
        }
    },
    u'env_uuid': u'9a37a583-ec1b-4678-bb92-cc5967941ae0',
    u'container_manager': u'MESOS'
}

PHOENIX_DATA = {
    u'commands': {
        u'install': u'http://test --node-type=mesos-slave ' +
        u'--docker-path=/var/lib/docker --docker-version=1.12.6 --experimental=1 ' +
        u'--insecure-registry-list claasv1-integration-region-test.index.myalauda.cn:5000',
        u'uninstall': u'curl -fsSL http://get.alauda.cn/remove/spectre/ | sudo bash'
    }
}

PHOENIX_SETTINGS = {
    u'TINY_LB': u'http://api.int.alauda.io/v1/callback/tiny',
    u'MEEPO_NODE': u'3.3.3.3',
    u'HAPROXY_NODE': {
        u'public_ip': u'52.11.134.128',
        u'private_ip': u'172.31.9.6'
    },
    u'PUCK_NODES': {
        u'public_ip': u'52.42.181.165',
        u'private_ip': u'172.31.32.187'
    },
    u'CLOUD_LB_SETTINGS': {
        u'elb': {
            u'access_key': u'AKIAJB4NAHXKHG2BI5QA',
            u'subnets': [u'subnet-6ba7ac2d'],
            u'secret_access_key': u'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',
            u'security_groups': [u'sg-4955ac2d']
        },
        u'ulb': {
            u'public_key': u'public_key',
            u'private_key': u'private_key',
            u'project_id': u'project_id',
            u'bandwidth': u'0'
        },
        u'slb': {
            u'access_key': u'access_key',
            u'secret_access_key': u'secret_access_key'
        },
        u'blb': {u'access_key': u'access_key',
                 u'secret_access_key': u'secret_access_key'
                 }
    },
    u'SLAVE_NODES': [u'172.31.9.9'],
    u'MEEPO_IP': u'3.3.3.3',
    u'DOOM_NODES': {
        u'public_ip': u'52.42.181.165',
        u'private_ip': u'172.31.32.187'
    },
    u'CONTROLLER_HAPROXY_NODES': [u'172.31.1.1', u'172.31.2.2'],
    u'CONTROLLER_HAPROXY_FLOATING_IP': u'192.168.1.1',
    u'CONTROLLER_HAPROXY_MONITOR_IP': u'114.114.114.114',
    u'USER_SETTINGS': {
        u'JAKIRO_AUTH': {
            u'USERNAME': u'xdzhang', u'TOKEN': u'b7beb6ce29bf89f3f0abaa18dda69d93dd9989f3'
        },
        u'REGION': {
            u'SLAVE_USERNAME': u'ubuntu',
            u'DISPLAY_NAME': u'show-phoenix-prod',
            u'NAME': u'unique_phoenix',
            u'NODE_LOCATION': u'us-west-2',
            u'NAMESPACE': u'claasv1',
            u'CLOUD_NAME': u'aws',
            u'OVER_COMMIT': {
                u'CPU': 1,
                u'MEMORY': 1
            },
            u'FEATURES':
                [u'host-network', u'elb', u'exec', u'raw-container',
                 u'haproxy', u'application', u'file-log']
        },
        u'PUCK_AUTH':
            {
                u'USERNAME': u'puck_username', u'PASSWORD': u'puck_password'
            }
    },
    u'IMAGE_LOCATION': u'IO',
    u'PUCK_PUBLIC_LB': u'',
    u'PUCK_INTERNAL_LB': u'',
    u'REGION_LOCATION': u'INT',
    u'cluster_template_type': u'prod',
    u'COMPONENT': [],
    u'MARATHON_LB': u'',
    u'MASTER_NODES': [u'172.31.9.5', u'172.31.9.6', u'172.31.9.7'],
    u'DOOM_LB': u'',
    u'USER_REGISTRIES': {
        u'index.alauda.cn': {
            u'username': u'xdzhang', u'token': u'111111'
        },
        u'index.alauda.io': {
            u'username': u'xdzhang', u'token': u'029dcd3cb16d4d4934815c995301f25b8683cc89'
        }
    },
    u'OTHERS_NODES': [u'172.31.3.3', u'172.31.4.4']
}

VERSIONS = {
    u'haproxy': {
        u'version': u'this-is-haporxy-version',
        u'description': 'this_is_haproxy',
        u'incarnation': 1
    },
    u'meepo': {
        u'version': u'this-is-meepo-version',
        u'description': 'this_is_meepo',
        u'incarnation': 1
    },
    u'puck': {
        u'version': u'this-is-puck-version',
        u'description': 'this_is_puck',
        u'incarnation': 1
    },
    u'mesos_slave': {
        u'version': u'this-is-mesos-slave-version',
        u'description': 'this_is_mesos_slave',
        u'incarnation': 1
    },
    u'dd_agent': {
        u'version': u'this-is-ddagent-version',
        u'description': 'this_is_ddagent',
        u'incarnation': 1
    },
    u'zookeeper': {
        u'version': u'this-is-zookeeper-version',
        u'description': 'this_is_zookeeper',
        u'incarnation': 1
    },
    u'nevermore': {
        u'version': u'this-is-nevermore-version',
        u'description': 'this_is_nevermore',
        u'incarnation': 1
    },
    u'docker_daemon_proxy': {
        u'version': u'this-is-docker-daemon-proxy-version',
        u'description': 'this_is_docker_daemon_proxy',
        u'incarnation': 1
    },
    u'doom': {
        u'version': u'this-is-doom-version',
        u'description': 'this_is_doom',
        u'incarnation': 1
    },
    u'mesos_master': {
        u'version': u'this-is-mesos-master-version',
        u'description': 'this_is_mesos_master',
        u'incarnation': 1
    },
    u'marathon': {
        u'version': u'this-is-marathon-version',
        u'description': 'this_is_marathon',
        u'incarnation': 1
    }
}

EMPTY_REGION = {
    "namespace": "ut",
    "name": "all_test",
    "display_name": "all_test",
    "container_manager": "NONE",
    "features": {
    },
    "attr": {
        "cloud": {
            "name": "PRIVATE"
        },
        "docker": {
            "version": "1.12.6",
            "path": "/var/lib/docker"
        }
    },
    "nic": 'eth1'
}

AWS_EMPTY = {
    "namespace": "ut",
    "name": "aws_test",
    "display_name": "aws_test",
    "container_manager": "NONE",
    "features": {
        "node": {
            "instance": {
                "access_key": "AKIAOZQYQY4L4C2CLA4Q",
                "subnet_id": "subnet-e870638a",
                "secret_access_key": "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",
                "ssh_key": "alaudacn",
                "image_id": "ami-0220b23b",
                "security_groups": [
                    "sg-45126120"
                ]
            },
            "features": [
                "instance"
            ]
        },
        "service": {
            "elb": {
                "access_key": "AKIAOZQYQY4L4C2CLA4Q",
                "secret_access_key": "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",
                "subnets": ["subnet-e870638a"],
                "security_groups": ["sg-73a8b011", "sg-45126120"]
            },
            "features": ["elb"]
        }
    },
    "attr": {
        "cloud": {
            "region_id": "cn-north-1",
            "name": "aws"
        },
        "docker": {
            "version": "1.12.6",
            "path": "/var/lib/docker"
        },
        "nic": "eth1"
    }
}

ACMP_REGION = {
    "IMAGE_LOCATION": "IO",
    "HAPROXY_NODE": {
        "public_ip": "52.42.110.41",
        "private_ip": "172.31.13.34"
    },
    "CLOUD_LB_SETTINGS": {
        "elb": {
            "access_key": "AKIAJB4NAHXKHG2BI5QA",
            "subnets": [
                "subnet-a2779cd5"
            ],
            "secret_access_key": "B6Gs65z61kpYCLKI26/Qf2u0b9egbudX+mkzpoWS",
            "security_groups": [
                "sg-4955ac2d"
            ]
        }
    },
    "SLAVE_NODES": [
        "172.31.13.36",
        "172.31.13.37",
        "172.31.13.38"
    ],
    "USER_SETTINGS": {
        "JAKIRO_AUTH": {
            "USERNAME": "xdzhang",
            "TOKEN": "b7beb6ce29bf89f3f0abaa18dda69d93dd9989f3"
        },
        "REGION": {
            "CLOUD_NAME": "aws",
            "SLAVE_USERNAME": "ubuntu",
            "DISPLAY_NAME": "show-phoenix-poc",
            "NAME": "unique_phoenix",
            "OVER_COMMIT": {
                "CPU": 1,
                "MEMORY": 1
            },
            "NODE_LOCATION": "us-west-2",
            "NAMESPACE": "claasv1",
            "FEATURES": [
                "host-network",
                "elb",
                "exec",
                "raw-container",
                "haproxy",
                "application",
                "file-log"
            ]
        },
        "PUCK_AUTH": {
            "USERNAME": "puck_username",
            "PASSWORD": "puck_password"
        }
    },
    "REGION_LOCATION": "INT",
    "MASTER_NODES": {
        "public_ip": "52.27.197.247",
        "private_ip": "172.31.13.35"
    },
    "USER_REGISTRIES": {
        "index.alauda.cn": {
            "username": "xdzhang",
            "token": "111111"
        },
        "index.alauda.io": {
            "username": "xdzhang",
            "token": "029dcd3cb16d4d4934815c995301f25b8683cc89"
        }
    }
}

ACMP_SWARM_REGION = {
    "IMAGE_LOCATION": "IO",
    "HAPROXY_NODE": {
        "public_ip": "52.42.110.41",
        "private_ip": "172.31.13.34"
    },
    "CLOUD_LB_SETTINGS": {
        "elb": {
            "access_key": "AKIAJB4NAHXKHG2BI5QA",
            "subnets": [
                "subnet-a2779cd5"
            ],
            "secret_access_key": "B6Gs65z61kpYCLKI26/Qf2u0b9egbudX+mkzpoWS",
            "security_groups": [
                "sg-4955ac2d"
            ]
        }
    },
    "SWARM_WORKER_NODES": [
        "172.31.13.36",
        "172.31.13.37",
        "172.31.13.38"
    ],
    "USER_SETTINGS": {
        "JAKIRO_AUTH": {
            "USERNAME": "xdzhang",
            "TOKEN": "b7beb6ce29bf89f3f0abaa18dda69d93dd9989f3"
        },
        "REGION": {
            "CLOUD_NAME": "aws",
            "SLAVE_USERNAME": "ubuntu",
            "DISPLAY_NAME": "show-phoenix-poc",
            "NAME": "unique_phoenix",
            "OVER_COMMIT": {
                "CPU": 1,
                "MEMORY": 1
            },
            "NODE_LOCATION": "us-west-2",
            "NAMESPACE": "claasv1",
            "FEATURES": [
                "host-network",
                "elb",
                "exec",
                "raw-container",
                "haproxy",
                "application",
                "file-log"
            ]
        },
        "PUCK_AUTH": {
            "USERNAME": "puck_username",
            "PASSWORD": "puck_password"
        }
    },
    "REGION_LOCATION": "INT",
    "SWARM_INIT_NODE": "172.31.13.35",
    "SWARM_MANAGER_NODES": [],
    "USER_REGISTRIES": {
        "index.alauda.cn": {
            "username": "xdzhang",
            "token": "111111"
        },
        "index.alauda.io": {
            "username": "xdzhang",
            "token": "029dcd3cb16d4d4934815c995301f25b8683cc89"
        }
    }
}


CLOUD_FEATURES = {
    u'service':
        {
            u'elb': {
                u'access_key': u'AKIAJB4NAHXKHG2BI5QA',
                u'subnets': [u'subnet-a2779cd5'],
                u'secret_access_key': u'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',
                u'security_groups': [u'sg-4955ac2d']},
            u'features': [u'elb']
        }
}


IPS = {
    'controller': ['1.1.1.1', '2.2.2.2'],
    'mesos-slave': ['3.3.3.3', '4.4.4.4']
}

ENABLE_REGISTRY = {
    "node": "10.1.0.24",
    "registry_settings": {
        "storage": "local",
        "name": "enable_registry3",
        "local_hostdir": "/home/ubuntu",
        "local_rootdir": "/root/"
    },
    'root_token': 'aaa'
}

ENABLE_HAPROXY = {
    "ip": {
        "public_ip": "123.456.1.1",
        "private_ip": "10.1.0.26"
    }
}

ENABLE_INTERNAL_HAPROXY = {
    "ip": "10.1.0.23"
}

ENABLE_EXEC = {
    "node": {
        "private_ip": "10.1.0.26",
        "public_ip": "111.222.333.444"
    }
}


class R(object):
        def __init__(self, code=status.HTTP_204_NO_CONTENT):
            self.status_code = code
