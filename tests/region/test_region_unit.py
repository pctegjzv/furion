from rest_framework.test import APITestCase
from controls.region import _xcode_region_info
from util.func import _prefix


class XcodeRegionInfoTest(APITestCase):

    def setUp(self):
        self.features = {
            "service": {
                "features": [
                    "elb"
                ],
                "elb": {
                    "access_key": "*******",
                    "secret_access_key": "*******",
                },
                "manager": {
                    "password": "hehehehe"
                },
            }
        }
        self.attr = {
            "mesos": {
                "auth": {
                    "token": "*******",
                    "log_token": "*******"
                },
            }
        }

    def test_elb(self):
        features = self.features.copy()
        attr = self.attr.copy()
        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, features['service']['elb']['access_key'])
        self.assertIn(_prefix, features['service']['elb']['secret_access_key'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.features, features)

    def test_slb(self):
        attr = self.attr.copy()

        self.features['service'].pop('elb')
        self.features['service']['slb'] = {
            "access_key": "NHyfR3LQaRTg8yLw",
            "secret_access_key": "xtGc35jUr4nvkWYUIlBbmT7wZzV5OH"
        }
        self.features['service']['features'] = ['slb']
        features = self.features.copy()

        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, features['service']['slb']['access_key'])
        self.assertIn(_prefix, features['service']['slb']['secret_access_key'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.features, features)

    def test_ulb(self):
        attr = self.attr.copy()
        self.features['service'].pop('elb')
        self.features['service']['ulb'] = {
            "public_key": "h/riqxbunGysA3o+pzdTyAVbK/ZhaGhJpGs70RzqLmvtELhqOOWYnA==",
            "private_key": "95ee4a21c6a637488fccc83b9fb5dbe266797a46",
        }
        self.features['service']['features'] = ['ulb']
        features = self.features.copy()
        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, features['service']['ulb']['public_key'])
        self.assertIn(_prefix, features['service']['ulb']['private_key'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.features, features)

    def test_blb(self):
        attr = self.attr.copy()
        self.features['service'].pop('elb')
        self.features['service']['blb'] = {
            "access_key": "NHyfR3LQaRTg8yLw",
            "secret_access_key": "xtGc35jUr4nvkWYUIlBbmT7wZzV5OH"
        }
        self.features['service']['features'] = ['blb']
        features = self.features.copy()
        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, features['service']['blb']['access_key'])
        self.assertIn(_prefix, features['service']['blb']['secret_access_key'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.features, features)

    def test_clb(self):
        attr = self.attr.copy()
        self.features['service'].pop('elb')
        self.features['service']['clb'] = {
            "secret_key": "mQmW3k0cSAKWjg4Yeq9wNYYE9OXwS6Y9",
            "secret_id": "AKID9Wsy27Rvd2fypNpl7irlPh1IoGGv83oE",
        }
        self.features['service']['features'] = ['clb']
        features = self.features.copy()
        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, features['service']['clb']['secret_key'])
        self.assertIn(_prefix, features['service']['clb']['secret_id'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.features, features)

    def test_manager(self):
        attr = self.attr.copy()
        features = self.features.copy()
        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, features['service']['manager']['password'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.features, features)

    def test_auth(self):
        features = self.features.copy()
        attr = self.attr.copy()
        _xcode_region_info('encode', features, attr)
        self.assertIn(_prefix, attr['mesos']['auth']['token'])
        self.assertIn(_prefix, attr['mesos']['auth']['log_token'])
        _xcode_region_info('decode', features, attr)
        self.assertEqual(self.attr, attr)
