import json
from mock.mock import patch
import uuid

from controls.region import RegionControl
from util import const
from app.models import Node, Region, RegionSetting, Label
from Crypto.Cipher import AES
from alauda.common.func import encode_aes, pad_str
from django.conf import settings
from django.utils import timezone
from rest_framework.test import APITestCase, APIClient
from rest_framework import status
from constant import EMPTY_REGION, R, AWS_EMPTY
from cron.sync_node_state import handle_empty_regions
from controls.self_service.service_register import ServiceRegister
import ss_original_data as UT
import copy
from controls.features import EnableChronos, DisableChronos, EnableRegistry, \
    DisableRegistry, EnableInternalhaproxy, DisableInternalhaproxy, EnableHaproxy, \
    DisableHaproxy, EnableExec, DisableExec, get_class


def generate_ssh_keys():
    from Crypto.PublicKey import RSA
    key = RSA.generate(2048)
    private_key, pub_key = key.exportKey('PEM'), key.publickey().exportKey('OpenSSH')
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    return encode_aes(cipher, private_key), encode_aes(cipher, pub_key)


class PermissionsBaseTestCase(APITestCase):
    client = APIClient()
    private_key, pub_key = generate_ssh_keys()

    def setUp(self):
        self.empty_aws = 1
        self.tunnel = {
            'private_key': self.private_key,
            'public_key': self.pub_key,
            'port_mapping': {
                'ssh': "1.1.1.1:1111",
                'puck': "2.2.2.2:2222",
                'tiny': "3.3.3.3:3333",
                'doom': "4.4.4.4:4444",
                'registry': "5.5.5.5:5555",
                'reserved': "6.6.6.6:6666"
            }
        }
        self.nodes = {
            'controller': ['10.1.0.13'],
            'mesos-slave': ['10.1.0.14', '10.1.0.16']
        }
        self.mock_return = R()
        self.old_spectre_version = 2.7
        self.new_spectre_version = 2.8

        self.aws_info = {
            u"features": {
                u"service": {
                    u"elb": {
                        u"access_key": u"AKIAOZQYQY4L4C2CLA4Q",
                        u"secret_access_key": u"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",
                        u"subnets": [u"subnet-e870638a"],
                        u"security_groups": [u"sg-73a8b011", u"sg-45126120"]
                    },
                    u"features": [u"elb"]
                }
            },
            u"cloud": {
                u"region_id": u"cn-north-1",
                u"name": u"aws"
            }
        }

    def update_region_state(self, id, state):
        Region.objects.filter(id=id).update(state=state)

    def create_node(self, region_id, ip, node_type, state):
        data = {
            'region_id': region_id,
            'private_ip': ip,
            'type': node_type,
            'state': state
        }
        node = Node.objects.create(**data)
        return node

    def get_swarm_basic_data(self):
        data = copy.deepcopy(UT.BASE_REGION)
        data['container_manager'] = 'SWARM'
        data['features'].append('haproxy')
        data['haproxy'] = copy.deepcopy(UT.HAPROXY)
        return data

    @patch('util.phoenix.EnvironmentManager.create')
    def create_empty_region(self, mock_create, region_name=None, region_data=None):
        mock_create.return_value = str(uuid.uuid4())
        url = '/v1/regions'
        if region_data:
            data = region_data
        else:
            if self.empty_aws:
                data = copy.deepcopy(AWS_EMPTY)
            else:
                data = copy.deepcopy(EMPTY_REGION)
        if region_name:
            data['name'] = region_name
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 200)
        rep = json.loads(response.content)
        return rep

    @patch("util.phoenix.EnvironmentManager.get_spectre_version")
    @patch('util.phoenix.RegionManager.create')
    @patch('util.phoenix.NodeManager.update_type')
    @patch('controls.self_service.transverter_base.generate_tunnel_info')
    def create_acmp_region(self, region_id, data, mock_generate_tunnel_info,
                           mock_update_type, mock_create, get_spectre_version):
        get_spectre_version.return_value = 2.8
        mock_generate_tunnel_info.return_value = self.tunnel
        mock_create.return_value = self.nodes
        url = '/v1/regions/{}'.format(region_id)
        data['container_manager'] = 'MESOS'
        data['template_type'] = 'poc'
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 204)

    @patch.object(ServiceRegister, 'check')
    @patch.object(ServiceRegister, 'do_request')
    @patch("util.phoenix.EnvironmentManager.get_spectre_version")
    @patch('util.phoenix.RegionManager.create')
    @patch('util.phoenix.NodeManager.update_type')
    @patch('controls.self_service.transverter_base.generate_tunnel_info')
    def create_swarm_acmp_region(self, region_id, data, mock_generate_tunnel_info,
                                 mock_update_node_type, mock_create, mock_get_spectre_version,
                                 mock_do_request, mock_check):
        mock_check.return_value = True
        mock_do_request.return_value = R(status.HTTP_201_CREATED)
        mock_generate_tunnel_info.return_value = self.tunnel
        mock_create.return_value = self.nodes
        url = '/v1/regions/{}'.format(region_id)
        data['container_manager'] = 'SWARM'
        data['template_type'] = 'swarm'
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, 204)

    @patch('util.phoenix.EnvironmentManager.get_spectre_version')
    @patch('util.phoenix.NodeManager.list_nodes')
    @patch('util.phoenix.ResourceManager.clean_up')
    @patch.object(ServiceRegister, 'remove_private_build')
    @patch.object(ServiceRegister, 'remove_registry')
    @patch.object(ServiceRegister, 'remove_alb')
    def test_clean_up_region(self, remove_alb, remove_registry, remove_private_build,
                             mock_resource_cleanup, mock_list_nodes, mock_retrieve):
        mock_retrieve.return_value = self.old_spectre_version
        remove_alb.return_value = self.mock_return
        remove_registry.return_value = self.mock_return
        remove_private_build.return_value = self.mock_return
        mock_list_nodes.return_value = []
        self.maxDiff = None
        result = self.create_empty_region()
        node = Node.objects.create(
            id=str(uuid.uuid4()),
            private_ip='1.2.3.4',
            region_id=result['id'],
            attr={
                "components": {}
            },
            resources={
                "cpu": 1,
                "mem": 2000000000
            }
        )
        Label.objects.create(
            key='a',
            value='a',
            editable=True,
            node=node
        )
        self.create_acmp_region(result['id'], UT.BASE_REGION)
        data = {
            'container_manager': 'NONE',
            'root_token': 'aaa'
        }
        url = '/v1/regions/{}'.format(result['id'])
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, 204)
        response = self.client.get(url)
        result = json.loads(response.content)
        if self.empty_aws:
            self.assertEqual(result['attr']['cloud'],
                             {u'name': u'AWS', u'region_id': u'cn-north-1'})
            self.assertEqual(result['attr']['nic'], u'eth1')
            self.assertEqual(result['features']['service'], self.aws_info['features']['service'])
            self.assertIn('node', result['features'])
        else:
            self.assertEqual(result['attr']['cloud'], {u'name': u'PRIVATE', u'region_id': None})
            self.assertEqual(result['features'], {"service": {"features": []}})
        self.assertEqual(result['attr']['docker'], {u'version': u'1.12.6',
                                                    u'path': '/var/lib/docker'})
        self.assertEqual(result['attr']['nic'], u'eth1')
        self.assertEqual(result['container_manager'], 'NONE')

        self.assertEqual(RegionControl.get_labels(region_id=result['id'])['labels'], [])
        return result

    def test_update_region_with_wrong_type(self):
        self.maxDiff = None
        result = self.create_empty_region()
        url = '/v1/regions/{}'.format(result['id'])
        data = {'container_manager': 'WRONG_TYPE'}
        response = self.client.put(url, data, format='json')
        ret = json.loads(response.content)
        self.assertEqual(ret['errors'][0]['code'], 'invalid_args')

    def test_create_empty_region(self):
        self.maxDiff = None
        self.create_empty_region()

    @patch('util.phoenix.EnvironmentManager.create')
    def test_create_empty_region_with_invalid_data(self, mock_create):
        mock_create.return_value = str(uuid.uuid4())
        url = '/v1/regions'
        data = copy.deepcopy(AWS_EMPTY)
        data['attr']['docker']['version'] = '1.10'
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 400)
        data['attr']['docker']['version'] = '1.12.6'
        del data['features']['service']['elb']['access_key']
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 400)
        data = copy.deepcopy(AWS_EMPTY)
        data['features']['service']['features'].insert(0, 'ulb')
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 400)
        data['features']['service']['ulb'] = {"bandwidth": 0}
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 400)
        data = copy.deepcopy(AWS_EMPTY)
        data['attr']['docker']['path'] = '/var/lib /docker'
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_create_acmp_region(self):
        self.maxDiff = None
        result = self.create_empty_region()
        self.create_acmp_region(result['id'], UT.BASE_REGION)
        url = '/v1/regions/{}'.format(result['id'])
        response = self.client.get(url)
        data = json.loads(response.content)
        self.assertEqual(data['container_manager'], 'MESOS')
        self.assertIn('mesos_endpoint', data['attr'].keys())
        self.assertIn('cloud', data['attr'].keys())
        self.assertIn('features', data['features']['service'].keys())
        nodes = Node.objects.filter(**{'region_id': result['id']})
        self.assertEqual(len(nodes), 3)
        return result

    def test_valid_acmp_region_settings(self):
        self.maxDiff = None
        aws_region = copy.deepcopy(EMPTY_REGION)
        aws_region['features'] = self.aws_info['features']
        aws_region['attr']['cloud'] = self.aws_info['cloud']
        result = self.create_empty_region(region_data=aws_region)
        self.create_acmp_region(result['id'], UT.BASE_REGION)
        region_settings = RegionSetting.objects.filter(**{'region_id': result['id']}).first()
        cluster_settings = region_settings.cluster_settings
        self.assertEqual(self.aws_info['features']['service']['elb'],
                         cluster_settings['CLOUD_LB_SETTINGS']['elb'])

    def test_create_swarm_acmp_region(self):
        self.maxDiff = None
        result = self.create_empty_region()
        swarm_settings = self.get_swarm_basic_data()
        self.create_swarm_acmp_region(result['id'], swarm_settings)
        url = '/v1/regions/{}'.format(result['id'])
        response = self.client.get(url)
        data = json.loads(response.content)
        self.assertEqual(data['container_manager'], 'SWARM')
        self.assertIn('mesos_endpoint', data['attr'].keys())
        self.assertIn('cloud', data['attr'].keys())
        self.assertIn('features', data['features']['service'].keys())
        nodes = Node.objects.filter(**{'region_id': result['id']})
        self.assertEqual(len(nodes), 3)
        return result

    def test_delete_acmp_region(self):
        self.maxDiff = None
        result = self.create_empty_region()
        self.create_acmp_region(result['id'], UT.BASE_REGION)
        url = '/v1/regions/{}'.format(result['id'])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(json.loads(response.content)['errors'][0]['code'], 'not_support')
        return result

    @patch('util.phoenix.NodeManager.list_nodes')
    @patch("util.phoenix.EnvironmentManager.get_spectre_version")
    @patch('util.phoenix.ResourceManager.clean_up')
    @patch('util.phoenix.NodeManager.delete')
    @patch('util.phoenix.NodeManager.clean_up')
    @patch('util.phoenix.EnvironmentManager.delete')
    def test_delete_empty_region(self, mock_delete, mock_cleanup, mock_node_delete,
                                 mock_resource_clean_up, mock_get_spectre_version,
                                 mock_list_nodes):
        mock_list_nodes.return_value = []
        mock_get_spectre_version.return_value = 2.8
        result = self.test_clean_up_region()
        url = '/v1/regions/{}'.format(result['id'])
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 204)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_list_regions(self):
        self.maxDiff = None
        acmp_region = self.create_empty_region()
        self.create_acmp_region(acmp_region['id'], UT.BASE_REGION)
        empty_region = self.create_empty_region(region_name='ep')
        url = '/v1/regions'
        response = self.client.get(url)
        regions = json.loads(response.content)
        region_ids = [x['id'] for x in regions]
        self.assertEqual(len(regions), 2)
        self.assertIn(acmp_region['id'], region_ids)
        self.assertIn(empty_region['id'], region_ids)
        for region in regions:
            if region['id'] == empty_region['id']:
                self.assertEqual(region['container_manager'], 'NONE')
            if region['id'] == acmp_region['id']:
                self.assertEqual(region['container_manager'], 'MESOS')

        url += '?container_manager=NONE'
        response = self.client.get(url)
        empty_regions = json.loads(response.content)
        self.assertEqual(len(empty_regions), 1)

    def valid_region_feature(self, id, feature, in_or_not):
        url = '/v1/regions/{}'.format(id)
        response = self.client.get(url)
        region_data = json.loads(response.content)
        if in_or_not:
            self.assertIn(feature, region_data['features']['service']['features'])
        else:
            self.assertNotIn(feature, region_data['features']['service']['features'])
        return region_data

    def get_region_setting(self, id):
        url = '/v1/regions/{}/settings/region'.format(id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        return json.loads(response.content)

    def create_region(self, ip):
        acmp_region = self.create_empty_region()
        self.create_acmp_region(acmp_region['id'], UT.BASE_REGION)
        self.update_region_state(acmp_region['id'], const.STATE_RUNNING)
        self.create_node(acmp_region['id'], ip, const.NODE_TYPE_EMPTY, const.STATE_RUNNING)
        return acmp_region['id']

    def is_in_user_settings(self, feature, component, region_setting_data, in_or_not):
        if in_or_not:
            self.assertIn(
                feature,
                region_setting_data['user_settings']['USER_SETTINGS']['REGION']['FEATURES']
            )
            self.assertIn(component, region_setting_data['user_settings']['COMPONENT'])
        else:
            self.assertNotIn(
                feature,
                region_setting_data['user_settings']['USER_SETTINGS']['REGION']['FEATURES']
            )
            self.assertNotIn(component, region_setting_data['user_settings']['COMPONENT'])

    def test_get_class(self):
        self.assertEqual(get_class('Enable', 'exec'), EnableExec)
        self.assertEqual(get_class('Enable', 'chronos'), EnableChronos)
        self.assertEqual(get_class('Enable', 'registry'), EnableRegistry)
        self.assertEqual(get_class('Enable', 'haproxy'), EnableHaproxy)
        self.assertEqual(get_class('Enable', 'internal-haproxy'), EnableInternalhaproxy)
        self.assertEqual(get_class('Disable', 'exec'), DisableExec)
        self.assertEqual(get_class('Disable', 'chronos'), DisableChronos)
        self.assertEqual(get_class('Disable', 'registry'), DisableRegistry)
        self.assertEqual(get_class('Disable', 'haproxy'), DisableHaproxy)
        self.assertEqual(get_class('Disable', 'internal-haproxy'), DisableInternalhaproxy)

    def test_region_state_change(self):
        region_data = self.create_empty_region(region_name='test_region_1')
        node = self.create_node(region_data['id'],
                                '10.1.1.2',
                                const.NODE_TYPE_EMPTY,
                                const.STATE_ERROR)
        Region.objects.filter(id=region_data['id']).update(updated_at=timezone.now(),
                                                           state=const.STATE_ERROR)
        handle_empty_regions()
        empty_region = Region.objects.filter(id=region_data['id']).first()
        self.assertEqual(empty_region.state, const.STATE_ERROR)
        node.delete()
        handle_empty_regions()
        empty_region = Region.objects.filter(id=region_data['id']).first()
        self.assertEqual(empty_region.state, const.STATE_RUNNING)
