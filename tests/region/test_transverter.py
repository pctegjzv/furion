import unittest
from app.models import Region
import uuid
from util import const
from tests.region.constant import R
from controls.self_service.transverter import transverter
from controls.region import RegionControl
from rest_framework import status
import ss_original_data as UT
import copy
import json
from requests import models
from mock import patch
from alauda.alchemist.cluster_templates import ClusterTemplate
from controls.self_service.service_register import ServiceRegister
from furion.exception import FurionException
from util.phoenix import EnvironmentManager


class TestTransverter(unittest.TestCase):
    def setUp(self):
        self.region_features = {
            "tunnel": {
                "public_key": "public_key",
                "private_key": "private_key",
                "port_mapping": {
                    "reserved": "10.162.192.134:31039",
                    "puck": "10.162.192.134:31035",
                    "tiny": "10.162.192.134:31036",
                    "ssh": "182.92.0.148:30007",
                    "doom": "10.162.192.134:31037",
                    "registry": "10.162.192.134:31038"
                }
            },
            "service": {
                "features": []
            }
        }
        self.aws_region_attr = {
            "docker": {
                "path": "/var/lib/docker",
                "version": "1.12.6"
            },
            "cloud": {
                "name": "AWS",
                "region_id": "cn-north-1"
            },
            'nic': 'eth1'
        }
        self.aws_region_features = {
            "node": {
                "instance": {
                    "access_key": "__encrypted:vWYGZkGjeYJSPn0HfRywFjgoTSStUcqusxjJZscJ5ks=",
                    "image_id": "ffff",
                    "subnet_id": "dddd",
                    "secret_access_key": "__encrypted:lt2zF11gqu4UZkge2BZagDgoTSStUcqusxjJZscJ5ks=",
                    "security_groups": [
                        "eeee"
                    ]
                },
                "features": [
                    "instance",
                    "auto-scaling"
                ]
            },
            "service": {
                "elb": {
                    "access_key": "__encrypted:vWYGZkGjeYJSPn0HfRywFjgoTSStUcqusxjJZscJ5ks=",
                    "subnets": [
                        "dddd"
                    ],
                    "secret_access_key": "__encrypted:lt2zF11gqu4UZkge2BZagDgoTSStUcqusxjJZscJ5ks=",
                    "security_groups": [
                        "eeee"
                    ]
                },
                "features": [
                    "elb"
                ]
            }
        }
        self.lbs = {
            'puck_public_lb': 'http://puck:8080',
            'puck_internal_lb': 'http://puck:8080',
            'marathon_lb': 'http://marathon:8081',
            'doom_lb': 'doom:4022'
        }
        self.region_attr = {
            "docker": {
                "path": "/var/lib/docker",
                "version": "1.12.6"
            },
            "cloud": {
                "name": "private"
            }
        }
        self.template = ClusterTemplate()

        self.swarm_illegal_features = ['alb', 'pipeline', 'chronos']

        self.tunnel = {
            'private_key': '1.1.1.1',
            'public_key': '2.2.2.2',
            'port_mapping': {
                'ssh': '1.1.1.1:10001',
                'puck': '1.1.1.1:10002',
                'tiny': '1.1.1.1:10003',
                'doom': '1.1.1.1:10004',
                'registry': '1.1.1.1:10005',
                'reserved': '1.1.1.1:10006',
            }
        }

        self.env_uuid = '9D255D3E-904F-40D5-8B67-7AFD58461077'

    @patch.object(EnvironmentManager, 'create')
    def create_private_empty(self, create):
        create.return_value = self.env_uuid
        region = {
            'name': 'UT',
            'state': 'RUNNING',
            'features': self.region_features,
            'display_name': 'UT',
            'attr': self.region_attr,
            'namespace': 'ut_test',
            'env_uuid': str(uuid.uuid4()),
            'container_manager': const.CONTAINER_MANAGER_NONE
        }
        obj = RegionControl.save(region)
        return obj

    @patch.object(EnvironmentManager, 'create')
    def create_aws_empty(self, create):
        create.return_value = self.env_uuid
        region = {
            'name': 'UT',
            'state': 'RUNNING',
            'features': self.aws_region_features,
            'display_name': 'UT',
            'attr': self.aws_region_attr,
            'namespace': 'ut_test',
            'env_uuid': str(uuid.uuid4()),
            'container_manager': const.CONTAINER_MANAGER_NONE
        }

        obj = RegionControl.save(region)
        return obj

    def get_poc_basic_data(self):
        data = copy.deepcopy(UT.BASE_REGION)
        data['features'].append('haproxy')
        return data

    def get_poc_with_internal_haproxy(self):
        data = self.get_poc_basic_data()
        data['features'].append('internal-haproxy')
        data['internal-haproxy'] = UT.INTERNAL_HAPROXY
        return data

    def get_poc_with_registry(self):
        data = self.get_poc_with_internal_haproxy()
        data['features'].append('registry')
        data['registry'] = copy.deepcopy(UT.REGISTRY)
        return data

    def get_poc_with_chronos(self):
        data = self.get_poc_with_registry()
        data['features'].append('chronos')
        return data

    def get_poc_with_all_volume(self):
        data = self.get_poc_with_registry()
        data['features'].append('volume')
        data['features'].append('glusterfs')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        return data

    def get_poc_with_local_volume(self):
        data = self.get_poc_with_registry()
        data['features'].append('volume')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        del data['volume']['volume_settings']['glusterfs']
        del data['volume']['volume_settings']['ebs']
        return data

    def get_poc_with_ebs_volume(self):
        data = self.get_poc_with_all_volume()
        del data['volume']['volume_settings']['glusterfs']
        data['features'].remove('glusterfs')
        return data

    def get_poc_with_glusterfs_volume(self):
        data = self.get_poc_with_all_volume()
        del data['volume']['volume_settings']['ebs']
        return data

    def get_poc_alb(self):
        data = self.get_poc_with_glusterfs_volume()
        data['features'].append('alb')
        return data

    def get_poc_without_tunnel(self):
        data = self.get_poc_alb()
        del data['']

    def get_valid_data_invalid_registry_path(self):
        data = self.get_poc_with_local_volume()
        data['registry']['registry_settings']['local_hostdir'] = '/var/ lib/123'
        return data

    def get_prod_basic_data(self):
        data = copy.deepcopy(UT.BASE_REGION)
        master = copy.deepcopy(UT.PROD_MASTER)
        data['master'] = master
        data['template_type'] = 'prod'
        data.update(UT.CONTROLLER_HAPROXY)
        return data

    def get_prod_with_internal_haproxy(self):
        data = self.get_prod_basic_data()
        data['features'].append('internal-haproxy')
        data['internal-haproxy'] = UT.INTERNAL_HAPROXY
        return data

    def get_prod_with_compatible_internal_haproxy(self):
        data = self.get_prod_basic_data()
        data['features'].append('internal-haproxy')
        data['internal-haproxy'] = UT.INTERNAL_HAPROXY_HA
        return data

    def get_prod_data_with_haproxy_exec(self):
        data = self.get_prod_with_internal_haproxy()
        data['features'].append('haproxy')
        data['features'].append('exec')
        data['doom'] = copy.deepcopy(UT.DOOM_NODE)
        data['haproxy'] = copy.deepcopy(UT.HAPROXY)
        return data

    def get_prod_data_with_compatible_haproxy(self):
        data = self.get_prod_with_compatible_internal_haproxy()
        data['features'].append('haproxy')
        data['features'].append('exec')
        data['doom'] = copy.deepcopy(UT.DOOM_NODE)
        data['haproxy'] = copy.deepcopy(UT.HAPROXY_HA)
        return data

    def get_prod_with_registry(self):
        data = self.get_prod_data_with_compatible_haproxy()
        data['features'].append('registry')
        data['registry'] = copy.deepcopy(UT.REGISTRY)
        return data

    def get_prod_with_chronos(self):
        data = self.get_prod_with_registry()
        data['features'].append('chronos')
        return data

    def get_prod_with_all_volume(self):
        data = self.get_prod_with_chronos()
        data['features'].append('volume')
        data['features'].append('glusterfs')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        return data

    def get_prod_with_local_volume(self):
        data = self.get_prod_with_registry()
        data['features'].append('volume')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        del data['volume']['volume_settings']['glusterfs']
        del data['volume']['volume_settings']['ebs']
        return data

    def get_prod_with_ebs_volume(self):
        data = self.get_prod_with_all_volume()
        del data['volume']['volume_settings']['glusterfs']
        data['features'].remove('glusterfs')
        return data

    def get_prod_with_glusterfs_volume(self):
        data = self.get_prod_with_all_volume()
        del data['volume']['volume_settings']['ebs']
        return data

    def get_prod_alb(self):
        data = self.get_prod_with_glusterfs_volume()
        data['features'].append('alb')
        return data

    def get_prod_with_compatible_registry(self):
        data = self.get_prod_alb()
        data['registry']['node'] = ['10.2.0.1', '10.2.0.2']
        return data

    def get_prod_with_customer_lbs(self):
        data = self.get_prod_with_compatible_registry()
        del data['controller_haproxy']
        data['customer_lbs'] = UT.MESOS_CUSTOMER_LBS
        return data

    def get_swarm_basic_data(self):
        data = copy.deepcopy(UT.BASE_REGION)
        data['container_manager'] = 'SWARM'
        data['features'].append('haproxy')
        data['haproxy'] = copy.deepcopy(UT.HAPROXY)
        return data

    def get_swarm_with_manager(self):
        data = self.get_swarm_basic_data()
        master = copy.deepcopy(UT.PROD_MASTER)
        data['master'] = master
        return data

    def get_swarm_with_haproxy(self):
        data = self.get_swarm_with_manager()
        data['features'].append('haproxy')
        data['haproxy'] = copy.deepcopy(UT.HAPROXY)
        return data

    def get_swarm_with_registry(self):
        data = self.get_swarm_with_haproxy()
        data['features'].append('registry')
        data['registry'] = copy.deepcopy(UT.REGISTRY)
        return data

    def get_swarm_with_local_volume(self):
        data = self.get_swarm_with_registry()
        data['features'].append('volume')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        del data['volume']['volume_settings']['glusterfs']
        del data['volume']['volume_settings']['ebs']
        return data

    def get_swarm_with_ebs_volume(self):
        data = self.get_swarm_with_registry()
        data['features'].append('volume')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        del data['volume']['volume_settings']['glusterfs']
        return data

    def get_swarm_with_glusterfs_volume(self):
        data = self.get_swarm_with_registry()
        data['features'].append('volume')
        data['features'].append('glusterfs')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        del data['volume']['volume_settings']['ebs']
        return data

    def get_swarm_with_all_volume(self):
        data = self.get_swarm_with_registry()
        data['features'].append('volume')
        data['features'].append('glusterfs')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        return data

    def get_k8s_poc_basic(self):
        data = copy.deepcopy(UT.BASE_REGION)
        data['container_manager'] = 'KUBERNETES'
        data['template_type'] = 'kubernetes_poc'
        data['features'].append(UT.K8S_CNI_FLANNEL['type'])
        data['cni'] = copy.deepcopy(UT.K8S_CNI_FLANNEL)
        data['cidr'] = UT.K8S_CIDR
        data['haproxy'] = UT.HAPROXY
        data['platform_version'] = 'v2'
        return data

    def get_k8s_poc_with_flannel_hostgw(self):
        data = self.get_k8s_poc_basic()
        data['cni']['flannel']['type'] = 'host-gw'
        return data

    def get_k8s_poc_with_macvlan(self):
        data = self.get_k8s_poc_basic()
        data['features'] = []
        data['features'].append(UT.K8S_CNI_MACVLAN['type'])
        data['cni'] = copy.deepcopy(UT.K8S_CNI_MACVLAN)
        return data

    def get_k8s_poc_with_calico(self):
        data = self.get_k8s_poc_basic()
        data['features'] = []
        data['features'].append(UT.K8S_CNI_CALICO['type'])
        data['cni'] = copy.deepcopy(UT.K8S_CNI_CALICO)
        return data

    def get_k8s_poc_with_registry(self):
        data = self.get_k8s_poc_with_macvlan()
        data['features'].append('registry')
        data['registry'] = copy.deepcopy(UT.REGISTRY)
        return data

    def get_k8s_poc_with_alb(self):
        data = self.get_k8s_poc_with_registry()
        data['features'].append('alb')
        data['features'].append('haproxy')
        data['features'].append('elb')
        return data

    def get_k8s_poc_with_all_volume(self):
        data = self.get_k8s_poc_with_alb()
        data['features'].append('volume')
        data['features'].append('glusterfs')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        data['volume']['volume_settings']['ebs']['zone'] = 'cn-north-1a'
        return data

    def get_k8s_poc_with_ebs_volume(self):
        data = self.get_k8s_poc_with_all_volume()
        del data['volume']['volume_settings']['glusterfs']
        data['features'].remove('glusterfs')
        return data

    def get_k8s_poc_with_glusterfs_volume(self):
        data = self.get_k8s_poc_with_all_volume()
        del data['volume']['volume_settings']['ebs']
        return data

    def get_k8s_poc_with_doom(self):
        data = self.get_k8s_poc_with_glusterfs_volume()
        data['features'].append('exec')
        return data

    def get_k8s_prod_basic(self):
        data = copy.deepcopy(UT.BASE_REGION)
        data['container_manager'] = 'KUBERNETES'
        master = copy.deepcopy(UT.PROD_MASTER)
        data['master'] = master
        data.update(UT.CONTROLLER_HAPROXY)
        data['template_type'] = 'kubernetes_prod'
        data['features'].append(UT.K8S_CNI_FLANNEL['type'])
        data['cni'] = copy.deepcopy(UT.K8S_CNI_FLANNEL)
        data['cidr'] = UT.K8S_CIDR
        data['haproxy'] = UT.HAPROXY
        data['platform_version'] = 'v2'
        return data

    def get_k8s_prod_with_hostgw(self):
        data = self.get_k8s_prod_basic()
        data['cni']['flannel']['type'] = 'host-gw'
        return data

    def get_k8s_prod_with_macvlan(self):
        data = self.get_k8s_prod_basic()
        data['features'] = []
        data['features'].append(UT.K8S_CNI_MACVLAN['type'])
        data['cni'] = copy.deepcopy(UT.K8S_CNI_MACVLAN)
        return data

    def get_k8s_prod_with_registry(self):
        data = self.get_k8s_prod_with_macvlan()
        data['features'].append('registry')
        data['registry'] = copy.deepcopy(UT.REGISTRY)
        return data

    def get_k8s_prod_with_alb(self):
        data = self.get_k8s_prod_with_registry()
        data['features'].append('alb')
        data['features'].append('haproxy')
        data['haproxy'] = copy.deepcopy(UT.HAPROXY_HA)
        return data

    def get_k8s_prod_with_all_volume(self):
        data = self.get_k8s_prod_with_alb()
        data['features'].append('volume')
        data['features'].append('glusterfs')
        data['volume'] = copy.deepcopy(UT.VOLUME)
        data['volume']['volume_settings']['ebs']['zone'] = 'cn-north-1a'
        return data

    def get_k8s_prod_with_ebs_volume(self):
        data = self.get_k8s_prod_with_all_volume()
        del data['volume']['volume_settings']['glusterfs']
        data['features'].remove('glusterfs')
        return data

    def get_k8s_prod_with_glusterfs_volume(self):
        data = self.get_k8s_prod_with_all_volume()
        del data['volume']['volume_settings']['ebs']
        return data

    def get_k8s_prod_with_doom(self):
        data = self.get_k8s_prod_with_all_volume()
        data['features'].append('exec')
        return data

    def get_k8s_prod_with_customer_lbs(self):
        data = self.get_k8s_prod_with_doom()
        del data['controller_haproxy']
        data['customer_lbs'] = UT.K8S_CUSTOMER_LBS
        return data

    @patch.object(ServiceRegister, 'check')
    @patch.object(ServiceRegister, 'do_request')
    @patch('controls.self_service.transverter_base.generate_tunnel_info')
    def create_template(self, generate_tunnel_info, do_request,
                        check, data, region, output=False, cst=None):
        check.return_value = True
        do_request.return_value = R(status.HTTP_201_CREATED)
        generate_tunnel_info.return_value = self.tunnel
        obj = transverter(data, region)
        customer_settings = obj.get_result()
        if output:
            print customer_settings
        if cst is not None:
            cst.update(customer_settings)
        template_type = customer_settings['template_type']
        return self.template.create(customer_settings, template_type)

    def get_component(self, resources):
        components = {}
        for resource in resources:
            if resource['resource']['type'] != 'CONTAINERS':
                continue
            components[resource['resource']['identifier']['name']] = resource['nodes']
        return components

    def get_details(self, resources):
        component_details = {}
        for resource in resources:
            if resource['resource']['type'] != 'CONTAINERS':
                continue
            component_details[resource['resource']['identifier']['name']] = \
                resource['resource']['configuration']
        return component_details

    def valid_component(self, component, components):
        self.assertIn(component, components)
        self.assertGreater(len(components[component]), 0)

    def valid_ips(self, src, dest):
        for k, v in src.items():
            self.assertEqual(set(v), set(dest[k]))

    def output(self, resources):
        print json.dumps(resources, indent=4)

    def test_base_poc_region(self):
        # region = self.create_private_empty()
        # data = self.get_poc_basic_data()
        # _, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.13'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        pass

    def test_poc_with_mount_points(self):
        region = self.create_private_empty()
        data = self.get_poc_basic_data()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('xin_agent', components)
        self.valid_component('xin_driver', components)

    def test_poc_with_internal_registry(self):
        # region = self.create_private_empty()
        # data = self.get_poc_with_internal_haproxy()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.13'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('internal_haproxy', components)
        pass

    def test_poc_with_registry(self):
        # region = self.create_private_empty()
        # data = self.get_poc_with_registry()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.15', '10.1.0.13'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('registry', components)
        pass

    def test_poc_with_chronos(self):
        region = self.create_private_empty()
        data = self.get_poc_with_chronos()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        # self.valid_component('chronos-master', components)

    def test_poc_with_all_volume(self):
        # region = self.create_aws_empty()
        # data = self.get_poc_with_all_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.15', '10.1.0.13',
        #                    '10.1.0.22', '10.1.0.21', '10.1.0.20'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_poc_with_local_volume(self):
        # region = self.create_private_empty()
        # data = self.get_poc_with_local_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.15', '10.1.0.13'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.assertNotIn('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_poc_with_ebs_volume(self):
        # region = self.create_aws_empty()
        # data = self.get_poc_with_ebs_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.15', '10.1.0.13'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.assertNotIn('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_poc_with_glusterfs_volume(self):
        # region = self.create_private_empty()
        # data = self.get_poc_with_glusterfs_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.15', '10.1.0.13',
        #                    '10.1.0.22', '10.1.0.21', '10.1.0.20'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_poc_with_alb(self):
        # region = self.create_private_empty()
        # data = self.get_poc_alb()
        # cst = {}
        # resources, ips = self.create_template(data=data, region=region, cst=cst)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.15', '10.1.0.13',
        #                    '10.1.0.22', '10.1.0.21', '10.1.0.20'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('alb-haproxy', components)
        # self.assertNotIn('haproxy', components)
        # self.assertIn('alb', cst['USER_SETTINGS']['REGION']['FEATURES'])
        # self.assertIn('haproxy', cst['USER_SETTINGS']['REGION']['FEATURES'])
        pass

    def test_valid_data_invalid_registry_path(self):
        region = self.create_private_empty()
        data = self.get_valid_data_invalid_registry_path()
        try:
            self.create_template(data=data, region=region)
            self.assertEqual("test_valid_data_invalid_registry_path fail", '')
        except FurionException as e:
            self.assertIn('local_hostdir', e.message)

    def test_base_prod_region(self):
        # region = self.create_private_empty()
        # data = self.get_prod_basic_data()
        # _, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        pass

    def test_prod_with_mount_points(self):
        region = self.create_private_empty()
        data = self.get_prod_basic_data()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('xin_agent', components)
        self.valid_component('xin_driver', components)

    def test_prod_with_haproxy_exec(self):
        # region = self.create_private_empty()
        # data = self.get_prod_data_with_haproxy_exec()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.12', '10.1.0.13',
        #                    '10.1.0.10', '10.1.0.11', '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('haproxy', components)
        # self.valid_component('doom', components)
        pass

    def test_prod_with_compatible_haproxy(self):
        # region = self.create_private_empty()
        # data = self.get_prod_data_with_compatible_haproxy()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.2.0.3', '10.2.0.4', '10.3.0.2', '10.3.0.1',
        #                    '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('haproxy', components)
        # self.valid_component('doom', components)
        pass

    def test_prod_with_internal_haproxy(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_internal_haproxy()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.17', '10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('internal_haproxy', components)
        pass

    def test_prod_with_compatible_internal_haproxy(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_compatible_internal_haproxy()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.12', '10.1.0.10', '10.1.0.11', '10.2.0.4', '10.2.0.3',
        #                    '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('internal_haproxy', components)
        pass

    def test_prod_with_registry(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_registry()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.15', '10.1.0.12', '10.1.0.10',
        #                    '10.1.0.11', '10.2.0.3', '10.2.0.4', '10.3.0.2',
        #                    '10.3.0.1', '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('registry', components)
        # # self.valid_component('chronos-master', components)
        pass

    def test_prod_with_chronos(self):
        region = self.create_private_empty()
        data = self.get_prod_with_chronos()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        # self.valid_component('chronos-master', components)

    def test_prod_with_all_volume(self):
        # region = self.create_aws_empty()
        # data = self.get_prod_with_all_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.15', '10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.2.0.3', '10.2.0.4', '10.1.0.22', '10.1.0.21',
        #                    '10.1.0.20', '10.3.0.2', '10.3.0.1',
        #                    '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_prod_with_local_volume(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_local_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.15', '10.1.0.12', '10.1.0.10',
        #                    '10.1.0.11', '10.2.0.3', '10.2.0.4', '10.3.0.2',
        #                    '10.3.0.1', '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_prod_with_ebs_volume(self):
        # region = self.create_aws_empty()
        # data = self.get_prod_with_ebs_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.15', '10.1.0.12', '10.1.0.10',
        #                    '10.1.0.11', '10.2.0.3', '10.2.0.4', '10.3.0.2',
        #                    '10.3.0.1', '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.assertNotIn('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_prod_with_glusterfs_volume(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_glusterfs_volume()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.1.0.15', '10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.2.0.3', '10.2.0.4', '10.1.0.22', '10.1.0.21', '10.1.0.20',
        #                    '10.3.0.2', '10.3.0.1', '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        # self.valid_component('xin_agent', components)
        pass

    def test_prod_with_alb(self):
        # region = self.create_private_empty()
        # data = self.get_prod_alb()
        # cst = {}
        # resources, ips = self.create_template(data=data, region=region, cst=cst)
        # expect_ips = {
        #     'controller': ['10.1.0.15', '10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.2.0.3', '10.2.0.4', '10.1.0.22', '10.1.0.21',
        #                    '10.1.0.20', '10.3.0.2', '10.3.0.1',
        #                    '10.1.0.24', '10.1.0.23'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('alb-haproxy', components)
        # self.assertNotIn('haproxy', components)
        # self.assertIn('alb', cst['USER_SETTINGS']['REGION']['FEATURES'])
        # self.assertIn('haproxy', cst['USER_SETTINGS']['REGION']['FEATURES'])
        pass

    def test_prod_with_compatible_registry(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_compatible_registry()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.2.0.4', '10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.2.0.3', '10.1.0.24', '10.1.0.23', '10.1.0.22', '10.1.0.21',
        #                    '10.1.0.20', '10.3.0.2', '10.2.0.2', '10.2.0.1', '10.3.0.1'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('registry', components)
        # # self.valid_component('chronos-master', components)
        pass

    def test_prod_with_customer_lbs(self):
        # region = self.create_private_empty()
        # data = self.get_prod_with_customer_lbs()
        # resources, ips = self.create_template(data=data, region=region)
        # expect_ips = {
        #     'controller': ['10.2.0.4', '10.1.0.12', '10.1.0.10', '10.1.0.11',
        #                    '10.2.0.3', '10.1.0.22', '10.1.0.21', '10.1.0.20',
        #                    '10.3.0.2', '10.2.0.2', '10.2.0.1', '10.3.0.1'],
        #     'mesos-slave-attribute': ['10.1.0.16'],
        #     'mesos-slave': ['10.1.0.14']
        # }
        # self.valid_ips(ips, expect_ips)
        # components = self.get_component(resources)
        # self.valid_component('registry', components)
        # # self.valid_component('chronos-master', components)
        pass

    def test_basic_swarm_region(self):
        region = self.create_private_empty()
        data = self.get_swarm_basic_data()
        _, ips = self.create_template(data=data, region=region)
        expect_ips = {
            'controller': ['10.1.0.13'],
            'swarm_worker': ['10.1.0.16', '10.1.0.14']
        }
        self.valid_ips(ips, expect_ips)

    def test_swarm_with_mount_points(self):
        region = self.create_private_empty()
        data = self.get_swarm_basic_data()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('xin_agent', components)
        self.valid_component('xin_driver', components)

    def test_swarm_with_manager(self):
        region = self.create_private_empty()
        data = self.get_swarm_with_manager()
        _, ips = self.create_template(data=data, region=region)
        expect_ips = {
            'controller': ['10.1.0.12', '10.1.0.13', '10.1.0.10', '10.1.0.11'],
            'swarm_worker': ['10.1.0.16', '10.1.0.14']
        }
        self.valid_ips(ips, expect_ips)

    def test_swarm_with_haproxy(self):
        region = self.create_private_empty()
        data = self.get_swarm_with_haproxy()
        resources, ips = self.create_template(data=data, region=region)
        expect_ips = {
            'controller': ['10.1.0.12', '10.1.0.13', '10.1.0.10', '10.1.0.11'],
            'swarm_worker': ['10.1.0.16', '10.1.0.14']
        }
        self.valid_ips(ips, expect_ips)
        components = self.get_component(resources)
        self.valid_component('haproxy', components)

    def test_swarm_with_registry(self):
        region = self.create_private_empty()
        data = self.get_swarm_with_registry()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('registry', components)

    def test_swarm_with_local_volume(self):
        region = self.create_private_empty()
        data = self.get_swarm_with_registry()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('xin_agent', components)
        self.valid_component('xin_driver', components)

    def test_swarm_with_glusterfs_volume(self):
        region = self.create_private_empty()
        data = self.get_swarm_with_glusterfs_volume()
        resources, _ = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('xin_agent', components)
        self.valid_component('xin_driver', components)

    def test_swarm_with_ebs_volume(self):
        # region = self.create_aws_empty()
        # data = self.get_swarm_with_ebs_volume()
        # resources, _ = self.create_template(data=data, region=region)
        # components = self.get_component(resources)
        # self.valid_component('xin_agent', components)
        # self.valid_component('xin_driver', components)
        pass

    def test_swarm_with_illegal_features(self):
        region = self.create_private_empty()
        for feature in self.swarm_illegal_features:
            data = self.get_swarm_with_haproxy()
            data['features'].append(feature)
            try:
                self.create_template(data=data, region=region)
            except Exception as e:
                self.assertEqual("Swarm doesn't support {} yet".format(feature), e.message)
                continue
            raise

    def test_k8s_poc_basic(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_basic()
        resources, ips = self.create_template(data=data, region=region)
        compnents = self.get_component(resources)
        self.valid_component('kubernetes_init', compnents)
        self.valid_component('kubernetes_compute', compnents)

    def test_k8s_poc_with_hostgw(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_flannel_hostgw()
        resources, ips = self.create_template(data=data, region=region)
        component_details = self.get_details(resources)
        self.assertEqual(component_details['kubernetes_init']['environment']['FLANNEL_TYPE'],
                         'host-gw')
        self.assertEqual(component_details['kubernetes_compute']['environment']['FLANNEL_TYPE'],
                         'host-gw')

    def test_k8s_poc_with_macvlan(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_macvlan()
        resources, ips = self.create_template(data=data, region=region)
        compnents = self.get_component(resources)
        self.valid_component('kubernetes_init', compnents)
        self.valid_component('kubernetes_compute', compnents)
        self.valid_component('monkey', compnents)

    def test_k8s_poc_with_calico(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_calico()
        resources, ips = self.create_template(data=data, region=region)
        compnents = self.get_component(resources)
        self.valid_component('kubernetes_init', compnents)
        self.valid_component('kubernetes_compute', compnents)
        self.valid_component('calico', compnents)

    def test_k8s_poc_with_registry(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_registry()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('registry', components)

    def test_k8s_poc_with_alb(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_alb()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('alb-haproxy', components)
        self.valid_component('alb-xlb', components)

    def test_k8s_poc_with_all_volume(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_all_volume()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        self.valid_component('xin_agent', components)

    def test_k8s_poc_with_glusterfs_volume(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_glusterfs_volume()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        self.valid_component('xin_agent', components)

    def test_k8s_poc_with_ebs_volume(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_ebs_volume()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.assertNotIn('glusterfs', components)
        # self.valid_component('xin_driver', components)
        self.valid_component('xin_agent', components)

    def test_k8s_poc_with_doom(self):
        region = self.create_aws_empty()
        data = self.get_k8s_poc_with_doom()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('doom', components)

    def test_k8s_prod_basic(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_basic()
        resources, ips = self.create_template(data=data, region=region)
        compnents = self.get_component(resources)
        self.valid_component('kubernetes_init', compnents)
        self.valid_component('kubernetes_compute', compnents)

    def test_k8s_prod_with_hostgw(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_hostgw()
        resources, ips = self.create_template(data=data, region=region)
        compnents = self.get_component(resources)
        component_details = self.get_details(resources)
        self.assertEqual(component_details['kubernetes_init']['environment']['FLANNEL_TYPE'],
                         'host-gw')
        self.assertEqual(component_details['kubernetes_compute']['environment']['FLANNEL_TYPE'],
                         'host-gw')

    def test_k8s_prod_with_macvlan(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_macvlan()
        resources, ips = self.create_template(data=data, region=region)
        compnents = self.get_component(resources)
        self.valid_component('kubernetes_init', compnents)
        self.valid_component('kubernetes_compute', compnents)
        self.valid_component('monkey', compnents)

    def test_k8s_prod_with_registry(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_registry()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('registry', components)

    def test_k8s_prod_with_alb(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_alb()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('alb-haproxy', components)
        self.valid_component('alb-xlb', components)

    def test_k8s_prod_with_all_volume(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_all_volume()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        self.valid_component('xin_agent', components)

    def test_k8s_prod_with_glusterfs_volume(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_glusterfs_volume()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        # self.valid_component('glusterfs', components)
        # self.valid_component('xin_driver', components)
        self.valid_component('xin_agent', components)

    def test_k8s_prod_with_ebs_volume(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_ebs_volume()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.assertNotIn('glusterfs', components)
        # self.valid_component('xin_driver', components)
        self.valid_component('xin_agent', components)

    def test_k8s_prod_with_doom(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_doom()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('doom', components)

    def test_k8s_prod_with_clustomer_lbs(self):
        region = self.create_aws_empty()
        data = self.get_k8s_prod_with_customer_lbs()
        resources, ips = self.create_template(data=data, region=region)
        components = self.get_component(resources)
        self.valid_component('doom', components)

    @patch('requests.sessions.Session.request')
    def test_register_error(self, mock_request):
        mock_request.return_value = models.Response()
        mock_request.return_value.status_code = 500
        reg = ServiceRegister('token')
        region = {
            'id': '123',
            'name': 'ut',
            'namespace': 'utnamespace',
        }
        data = {
            'REGISTRY_NODE': '1.1.1.1',
            'REGISTRY_LB': 'reg-lb',
            'USER_SETTINGS': {
                'REGION': {
                    'FEATURES': []
                }
            },
            'HAPROXY_NODE': {
                'public_ip': "11.1.1.1",
                'private_ip': '2.2.2.2'
            },
            'INTERNAL_HAPROXY_PRIVATE_IP': '4.4.4.4',
            'CHRONOS_PUBLIC_LB': 'lb'
        }
        try:
            reg.create_registry(region, data, data['REGISTRY_LB'], 'REGISTRY_LB')
        except FurionException as e:
            self.assertIn('Type: 1001 code: registry-500', e.message)
        try:
            reg.create_private_build(region, data)
        except FurionException as e:
            self.assertIn('Type: 1001 code: private_build-500', e.message)
        try:
            reg.create_alb(region, data, 'internal', 'haproxy')
        except FurionException as e:
            self.assertIn('Type: 1001 code: alb-500', e.message)
        mock_request.return_value.status_code = 201
        reg.create_registry(region, data, data['REGISTRY_LB'], 'REGISTRY_LB')
        reg.create_private_build(region, data)
        reg.create_alb(region, data, 'internal', 'haproxy')

    def tearDown(self):
        Region.objects.all().delete()
