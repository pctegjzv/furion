import json
import copy

from django.conf import settings
from mock.mock import patch
from rest_framework.test import APITestCase, APIClient
from constant import CREATE_REQUEST, PORT_POOL_MAPPING_PORT, PHOENIX_SETTINGS, \
    PORT_POOL_PRIVATE_IP, PORT_POOL_PUBLIC_IP, PORT_POOL_SSH_PORT, PHOENIX_DATA, VERSIONS
from app.models import PortPool, Region, ComponentVersion, RegionSetting
from controls.component_version import ComponentVersionControl
from controls.region_settings import RegionSettingControl
from util import const
from django.core.urlresolvers import reverse
from tests import helper
from jsonschema.validators import Draft4Validator
from util.func import _prefix
from controls.self_service.service_register import ServiceRegister

from tests.base import Base


class PermissionsBaseTestCase(APITestCase):
    client = APIClient()

    def setUp(self):
        PortPool.objects.bulk_create([PortPool(public_ip=PORT_POOL_PUBLIC_IP,
                                               private_ip=PORT_POOL_PRIVATE_IP,
                                               used=False,
                                               region_name='',
                                               type=const.SSH,
                                               port=port) for port in PORT_POOL_SSH_PORT])
        PortPool.objects.bulk_create([PortPool(public_ip=PORT_POOL_PUBLIC_IP,
                                               private_ip=PORT_POOL_PRIVATE_IP,
                                               used=False,
                                               region_name='',
                                               type=const.MAPPING,
                                               port=port) for port in PORT_POOL_MAPPING_PORT])

    def test_ping(self):
        url = '/_ping/'
        response = self.client.get(url)
        self.assertEqual(response.content, "{}:{}".format(settings.MATHILDE_COMPONENT,
                                                          settings.COMPONENT_MOTTO))
        self.assertEqual(response.status_code, 200)

    def test_diagnose(self):
        url = '/_diagnose/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['status'], "OK")

    def _validate_region_info_encoded(self, region_id):
        region = Region.objects.get(id=region_id)
        features = region.features
        attr = region.attr
        self.assertTrue(features['service']['elb']['access_key'].startswith(_prefix))
        self.assertTrue(features['service']['elb']['secret_access_key'].startswith(_prefix))
        self.assertTrue(features['service']['manager']['password'].startswith(_prefix))
        self.assertTrue(attr['mesos']['auth']['token'].startswith(_prefix))
        self.assertTrue(attr['mesos']['auth']['log_token'].startswith(_prefix))

    def test_create_region_without_tunnel(self):
        self.maxDiff = None
        url = '/v1/regions'
        response = self.client.post(url, data=CREATE_REQUEST, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rep.pop('created_at'), rep.pop('updated_at')
        self.assertEqual(CREATE_REQUEST, rep)
        self._validate_region_info_encoded(self.region_id)

    def test_create_region_with_phoenix_settings(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rep.pop('created_at'), rep.pop('updated_at')
        self.assertEqual(CREATE_REQUEST, rep)
        self._validate_region_info_encoded(self.region_id)
        setting = RegionSetting.objects.get(**{'region_id': self.region_id})
        self.assertEqual(setting.master_node, PHOENIX_SETTINGS[u'MASTER_NODES'])
        self.assertEqual(setting.slave_node, PHOENIX_SETTINGS[u'SLAVE_NODES'])
        self.assertEqual(setting.image_location, PHOENIX_SETTINGS[u'IMAGE_LOCATION'])
        self.assertEqual(setting.region_location, PHOENIX_SETTINGS[u'REGION_LOCATION'])
        self.assertEqual(setting.user_settings, PHOENIX_SETTINGS[u'USER_SETTINGS'])
        self.assertEqual(setting.user_registries, PHOENIX_SETTINGS[u'USER_REGISTRIES'])
        region_setting = RegionSettingControl.get_object(**{'region_id': self.region_id})
        versions = ComponentVersion.objects.filter(**{'region_settings_id': region_setting.id})
        self.assertEqual(len(versions), 11)

    def test_update_component(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        update_data = {
            'version': 'this-is-update-version',
            'incarnation': 2,
            'description': 'hahaha'
        }
        url = '/v1/regions/{}/versions/{}'.format(self.region_id, 'mesos_master')
        response = self.client.put(url, data=update_data, format='json')
        self.assertEqual(response.status_code, 204)
        response = self.client.get('/v1/regions/{}/versions/current/'.format(self.region_id))
        current = json.loads(response.content)
        self.assertEqual(update_data, current['mesos_master'])
        region_setting = RegionSetting.objects.get(**{'region_id': self.region_id})
        self.assertEqual(True, region_setting.is_update_blocked)

    def test_get_version(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        response = self.client.get('/v1/regions/{}/versions/current/'.format(self.region_id))
        current = json.loads(response.content)
        self.assertEqual(VERSIONS, current)
        response = self.client.get('/v1/regions/{}/versions/default/'.format(self.region_id))
        default = json.loads(response.content)
        self.assertEqual(len(default), 11)
        filter = ComponentVersionControl.filter_components(current, default)
        self.assertEqual(len(filter), 11)
        default['nevermore'] = {
            u'version': u'this-is-nevermore-version',
            u'description': 'this_is_nevermore',
            u'incarnation': 1
        }
        filter = ComponentVersionControl.filter_components(current, default)
        self.assertEqual(len(filter), 10)
        default['nevermore']['incarnation'] = 2
        filter = ComponentVersionControl.filter_components(current, default)
        self.assertEqual(len(filter), 11)

    def test_get_setting(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        response = self.client.get('/v1/regions/{}/settings/region/'.format(self.region_id))
        rep = json.loads(response.content)
        self.assertEqual(rep['user_settings']['MASTER_NODES'], PHOENIX_SETTINGS[u'MASTER_NODES'])
        self.assertEqual(rep['user_settings']['SLAVE_NODES'], PHOENIX_SETTINGS[u'SLAVE_NODES'])
        self.assertEqual(rep['user_settings']['IMAGE_LOCATION'],
                         PHOENIX_SETTINGS[u'IMAGE_LOCATION'])
        self.assertEqual(rep['user_settings']['REGION_LOCATION'],
                         PHOENIX_SETTINGS[u'REGION_LOCATION'])
        self.assertEqual(rep['user_settings']['USER_SETTINGS'], PHOENIX_SETTINGS[u'USER_SETTINGS'])
        self.assertEqual(rep['user_settings']['USER_REGISTRIES'],
                         PHOENIX_SETTINGS[u'USER_REGISTRIES'])

    def test_valid_settings(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = copy.deepcopy(PHOENIX_SETTINGS)
        all_data['attr']['component_versions'] = VERSIONS
        all_data['attr']['phoenix_settings']['MASTER_NODES'] = {}
        response = self.client.post(url, data=all_data, format='json')
        self.assertEqual(response.status_code, 400)

    def test_valid_upgrade(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rs_obj = RegionSetting.objects.get(**{'region_id': self.region_id})
        rs_obj.is_update_blocked = True
        rs_obj.save()
        response = self.client.put('/v1/regions/{}/upgrade'.format(self.region_id))
        self.assertEqual(response.status_code, 400)

    @patch('alauda.alchemist.resources.Resource.list')
    @patch('alauda.alchemist.resources.Resource.update')
    def test_upgrade_one_component(self, mock_update, mock_list):
        mock_list.return_value = [{'name': 'nevermore', 'resource_uuid': 'nevermore-resource-uuid'}]
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = copy.deepcopy(PHOENIX_SETTINGS)
        all_data['attr']['phoenix_settings']['COIL_CLIENT_NODE'] = ['172.31.1.1']
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rs_obj = RegionSetting.objects.get(**{'region_id': self.region_id})
        version = ComponentVersion.objects.get(region_settings=rs_obj, name='nevermore')
        version.version = 'not-exist-version'
        version.save()
        data = {'components': ['nevermore']}
        response = self.client.put('/v1/regions/{}/upgrade'.format(self.region_id),
                                   data=data, format='json')
        self.assertEqual(response.status_code, 204)

    @patch('alauda.alchemist.resources.Resource.retrieve_by_name')
    @patch('alauda.alchemist.resources.Resource.list')
    @patch('alauda.alchemist.resources.Resource.update')
    def test_upgrade(self, mock_update, mock_list, resource_id):
        mock_list.return_value = [{'name': 'nevermore', 'resource_uuid': 'nevermore-resource-id'},
                                  {'name': 'doom', 'resource_uuid': 'doom-resource-id'}]
        resource_id.return_value = [{'resource_uuid': 'xxxxxx'}]
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = copy.deepcopy(PHOENIX_SETTINGS)
        all_data['attr']['phoenix_settings']['COIL_CLIENT_NODE'] = ['172.31.1.1']
        all_data['attr']['phoenix_settings']['COMPONENT'].append('controller_haproxy')
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rs_obj = RegionSetting.objects.get(**{'region_id': self.region_id})
        version = ComponentVersion.objects.get(region_settings=rs_obj, name='nevermore')
        version.version = 'not-exist-version'
        version.save()
        data = {'components': ['nevermore', 'doom']}
        response = self.client.put('/v1/regions/{}/upgrade'.format(self.region_id),
                                   data=data, format='json')
        self.assertEqual(response.status_code, 204)

    def test_block_unblock_upgrade(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rep = self.client.put('/v1/regions/{}/upgrade/block'.format(self.region_id))
        self.assertEqual(rep.status_code, 204)
        rs_obj = RegionSetting.objects.get(**{'region_id': self.region_id})
        self.assertEqual(rs_obj.is_update_blocked, True)
        rep = self.client.put('/v1/regions/{}/upgrade/unblock'.format(self.region_id))
        self.assertEqual(rep.status_code, 204)
        rs_obj = RegionSetting.objects.get(**{'region_id': self.region_id})
        self.assertEqual(rs_obj.is_update_blocked, False)

    def test_update_one_component(self):
        self.maxDiff = None
        url = '/v1/regions'
        all_data = copy.deepcopy(CREATE_REQUEST)
        all_data['attr']['phoenix_settings'] = PHOENIX_SETTINGS
        all_data['attr']['component_versions'] = VERSIONS
        response = self.client.post(url, data=all_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        update_data = {
            'version': 'this-is-test-update-version',
            'incarnation': 6,
            'description': 'this is UT to test update a component method.'
        }
        url = '/v1/regions/{}/versions/nevermore'.format(self.region_id)
        self.client.put(url, data=update_data, format='json')
        response = self.client.get('/v1/regions/{}/versions/current/'.format(self.region_id))
        current = json.loads(response.content)
        self.assertEqual(current['nevermore'], update_data)
        rs_obj = RegionSetting.objects.get(**{'region_id': self.region_id})
        self.assertEqual(rs_obj.is_update_blocked, True)

    def test_get_command(self):
        self.maxDiff = None
        url = '/v1/regions'
        response = self.client.post(url, data=CREATE_REQUEST, format='json')
        rep = json.loads(response.content)
        uuid = rep.pop('id')
        response = self.client.get('/v1/regions/{}/node-scripts?node_type=mesos-slave'.format(uuid))
        self.assertEqual(PHOENIX_DATA, json.loads(response.content))
        command = json.loads(response.content)['commands']['install']
        pos = command.find('node-type=mesos-slave')
        self.assertGreater(pos, 0)

    def test_get_command_with_attribute(self):
        self.maxDiff = None
        url = '/v1/regions'
        response = self.client.post(url, data=CREATE_REQUEST, format='json')
        rep = json.loads(response.content)
        uuid = rep.pop('id')
        url = '/v1/regions/{}/node-scripts?node_type=mesos-slave-attribute'.format(uuid)
        response = self.client.get(url)
        command = json.loads(response.content)['commands']['install']
        pos = command.find('node-type=mesos-slave-attribute')
        self.assertGreater(pos, 0)

    def test_create_region_with_tunnel(self):
        url = '/v1/regions'
        request_data = copy.deepcopy(CREATE_REQUEST)
        request_data['features']['service']['features'].append('tunnel')
        response = self.client.post(url, data=request_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        rep.pop('created_at'), rep.pop('updated_at')
        tunnel = rep['features'].pop('tunnel')
        self.assertIn('private_key', tunnel)
        self.assertIn('public_key', tunnel)
        self.assertIn('port_mapping', tunnel)
        self.assertEqual(tunnel['port_mapping']['ssh'], '1.1.1.1:22')
        tunnel['port_mapping'].pop('ssh')
        for port in tunnel['port_mapping']:
            self.assertIn(tunnel['port_mapping'][port],
                          ['{}:{}'.format(PORT_POOL_PRIVATE_IP, mapping) for
                           mapping in PORT_POOL_MAPPING_PORT])
        self.assertEqual(PortPool.objects.filter(used=True).count(), 6)
        self.assertEqual(rep['features']['service']['manager']['endpoint'],
                         'http://2.2.2.2:0/marathon')
        request_data['features']['service']['manager']['endpoint'] = \
            'http://2.2.2.2:0/marathon'
        self.assertEqual(rep, request_data)
        self._validate_region_info_encoded(self.region_id)
        request_data.pop('display_name')
        request_data.pop('env_uuid')
        request_data.pop('namespace')
        request_data.pop('name')
        request_data.pop('state')
        updated_response = self.client.put('{}/{}'.format(url, self.region_id),
                                           data=request_data,
                                           format='json')
        updated_response = self.client.get('{}/{}'.format(url, self.region_id))
        rep = json.loads(updated_response.content)
        tunnel = rep['features'].pop('tunnel')
        self.assertIn('private_key', tunnel)
        self.assertIn('public_key', tunnel)
        self.assertIn('port_mapping', tunnel)
        self.assertEqual(tunnel['port_mapping']['ssh'], '1.1.1.1:22')
        tunnel['port_mapping'].pop('ssh')
        for port in tunnel['port_mapping']:
            self.assertIn(tunnel['port_mapping'][port],
                          ['{}:{}'.format(PORT_POOL_PRIVATE_IP, mapping)
                           for mapping in PORT_POOL_MAPPING_PORT])

    def test_create_region_with_volume(self):
        url = '/v1/regions'
        request_data = copy.deepcopy(CREATE_REQUEST)
        request_data['features']['service']['features'].append('volume')
        response = self.client.post(url, data=request_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        volume = rep['features']['service'].pop('volume')
        self.assertIn('drivers', volume)
        self.assertIn('agent', volume)
        self.assertIn('username', volume['agent'])
        self.assertIn('password', volume['agent'])
        self.assertEqual(volume['drivers'],
                         request_data['features']['service']['volume']['drivers'])
        self.assertEqual(volume['agent']['username'],
                         request_data['features']['service']['volume']['agent']['username'])
        self.assertEqual(volume['agent']['password'],
                         request_data['features']['service']['volume']['agent']['password'])
        self.assertEqual(volume['agent']['endpoint'],
                         request_data['features']['service']['volume']['agent']['endpoint'])

    def test_create_region_with_volume_and_tunnel(self):
        url = '/v1/regions'
        request_data = copy.deepcopy(CREATE_REQUEST)
        request_data['features']['service']['features'].append('tunnel')
        request_data['features']['service']['features'].append('volume')
        response = self.client.post(url, data=request_data, format='json')
        rep = json.loads(response.content)
        volume = rep['features']['service'].pop('volume')
        endpoint = rep['features']['service']['manager']['endpoint'].replace("marathon",
                                                                             "volume_driver")
        self.assertIn('drivers', volume)
        self.assertIn('agent', volume)
        self.assertIn('username', volume['agent'])
        self.assertIn('password', volume['agent'])
        self.assertEqual(volume['drivers'],
                         request_data['features']['service']['volume']['drivers'])
        self.assertEqual(volume['agent']['username'],
                         request_data['features']['service']['volume']['agent']['username'])
        self.assertEqual(volume['agent']['password'],
                         request_data['features']['service']['volume']['agent']['password'])
        self.assertEqual(volume['agent']['endpoint'], endpoint)

    def test_create_region_with_pipeline_and_tunnel(self):
        url = '/v1/regions'
        request_data = copy.deepcopy(CREATE_REQUEST)
        predata = {
            u"features": [
                u"temporary"
            ],
            u"temporary": {
                u"type": u"chronos",
                u"ip": u"http://1.1.1.1:4400"
            }
        }
        request_data['features']['service']['features'].append('tunnel')
        request_data['features']['service']['features'].append('pipeline')
        request_data['features']['cd'] = predata
        response = self.client.post(url, data=request_data, format='json')
        rep = json.loads(response.content)
        pipeline = rep['features'].pop('cd')
        chronos_endpoint = u"http://{}".format(rep['features']['tunnel']['port_mapping']['doom'])
        self.assertIn('features', pipeline)
        self.assertIn('temporary', pipeline)
        self.assertIn('type', pipeline['temporary'])
        self.assertIn('ip', pipeline['temporary'])
        self.assertEqual(pipeline['temporary']['ip'], chronos_endpoint)

    def test_create_region_with_pipeline_without_tunnel(self):
        url = '/v1/regions'
        request_data = copy.deepcopy(CREATE_REQUEST)
        predata = {
            u"features": [
                u"temporary"
            ],
            u"temporary": {
                u"type": u"chronos",
                u"ip": u"http://1.1.1.1:4400"
            }
        }
        request_data['features']['service']['features'].append('pipeline')
        request_data['features']['cd'] = predata
        response = self.client.post(url, data=request_data, format='json')
        rep = json.loads(response.content)
        pipeline = rep['features'].pop('cd')
        self.assertIn('features', pipeline)
        self.assertIn('temporary', pipeline)
        self.assertIn('type', pipeline['temporary'])
        self.assertIn('ip', pipeline['temporary'])
        self.assertEqual(pipeline['temporary']['ip'], predata['temporary']['ip'])

    def _validate_attr_schema(self, attr):
        attr_schema = helper.load_json('fixtures/region_attr_schema.json')
        Draft4Validator.check_schema(attr_schema)
        validator = Draft4Validator(attr_schema)
        validator.validate(attr)

    def _validate_feature_schema(self, tunnel):
        feature_schema = helper.load_json('fixtures/region_feature_schema.json')
        Draft4Validator.check_schema(feature_schema)
        validator = Draft4Validator(feature_schema['definitions']['tunnel'])
        validator.validate(tunnel)
        self.assertTrue(validator.is_valid(tunnel))

    def _assert_region_schema(self, region):
        self.assertTrue('attr' in region)
        self.assertTrue('features' in region)
        attr = region['attr']
        # features = region['features']
        attr_schema = {
            'mesos': {
                'marathon_endpoint': [],
                'auth': ['username', 'token'],
                'versions': ['nevermore', 'zeus', 'mesos']
            },
            'mesos_endpoint': [],
            'cloud': []
        }

        def _recurive_validate(schema, value):
            if isinstance(schema, dict):
                for k, v in schema.iteritems():
                    _recurive_validate(v, value[k])
                    self.assertIn(k, value)
            if isinstance(value, list):
                for v in value:
                    self.assertIn(v, value)
                return

        _recurive_validate(attr_schema, attr)

    def _test_get_region_list(self):
        url = reverse('regions/')
        response = self.client.get(url)
        regions = json.loads(response.content)
        self.assertEqual(len(regions), 1)
        region = regions[0]
        self.assertEqual(region['id'], self.region_id)
        self._validate_attr_schema(region['attr'])

    def _test_get_region(self):
        url = reverse('regions/region-id', kwargs={'region_id': self.region_id})
        response = self.client.get(url)
        region = json.loads(response.content)
        self._validate_attr_schema(region['attr'])
        return region

    def test_create_get_region_base(self):
        self.test_create_region_without_tunnel()
        self._test_get_region_list()
        self._test_get_region()

    def test_create_get_tunnel_region(self):
        self.test_create_region_with_tunnel()
        self._test_get_region_list()
        region = self._test_get_region()
        self._validate_feature_schema(region['features']['tunnel'])
        public_key = region['features']['tunnel']['public_key']
        private_key = region['features']['tunnel']['private_key']
        self.assertTrue(helper.validate_rsa_match(public_key, private_key))

    @patch('util.phoenix.NodeManager.list_nodes')
    @patch("util.phoenix.EnvironmentManager.get_spectre_version")
    @patch('util.phoenix.ResourceManager.clean_up')
    @patch('util.phoenix.NodeManager.delete')
    @patch('util.phoenix.NodeManager.clean_up')
    @patch('util.phoenix.EnvironmentManager.delete')
    def test_delete_region_with_tunnel(self, mock_delete, mock_clean_up, mock_node_delete,
                                       mock_resource_clean_up, mock_get_spectre_version,
                                       mock_list_nodes):
        mock_list_nodes.return_value = []
        mock_get_spectre_version.return_value = 2.8
        self.test_create_region_with_tunnel()
        self._test_get_region_list()
        region_info = self._test_get_region()
        self.region_id = region_info['id']
        url = '/v1/regions'
        region = Region.objects.filter(id=self.region_id).first()
        region.container_manager = "NONE"
        region.save()
        url = '{}/{}'.format(url, self.region_id)
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, 204)
        port_mapping = region_info['features']['tunnel']['port_mapping']
        for key in port_mapping.keys():
            port = port_mapping[key].split(":")[1]
            entity = PortPool.objects.filter(port=port).first()
            self.assertFalse(entity.used)
            self.assertEqual(entity.region_name, "")

    @patch("util.phoenix.EnvironmentManager.get_spectre_version")
    @patch('util.phoenix.NodeManager.clean_up')
    @patch('util.phoenix.EnvironmentManager.delete')
    @patch('util.phoenix.NodeManager.delete_resources')
    @patch('util.phoenix.NodeManager.update_type')
    @patch('util.phoenix.NodeManager.list_nodes')
    @patch('util.phoenix.ResourceManager.clean_up')
    @patch.object(ServiceRegister, 'remove_private_build')
    @patch.object(ServiceRegister, 'remove_registry')
    @patch.object(ServiceRegister, 'remove_alb')
    def test_delete_region_after_empty(self, remove_alb, remove_registry, remove_private_build,
                                       mock_resource_clean_up, mock_list_nodes,
                                       mock_update_type, mock_delete_resources,
                                       mock_delete, mock_node_clean_up, mock_get_spectre_version):
        mock_get_spectre_version.return_value = 2.7
        mock_list_nodes.return_value = []
        url = '/v1/regions'
        request_data = copy.deepcopy(CREATE_REQUEST)
        request_data['features']['service']['features'].append('tunnel')
        response = self.client.post(url, data=request_data, format='json')
        rep = json.loads(response.content)
        self.region_id = rep.pop('id')
        response = self.client.put('{}/{}'.format(url, self.region_id),
                                   data={'container_manager': 'NONE',
                                         'root_token': 'aaa'},
                                   format='json')
        self.assertEqual(response.status_code, 204)
        url = '{}/{}'.format(url, self.region_id)
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, 204)
        port_mapping = rep['features']['tunnel']['port_mapping']
        for key in port_mapping.keys():
            port = port_mapping[key].split(":")[1]
            entity = PortPool.objects.filter(port=port).first()
            self.assertFalse(entity.used)
            self.assertEqual(entity.region_name, "")


class TestRegionAPI(Base):

    def test_region_check_features(self):

        """ test plan: valid & invalid config result check
        features: {
        }
        """

        feature = helper.load_json('fixtures/region/region-features.json')
        region = Region.objects.create(
            name='test',
            display_name='test',
            namespace='',
            state='RUNNING',
            attr={},
            features=feature
        )

        region_id = region.id

        # test format list
        features = {}
        resp = self.api_region_features_check(region_id, features={
            'features': features,
            'container_manager': ''
        })

        errors = json.loads(resp.content)['errors'][0]

        self.assertEqual('invalid_args', errors['code'])

        self.assertEqual(400, resp.status_code)

        # test format non string
        features = [None]
        resp = self.api_region_features_check(region_id, features={
            'features': features
        })

        errors = json.loads(resp.content)['errors'][0]

        self.assertEqual('invalid_args', errors['code'])

        self.assertEqual(400, resp.status_code)

        # # test valid
        features = [
            "haproxy", "pipeline", "exec", "tunnel", "alb", "volume",
            "application", "host-network", "registry", "mount-points",
            "flannel", "glusterfs"
        ]
        resp = self.api_region_features_check(region_id, features={
            'features': features
        })

        self.assertEqual(200, resp.status_code)

        # test invalid
        features = ["haproxy1"]
        resp = self.api_region_features_check(region_id, features={
            'features': features
        })

        errors = json.loads(resp.content)['errors'][0]

        self.assertEqual('feature_error', errors['code'])

        self.assertEqual(400, resp.status_code)
