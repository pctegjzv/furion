import os
import json
import six
from Crypto.PublicKey import RSA
from django.core.urlresolvers import reverse
from django.utils.http import urlencode


def load_json(filename):
    path = os.path.dirname(os.path.abspath(__file__))
    filename = os.path.join(path, filename)
    with open(filename, 'r') as fh:
        return json.load(fh)


def validate_rsa_match(public_key, private_key):
    assert isinstance(public_key, six.string_types)
    assert isinstance(private_key, six.string_types)

    if 'ssh-rsa' in public_key:
        message = 'today shuai leaves, good luck to her in the future..'

        public_key_container = RSA.importKey(public_key)
        private_key_container = RSA.importKey(private_key)

        encrypted_message = public_key_container.encrypt(message, 0)
        decrypted_message = private_key_container.decrypt(encrypted_message)

        return message == decrypted_message

    return False


def my_reverse(viewname, kwargs=None, query_kwargs=None):
    """
    Custom reverse to add a query string after the url
    Example usage:
    url = my_reverse('my_test_url', kwargs={'pk': object.id}, query_kwargs={'next': reverse('home')})
    """
    url = reverse(viewname, kwargs=kwargs)

    if query_kwargs:
        return u'%s?%s' % (url, urlencode(query_kwargs))

    return url
