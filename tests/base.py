from rest_framework.test import APITestCase, APIClient
from django.core.urlresolvers import reverse

from . import helper
from mock.mock import patch


__author__ = 'Lei Gong <lgong@alauda.io>'


class Base(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def api_create_region(self, payload=None):
        if not payload:
            payload = helper.load_json('fixtures/region/create_region.json')
        url = reverse('regions/')
        return self.client.post(url, data=payload, format='json')

    def api_create_node(self, region_id, payload=None):
        url = reverse('regions/nodes/', kwargs={'region_id': region_id})
        return self.client.post(url, data=payload, format='json')

    def api_list_node(self, region_id):
        url = reverse('regions/nodes/', kwargs={'region_id': region_id})
        return self.client.get(url, format='json')

    def api_get_node(self, region_id, private_ip):
        url = reverse('regions/nodes/private-ip',
                      kwargs={'region_id': region_id, 'private_ip': private_ip})
        return self.client.get(url, format='json')

    @patch('controls.node.NodeControl._delete_phoenix_node')
    @patch('controls.node.NodeControl._update_region_settings')
    def api_delete_node(self, region_id, private_ip, mock_update_region_settings,
                        mock_delete_phoenix_node):
        url = reverse('regions/nodes/private-ip', kwargs={'region_id': region_id,
                                                          'private_ip': private_ip})
        return self.client.delete(url, format='json')

    def api_start_node(self, region_id, private_ip):
        url = reverse('regions/nodes/start', kwargs={'region_id': region_id,
                                                     'private_ip': private_ip})
        return self.client.put(url, format='json')

    def api_stop_node(self, region_id, private_ip):
        url = reverse('regions/nodes/stop', kwargs={'region_id': region_id,
                                                    'private_ip': private_ip})
        return self.client.put(url, format='json')

    @patch('controls.stats.send_metrics')
    def api_update_node_stats(self, region_id, payload, mock_send_metrics):
        if not payload:
            payload = helper.load_json('fixtures/stats/update_node_stats.json')
        url = reverse('regions/region-id/stats', kwargs={'region_id': region_id})
        return self.client.put(url, data=payload, format='json')

    def api_node_stats(self, region_id, private_ip):
        url = reverse('regions/nodes/stats', kwargs={'region_id': region_id,
                                                     'private_ip': private_ip})
        return self.client.get(url, format='json')

    def api_region_features_check(self, region_id, features):
        url = reverse('regions/region-id/check-features', kwargs={
            'region_id': region_id
        })
        return self.client.put(url, data=features, format='json')
