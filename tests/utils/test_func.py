from alauda.common.func import pad_str
from Crypto.Cipher import AES
from django.conf import settings
from rest_framework.test import APITestCase

from util.func import _prefix, wrapped_decode_aes, wrapped_encode_aes, encrypt, decrypt


class FuncTest(APITestCase):
    def setUp(self):
        self.message = 'endless encrypt'
        self.cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])

    def _test_encrypt_aes(self):
        self.encrypted = wrapped_encode_aes(self.cipher, self.message)
        self.assertTrue(self.encrypted.startswith(_prefix))
        self.assertNotEqual(self.encrypted[len(_prefix):], self.message)

    def _test_decrypt_aes(self):
        self.decrypted = wrapped_decode_aes(self.cipher, self.encrypted)
        self.assertEqual(self.message, self.decrypted)

    def test_xcrypt_aes(self):
        self._test_encrypt_aes()
        self._test_decrypt_aes()

    def test_encrypt_and_descrypt(self):
        data = "hehee"
        self.assertEqual(data, decrypt(encrypt("hehee")))
