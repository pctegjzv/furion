# -*- coding: utf-8 -*-
from rest_framework import serializers

from app.models import Region, Node, Stats, RegionSetting, ComponentVersion

__author__ = 'Hang Yan'


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region


class NodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Node


class StatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stats


class RegionSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = RegionSetting

    def validate(self, attr):
        message = '{} must not be empty!'
        master_node = attr.get('master_node', '')
        if len(master_node) == 0:
            raise serializers.ValidationError(message.format('MASTER_NODES'))
        user_settings = attr.get('user_settings', '')
        if len(user_settings) == 0:
            raise serializers.ValidationError(message.format('USER_SETTINGS'))
        user_registries = attr.get('user_registries', '')
        if len(user_registries) == 0:
            raise serializers.ValidationError(message.format('USER_REGISTRIES'))
        return attr


class ComponentVersionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComponentVersion


def validate_and_save(serializer):
    if serializer.is_valid(raise_exception=True):
        serializer.save()
        return serializer.data
