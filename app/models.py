from __future__ import unicode_literals

import uuid

import jsonfield
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

from util import const


def gen_uuid():
    return str(uuid.uuid4())


class Time(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Base(Time):
    id = models.CharField(max_length=36, primary_key=True, default=gen_uuid)

    class Meta:
        abstract = True


class Mirror(Base):
    name = models.CharField(max_length=36)
    display_name = models.CharField(max_length=36)
    description = models.CharField(max_length=1024, default='')
    flag = models.CharField(max_length=36)


@python_2_unicode_compatible
class Region(Base):
    name = models.CharField(max_length=36)
    mirror = models.ForeignKey(Mirror, null=True)
    display_name = models.CharField(max_length=36)
    namespace = models.CharField(max_length=36, blank=True, default='')
    state = models.CharField(max_length=16, choices=const.REGION_STATE, default=const.STATE_RUNNING)
    platform_version = models.CharField(max_length=16, default=const.KUBERNETES_PLATFORM_VERSION_V2, null=True)
    container_manager = models.CharField(max_length=16,
                                         choices=const.CONTAINER_MANAGER,
                                         default=const.CONTAINER_MANAGER_NONE)
    attr = jsonfield.JSONField()
    features = jsonfield.JSONField()
    env_uuid = models.CharField(max_length=36, null=True, db_index=True)

    class Meta:
        unique_together = ('namespace', 'name')
        index_together = ('namespace', 'name')

    def __str__(self):
        return '{} {}'.format(self.namespace, self.name)

    def is_swarm_region(self):
        return self.container_manager == const.CONTAINER_MANAGER_SWARM


class PortPool(models.Model):
    port = models.IntegerField(default=0)
    used = models.BooleanField(default=False)
    public_ip = models.CharField(max_length=32, null=True, db_index=True)
    private_ip = models.CharField(max_length=32, null=True)
    region_name = models.CharField(max_length=64, null=True)
    namespace = models.CharField(max_length=36, blank=True, default='')
    type = models.CharField(max_length=64, choices=const.PORT_TYPE, db_index=True)

    class Meta:
        unique_together = ('public_ip', 'private_ip', 'port')

    def to_address(self):
        return '{}:{}'.format(self.public_ip if self.type == const.SSH else self.private_ip,
                              self.port)


class Node(models.Model):
    id = models.CharField(max_length=36, primary_key=True, default=gen_uuid)
    private_ip = models.CharField(max_length=32)
    instance_id = models.CharField(max_length=128, blank=True)
    resources = jsonfield.JSONField(blank=True, default={})
    region = models.ForeignKey(Region)
    # TODO:SS_Milestone2: Change default to EMPTY. Currently, we have code paths where node gets
    # created from puck query on stats - where the assumption is that the default is slave. Change
    # this in future. For now, empty nodes will be explicitly created as empty.
    type = models.CharField(max_length=16, choices=const.NODE_TYPES,
                            default=const.NODE_TYPE_COMPUTE)
    state = models.CharField(max_length=16, choices=const.NODE_STATE, default=const.STATE_RUNNING)
    attr = jsonfield.JSONField(blank=True, default={})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not kwargs.pop('skip_update_time', False):
            self.updated_at = timezone.now()
        super(Node, self).save(*args, **kwargs)

    def is_schedulable(self):
        """Get node schedulable status(Kubernetes)."""
        return self.attr.get('schedulable', True)

    class Meta:
        unique_together = ('region', 'private_ip')
        index_together = ('region', 'private_ip')


@python_2_unicode_compatible
class Label(models.Model):
    key = models.CharField(max_length=60)
    value = models.CharField(max_length=60)
    editable = models.BooleanField(default=True)
    node = models.ForeignKey(Node, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('node', 'key')
        index_together = ('node', 'key')

    def __str__(self):
        return "{}:{}:{}".format(self.node, self.key, self.value)

    @classmethod
    def node_tag_to_label(cls, node_tag):
        """Convert old node_tag to new label format.
        Args:
            node_tag(str)
        Return:
            dict
        """
        kvs = node_tag.split(':')
        data = {
            'key': kvs[0],
            'value': kvs[1],
            'editable': False
        }
        return data


class Stats(Time):
    type = models.CharField(max_length=32, choices=const.STATS_TYPES)
    region = models.ForeignKey(Region)
    data = jsonfield.JSONField(blank=True, default={})

    class Meta:
        unique_together = ('region', 'type')
        index_together = ('region', 'type')


class RegionSetting(Time):
    id = models.CharField(max_length=36, primary_key=True, default=gen_uuid)
    region = models.ForeignKey(Region)
    master_node = jsonfield.JSONField(blank=True)
    slave_node = jsonfield.JSONField(blank=True, default=[])
    region_location = models.CharField(max_length=64)
    image_location = models.CharField(max_length=64)
    user_settings = jsonfield.JSONField(blank=True)
    user_registries = jsonfield.JSONField(blank=True)
    cluster_settings = jsonfield.JSONField(blank=True, default={})
    cluster_template_type = models.CharField(max_length=16)
    is_update_blocked = models.BooleanField(default=False)


class ComponentVersion(Time):
    region_settings = models.ForeignKey(RegionSetting)
    name = models.CharField(max_length=36)
    version = models.CharField(max_length=64)
    # This field is for a scenario like:
    # We just want to change some settings(env, volume etc...). No image id change.
    # So, we need change the component template. We have to trigger a update
    incarnation = models.IntegerField(default=1, null=False)
    description = models.CharField(max_length=1024, null=True, default='')

    class Meta:
        unique_together = ('region_settings', 'name')
        index_together = ('region_settings', 'name')
