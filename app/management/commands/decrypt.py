#!/usr/bin/env python
# -*- coding: utf-8 -*-

from alauda.common.func import pad_str
from Crypto.Cipher import AES
from django.conf import settings
from django.core.management import BaseCommand

from util.func import wrapped_decode_aes

__author__ = 'Hang Yan'


class Command(BaseCommand):
    help = "Decrypt region data fields"

    def add_arguments(self, parser):
        parser.add_argument('field', type=str)

    def handle(self, *args, **options):
        cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
        decrypted = wrapped_decode_aes(cipher, options['field'])
        self.stdout.write(decrypted)
