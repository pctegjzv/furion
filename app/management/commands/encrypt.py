#!/usr/bin/env python
# -*- coding: utf-8 -*-
from alauda.common.func import pad_str
from django.core.management import BaseCommand
from django.conf import settings
from util.func import wrapped_decode_aes, wrapped_encode_aes
from Crypto.Cipher import AES

__author__ = 'Hang Yan'


class Command(BaseCommand):
    help = 'Encrypt sensitive fields in region data'

    def add_arguments(self, parser):
        parser.add_argument('field', type=str)

    def handle(self, *args, **options):
        cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
        encrypted = wrapped_encode_aes(cipher, options['field'])
        origin = wrapped_decode_aes(cipher, encrypted)
        self.stdout.write('Encrypted: {}'.format(encrypted))
        self.stdout.write('Decrypted: {}'.format(origin))
