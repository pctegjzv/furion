from django.conf import settings
from django.conf.urls import patterns, url, include

from views import region, node
from alauda.common.web import ping

__author__ = 'Hang Yan'

REGEX = settings.URL_REGEX

urlpatterns = patterns(
    '',
    url(r'^regions/support-docker-version/?$', region.get_support_docker_version),
    url(r'^regions/?$', region.RegionsView.as_view(), name='regions/'),
    url(r'^regions/(?P<region_id>{})/?$'.format(REGEX),
        region.RegionView.as_view(),
        name='regions/region-id'),
    url(r'^regions/(?P<region_id>{})/'.format(REGEX), include([
        url(r'^components/?$', region.get_comps),
        url(r'^labels/?$', region.get_labels, name='regions/region-id/labels'),
        url(r'^node-selector/?$', region.select_node, name='regions/region-id/select-node'),
        url(r'^config/?$', region.get_region_config),
        url(r'^stats/?$', region.StatsView.as_view(), name='regions/region-id/stats'),
        url(r'^node-scripts/?$', region.get_node_scripts),
        url(r'^upgrade/?$', region.UpgradeView.as_view(), name='regions/region-id/upgrade'),
        url(r'^upgrade/block/?$', region.block),
        url(r'^upgrade/unblock/?$', region.unblock),
        url(r'^check-features/?$', region.check_region_features,
            name='regions/region-id/check-features'),
        url(r'^features/', include([
            url(r'^(?P<feature>{})/enable/?$'.format(REGEX), region.enable),
            url(r'^(?P<feature>{})/disable/?$'.format(REGEX), region.disable)
        ])),
        url(r'^versions/', include([
            url(r'^current/?$', region.get_current_version),
            url(r'^default/?$', region.get_default_version),
            url(r'^(?P<component_name>{})/?$'.format(REGEX), region.update_component_version)
        ])),
        url(r'^settings/', include([
            url(r'^node-scaling/?$', region.node_scale_settings),
            url(r'^key-pairs/?$', region.key_pairs),
            url(r'^region/?$', region.get_region_settings),

        ])),
        url('^log_sources/?$', region.LogSourceView.as_view(), name='regions/log_sources/'),
        url('^nodes/?$', node.NodesView.as_view(), name='regions/nodes/'),
        url('^nodes/(?P<private_ip>{})/?$'.format(REGEX),
            node.NodeView.as_view(),
            name='regions/nodes/private-ip'
            ),
        url('^nodes/(?P<private_ip>{})/'.format(REGEX), include([
            url(r'^stats/?$', node.get_stats, name='regions/nodes/stats'),
            url(r'^start/?$', node.start_node, name='regions/nodes/start'),
            url(r'^stop/?$', node.stop_node, name='regions/nodes/stop'),
            url(r'^actions/?$', node.do_actions, name='regions/nodes/actions'),
            url(r'^labels/?$', node.update_labels, name='regions/nodes/labels'),
            url(r'^update_type/?$', node.update_type, name='regions/nodes/update_type'),
        ])),
    ])),
    # TODO: remove
    url(r'^ping/?$', ping())
)
