# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0024_auto_20170710_1254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='state',
            field=models.CharField(default=b'RUNNING', max_length=16, choices=[(b'DEPLOYING', b'Deploying'), (b'RUNNING', b'Running'), (b'BUILD', b'BUILD'), (b'ACTIVE', b'ACTIVE'), (b'SHUTOFF', b'SHUTOFF'), (b'SHUTTING_DOWN', b'Shutting Down'), (b'STOPPED', b'Stopped'), (b'STOPPING', b'Stopping'), (b'REMOVED', b'Removed'), (b'UNKNOWN', b'Unknown'), (b'OFFLINE', b'Offline'), (b'ERROR', b'Error'), (b'CRITICAL', b'Critical'), (b'PREPARING', b'Preparing'), (b'DRAINING', b'Draining')]),
        ),
    ]
