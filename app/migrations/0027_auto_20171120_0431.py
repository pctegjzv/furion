# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0026_region_version'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='region',
            name='version',
        ),
        migrations.AddField(
            model_name='region',
            name='platform_version',
            field=models.CharField(default=b'v2', max_length=16, null=True),
        ),
    ]
