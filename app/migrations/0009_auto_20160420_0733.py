# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_auto_20160409_0708'),
    ]

    operations = [
        migrations.CreateModel(
            name='Stats',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('type', models.CharField(max_length=16, choices=[(b'MESOS_METRICS', b'Mesos Metrics')])),
                ('data', jsonfield.fields.JSONField(default={}, blank=True)),
                ('region', models.ForeignKey(to='app.Region')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='activity',
            unique_together=set([]),
        ),
        migrations.AlterIndexTogether(
            name='activity',
            index_together=set([]),
        ),
        migrations.RemoveField(
            model_name='activity',
            name='region',
        ),
        migrations.DeleteModel(
            name='Activity',
        ),
        migrations.AlterUniqueTogether(
            name='stats',
            unique_together=set([('region', 'type')]),
        ),
        migrations.AlterIndexTogether(
            name='stats',
            index_together=set([('region', 'type')]),
        ),
    ]
