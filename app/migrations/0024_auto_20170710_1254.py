# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0023_portpool_namespace'),
    ]

    operations = [
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=60)),
                ('value', models.CharField(max_length=60)),
                ('editable', models.BooleanField(default=True)),
                ('node', models.ForeignKey(to='app.Node')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='label',
            unique_together=set([('node', 'key')]),
        ),
        migrations.AlterIndexTogether(
            name='label',
            index_together=set([('node', 'key')]),
        ),
    ]
