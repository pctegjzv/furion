# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SlaveActivity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('private_ip', models.CharField(max_length=32)),
                ('slave_id', models.CharField(unique=True, max_length=64, db_index=True)),
                ('state', models.CharField(default=b'RUNNING', max_length=16, choices=[(b'RUNNING', b'Running'), (b'UNKNOWN', b'Unknown'), (b'REMOVED', b'Removed')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('region', models.ForeignKey(to='app.Region')),
            ],
        ),
    ]
