# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20160401_0736'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('type', models.CharField(max_length=16, choices=[(b'MESOS_METRICS', b'Mesos Metrics')])),
                ('data', jsonfield.fields.JSONField(default={}, blank=True)),
                ('region', models.ForeignKey(to='app.Region')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='activity',
            unique_together=set([('region', 'type')]),
        ),
        migrations.AlterIndexTogether(
            name='activity',
            index_together=set([('region', 'type')]),
        ),
    ]
