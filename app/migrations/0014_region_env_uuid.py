# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20160627_1151'),
    ]

    operations = [
        migrations.AddField(
            model_name='region',
            name='env_uuid',
            field=models.CharField(max_length=36, null=True, db_index=True),
        ),
    ]
