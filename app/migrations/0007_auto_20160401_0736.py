# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_auto_20160330_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='state',
            field=models.CharField(default=b'RUNNING', max_length=16, choices=[(b'DEPLOYING', b'Deploying'), (b'RUNNING', b'Running'), (b'SHUTTING_DOWN', b'Shutting Down'), (b'STOPPED', b'Stopped'), (b'STOPPING', b'Stopping'), (b'REMOVED', b'Removed'), (b'UNKNOWN', b'Unknown')]),
        ),
        migrations.AlterField(
            model_name='region',
            name='state',
            field=models.CharField(default=b'RUNNING', max_length=16, choices=[(b'PREPARING', b'Preparing'), (b'DEPLOYING', b'Deploying'), (b'RUNNING', b'Running'), (b'STOPPED', b'Stopped'), (b'ERROR', b'Error')]),
        ),
    ]
