# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields
import app.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.CharField(default=app.models.gen_uuid, max_length=36, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=36)),
                ('display_name', models.CharField(max_length=36)),
                ('namespace', models.CharField(default='', max_length=36, blank=True)),
                ('state', models.CharField(default='RUNNING', max_length=16, choices=[('PREPARING', 'Preparing'), ('DELETING', 'Deploying'), ('RUNNING', 'Running'), ('STOPPED', 'Stopped'), ('FAILED', 'Failed'), ('DELETING', 'Deleting'), ('UPDATING', 'Updating')])),
                ('attr', jsonfield.fields.JSONField()),
                ('features', jsonfield.fields.JSONField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='region',
            unique_together=set([('namespace', 'name')]),
        ),
        migrations.AlterIndexTogether(
            name='region',
            index_together=set([('namespace', 'name')]),
        ),
    ]
