# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields
import app.models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_region_env_uuid'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComponentVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=36)),
                ('version', models.CharField(max_length=64)),
                ('incarnation', models.IntegerField(default=1)),
                ('description', models.CharField(default='', max_length=1024, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='RegionSetting',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('id', models.CharField(default=app.models.gen_uuid, max_length=36, serialize=False, primary_key=True)),
                ('master_node', jsonfield.fields.JSONField(default=dict, blank=True)),
                ('slave_node', jsonfield.fields.JSONField(default=[], blank=True)),
                ('region_location', models.CharField(max_length=64)),
                ('image_location', models.CharField(max_length=64)),
                ('user_settings', jsonfield.fields.JSONField(default={}, blank=True)),
                ('user_registries', jsonfield.fields.JSONField(default={}, blank=True)),
                ('cluster_settings', jsonfield.fields.JSONField(default={}, blank=True)),
                ('cluster_template_type', models.CharField(max_length=16)),
                ('is_update_blocked', models.BooleanField(default=False)),
                ('region', models.ForeignKey(to='app.Region')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='componentversion',
            name='region_settings',
            field=models.ForeignKey(to='app.RegionSetting'),
        ),
        migrations.AlterUniqueTogether(
            name='componentversion',
            unique_together=set([('region_settings', 'name')]),
        ),
        migrations.AlterIndexTogether(
            name='componentversion',
            index_together=set([('region_settings', 'name')]),
        ),
    ]
