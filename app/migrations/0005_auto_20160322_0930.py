# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20160321_0802'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='slaveactivity',
            unique_together=set([('region', 'private_ip')]),
        ),
        migrations.AlterIndexTogether(
            name='slaveactivity',
            index_together=set([('region', 'private_ip')]),
        ),
        migrations.RemoveField(
            model_name='slaveactivity',
            name='slave_id',
        ),
    ]
