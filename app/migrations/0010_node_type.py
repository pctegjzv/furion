# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0009_auto_20160420_0733'),
    ]

    operations = [
        migrations.AddField(
            model_name='node',
            name='type',
            field=models.CharField(default=b'SLAVE', max_length=16, choices=[(b'SLAVE', b'Slave'), (b'MANAGER', b'Manager'), (b'EXEC', b'Exec'), (b'LB', b'lb'), (b'LOG', b'Log'), (b'SCALE', b'Scale')]),
        ),
    ]
