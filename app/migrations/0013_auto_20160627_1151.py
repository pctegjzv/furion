# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_auto_20160624_0514'),
    ]

    operations = [
        migrations.CreateModel(
            name='PortPool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('port', models.IntegerField(default=0)),
                ('used', models.BooleanField(default=False)),
                ('public_ip', models.CharField(max_length=32, null=True, db_index=True)),
                ('private_ip', models.CharField(max_length=32, null=True)),
                ('region_name', models.CharField(max_length=64, null=True)),
                ('type', models.CharField(db_index=True, max_length=64, choices=[(b'SSH', b'SSH'), (b'Mapping', b'Mapping')])),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='portpool',
            unique_together=set([('public_ip', 'private_ip', 'port')]),
        ),
    ]
