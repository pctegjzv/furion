# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields
import app.models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_slaveactivity_resources'),
    ]

    operations = [
        migrations.CreateModel(
            name='Node',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('id', models.CharField(default=app.models.gen_uuid, max_length=36, serialize=False, primary_key=True)),
                ('private_ip', models.CharField(max_length=32)),
                ('instance_id', models.CharField(max_length=128)),
                ('state', models.CharField(default=b'RUNNING', max_length=16, choices=[(b'PENDING', b'Pending'), (b'RUNNING', b'Running'), (b'SHUTTING_DOWN', b'Shutting Down'), (b'STOPPED', b'Stopped'), (b'STOPPING', b'Stopping'), (b'TERMINATED', b'Terminated')])),
                ('attr', jsonfield.fields.JSONField(default=dict)),
                ('region', models.ForeignKey(to='app.Region')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='node',
            unique_together=set([('region', 'private_ip')]),
        ),
        migrations.AlterIndexTogether(
            name='node',
            index_together=set([('region', 'private_ip')]),
        ),
    ]
