# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import app.models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0027_auto_20171120_0431'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mirror',
            fields=[
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('id', models.CharField(default=app.models.gen_uuid, max_length=36, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=36)),
                ('display_name', models.CharField(max_length=36)),
                ('description', models.CharField(default='', max_length=1024)),
                ('flag', models.CharField(max_length=36)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='region',
            name='mirror',
            field=models.ForeignKey(to='app.Mirror', null=True),
        ),
    ]
