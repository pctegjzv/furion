# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0019_auto_20161028_0809'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='state',
            field=models.CharField(default=b'RUNNING', max_length=16, choices=[(b'DEPLOYING', b'Deploying'), (b'RUNNING', b'Running'), (b'SHUTTING_DOWN', b'Shutting Down'), (b'STOPPED', b'Stopped'), (b'STOPPING', b'Stopping'), (b'REMOVED', b'Removed'), (b'UNKNOWN', b'Unknown'), (b'OFFLINE', b'Offline'), (b'ERROR', b'Error'), (b'CRITICAL', b'Critical'), (b'PREPARING', b'Preparing')]),
        ),
        migrations.AlterField(
            model_name='node',
            name='type',
            field=models.CharField(default=b'SLAVE', max_length=16, choices=[(b'SLAVE', b'Slave'), (b'SYS', b'Sys'), (b'EMPTY', b'Empty'), (b'SYS', b'Sys'), (b'SLAVE', b'Slave')]),
        ),
        migrations.AlterField(
            model_name='region',
            name='container_manager',
            field=models.CharField(default=b'NONE', max_length=16, choices=[(b'NONE', b'NONE'), (b'MESOS', b'MESOS'), (b'SWARM', b'SWARM'), (b'LEGACY', b'LEGACY'), (b'K8S', b'K8S')]),
        ),
        migrations.AlterField(
            model_name='region',
            name='state',
            field=models.CharField(default=b'RUNNING', max_length=16, choices=[(b'PREPARING', b'Preparing'), (b'DEPLOYING', b'Deploying'), (b'RUNNING', b'Running'), (b'STOPPED', b'Stopped'), (b'ERROR', b'Error'), (b'CRITICAL', b'Critical')]),
        ),
        migrations.AlterField(
            model_name='stats',
            name='type',
            field=models.CharField(max_length=16, choices=[(b'MESOS_METRICS', b'Mesos Metrics'), (b'SWARM_METRICS', b'Swarm Metrics'), (b'K8S_METRICS', b'K8S Metrics')]),
        ),
    ]
