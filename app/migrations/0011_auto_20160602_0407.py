# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_node_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='type',
            field=models.CharField(default=b'SLAVE', max_length=16, choices=[(b'SLAVE', b'Slave'), (b'SYS', b'Sys')]),
        ),
    ]
