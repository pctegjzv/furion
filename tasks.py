# -*- coding: utf-8 -*-

import os

from invoke import task


def status(s):
    """Prints things in bold."""
    print("\033[1m{0}\033[0m".format(s))


@task
def lint(ctx):
    """
    Lint code using flake8 tools.
    """
    status("linting code ...")
    ctx.run("flake8 --show-source --statistics --count")


@task
def install(ctx):
    """
    Install furion as package for local test.
    """
    status("installing furion ...")
    ctx.run("pip install --trusted-host pypi.alauda.io "
            "--extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/"
            "--editable .")
    status("furion installed.")


@task(lint)
def test(ctx, verbose=False):
    """
    Run furion v2 test cases.
    """
    status("begining to run test cases ...")
    if verbose:
        ctx.run("pytest tests_v2 -vv")
    else:
        ctx.run("pytest tests_v2 -v")