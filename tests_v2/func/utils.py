import os
import json
import collections

from app.models import Region, Mirror


here = os.path.abspath(os.path.dirname(__file__))


def load_json_data(path):
    with open(os.path.join(here, "data/{}".format(path))) as f:
        data = json.load(f)
    return data


def create_region(api_client, monkeypatch, region_data, mirror_data=None):
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: region_data["attr"]["kubernetes"]["version"])
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (False, None))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.add_namespace_label", lambda self, ns, lb: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace", lambda *args, **kwargs: True)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_cluster_role_binding", lambda *args, **kwargs: True)

    r = api_client.post("/v2/regions/", region_data, format="json")
    region = Region.objects.get(id=r.data["id"])
    if mirror_data:
        mirror = Mirror.objects.create(**mirror_data)
        region.mirror = mirror
        region.save()
    return region.id


def create_feature(api_client, monkeypatch, region_id, feature_name, feature_data):
    r = api_client.post("/v2/regions/{}/features/{}".format(region_id, feature_name),
                        feature_data, format="json")
    return r


def assert_dict_struct_equal(dict1, dict2):
    assert isinstance(dict1, collections.Mapping)
    assert isinstance(dict2, collections.Mapping)
    for k in dict1:
        assert k in dict2
        if isinstance(dict1[k], collections.Mapping):
            assert_dict_struct_equal(dict1[k], dict2[k])