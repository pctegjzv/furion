import pytest


@pytest.mark.django_db(transaction=True)
def test_list_regions(request, db_with_example_region, api_client, list_regions_example, monkeypatch):
    id = request.getfixturevalue("db_with_example_region")
    expected_result = list_regions_example
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: expected_result["attr"]["kubernetes"]["version"])
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (False, None))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.add_namespace_label", lambda self, ns, lb: None)

    r = api_client.get("/v2/regions/?uuids={}".format(id))
    for d in r.data:
        del d["id"]
        del d["created_at"]
        del d["updated_at"]
    for d in expected_result:
        del d["id"]
        del d["created_at"]
        del d["updated_at"]
    assert r.data == expected_result


@pytest.mark.django_db(transaction=True)
def test_list_regions_and_mirror(request, db_with_example_region_and_mirror, api_client, list_regions_with_mirror_example, monkeypatch):
    id = request.getfixturevalue("db_with_example_region_and_mirror")
    expected_result = list_regions_with_mirror_example
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: expected_result["attr"]["kubernetes"]["version"])
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (False, None))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.add_namespace_label", lambda self, ns, lb: None)

    r = api_client.get("/v2/regions/?uuids={}".format(id))
    for d in r.data:
        del d["id"]
        del d["created_at"]
        del d["updated_at"]
        for reg in d["mirror"]["regions"]:
            del reg["id"]
            del reg["created_at"]
        del d["mirror"]["id"]
    for d in expected_result:
        del d["id"]
        del d["created_at"]
        del d["updated_at"]
        for reg in d["mirror"]["regions"]:
            del reg["id"]
        del d["mirror"]["id"]
    assert r.data == expected_result