import pytest


def test_check_region_works(api_client, check_region_example, monkeypatch):
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: "1.9.6")

    post_data, expected_result = check_region_example
    r = api_client.post("/v2/regions/version-check/", post_data, format="json") 
    assert r.data == expected_result