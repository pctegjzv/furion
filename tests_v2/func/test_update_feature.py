import pytest
import uuid

from .utils import load_json_data as _json
from .utils import create_region, create_feature, assert_dict_struct_equal


update_feature_examples = {
    "update_log_config": (
        "log",
        _json("example_create_log_feature_post.json"),
        _json("example_update_log_feature_post.json"),
        _json("example_update_log_feature_response.json"),
    ),
    "change_metric_type": (
        "metric",
        _json("example_create_metric_feature_post.json"),
        _json("example_update_metric_type_feature_post.json"),
        _json("example_update_metric_type_feature_response.json"),
    )
}
update_feature_data = [d for _, d in update_feature_examples.items()]
feature_ids = [i for i in update_feature_examples]
valid_region_post_data = _json("example_create_region_post.json")


class Obj:
    pass


class MockApplicationOperator:

    @classmethod
    def get_template(*args, **kwargs):
        return {
            "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
            "is_active": True,
            "name": "log",
            "display_name": "official log",
            "description": "",
            "versions": [{
                "uuid":"82435da1-4915-4d32-87c2-5e1c984a6689",
                "values_yaml_content": "the yaml content"
            }]
        }

    @classmethod
    def list_public_templates(*args, **kwargs):
        return {
            "log": {
                "resource_actions": [
                    "public_helm_template_repo:view",
                ],
                "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
                "is_active": True,
                "name": "log",
                "display_name": "official log",
                "description": "",
                "icon": "http://icon.com",
                "installed_app_num": 5
            },
            "metric": {
                "resource_actions": [
                    "public_helm_template_repo:view",
                ],
                "uuid": "99435da1-4915-4d32-87c2-5e1c984a6689",
                "is_active": True,
                "name": "metric",
                "display_name": "official metric",
                "description": "",
                "icon": "http://icon.com",
                "installed_app_num": 5
            }
        }

    @classmethod
    def create_template_app(*arg, **kwargs):
        d = Obj()
        d.data = {
            "uuid": str(uuid.uuid4()),
            "name": "official-log",
            "status": "Running"
        }
        return d

    @classmethod
    def delete_application_instance(*arg, **kwargs):
        return None


class MockIntegrationOperator:

    @classmethod
    def get_integration_instance(cls, *args, **kwargs):
        d = Obj()
        d.data = {
            "uuid": "91bf91b7-bfdb-4757-8a3b-535b6ad7a25e",
            "type": "prometheus",
            "enabled": True
        }
        return d

@pytest.fixture(params=update_feature_data, ids=feature_ids)
def update_feature_example(request, api_client, monkeypatch):
    region_id = create_region(api_client, monkeypatch, valid_region_post_data)
    feature_name, feature_data, feature_update_data, expected_data = request.param
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_secret_data", lambda *args, **kwargs: {})
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_configmap_data", lambda *args, **kwargs: {})
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_dockercfg_secret", lambda *args, **kwargs: "abcdefg")
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_secret_data", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_configmap_data", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.apply_dockercfg_secret", lambda *args, **kwargs: None)
    monkeypatch.setattr("operators.integration.IntegrationOperator.get_integration_instance", MockIntegrationOperator.get_integration_instance)
    monkeypatch.setattr("operators.application.ApplicationOperator.get_template", MockApplicationOperator.get_template)
    monkeypatch.setattr("operators.application.ApplicationOperator.list_public_templates", MockApplicationOperator.list_public_templates)
    monkeypatch.setattr("operators.application.ApplicationOperator.create_template_app", MockApplicationOperator.create_template_app)
    create_feature(api_client, monkeypatch, region_id, feature_name, feature_data)
    return (region_id, feature_name, feature_update_data, expected_data)


@pytest.mark.django_db(transaction=True)
def test_update_feature(request, update_feature_example, api_client, monkeypatch):
    region_id, feature_name, post_data, expected_data = request.getfixturevalue("update_feature_example")
    monkeypatch.setattr("operators.application.ApplicationOperator.delete_application_instance", MockApplicationOperator.delete_application_instance)

    r = api_client.put("/v2/regions/{}/features/{}".format(region_id, feature_name),
                        post_data, format="json")
    assert_dict_struct_equal(expected_data, r.data)