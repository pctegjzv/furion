import os
import json

import pytest
from rest_framework.test import APIClient

from app.models import Region, Mirror

from .utils import load_json_data as _json
from .utils import create_region


here = os.path.abspath(os.path.dirname(__file__))


@pytest.fixture()
def api_client():
    return APIClient()


@pytest.fixture()
def check_region_example():
    post_data = _json("example_check_region_post.json")
    response_data = _json("example_check_region_response.json")
    return (post_data, response_data)


@pytest.fixture()
def create_region_example():
    post_data = _json("example_create_region_post.json")
    response_data = _json("example_create_region_response.json")
    return (post_data, response_data)


@pytest.fixture()
def get_region_example():
    response_data = _json("example_retrieve_region_response.json")
    return response_data


@pytest.fixture()
def list_regions_example():
    response_data = _json("example_list_regions_response.json")
    return response_data


@pytest.fixture()
def region_registered_error():
    cluster_name = "test"
    return (cluster_name, {
      "errors": [
        {
          "source": "1004",
          "message": "This cluster is already registered,cluster name is {}.".format(cluster_name),
          "code": "RegionRegistered"
        }
      ]
    })


@pytest.fixture()
def mirror_example(get_region_example):
    return {
        "name": "test", "display_name": "test", "flag": "test",
    }


@pytest.fixture()
def get_region_with_mirror_example(get_region_example, mirror_example):
    new_region_example = get_region_example.copy()
    new_region_example["mirror"] = mirror_example
    new_region_example["mirror"]["id"] = "123123"
    new_region_example["mirror"]["regions"] = [{
        "id": get_region_example["id"],
        "name": get_region_example["name"],
        "display_name": get_region_example["display_name"]
    }]
    return new_region_example


@pytest.fixture()
def list_regions_with_mirror_example(get_region_with_mirror_example):
    return [get_region_with_mirror_example]


@pytest.fixture()
def db_with_example_region(api_client, create_region_example, monkeypatch):
    region_id = create_region(api_client, monkeypatch, create_region_example[0])
    return region_id


@pytest.fixture()
def db_with_example_region_and_mirror(api_client, create_region_example, mirror_example, monkeypatch):
    region_id = create_region(api_client, monkeypatch, create_region_example[0], mirror_data=mirror_example)
    return region_id