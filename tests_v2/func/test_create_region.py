import json

import pytest


@pytest.mark.django_db(transaction=True)
def test_create_region_works(api_client, create_region_example, monkeypatch):
    post_data, expected_result = create_region_example
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: post_data["attr"]["kubernetes"]["version"])
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (False, None))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.add_namespace_label", lambda self, ns, lb: None)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace", lambda *args, **kwargs: True)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_cluster_role_binding", lambda *args, **kwargs: True)

    r = api_client.post("/v2/regions/", post_data, format="json") 
    for field in ["id", "created_at", "updated_at"]:
        del r.data[field]
        del expected_result[field]
    assert r.data == expected_result


def test_create_region_registered(api_client, region_registered_error, create_region_example, monkeypatch):
    cluster_name, error_data = region_registered_error
    post_data = create_region_example[0]
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.get_version", lambda self: "1.10")
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace_label", lambda self, ns: (True, cluster_name))
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_namespace", lambda *args, **kwargs: True)
    monkeypatch.setattr("operators.kubernetes.KubernetesManager.check_cluster_role_binding", lambda *args, **kwargs: True)

    r = api_client.post("/v2/regions/", post_data, format="json") 
    assert json.loads(r.content) == error_data