import pytest

from operators import utils


def test_md5sum_return_str_type():
    """"Test md5sum() return str type."""
    content = "this is test content"
    res = utils.md5sum(content)
    assert isinstance(res, str)


version_to_test = [
    ("1.9.1", "1.9.1", True),
    ("1.9.1", "1.9", True),
    ("1.9.1", "1.9.2", False),
    ("invalid_ver", "1.9.2", False),
    ("invalid_ver", "invalid_ver", False)
]
version_ids = [
    "{}-{}-{}".format(v[0], v[1], "matched" if v[2] else "unmatched")
    for v in version_to_test
]
@pytest.mark.parametrize("ver1, ver2, matched", version_to_test, ids=version_ids)
def test_check_versions_matched(ver1, ver2, matched):
    """Test check_versions_matched() worked as expected."""
    assert utils.check_versions_matched(ver1, ver2) is matched