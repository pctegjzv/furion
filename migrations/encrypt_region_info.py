#!/usr/bin/env python
# -*- coding: utf-8 -*-
# flake8: noqa

import sys
import os

import django
from Crypto.Cipher import AES
from django.utils.importlib import import_module
import json

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'furion.settings')
django.setup()
import_module('furion')

from django.conf import settings
from alauda.common.func import pad_str
from app.models import Region
from controls.region import _xcode_region_info
from util.func import _prefix


if __name__ == '__main__':
    print 'migrate begins'
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    regions = Region.objects.all()
    total_count = regions.count()
    count = 0
    lost_ones = []
    reverse = 'reverse' in sys.argv
    for region in regions:
        try:
            if not reverse:
                feature_str = json.dumps(region.features)
                attr_str = json.dumps(region.attr)
                if _prefix in feature_str or _prefix in attr_str:
                    print "Warning: region {} has already migrated or has string '__encrypted' inside feature/attr".format(region.name)
                    print "Ignore and continue"
                    lost_ones.append((region.id, 'has __encrypted inside'))
                    continue
                _xcode_region_info('encode', region.features, region.attr)
                print '({0:<2}/{0:<2})                            region {0:>0}/{0:>0} encrypted'.format(count, total_count, region.name, region.namespace)
            else:
                _xcode_region_info('decode', region.features, region.attr)
        except Exception as e:
            print '({0:<2}/{0:<2})                       region {0:>0}/{0:>0} encrypt failed'.format(count, total_count, region.name, region.namespace)
            lost_ones.append((region.id, e.message))
        count = count + 1
        region.save()

    print 'total rows: ', count
    if len(lost_ones):
        print 'total lost: ', len(lost_ones)
        print 'lost ones: ', lost_ones
        print 'migrate failed'
    else:
        print 'migrate success'
