#!/usr/bin/env python
# -*- coding: utf-8 -*-
import importlib

__author__ = 'Hang Yan'


def get_cloud(args):
    module = importlib.import_module('cloud.{}'.format(args['name'].lower()))
    return getattr(module, 'Manager')(args)
