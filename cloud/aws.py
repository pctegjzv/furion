#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

import boto3
from botocore.exceptions import ClientError

from furion.exception import FurionException
from util import const
from util.func import check_keys, check_integer
from django.conf import settings

__author__ = 'Hang Yan'

logging.getLogger('botocore').setLevel(logging.WARNING)
LOG = logging.getLogger(__name__)


class Manager(object):
    def __init__(self, args):
        self.session = boto3.session.Session(region_name=args['region_id'],
                                             aws_access_key_id=args['access_key'],
                                             aws_secret_access_key=args['secret_access_key'])
        self.ec2_client = self.session.client('ec2')
        self.ec2_res = self.session.resource('ec2')

    def get_instance_info(self, private_ip):
        return self.__get_instance(Filters=[
            {
                'Name': 'private-ip-address',
                'Values': [private_ip]
            }
        ])

    def _get_instance_by_id(self, inst_id):
        return self.__get_instance(InstanceIds=[inst_id])

    def __get_instance(self, **args):
        res = self.ec2_client.describe_instances(**args)
        data = res['Reservations']
        if not data:
            raise FurionException('resource_not_exist', message="Cannot find ec2 instance on aws. | {}".format(args))
        return self._format_instance_info(data[0]['Instances'][0])

    def delete_instance(self, inst_id):
        try:
            self.ec2_client.terminate_instances(InstanceIds=[inst_id])
        except ClientError as e:
            if e.response['Error'].get('Code', 'Unknown') == 'InvalidInstanceID.NotFound':
                return
            raise FurionException('iaas_api_error', message='AWS delete instance error! | {}'.format(e))

    def _change_instance_state(self, inst_id, cur_state, action):
        info = self._get_instance_by_id(inst_id)
        if info['state'] != cur_state:
            raise FurionException('resource_state_conflict',
                                  message="Instance is not {}, cannot {} it".format(cur_state,
                                                                                    action))
        return getattr(self.ec2_client, '{}_instances'.format(action))(InstanceIds=[inst_id])

    def stop_instance(self, inst_id):
        return self._change_instance_state(inst_id, const.STATE_RUNNING, 'stop')

    def start_instance(self, inst_id):
        return self._change_instance_state(inst_id, const.STATE_STOPPED, 'start')

    def _add_tag_for_res(self, res_id, tags):
        try:
            self.ec2_client.create_tags(Resources=[res_id], Tags=tags)
        except Exception as e:
            LOG.warning("Add tags for resource error. | {} {}".format(res_id, e))

    def list_key_pairs(self):
        try:
            res = self.ec2_client.describe_key_pairs()
            return {
                "key_pairs": [x['KeyName'] for x in res['KeyPairs']]
            }
        except Exception as e:
            return FurionException('iaas_api_error', message="Get aws key-pairs list error! | {}".format(e))

    def get_instance_types(self):
        """ return dict
            {
                "instance_types": [
                    "xxx",
                ]
          }
        """
        return settings.CLOUD_SPEC_CONFIGS.get('AWS')

    def _valid_args(self, args, num, volume_size):
        required_keys = ['image_id', 'num', 'instance_type', 'security_groups', 'subnet_id',
                         'user_data']
        args = args.copy()
        args.update({'num': num})
        check_keys(required_keys, args, request_data=True)
        schema_list = [
            {
                "num": num,
                "validator": {
                    "min": 0,
                }
            }]
        if volume_size:
            schema_list.append({
                "volume_size": volume_size,
                "validator": {
                    "min": 8,
                    "max": 16384
                }
            })
        check_integer(schema_list)

    def create_instances(self, args, num, volume_size=''):
        self._valid_args(args, num, volume_size)
        try:
            data = {
                "ImageId": args['image_id'],
                "MinCount": num,
                "MaxCount": num,
                "InstanceType": args['instance_type'],
                "SecurityGroupIds": args['security_groups'],
                "SubnetId": args['subnet_id'],
                "UserData": args['user_data'],
            }
            if 'iam_instance_profile' in args and args['iam_instance_profile']:
                data['IamInstanceProfile'] = args['iam_instance_profile']  # IamInstanceProfile={'Name': 'string'}

            if volume_size:
                data["BlockDeviceMappings"] = [
                    {
                        "DeviceName": "/dev/sda1",
                        "Ebs": {
                            "VolumeSize": int(volume_size),
                            "DeleteOnTermination": True,
                            "VolumeType": "gp2",
                        },
                    }
                ]
            if 'ssh_key_name' in args:
                data['KeyName'] = args['ssh_key_name']
            resp = self.ec2_res.create_instances(**data)
            insts = [self._parse_instance_info(x, volume_size or '') for x in resp]
            [self._add_tag_for_res(x['instance_id'], [{'Key': 'Name', 'Value': args['name_tag']}])
             for x in insts]
            return insts
        except Exception as e:
            raise FurionException('iaas_api_error', message='Aws create instances failed. | {}'.format(e))

    def disable_instance_source_check(self, instance_id):
        """
        modify the instance attributes ,docs see
        https://docs.aws.amazon.com/AWSEC2/latest/APIReference/API_ModifyInstanceAttribute.html
        http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#service-resource
        :return:
        """
        try:
            data = {
                'InstanceId': instance_id,
                'SourceDestCheck': {
                    'Value': False
                }
            }
            resp = self.ec2_client.modify_instance_attribute(**data)
            if not resp:
                LOG.error('Aws modify instances attribute failed for instance {}'.format(instance_id))
        except Exception as e:
            LOG.error('Aws modify instances attribute failed for instance {} | {}'.format(instance_id, e))
            # raise FurionException('iaas_api_error', message='Aws modify instances attribute failed. | {}'.format(e))

    @staticmethod
    def _parse_instance_info(instance, init_volume_size=''):
        return {
            'instance_id': instance.instance_id,
            'private_ip': instance.private_ip_address,
            'state': const.STATE_DEPLOYING,
            'attr': {
                'instance_type': instance.instance_type,
                'public_ip': instance.public_ip_address,
                'init_volume_size': init_volume_size
            }
        }

    @staticmethod
    def _format_instance_info(info):
        return {
            'instance_id': info['InstanceId'],
            'private_ip': info['PrivateIpAddress'],
            'state': const.NODE_STATE_MAP.get(info['State']['Name'], const.STATE_UNKNOWN),
            'attr': {
                'instance_type': info['InstanceType'],
                'public_ip': info.get('PublicIpAddress', ''),
            }
        }
