#!/usr/bin/env python
import json
import requests
import logging
import random
import base64

from util import const
from furion.exception import FurionException

LOG = logging.getLogger(__name__)


class Manager():
    def __init__(self, kw):
        self.loginurl = kw['loginurl']
        self.apiurl = kw['apiurl']
        self.uname = kw['uname']
        self.passwd = kw['passwd']
        self.domainname = kw['domain_name']
        self.otoken = kw.get('otoken')
        self.ProjectId = None
        self.Token = None

    def __gettoken(self):
        uname = self.uname
        domainid = self.domainname
        passwd = self.passwd
        loginurl = self.loginurl
        loginheader = {"Content-Type": "application/json"}
        logindata = {
            "auth": {
                "identity": {
                    "methods": ["password"],
                    "password": {
                        "user": {
                            "name": uname,
                            "domain": {"name": domainid},
                            "password": passwd
                        }
                    }
                }
            }
        }
        r = requests.post(loginurl, json=logindata, headers=loginheader)
        temp = json.loads(r.text)
        if r.status_code != 201:
            raise FurionException('request_error', message='login fail | {}'.format(r.status_code))
        self.Token = r.headers['X-Subject-Token']
        self.ProjectId = temp['token']['project']['id']

    def __judge(self):
        if self.otoken is None:
            self.__gettoken()
            loginheader = {"Content-Type": "application/json", "X-Auth-Token": self.Token}
        else:
            loginheader = {"Content-Type": "application/json", "X-Auth-Token": self.otoken}
            r = requests.get(self.apiurl, headers=loginheader)
            if r.status_code != 200:
                self.__gettoken()
                loginheader = {"Content-Type": "application/json", "X-Auth-Token": self.Token}
        return loginheader

    def _return_token(self):
        if self.Token is not None:
            token = self.Token
        else:
            token = self.otoken
        return token

    def get_instance_info(self, inst_id):
        return self.__get_instance(InstanceIds=[inst_id])

    def _get_instance_by_id(self, inst_id):
        return self.__get_instance(InstanceIds=[inst_id])

    def __get_instance(self, **args):
        loginheader = self.__judge()
        if 'InstanceIds' in args:
            for x in args['InstanceIds']:
                apiurl = self.apiurl + "/servers/" + x
                r = requests.get(apiurl, headers=loginheader)
                data = json.loads(r.text)
                if not data:
                    raise FurionException(
                        'resource_not_exist',
                        message='cannot find instance on openstack. | {}'.format(args))
                return self._format_instance_info(data['server'])
        else:
            apiurl = self.apiurl + "/servers"
            r = requests.get(apiurl, headers=loginheader)
            data = json.loads(r.text)
            if not data:
                raise FurionException(
                    'resource_not_exist',
                    message='cannot find instance on openstack. | {}'.format(r.status_code))
            return data['servers']

    def _format_instance_info(self, info):
        if not info['addresses']:
            ip = '169.254.0.' + str(random.randint(1, 254))
            tmp = ip
            LOG.info('=====fake ip: {}'.format(tmp))
        else:
            for y in info['addresses'].itervalues():
                for x in y:
                    tmp = x['addr']
        return {
            'instance_id': info['id'],
            'private_ip': tmp,
            'state': const.NODE_STATE_MAP.get(info['status'], const.STATE_UNKNOWN),
            'attr': {
                'instance_type': info['flavor']['id']
            }
        }

    def _parse_instance_info(self, inst, init_volume_size=''):
        return self._get_instance_by_id(inst['id'])

    def _change_instance_state(self, inst_id, action, cur_state='UNKNOWN'):
        loginheader = self.__judge()
        if action == 'stop' or action == 'start':
            info = self._get_instance_by_id(inst_id)
            if info['state'] != cur_state:
                raise FurionException(
                    'resource_state_conflict',
                    message="Instance is not {}, cannot {} it".format(cur_state, action))
            apiurl = self.apiurl + '/servers/' + inst_id + '/action'
            key = "os-" + action
            payload = {key: None}
            r = requests.post(apiurl, headers=loginheader, json=payload)
        elif action == 'delete':
            apiurl = self.apiurl + '/servers/' + inst_id
            r = requests.delete(apiurl, headers=loginheader)
            if r.status_code != 204:
                raise FurionException('change_instance_fail', message="{} fail".format(action))

    def stop_instance(self, inst_id):
        return self._change_instance_state(inst_id, 'stop', 'RUNNING')

    def start_instance(self, inst_id):
        return self._change_instance_state(inst_id, 'start', 'SHUTTING_DOWN')

    def delete_instance(self, inst_id):
        return self._change_instance_state(inst_id, 'delete')

    def create_instances(self, paras, num, volume_size=''):
        data = {
            "server": {
                "name": paras['name_tag'],
                "min_count": num,
                "max_count": num,
                "imageRef": paras['image_id'],
                "flavorRef": paras['instance_type'],
                "networks": [{"uuid": paras['subnet_id']}],
                "adminPass": 'alauda@123',
                "user_data": ""
            }
        }
        if 'ssh_key_name' in paras:
            data['server']['key_name'] = paras['ssh_key_name']
        else:
            raise FurionException('request_error', message="need ssh key")

        if 'user_data' in paras:
            udata = paras['user_data']
            udata = base64.b64encode(udata)
            LOG.info('=====user data: {}'.format(udata))
            data['server']['user_data'] = udata

        if num > 1:
            data['server']['return_reservation_id'] = 'True'

        loginheader = self.__judge()
        apiurl = self.apiurl + "/servers"

        try:
            r = requests.post(apiurl, json=data, headers=loginheader)
            if r.status_code != 202:
                raise FurionException('status code is {},not 202, please check payload {}'.
                                      format(r.status_code, data))
            resp = json.loads(r.text)
            # time.sleep(5)
            if 'reservation_id' in resp:
                r = requests.get(apiurl, headers=loginheader, params=resp)
                resp = json.loads(r.text)
                inst = [self._parse_instance_info(x) for x in resp['servers']]
            else:
                tmps = self._parse_instance_info(resp['server'])
                inst = [tmps]
            return inst
        except Exception as e:
            raise FurionException(
                'iaas_api_error',
                message='openstack create instances failed. | {}'.format(e))

    def list_key_pairs(self):
        loginheader = self.__judge()
        apiurl = self.apiurl + "/os-keypairs"
        try:
            r = requests.get(apiurl, headers=loginheader)
            tmp = json.loads(r.text)
            t = {"key_pairs": []}
            for i in tmp['keypairs']:
                t['key_pairs'].append(i['keypair']['name'])
            return t
        except Exception as e:
            return FurionException(
                'iaas_api_error',
                message="Get openstack key-pairs list error! | {}".format(e))

    def get_instance_types(self):
        """ return dict
            {
                "instance_type_x": "id",
                "instance_type_y": "id",
                "instance_type_z": "id",
                ...
            }
        """
        loginheader = self.__judge()
        apiurl = self.apiurl + "/flavors"
        try:
            r = requests.get(apiurl, headers=loginheader)
            tmp = json.loads(r.text)
            t = {}
            for i in tmp['flavors']:
                x, y = i['id'], i['name']
                t[y] = x
            return t
        except Exception as e:
            return FurionException(
                'iaas_api_error',
                message="Get openstack instance type list error! | {}".format(e))

    def _get_images(self):
        loginheader = self.__judge()
        apiurl = self.apiurl + "/images"
        try:
            r = requests.get(apiurl, headers=loginheader)
            return r.text
        except Exception as e:
            return FurionException(
                'iaas_api_error',
                message="Get openstack key-pairs list error! | {}".format(e))


if __name__ == '__main__':

    def readf():
        f = open('./token', 'r')
        otoken = f.read()
        f.close()
        return otoken

    def write(token):
        f = open('./token', 'w')
        f.write('%s' % token)
        f.close()

    ot = readf()
    if ot == "" or ot == "None":
        ot = None

    dic = {
        "loginurl": 'http://10.66.142.2:5000/v3/auth/tokens?nocatalog',
        "apiurl": "http://10.66.142.2:8774/v2/",
        "uname": "paas_docker",
        "passwd": "dockerosm@1",
        "domain_name": "YuTong_BUS",
        "otoken": ot,
        "projectid": "21cd2cb5345c4054a52892a8bfb76677"
    }
    para = {
        "name_tag": "test-1",
        "image_id": "f6c7aaa6-f740-44d1-852c-21e20d729c40",
        "instance_type": "79",
        "subnet_id": "05356f77-a123-4f06-b479-309b4d36c409",
        "ssh_key_name": "key-1"
    }

    abb = Manager(dic)
    tmp = abb.create_instances(para, 1)
    print tmp
    token = abb._return_token()
    if ot != token:
        write(token)
