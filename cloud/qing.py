#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging

from furion.exception import FurionException
from util import const
import qingcloud.iaas
from util.func import check_integer, check_keys
from django.conf import settings
import base64

__author__ = 'Lei Gong <lgong@alauda.io>'

LOG = logging.getLogger(__name__)

ALREADY_EXISTS = 2110
SUCCESS = 0

UNALLOCATED_IP = const.QING_PRIVATE_IP_PREFIX + '({})'


class Manager(object):
    def __init__(self, args):
        try:
            self.session = qingcloud.iaas.connect_to_zone(
                zone=str(args['region_id']),
                access_key_id=str(args['access_key']),
                secret_access_key=str(args['secret_access_key'])
            )
        except Exception:
            raise FurionException('iaas_api_error', message='Qingcloud api error | Failed to '
                                                            'connect to zone {}'.format(args['region_id'])
                                  )

    def __check_res(self, res):
        if SUCCESS != res['ret_code']:
            raise FurionException('iaas_api_error', message='Qingcloud api error | {message}'.format(**res))

    def _get_instance_by_id(self, inst_id):
        return self.__get_instance(instances=[inst_id])[0]

    def __get_instance(self, **args):
        res = self.session.describe_instances(**args)
        self.__check_res(res)
        total_count = res['total_count']
        if not total_count:
            raise FurionException(
                'resource_not_exist', message="Cannot find instance on qingcloud. | {}".format(args))
        return [
            self._format_instance_info(x)
            for x in res['instance_set']
        ]

    def _describe_instance_types(self):
        try:
            res = self.session.send_request('DescribeInstanceTypes', {})
            self.__check_res(res)
            return res
        except Exception as e:
            raise FurionException('iaas_api_error', message='Qingcloud get instance type error! | {}'.format(e))

    def _change_instance_state(self, inst_id, cur_state, action):
        info = self._get_instance_by_id(inst_id)
        if info['state'] != cur_state:
            raise FurionException('resource_state_conflict',
                                  message="Instance is not {}, cannot {} it".format(cur_state, action))
        return getattr(self.session, '{}_instances'.format(action))(instances=[inst_id])

    def __get_or_create_tag_for_res(self, tag_name):
        res = self.session.create_tag(tag_name=tag_name)
        if ALREADY_EXISTS == res['ret_code']:
            res = self.session.describe_tags(search_word=tag_name)
            self.__check_res(res)
            return res['tag_set'][0]['tag_id']
        elif SUCCESS == res['ret_code']:
            return res['tag_id']
        else:
            LOG.warning("Create tags for resource error. | {message}".format(**res))

    def _add_tag_for_res(self, res_id, tag_name):
        try:
            tag_id = self.__get_or_create_tag_for_res(tag_name)
            res = self.session.attach_tags([{
                'tag_id': tag_id,
                'resource_type': 'instance',
                'resource_id': res_id
            }])
            self.__check_res(res)
        except FurionException as e:
            LOG.warning("Add tags for resource error. | {} {}".format(res_id, e))

    def _get_cpu_by_instance_type_id(self, type_id):
        res = self._describe_instance_types()
        self.__check_res(res)
        for instance_type in res['instance_type_set']:
            if instance_type['instance_type_id'] == type_id:
                return instance_type['vcpus_current']
        return 8

    def _validate_args(self, args, num, volume_size):
        required_keys = ['image_id', 'num', 'instance_type', 'user_data', 'login_mode']
        args = args.copy()
        args.update({'num': num})
        check_keys(required_keys, args, request_data=True)
        schema_list = [
            {
                "num": num,
                "validator": {
                    "min": 0,
                }
            }]
        if volume_size:
            schema_list.append({
                "volume_size": volume_size,
                "validator": {
                    "enum": [1024, 2048, 4096, 6144, 8192, 12288, 16384, 24576, 32768]
                }
            })
        check_integer(schema_list)

    @staticmethod
    def _parse_instance_info(instance, init_volume_size=''):
        return {
            'instance_id': instance['instance_id'],
            'private_ip': instance['private_ip'] or UNALLOCATED_IP.format(instance['instance_id']),
            'state': instance['state'],
            'attr': {
                'instance_type': instance['attr']['instance_type'],
                'public_ip': instance['attr']['public_ip'],
                'init_volume_size': init_volume_size
            }
        }

    @staticmethod
    def _format_instance_info(info):
        eip = info.get('eip')
        if info['vxnets']:
            private_ip = info['vxnets'][0]['private_ip']
        else:
            private_ip = UNALLOCATED_IP.format(info['instance_id'])
        return {
            'instance_id': info['instance_id'],
            'private_ip': private_ip,
            'state': const.NODE_STATE_MAP.get(info['status'], const.STATE_UNKNOWN),
            'attr': {
                'instance_type': info['instance_type'],
                'public_ip': eip['eip_addr'] if eip else '',
            }
        }

    def list_key_pairs(self):
        """return a dict
            {
                "key_pairs": [
                    "XXX"
                ]
            }
        """
        try:
            res = self.session.describe_key_pairs()
            self.__check_res(res)
            return {"key_pairs": [key['keypair_id'] for key in res['keypair_set']]}
        except Exception as e:
            return FurionException('iaas_api_error', message="Get aingcloud key-pairs list error! | {}".format(e))

    def get_instance_info(self, private_ip):
        """ return dict
            {
                "instance_id": "i-08fu0fx1",
                "state": "RUNNING",
                "private_ip": "10.62.124.xxx",
                "attr": {
                    "public_ip": "121.201.69.xxx",
                    "instance_type": "c1m1"
                }
           }
        """
        res = self.__get_instance(status=[const.STATE_RUNNING.lower()], private_ip=[private_ip])
        instances = [x for x in res if x['private_ip'] == private_ip]
        if not instances:
            raise FurionException(
                'resource_not_exist',
                message="Cannot find instance on qingcloud. | ip {}".format(private_ip))
        return instances[0]

    def _get_qing_userdata(self, userdata):
        wait_net = ('while : ; do ret=0; curl --connect-timeout 1 '
                    'https://baidu.com 2>&1 >/dev/null || ret=$? || true; if '
                    '[ $ret -eq 0 ]; then break; fi; sleep 45; done')

        if settings.DEPLOY_ENV == 'INT':
            install_command = userdata.replace('get.alauda.cn', 'get.int.alauda.io')
        else:
            install_command = userdata

        install_command = install_command.split('\n')

        if '#!/bin/bash' == install_command[0]:
            install_command.insert(1, wait_net)
            return '\n'.join(install_command)

        return userdata

    def create_instances(self, args, num, volume_size=''):
        self._validate_args(args, num, volume_size)
        if volume_size:
            raise FurionException('not_support', message='Qingcloud volume is not support now.')
        try:
            data = {
                'image_id': args['image_id'],
                'login_mode': args['login_mode'],
                'login_keypair': args.get('ssh_key_name'),
                'login_passwd': args.get('login_passwd'),
                'instance_type': args['instance_type'] if not volume_size else None,
                'cpu': (args.get('cpu') or self._get_cpu_by_instance_type_id(args['instance_type'])),
                'memory': volume_size,
                'count': int(num),
                'vxnets': [args.get('vxnets') or settings.CLOUD_SPEC_CONFIGS['QINGCLOUD']['default_network']],
                'security_groups': args.get('security_groups'),
                'need_userdata': 1,
                'userdata_type': 'exec',
                "userdata_value": base64.b64encode(self._get_qing_userdata(args['user_data'])),
            }

            res = self.session.run_instances(**data)
            self.__check_res(res)

            instances = res['instances']

            insts = [
                self._parse_instance_info(self._get_instance_by_id(x), volume_size or '')
                for x in instances
            ]
            for x in insts:
                self._add_tag_for_res(x['instance_id'], args['name_tag'])
            return insts
        except Exception as e:
            raise FurionException('iaas_api_error', message='Create Qingcloud instances failed. | {}'.format(e))

    def delete_instance(self, inst_id):
        try:
            res = self.session.terminate_instances(instances=[inst_id])
            self.__check_res(res)
        except Exception as e:
            if 'ResourceNotFound' in e.message:
                return
            raise FurionException('iaas_api_error', message='Qingcloud delete instance error! | {}'.format(e))

    def stop_instance(self, inst_id):
        return self._change_instance_state(inst_id, const.STATE_RUNNING, 'stop')

    def start_instance(self, inst_id):
        return self._change_instance_state(inst_id, const.STATE_STOPPED, 'start')

    def get_instance_types(self):
        """ return dict
            {
                "instance_types": [
                    "xxx",
                ]
          }
        """
        res = self._describe_instance_types()
        return {
            "instance_types": [
                instance_type['instance_type_id']
                for instance_type in res['instance_type_set']
            ]
        }


if __name__ == '__main__':
    args = {
        'region_id': 'GD1',
        'access_key': 'MDFQKAPEDEWIMILHXWXM',
        'secret_access_key': 'rjdv9s29Zw3ZO0Onn3E0tvtLPZAUfafdTtFnUsCC'
    }
    session = qingcloud.iaas.connect_to_zone(
        zone=args['region_id'],
        access_key_id=args['access_key'],
        secret_access_key=args['secret_access_key']
    )

    data = {
        'image_id': 'trustysrvx64g',
        'login_mode': 'keypair',
        'login_keypair': 'kp-iioo1rih',
        'login_passwd': '',
        'instance_type': 'c1m1',
        'cpu': None,
        'memory': None,
        'count': '1',
        'vxnets': [],
        'security_groups': ''
    }
