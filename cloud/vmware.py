#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from django.http import Http404
import time
import urlparse
from furion.exception import FurionException
from pyVmomi import vim
from pyVim.connect import SmartConnect, SmartConnectNoSSL
from util import const
from util.func import check_keys
from django.conf import settings
from controls.region import RegionControl
from controls.node import NodeControl
__author__ = 'Lingming Xia'

LOG = logging.getLogger(__name__)


class Manager(object):
    def __init__(self, args):
        r = urlparse.urlparse(args['address'])
        port_num = args.get('port', 443)
        self.region_id = args['region_id']
        if r.scheme == 'http':
            si = SmartConnectNoSSL(
                host=r.netloc,
                user=args.get('user'),
                pwd=args.get('password'),
                port=port_num)
        else:
            si = SmartConnect(
                host=r.netloc,
                user=args.get('user'),
                pwd=args.get('password'),
                port=port_num)
        self.content = si.RetrieveContent()

    def _get_obj(self, vimtype, name):
        """
            return an object by name, if name is None the
            first found object is returned
        """
        container = self.content.viewManager.CreateContainerView(
            self.content.rootFolder, vimtype, True)

        for c in container.view:
            if name:
                if c.name == name:
                    return c
            else:
                return c

    def get_instance_by_name(self, inst_name):
        instance = self._get_obj([vim.VirtualMachine], inst_name)

        if not instance:
            raise FurionException('instance_not_exist',
                                  message="cannot find instance on vmware. | {}".format(inst_name))

        return instance

    def get_instance_by_uuid(self, inst_uuid):
        instance = self.content.searchIndex.FindByUuid(None, inst_uuid, True, False)

        if not instance:
            raise FurionException('instance_not_exist',
                                  message="cannot find instance on vmware. | {}".format(inst_uuid))

        return instance

    def get_instance_by_ip(self, inst_ip):
        instance = self.content.searchIndex.FindByIp(None, inst_ip, True, False)

        if not instance:
            raise FurionException('instance_not_exist',
                                  message="cannot find instance on vmware. | {}".format(inst_ip))
        return instance

    def destroy_instance(self, inst_name):
        instance = self.get_instance_by_name(inst_name)
        try:
            LOG.info("destroying vm from vsphere.")
            instance.Destroy_Task()
            LOG.info("vm {} has been destroyed successfully.".format(instance.name))
        except Exception as e:
            raise FurionException('iaas_api_error', message='vmware delete vm error! | {}'.format(e))

    def delete_instance(self, inst_name):
        instance = self.get_instance_by_name(inst_name)

        if instance.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
            LOG.info("vm {} is powering off...".format(instance.name))
            instance.PowerOff()
            LOG.info("vm {} power is off.".format(instance.name))

    def stop_instance(self, inst_name):
        instance = self.get_instance_by_name(inst_name)

        if instance.runtime.powerState == vim.VirtualMachinePowerState.poweredOn:
            LOG.info("vm {} is powering off...".format(instance.name))
            instance.PowerOff()
            LOG.info("vm {} power is off.".format(instance.name))

    def is_deletable(self, inst_name):
        instance = self.get_instance_by_name(inst_name)
        if instance.runtime.powerState != vim.VirtualMachinePowerState.poweredOn:
            return True
        return False

    def start_instance(self, inst_name):
        instance = self.get_instance_by_name(inst_name)

        if instance.runtime.powerState != vim.VirtualMachinePowerState.poweredOn:
            LOG.info("vm {} is powering on...".format(instance.name))
            instance.PowerOn()
            LOG.info("vm {} power is off.".format(instance.name))

    @staticmethod
    def get_instance_types():
        return settings.cloud_spec_configs.get('vmware')

    @staticmethod
    def _valid_args(args):
        required_keys = ['template_name', 'cpu_num', 'mem_size', 'user_data', 'name_tag']
        args = args.copy()
        # don't change origin args
        check_keys(required_keys, args, request_data=True)

    def _clone_vm(
            self, args, vm_name, datacenter_name, vm_folder, datastore_name,
            cluster_name, resource_pool, power_on, datastorecluster_name):
        """
        clone a vm from a template/vm, datacenter_name, vm_folder, datastore_name
        cluster_name, resource_pool, and power_on are all optional.
        """
        template = args['template_name']
        vm_obj = self.get_instance_by_name(template)
        # if None get the first one
        data_center = self._get_obj([vim.Datacenter], datacenter_name)

        if vm_folder:
            dest_folder = self._get_obj([vim.Folder], vm_folder)
        else:
            dest_folder = data_center.vmFolder

        if datastore_name:
            data_store = self._get_obj([vim.Datastore], datastore_name)
        else:
            data_store = self._get_obj([vim.Datastore], vm_obj.datastore[0].info.name)

        # if None, get the first one
        cluster = self._get_obj([vim.ClusterComputeResource], cluster_name)

        if resource_pool:
            resource_pool = self._get_obj([vim.ResourcePool], resource_pool)
        else:
            resource_pool = cluster.resourcePool

        vmconf = vim.vm.ConfigSpec()

        if datastorecluster_name:
            podsel = vim.storageDrs.PodSelectionSpec()
            pod = self._get_obj([vim.StoragePod], datastorecluster_name)
            podsel.storagePod = pod

            storagespec = vim.storagedrs.StoragePlacementSpec()
            storagespec.podSelectionSpec = podsel
            storagespec.type = 'create'
            storagespec.folder = dest_folder
            storagespec.resourcePool = resource_pool
            storagespec.configSpec = vmconf

            try:
                rec = self.content.storageResourceManager.RecommendDatastores(
                    storageSpec=storagespec)
                rec_action = rec.recommendations[0].action[0]
                real_datastore_name = rec_action.destination.name
            except Exception as e:
                LOG.warning('error happens while get storageResourceManager data | {}'.format(e))
                real_datastore_name = vm_obj.datastore[0].info.name

            data_store = self._get_obj([vim.Datastore], real_datastore_name)

        config_spec = vim.vm.ConfigSpec()
        config_spec.numCPUs = args['cpu_num']
        config_spec.memoryMB = long(int(args['mem_size']) * 1024)

        # set relospec
        relospec = vim.vm.RelocateSpec()
        relospec.datastore = data_store
        relospec.pool = resource_pool

        clonespec = vim.vm.CloneSpec()
        clonespec.location = relospec
        clonespec.powerOn = power_on
        clonespec.config = config_spec
        vm_obj.Clone(folder=dest_folder, name=vm_name, spec=clonespec)
        x = dict()
        x['attr'] = dict()
        x['instance_id'] = vm_name
        x['private_ip'] = const.VMWARE_CLONING_STATE_IP
        x['state'] = const.STATE_DEPLOYING
        return [x]

    def check_clone_ip(self, target_name):
        vm = self.get_instance_by_name(target_name)
        if not vm:
            raise FurionException(message="cannot find instance on vmware. | {}".format(target_name))
        tools_status = vm.guest.toolsStatus
        if (tools_status == 'toolsNotInstalled' or
                tools_status == 'toolsNotRunning'):
            return None
        return vm.summary.guest.ipAddress

    def execute_command(self, target_name, user_data, template_user, template_password):
        vm = self.get_instance_by_name(target_name)
        if not vm:
            raise FurionException(message="cannot find instance on vmware. | {}".format(target_name))

        creds = vim.vm.guest.NamePasswordAuthentication(
            username=template_user, password=template_password
        )

        try:
            pm = self.content.guestOperationsManager.processManager
            command_arguments = "\-c \'{}\'".format(user_data)
            LOG.debug("arguments is  {}".format(command_arguments))
            ps = vim.vm.guest.ProcessManager.ProgramSpec(
                programPath="/bin/bash",
                arguments=command_arguments
            )
            pm.StartProgramInGuest(vm, creds, ps)

        except IOError, e:
            LOG.warning('error happens while execute command in vmware node | {}'.format(e))

    def create_instances(self, args, num, volume_size=''):
        self._valid_args(args)
        try:
            NodeControl.get_object(region=RegionControl.get_object(id=self.region_id),
                                   private_ip=const.VMWARE_CLONING_STATE_IP)
            return []
        except Http404:
            t_now = time.time()
            target_name = args['name_tag'].replace('%', '\%') + str(int(round(t_now * 1000)))
            return self._clone_vm(args, target_name, None, None, None, None, None, True, None)

    @staticmethod
    def _format_instance_info(instance):
        """
        Print information for a particular virtual machine or recurse into a
        folder with depth protection
        """
        summary = instance.summary
        instance_info = {
            'name': summary.config.name,
            'path': summary.config.vmPathName,
            'UUID': summary.config.uuid,
            'instance_UUID': summary.config.instanceUuid
        }
        annotation = summary.config.annotation
        if annotation:
            instance_info['annotation'] = annotation
            instance_info['state'] = summary.runtime.powerState
        if summary.guest is not None:
            ip_address = summary.guest.ipAddress
            tools_version = summary.guest.toolsStatus
            if tools_version is not None:
                instance_info['VMware-tools'] = tools_version
            else:
                instance_info['VMware-tools'] = None
            if ip_address:
                instance_info['ip_address'] = ip_address
            else:
                instance_info['ip_address'] = None
        return instance_info
