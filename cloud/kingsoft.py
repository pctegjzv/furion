#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import requests
import logging
import base64
import shortuuid
from util import const
from furion.exception import FurionException
__author__ = 'Lingming Xia'

LOG = logging.getLogger(__name__)


class Manager(object):
    def __init__(self, args):
        self.login_url = args['login_url']
        self.nova_url = args['nova_url']
        self.user_name = args['user_name']
        self.password = args['password']
        self.tenant_name = args['tenant_name']
        self.tenant_id = None

    # Get Token
    def __get_api_header(self):
        headers = {'content-type': 'application/json'}
        data = {
                "auth": {
                        "tenantName": self.tenant_name,
                        "passwordCredentials": {
                            "username": self.user_name,
                            "password": self.password
                        }
                }
        }
        data = json.dumps(data)
        response = requests.post(self.login_url + '/tokens', data=data, headers=headers, verify=False)
        if response.status_code != 200:
            raise FurionException('request_error', message='login fail | {}'.format(response.status_code))
        token_id = json.loads(response.text)['access']['token']['id']
        self.tenant_id = json.loads(response.text)['access']['token']['tenant']['id']
        login_header = {"Content-Type": "application/json", "X-Auth-Token": token_id}
        return login_header

    def _change_instance_state(self, inst_id, action, cur_state=const.STATE_UNKNOWN):
        login_header = self.__get_api_header()
        if action == 'stop' or action == 'start':
            info = self._get_instance_by_id(inst_id)
            if info['state'] != cur_state:
                raise FurionException(
                    'resource_state_conflict',
                    message="Instance is not {}, cannot {} it".format(cur_state, action))
            api_url = self.nova_url + '/' + self.tenant_id + "/servers/" + inst_id + '/action'
            key = "os-" + action
            payload = {key: None}
            response = requests.post(api_url, headers=login_header, json=payload, verify=False)
            if response.status_code != 202:
                LOG.error("update instance {} failed, state code is {}".format(inst_id, response.status_code))
        elif action == 'delete':
            api_url = self.nova_url + '/' + self.tenant_id + "/servers/" + inst_id
            response = requests.delete(api_url, headers=login_header, verify=False)
            if response.status_code != 204:
                LOG.error("delete instance {} failed, state code is {}".format(inst_id, response.status_code))

    def stop_instance(self, inst_id):
        return self._change_instance_state(inst_id, 'stop', const.STATE_RUNNING)

    def start_instance(self, inst_id):
        return self._change_instance_state(inst_id, 'start', const.STATE_SHUTTING_DOWN)

    def delete_instance(self, inst_id):
        return self._change_instance_state(inst_id, 'delete')

    def list_key_pairs(self):
        login_header = self.__get_api_header()
        api_url = self.nova_url + '/' + self.tenant_id + "/os-keypairs"
        try:
            response = requests.get(api_url, headers=login_header, verify=False)
            content = json.loads(response.text)
            key_pair = {"key_pairs": []}
            for i in content['keypairs']:
                key_pair['key_pairs'].append(i['keypair']['name'])
            return key_pair
        except Exception as e:
            return FurionException(
                'iaas_api_error',
                message="Get kingsoft key-pairs list error! | {}".format(e))

    def create_instances(self, paras, num, volume_size=0):
        data = {
            "server": {
                "name": paras['name_tag'],
                "min_count": num,
                "max_count": num,
                "imageRef": paras['image_id'],
                "flavorRef": paras['instance_type'],
                "networks": [{"uuid": paras['subnet_id']}],
                "user_data": ""
            }
        }

        if 'ssh_key_name' in paras:
            data['server']['key_name'] = paras['ssh_key_name']
        else:
            raise FurionException('request_error', message="need ssh key")

        if 'user_data' in paras:
            user_date = paras['user_data']
            user_date = base64.b64encode(user_date)
            data['server']['user_data'] = user_date

        if num > 1:
            data['server']['return_reservation_id'] = 'True'

        login_header = self.__get_api_header()
        create_instance_url = self.nova_url + '/' + self.tenant_id + "/servers"

        try:
            r = requests.post(create_instance_url, json=data, headers=login_header, verify=False)
            if r.status_code != 202:
                raise FurionException('status code is {},not 202, please check payload {}'.
                                      format(r.status_code, data))
            resp = json.loads(r.text)
            if 'reservation_id' in resp:
                r = requests.get(create_instance_url + '/detail', headers=login_header, params=resp, verify=False)
                resp = json.loads(r.text)
                inst = [self._parse_instance_info(x) for x in resp['servers']]
            else:
                inst = [self._parse_instance_info(resp['server'])]
            return inst
        except Exception as e:
            raise FurionException(
                'iaas_api_error',
                message='kingsoft create instances failed. | {}'.format(e))

    def get_instance_info(self, inst_id):
        return self.__get_instance(instance_ids=[inst_id])

    def _get_instance_by_id(self, inst_id):
        return self.__get_instance(instance_ids=[inst_id])

    def _parse_instance_info(self, inst):
        return self._get_instance_by_id(inst['id'])

    @staticmethod
    def _format_instance_info(info):
        if not info['addresses']:
            tmp_private_ip = const.KINGSOFT_PRIVATE_IP_PREFIX + shortuuid.uuid()
        else:
            for y in info['addresses'].itervalues():
                for x in y:
                    tmp_private_ip = x['addr']
        return {
            'instance_id': info['id'],
            'private_ip': tmp_private_ip,
            'state': const.NODE_STATE_MAP.get(info['status'], const.STATE_UNKNOWN),
            'attr': {
                'instance_type': info['flavor']['id']
            }
        }

    def __get_instance(self, **args):
        login_header = self.__get_api_header()
        for x in args['instance_ids']:
            api_url = self.nova_url + '/' + self.tenant_id + "/servers/" + x
            r = requests.get(api_url, headers=login_header, verify=False)
            data = json.loads(r.text)
            if not data:
                raise FurionException(
                    'resource_not_exist',
                    message='cannot find instance on kingsoft. | {}'.format(args))
            return self._format_instance_info(data['server'])

    def get_instance_types(self):
        """ return dict
            {
                "instance_type_x": "id",
                "instance_type_y": "id",
                "instance_type_z": "id",
                ...
            }
        """
        login_header = self.__get_api_header()
        api_url = self.nova_url + "/" + self.tenant_id + "/flavors"
        try:
            r = requests.get(api_url, headers=login_header, verify=False)
            flavor_content = json.loads(r.text)
            t = {}
            for i in flavor_content['flavors']:
                x, y = i['id'], i['name']
                t[y] = x
            return t
        except Exception as e:
            return FurionException(
                'iaas_api_error',
                message="Get kingsoft instance type list error! | {}".format(e))
