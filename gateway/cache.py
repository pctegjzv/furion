import json
import logging

from django.conf import settings    


logger = logging.getLogger(__name__)


class CacheGateway:

    timeout = settings.CACHE_TIMEOUT
    enabled = settings.CACHE_ENABLED
    client = settings.REDIS_CLIENT

    @classmethod
    def set(cls, key, value):
        if not cls.enabled:
            return False

        logger.debug('Caching {} ...'.format(key))
        try:
            cls.client.set(key, json.dumps(value))
            cls.client.expire(key, cls.timeout)
        except Exception as exc:
            logger.warning('Set cache for {} failed: {}'.
                           format(key, exc))
            return False
        return True

    @classmethod
    def get(cls, key):
        if not cls.enabled:
            return False, None

        logger.debug('Retrieving cache {} ...'.format(key))
        try:
            return True, json.loads(cls.client.get(key))
        except Exception as exc:
            logger.warning('Retrieve cache {} failed: {}'.
                           format(key, exc))
            return False, None

    @classmethod
    def delete(cls, key):
        if not cls.enabled:
            return False

        logger.debug('Deleting cache {} ...'.format(key))
        try:
            cls.client.delete(key)
            return True
        except Exception as exc:
            logger.warning('Delete cache {} failed: {}'
                           .format(key, exc))
            return False
