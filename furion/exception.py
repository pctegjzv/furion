#!/usr/bin/env python
# -*- coding: utf-8 -*-


from mekansm.exceptions import MekAPIException
from mekansm.errors import ERR_TYPES, ERR_CODES

__author__ = 'Hang Yan'


class ErrCodes(object):
    """Error codes for furion.
    Examples:
        `raise FurionException(ErrCodes.NOT_SUPPORT)`
    """
    NOT_ALLOWED = 'not_allowed'
    NOT_SUPPORT = 'not_support'
    KUBERNETES_RESOURCE_ERROR = 'kubernetes_resource_error'
    INTERNAL_ERROR = 'internal_error'


class FurionException(MekAPIException):
    errors_map = {
        'region_not_exist': {
            'message': 'region not exist! | {}',
            'type': 'not_exist'
        },
        'region_data_incorrect': {
            "message": "Invalid region data! | {} {}",
            "type": "server_error"
        },
        'region_proxy_conn_error': {
            "message": "Connection to region proxy error! | {}",
            "type": "bad_gateway"
        },
        "iaas_api_error": {
            "message": "Connection error or operation error with upstream cloud provider! | {} ",
            "type": "bad_gateway"
        },
        ErrCodes.NOT_SUPPORT: {
            "message": "Current action is not supported! | {}",
            "type": ERR_TYPES.BAD_REQUEST
        },
        ErrCodes.NOT_ALLOWED: {
            'message': "Operation not allowed! | {}",
            "type": ERR_TYPES.BAD_REQUEST
        },
        ErrCodes.KUBERNETES_RESOURCE_ERROR: {
            'message': 'Unable to find target resource in kubernetes. | {} {}',
            'type': ERR_TYPES.SERVER_ERROR
        },
        "not_enough_port": {
            "message": "Not enough port for mapping tunnel! | {}",
            "type": "server_error"
        },
        "phoenix_command": {
            'message': "Get command fail from Phoenix! | {}",
            'type': 'server_error'
        },
        "alchemist_error": {
            'message': 'Alchemist error occur! | {}',
            'type': 'server_error'
        },
        "upgrade_error": {
            'message': 'Upgrade error! | {}',
            'type': ERR_TYPES.BAD_REQUEST
        },
        "phoenix_auth": {
            'message': 'Auth fail! | {}',
            'type': 'phoenix_auth'
        },
        "invalid_node_type": {
            'message': 'Invalid node_type change: {}',
            'type': ERR_TYPES.BAD_REQUEST
        },
        "insufficient_data": {
            'message': 'Insufficient Data - {}',
            'type': ERR_TYPES.BAD_REQUEST
        },
        "invalid_input": {
            'message': 'Invalid data',
            'type': ERR_TYPES.BAD_REQUEST
        },
        "feature_error": {
            'message': 'Feature not available in region ! | features={}, region={}',
            'type': ERR_TYPES.BAD_REQUEST
        },
        "request_error": {
            'message': 'Do request {} fail',
            'type': 'internal_error'
        },
        ErrCodes.INTERNAL_ERROR: {
            'message': "internal_error",
            'type': 'internal_error'
        }
    }


class InvalidArgsException(FurionException):
    def __init__(self, message=None):
        super(InvalidArgsException, self).__init__(code=ERR_CODES.INVALID_ARGS, message=message)


class InvalidRegionDataException(FurionException):
    def __init__(self, message="Invalid region data! | {} {}", message_params=None):
        super(InvalidRegionDataException, self).__init__(code='region_data_incorrect', message=message,
                                                         message_params=message_params)
