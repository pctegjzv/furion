from .settings import *  # NOQA

UNIT_TEST = True

METHOD_NAME = ''

PUCK_DEFAULT_TIMEOUT = 0

DNS_REGISTER = {
    'ENDPOINT': 'dns.aladua.cn',
    'SUFFIX': 'myalauda.cn',
    'INDEX': 'index'
}
