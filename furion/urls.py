# coding=utf-8
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.http import HttpResponse, JsonResponse
from util.misc import self_diagnose

urlpatterns = patterns(
    '',
    url(r'^_ping/?$', lambda request: HttpResponse("{}:{}".format(settings.MATHILDE_COMPONENT,
                                                                  settings.COMPONENT_MOTTO))),
    url(r'^_diagnose/?$', lambda request: JsonResponse(data=self_diagnose())),
    url(r'^' + settings.API_VERSION + '/', include('app.urls')),
    url(r'^v2/', include("app_v2.urls"))
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
    urlpatterns += [url(r'^silk/', include('silk.urls', namespace='silk'))]
