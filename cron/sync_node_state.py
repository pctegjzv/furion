#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import datetime
import json
from django.utils import timezone
from django.conf import settings
from app.models import Region, Node
from util import const
from util.cron import stdout, is_task_running, get_phoenix_nodes
from util.events import Event

__author__ = 'Xiaodong Zhang'

REGION_DEPLOY_TIMEOUT = 10


def get_out_of_goal_nodes(timed_out):
    """
    Fetches the nodes from Phoenix that are OUT_OF_GOAL. If timed_out
    parameter is True, it returns nodes older than the unhealthy
    timeout. If False, it returns nodes that are OUT_OF_GOAL but
    within unhealthy timeout.
    """
    current_time = timezone.now()
    timeout_minute = int(settings.ALARM_SETTINGS['TIMEOUT_INTERVAL'])
    cmp_time = current_time - datetime.timedelta(minutes=timeout_minute)
    last_unhealthy_status_update = '{}-{}-{}T{}:{}:{}'.format(
        cmp_time.year, cmp_time.month, cmp_time.day,
        cmp_time.hour, cmp_time.minute, cmp_time.second)
    time_query = 'first_unhealthy_time__{}={}'
    if timed_out:
        time_query = time_query.format('lt', last_unhealthy_status_update)
    else:
        time_query = time_query.format('gt', last_unhealthy_status_update)
    filter_string = 'health_state=HEALTHY&status=OUT_OF_GOAL_STATE&{}'.format(time_query)
    out_of_goal_nodes = get_phoenix_nodes(filter_string)
    return out_of_goal_nodes


def get_unhealthy_nodes():
    """
    This method returns nodes that are either in UNHEALTHY state in
    Phoenix or have been in OUT_OF_GOAL_STATE for a duration greater
    than the unhealthy timeout.
    """
    # Fetch the nodes in UNHEALTHY state in Phoenix.
    filter_string = 'health_state=UNHEALTHY'
    unhealthy_nodes = get_phoenix_nodes(filter_string)
    stdout('{} Nodes are UNHEALTHY in Phoenix.'.format(len(unhealthy_nodes)))

    # Fetch the nodes that are in OUT_OF_GOAL_STATE for greater than unhealthy timeout.
    out_of_goal_nodes = get_out_of_goal_nodes(timed_out=True)
    stdout('{} nodes are OUT_OF_GOAL_STATE longer than unhealthy timeout.'.format(len(out_of_goal_nodes)))

    # Aggregate the node set to return the entire list of unhealthy nodes.
    unhealthy_nodes.extend(out_of_goal_nodes)
    return unhealthy_nodes


def get_processing_nodes():
    """
    This method returns nodes that are in PROCESSING_STATE in Phoenix
    or in OUT_OF_GOAL_STATE for less than the unhealthy timeout.
    """
    # Fetch the nodes that are in PROCESSING_STATE in Phoenix.
    filter_string = 'health_state=HEALTHY&status=PROCESSING_STATE'
    processing_nodes = get_phoenix_nodes(filter_string)
    stdout('{} nodes are in PROCESSING_STATE.'.format(len(processing_nodes)))

    # Fetch the nodes that are in OUT_OF_GOAL_STATE for less than unhealthy timeout.
    out_of_goal_nodes = get_out_of_goal_nodes(timed_out=False)
    stdout('{} nodes are OUT_OF_GOAL_STATE less than unhealthy timeout.'.format(
        len(out_of_goal_nodes)))

    # Aggregate the node set to return the entire list of deploying nodes.
    processing_nodes.extend(out_of_goal_nodes)
    return processing_nodes


def get_in_goal_nodes():
    """
    This method returns nodes that are in IN_GOAL_STATE in Phoenix.
    """
    filter_string = 'health_state=HEALTHY&status=IN_GOAL_STATE'
    in_goal_nodes = get_phoenix_nodes(filter_string)
    stdout('{} nodes are in IN_GOAL_STATE.'.format(len(in_goal_nodes)))
    return in_goal_nodes


def add_region_info(nodes):
    result = {}
    for node in nodes:
        if node['env_uuid'] not in result.keys():
            result[node['env_uuid']] = Region.objects.filter(env_uuid=node['env_uuid']).first()
        if result[node['env_uuid']] is None:
            node['region_id'] = 'None'
            node['region_name'] = 'None'
        else:
            node['region_id'] = result[node['env_uuid']].id
            node['region_name'] = result[node['env_uuid']].name


def create_message(nodes):
    message = '\n'
    for node in nodes:
        message += 'region: {} name: {} env: {} ip: {} time: {}\n'.format(
            node['region_id'], node['region_name'], node['env_uuid'],
            node['ip_address'], node['first_unhealthy_time']
        )
    current_time = timezone.now()
    alarm_time = '{}-{}-{}T{}:{}:{}.0Z'.format(
        current_time.year, current_time.month, current_time.day,
        current_time.hour, current_time.minute, current_time.second
    )
    result = {
        "template_type": "alarm",
        "data": {
            "subject": "Node unhealthy alarm",
            "content": message,
            "time": alarm_time
        }
    }
    return result


def raise_unhealthy_alarm(unhealthy_nodes):
    """
    Raise an alarm if any unhealthy nodes were found.
    """
    # Unhealthy nodes were found. Add region info & create an alarm.
    add_region_info(unhealthy_nodes)
    stdout('{} unhealthy nodes found.'.format(len(unhealthy_nodes)))
    url = '{}/v1/notifications/{}/messages'.format(
        settings.ALARM_SETTINGS['ALARM_ENDPOINT'],
        settings.ALARM_SETTINGS['NOTIFICATION_ID'])
    message = create_message(unhealthy_nodes)
    stdout('Create Alarm: {}'.format(message))
    stdout('Send Alarm to: {}.'.format(url))
    ret = requests.post(
        url, data=json.dumps(message), headers={'Content-Type': 'application/json'})
    stdout('Alarm trigger response: {}'.format(ret.status_code))


def update_region_state(region, unhealthy_nodes, processing_nodes):
    """
    If there are no unhealthy nodes or no processing nodes in this
    region, set the state to Running. If there are unhealthy nodes
    and the region has been deploying for greater than the timeout
    duration, mark the region to be in error.
    """
    if not unhealthy_nodes and not processing_nodes:
        region.state = const.STATE_RUNNING
        region.save()
        detail = 'All nodes have reached expected state.'
        Event.region_state_update(region, const.STATE_RUNNING, detail)
        stdout('Change State for Region {}:{} from {} to {}.'.format(
            region.name, region.env_uuid, region.state, const.STATE_RUNNING))

    elif unhealthy_nodes and region.state == const.STATE_DEPLOYING:
        current_time = timezone.now()
        cmp_time = current_time - datetime.timedelta(minutes=REGION_DEPLOY_TIMEOUT)
        if region.updated_at < cmp_time:
            region.state = const.STATE_ERROR
            region.save()
            detail = 'Some nodes are in unhealthy state'
            Event.region_state_update(region, const.STATE_ERROR, detail)
            stdout('Change State for Region {}:{} from {} to {}.'.format(
                region.name, region.env_uuid, region.state, const.STATE_ERROR))


def update_node_state(env_uuid, unhealthy_nodes, in_goal_nodes, processing_nodes):
    """
    This method updates the state for nodes in an environment based on
    their Phoenix state. The state update logic is as follows:

    If the node is unhealthy in Phoenix but is in RUNNING or DEPLOYING or DRAINING
    state in Furion, then set the node state to ERROR.

    If the node is in goal state in Phoenix but is in DEPLOYING or in
    ERROR or in DRAINING state in Furion, set the node state to RUNNING.

    If the node is in a processing state in Phoenix but is in RUNNING
    state in Furion, then set the node state to DEPLOYING.  (Note: ignore DRAINING here, 
    because drain can only be triggered by UI) 
    """
    region = Region.objects.filter(env_uuid=env_uuid).first()
    if not region:
        stdout("No Region found in Furion for env_uuid: {}.".format(env_uuid))
        return

    furion_nodes = Node.objects.filter(region=region)
    for node in furion_nodes:
        try:
            new_state = node.state
            # If a node is unhealthy in Phoenix but is in RUNNING / DEPLOYING state in Furion,
            # update its state to STATE_ERROR.
            if (node.state in [const.STATE_RUNNING, const.STATE_DEPLOYING, const.STATE_DRAINING] and
                    node.private_ip in unhealthy_nodes.keys()):
                new_state = const.STATE_ERROR
            # If a node is in goal in Phoenix but is in DEPLOYING / ERROR / DRAINING state in Furion,
            # update its state to STATE_RUNNING.
            elif (node.state in [const.STATE_DEPLOYING, const.STATE_ERROR, const.STATE_DRAINING] and
                    node.private_ip in in_goal_nodes.keys()):
                new_state = const.STATE_RUNNING
            # If a node is processing in Phoenix but is in RUNNING state in Furion,
            # update its state to STATE_DEPLOYING.
            elif node.state == const.STATE_RUNNING and node.private_ip in processing_nodes.keys():
                new_state = const.STATE_DEPLOYING

            if node.state != new_state:
                stdout("Updating state for Node - {}, Region - {}:{} from {} to {}.".format(
                    node.private_ip, region.name, region.env_uuid, node.state, new_state))
                msg = ''
                if new_state == const.STATE_ERROR:
                    msg = json.dumps(unhealthy_nodes[node.private_ip]['failure_info'])
                Event.node_state_update(region, node, new_state, msg)
                node.state = new_state
                node.save()
        except Exception as e:
            stdout("Failed to update state for Node - {}, Env - {}:{}. Exception: {}.".format(
                node.private_ip, region.name, region.env_uuid, e))

    if region.state in [const.STATE_DEPLOYING, const.STATE_ERROR]:
        # If the region is deploying or in error, check if its state has changed.
        update_region_state(region, unhealthy_nodes, processing_nodes)


def add_nodes_to_map(region_map, nodes, state):
    """
    Add each node in the nodes list to a map for the environment it
    belongs to. The map groups these nodes by their state per
    environment.
    """
    for node in nodes:
        try:
            env_uuid = node['env_uuid']
            if env_uuid not in region_map:
                region_map[env_uuid] = {
                    'unhealthy_nodes': {},
                    'in_goal_nodes': {},
                    'processing_nodes': {}
                }
            region_map[env_uuid][state][node['ip_address']] = node
        except Exception as e:
            stdout('Adding Node {} to Region {} failed. Exception {}.'.format(
                node['ip_address'], node['env_uuid'], e))


def handle_empty_regions():
    """
    If some nodes of a region failed when emptying the region, which
    the user deleted we can be left with an empty region in deploying
    or error state. This method sets such regions to RUNNING state.
    """
    try:
        empty_regions = Region.objects.filter(node=None)
        for region in empty_regions:
            if region.state != const.STATE_RUNNING:
                stdout('Change State for Region {}:{} from {} to {}.'.format(
                    region.name, region.env_uuid, region.state, const.STATE_RUNNING))
                region.state = const.STATE_RUNNING
                region.save()
                detail = 'No nodes in the Region.'
                Event.region_state_update(region, const.STATE_RUNNING, detail)
    except Exception as e:
        stdout('Failed to update region state for empty regions. Exception: {}'.format(e))


def sync_node_state():
    if is_task_running('Illidan', 120):
        stdout("Another sync_node_state task running. Try later.")
        return
    stdout("Starting an sync_node_state task.")

    # Get set of nodes in different states in Phoenix.
    unhealthy_nodes = get_unhealthy_nodes()
    in_goal_nodes = get_in_goal_nodes()
    processing_nodes = get_processing_nodes()

    region_map = {}
    add_nodes_to_map(region_map, unhealthy_nodes, 'unhealthy_nodes')
    add_nodes_to_map(region_map, in_goal_nodes, 'in_goal_nodes')
    add_nodes_to_map(region_map, processing_nodes, 'processing_nodes')

    # Sync the Furion states for these nodes to Phoenix states. Here is the sync logic:
    # If a node is UNHEALTHY or in OUT_OF_GOAL_STATE greater than unhealthy timeout, set it
    # to STATE_ERROR in Furion.
    # If a node is in PROCESSING_STATE or OUT_OF_GOAL_STATE less than unhealthy timeout, set it
    # to STATE_DEPLOYING in Furion.
    # If a node is in STATE_ERROR or STATE_DEPLOYING in Furion & is IN_GOAL_STATE in Phoenix,
    # set it to STATE_RUNNING in Furion.
    for env_uuid in region_map:
        try:
            update_node_state(env_uuid,
                              region_map[env_uuid]['unhealthy_nodes'],
                              region_map[env_uuid]['in_goal_nodes'],
                              region_map[env_uuid]['processing_nodes'])
        except Exception as e:
            stdout('Failed to update node states for environment {}. Exception: {}'.format(env_uuid, e))

    # If a region has no nodes, set its state to RUNNING.
    handle_empty_regions()

    # If some nodes are Unhealthy, raise an alarm.
    if len(unhealthy_nodes) > 0:
        raise_unhealthy_alarm(unhealthy_nodes)
    else:
        stdout('No unhealthy nodes found.')
    stdout("Completed sync_node_state task.")
