#!/usr/bin/env python
# -*- coding: utf-8 -*-

import yaml
import json
import copy
from django.db import transaction
from app.models import Region, Node
from django.utils import timezone
from controls.region_settings import RegionSettingControl
from controls.node import add_resources_to_node, update_swarm_worker, get_same_node
from util import const
from util.phoenix import NodeManager
from util.phoenix import RegionManager
from util.phoenix import EnvironmentManager
from util.cron import stdout, is_task_running, get_phoenix_nodes_by_type
from util.events import Event
import traceback


__author__ = 'Saurabh Wagh'


def add_nodes_to_map(region_map, nodes, node_type):
    """
    Initializes the map for each environment that has a new node. Also,
    adds the nodes to the map for the specified type.
    
    Args:
        region_map(dict): Note: this args will be modified
        nodes(list[dict]):
        node_type(str):
        
    """
    for node in nodes:
        try:
            if node['env_uuid'] not in region_map:
                region_map[node['env_uuid']] = {
                    const.PHOENIX_NODE_TYPE_EMPTY: [],
                    const.PHOENIX_NODE_TYPE_COMPUTE: [],
                    const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE: [],
                    const.PHOENIX_NODE_TYPE_SWARM_WORKER: [],
                    const.PHOENIX_NODE_TYPE_K8S_SLAVE: [],
                    'region': None,
                    'user_settings': {}
                }
            region_map[node['env_uuid']][node_type].append(node['ip_address'])
        except Exception as e:
            stdout('Adding Node {} to Region {} failed. Exception {}.'.format(
                node['ip_address'], node['env_uuid'], e))


def update_region_settings(region_map):
    """
    Add appropriate region settings to the region_map. These will
    be used when adding nodes to Furion / setting goal state.
    """
    for env_uuid in region_map:
        try:
            # Populate Region object for the region in region_map.
            region = Region.objects.get(env_uuid=env_uuid)
            region_map[env_uuid]['region'] = region
            compute_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_COMPUTE, [])
            swarm_worker_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_SWARM_WORKER, [])
            k8s_compute_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_K8S_SLAVE, [])
            attribute_nodes = region_map[env_uuid].get(
                const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE, [])
            compute_nodes.extend(attribute_nodes)

            # Only Populate user_settings if there are any Compute Nodes.
            if compute_nodes:
                region_settings = RegionSettingControl.get_settings(region)
                if not region_settings:
                    stdout("{}'s region settings are not available.".format(env_uuid))
                    continue
                user_settings = yaml.safe_load(json.dumps(region_settings['user_settings']))
                if not user_settings:
                    stdout("{}'s user settings are not available.".format(env_uuid))
                    continue
                if isinstance(user_settings['MASTER_NODES'], dict):
                    user_settings['MASTER_NODES'] = [user_settings['MASTER_NODES']['private_ip']]
                user_settings['SLAVE_NODES'] = list(set(compute_nodes))
                user_settings['MESOS_SLAVE_WITH_TAG'] = list(set(attribute_nodes))
                region_map[env_uuid]['user_settings'] = user_settings

            if swarm_worker_nodes:
                region_settings = RegionSettingControl.get_settings(region)
                if not region_settings:
                    stdout("{}'s region settings are not available.".format(env_uuid))
                    continue
                user_settings = yaml.safe_load(json.dumps(region_settings['user_settings']))
                if not user_settings:
                    stdout("{}'s user settings are not available.".format(env_uuid))
                    continue

                user_settings['SWARM_WORKER_NODES'] = list(set(swarm_worker_nodes))
                region_map[env_uuid]['user_settings'] = user_settings

            if k8s_compute_nodes:
                region_settings = RegionSettingControl.get_settings(region)
                if not region_settings:
                    stdout("{}'s region settings are not available.".format(env_uuid))
                    continue
                user_settings = yaml.safe_load(json.dumps(region_settings['user_settings']))
                if not user_settings:
                    stdout("{}'s user settings are not available.".format(env_uuid))
                    continue

                user_settings['KUBERNETES_COMPUTE_NODES'] = list(set(k8s_compute_nodes))
                region_map[env_uuid]['user_settings'] = user_settings

        except Exception as e:
            stdout('Failed to update region map for Region {}. Exception {}'.format(
                env_uuid, e))


def create_furion_node(region, ip_address, node_type, node_state):
    """
    This method creates a new node in Furion for the specified region.
    """
    try:
        with transaction.atomic():
            node = Node.objects.filter(region=region, private_ip=ip_address).first()
            if node:
                stdout("Node {} exists in Region {}:{} with type {}. Skip creation.".format(
                    node.private_ip, region.name, region.env_uuid, node.type))
                return
            else:
                node_data = {
                    'private_ip': ip_address,
                    'region': region,
                    'type': node_type,
                    'state': node_state
                }
                node = Node.objects.create(**node_data)
                Event.node_add(region, node.private_ip, node_type, node_state)
                stdout("Created Node {} in Region {}:{} with type {}.".format(
                    node.private_ip, region.name, region.env_uuid, node.type))
    except Exception as e:
        stdout('Failed to create Node {}, Type {} in Region {}:{}. Exception {}.'.format(
            ip_address, node_type, region.name, region.env_uuid, e))


def add_critical_node(phoenix_nodes, region, node_type):
    """
    This method identify the phoenix nodes that are not already in Furion
    and adds them in CRITICAL state.
    """
    try:
        furion_nodes = Node.objects.filter(region=region, type=node_type)
        furion_node_ips = [node.private_ip for node in furion_nodes]
        for node_ip in phoenix_nodes:
            if node_ip not in furion_node_ips:
                create_furion_node(region, node_ip, node_type, const.STATE_CRITICAL)
    except Exception as e:
        stdout('Failed to add nodes {} of type {}(CRITICAL) to Region {}:{}. Exception {}.'.format(
            phoenix_nodes, node_type, region.name, region.env_uuid, e))


def add_empty_nodes(empty_nodes, region):
    """
    This method creates Furion node objects for empty nodes that are
    present in Phoenix but not in Furion for the given region.
    """
    try:
        furion_nodes = Node.objects.filter(region=region, type=const.NODE_TYPE_EMPTY)
        furion_node_ips = [node.private_ip for node in furion_nodes]
        for node_ip in empty_nodes:
            if node_ip not in furion_node_ips:
                create_furion_node(region, node_ip, const.NODE_TYPE_EMPTY, const.STATE_RUNNING)
    except Exception as e:
        stdout('Failed to add empty nodes {} to Region {}:{}. Exception {}.'.format(
            empty_nodes, region.name, region.env_uuid, e))


def set_critical_error(region, node_ips):
    """
    This method sets state for the Node objects in Furion to CRITICAL
    for the provided node ips.
    """
    for node_ip in node_ips:
        node = Node.objects.filter(region=region, private_ip=node_ip).first()
        if node:
            stdout("Setting state for Node {} in Region {}:{} to STATE_CRITICAL.".format(
                node_ip, region.env_uuid, region.name))
            node.state = const.STATE_CRITICAL
            node.save()


def add_compute_nodes(env_uuid, region, phoenix_settings):
    """
    This method creates Furion nodes for compute nodes in Phoenix that
    are not in Furion. It also sets goal state on these nodes.
    """

    def _new_way_to_add_nodes(node_ip, attribute_nodes):
        node_type = const.PHOENIX_NODE_TYPE_COMPUTE
        if node_ip in attribute_nodes:
            node_type = const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE
        ip = get_same_node(node_type, region, node_ip)
        if ip:
            stdout("Find same ip. src: {}, tar: {}".format(ip, node_ip))
            NodeManager.copy_resources_to_node(env_uuid, ip, node_ip)
        else:
            stdout("Find same ip fail for: {}".format(node_ip))
            resource_names = copy.deepcopy(const.COMPUTE_RESOURCE_MESOS)
            resource_names.append(node_type.replace('-', '_'))
            add_resources_to_node(region, node_ip, resource_names)

    # Note that in Phoenix Settings, the SLAVE_NODES already include ip addresses for both
    # compute and attribute nodes.
    compute_nodes = phoenix_settings.get('SLAVE_NODES', [])
    attribute_nodes = phoenix_settings.get('MESOS_SLAVE_WITH_TAG', [])
    stdout("Adding {} nodes to region {}. Settings: {}".format(
        compute_nodes, env_uuid, phoenix_settings))
    new_nodes = []

    try:
        # Create nodes in Furion for each COMPUTE node.
        furion_nodes = Node.objects.filter(region=region, type=const.NODE_TYPE_COMPUTE)
        furion_node_ips = [node.private_ip for node in furion_nodes]
        for node_ip in compute_nodes:
            if node_ip not in furion_node_ips:
                new_nodes.append(node_ip)

        try:
            spectre_version = EnvironmentManager.get_spectre_version(region.env_uuid)
            for node_ip in new_nodes:
                create_furion_node(region, node_ip, const.NODE_TYPE_COMPUTE, const.STATE_DEPLOYING)

            if spectre_version >= const.SPECTRE_OLD_VERSION:
                for node_ip in compute_nodes:
                    _new_way_to_add_nodes(node_ip, attribute_nodes)
            else:
                RegionManager.add_slave_to_region(env_uuid, phoenix_settings, update=False)

        except Exception as e:
            stdout('Failed to add goal state to nodes {} to Region {}:{}. Exception {} {}.'.format(
                new_nodes, region.name, region.env_uuid, e, traceback.format_exc()))
            for node_ip in compute_nodes:
                furion_node = Node.objects.filter(region=region, private_ip=node_ip).first()
                if furion_node:
                    furion_node.update(state=const.STATE_CRITICAL, updated_at=timezone.now())

        region_setting = RegionSettingControl.get_object(**{'region_id': region.id})
        region_setting.slave_node.extend(compute_nodes)
        region_setting.slave_node = list(set(region_setting.slave_node))
        if attribute_nodes:
            region_setting.cluster_settings.setdefault(
                'MESOS_SLAVE_WITH_TAG', []).extend(attribute_nodes)
            nodes = copy.deepcopy(region_setting.cluster_settings.setdefault(
                'MESOS_SLAVE_WITH_TAG', []))
            region_setting.cluster_settings['MESOS_SLAVE_WITH_TAG'] = list(set(nodes))
        region_setting.save()

    except Exception as e:
        # If we fail to add a compute node, set its state to CRITICAL since we do not
        # know what state the failed node is in. In this state, the user will see an
        # error but the node sync thread will not set the node to RUNNING even if it
        # is in goal state since we dont know if the correct goal state was set.
        stdout('Failed to add compute nodes {} to Region {}:{}. Exception {} {}.'.format(
            compute_nodes, region.name, region.env_uuid, e, traceback.format_exc()))
        set_critical_error(region, compute_nodes)


def add_swarm_worker_nodes(env_uuid, region, phoenix_settings):
    def _new_way_to_add_nodes(node_ip):
        if spectre_version >= const.SPECTRE_OLD_VERSION:
            node_type = const.PHOENIX_NODE_TYPE_SWARM_WORKER
            ip = get_same_node(node_type, region, node_ip)
            if ip:
                NodeManager.copy_resources_to_node(env_uuid, ip, node_ip)
            else:
                resource_names = copy.deepcopy(const.COMPUTE_RESOURCE_SWARM)
                resource_names.append(node_type)
                add_resources_to_node(region, node_ip, resource_names)
                update_swarm_worker(region, node_ip)

    swarm_worker_nodes = phoenix_settings.get('SWARM_WORKER_NODES', [])
    stdout("Adding {} nodes to region {}. Settings: {}".format(
        len(swarm_worker_nodes), env_uuid, phoenix_settings))
    new_nodes = []

    try:
        # Create nodes in Furion for each COMPUTE node.
        furion_nodes = Node.objects.filter(region=region, type=const.NODE_TYPE_COMPUTE)
        furion_node_ips = [node.private_ip for node in furion_nodes]
        for node_ip in swarm_worker_nodes:
            if node_ip not in furion_node_ips:
                new_nodes.append(node_ip)

        try:
            spectre_version = EnvironmentManager.get_spectre_version(region.env_uuid)
            for node_ip in swarm_worker_nodes:
                if node_ip in new_nodes:
                    create_furion_node(region, node_ip,
                                       const.NODE_TYPE_COMPUTE,
                                       const.STATE_DEPLOYING)
            if spectre_version >= const.SPECTRE_OLD_VERSION:
                for node_ip in swarm_worker_nodes:
                    _new_way_to_add_nodes(node_ip)
            else:
                RegionManager.add_slave_to_region(
                    env_uuid,
                    phoenix_settings=phoenix_settings,
                    template_type=const.CONTAINER_MANAGER_SWARM.lower(),
                    update=True)
        except Exception as e:
            stdout('Failed to add goal state to nodes {} to Region {}:{}. Exception {}.'.format(
                new_nodes, region.name, region.env_uuid, e))
            for node_ip in swarm_worker_nodes:
                furion_node = Node.objects.filter(region=region, private_ip=node_ip).first()
                if furion_node:
                    furion_node.update(state=const.STATE_CRITICAL, updated_at=timezone.now())

        region_setting = RegionSettingControl.get_object(**{'region_id': region.id})
        region_setting.slave_node.extend(swarm_worker_nodes)
        region_setting.slave_node = list(set(region_setting.slave_node))
        region_setting.save()

    except Exception as e:
        # If we fail to add a compute node, set its state to CRITICAL since we do not
        # know what state the failed node is in. In this state, the user will see an
        # error but the node sync thread will not set the node to RUNNING even if it
        # is in goal state since we dont know if the correct goal state was set.
        stdout('Failed to add compute nodes {} to Region {}:{}. Exception {}.'.format(
            swarm_worker_nodes, region.name, region.env_uuid, e))
        set_critical_error(region, swarm_worker_nodes)


def add_k8s_nodes(env_uuid, region, phoenix_settings):
    k8s_compute_nodes = phoenix_settings.get('KUBERNETES_COMPUTE_NODES', [])
    stdout("Adding {} nodes to region {}. Settings: {}".format(
        len(k8s_compute_nodes), env_uuid, phoenix_settings))
    new_nodes = []

    try:
        # Create nodes in Furion for each COMPUTE node.
        furion_nodes = Node.objects.filter(region=region, type=const.NODE_TYPE_K8S_SLAVE)
        furion_node_ips = [node.private_ip for node in furion_nodes]
        for node_ip in k8s_compute_nodes:
            if node_ip not in furion_node_ips:
                new_nodes.append(node_ip)

        try:
            for node_ip in new_nodes:
                create_furion_node(region, node_ip, const.NODE_TYPE_COMPUTE, const.STATE_DEPLOYING)
            for node_ip in k8s_compute_nodes:
                ip = get_same_node(const.PHOENIX_NODE_TYPE_K8S_SLAVE, region, node_ip)
                if ip:
                    NodeManager.copy_resources_to_node(env_uuid, ip, node_ip)
                else:
                    add_resources_to_node(region, node_ip, const.COMPUTE_RESOURCE_K8S)
        except Exception as e:
            stdout('Failed to add goal state to nodes {} to Region {}:{}. Exception {}.'.format(
                new_nodes, region.name, region.env_uuid, e))
            for node_ip in new_nodes:
                furion_node = Node.objects.filter(region=region, private_ip=node_ip).first()
                if furion_node:
                    furion_node.update(state=const.STATE_CRITICAL, updated_at=timezone.now())

        region_setting = RegionSettingControl.get_object(**{'region_id': region.id})
        region_setting.slave_node.extend(new_nodes)
        region_setting.slave_node = list(set(region_setting.slave_node))
        region_setting.save()
    except Exception as e:
        # If we fail to add a compute node, set its state to CRITICAL since we do not
        # know what state the failed node is in. In this state, the user will see an
        # error but the node sync thread will not set the node to RUNNING even if it
        # is in goal state since we dont know if the correct goal state was set.
        stdout('Failed to add compute nodes {} to Region {}:{}. Exception {}.'.format(
            new_nodes, region.name, region.env_uuid, e))
        set_critical_error(region, new_nodes)


def add_region_nodes(region_map):
    """
    This method adds empty & compute Nodes to Furion.
    """
    for env_uuid in region_map:
        region = region_map[env_uuid].get('region', None)
        if region is None:
            stdout("No region object found for {}. Unable to add nodes.".format(env_uuid))
            continue

        empty_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_EMPTY, [])
        compute_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_COMPUTE, [])
        attribute_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE, [])
        swarm_worker_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_SWARM_WORKER, [])
        k8s_compute_nodes = region_map[env_uuid].get(const.PHOENIX_NODE_TYPE_K8S_SLAVE, [])
        compute_nodes.extend(attribute_nodes)

        if empty_nodes:
            if region.state == const.STATE_CRITICAL:
                # Region in Critical state. Add nodes as CRITICAL.
                add_critical_node(empty_nodes, region, const.NODE_TYPE_EMPTY)
            else:
                add_empty_nodes(empty_nodes, region)

        if compute_nodes:
            if (region.container_manager != const.CONTAINER_MANAGER_MESOS or
                    region.state == const.STATE_CRITICAL):
                # Region is in Critical state or User is attempting to add a COMPUTE node
                # to an EMPTY region. Add node as CRITICAL.
                add_critical_node(compute_nodes, region, const.NODE_TYPE_COMPUTE)
            else:
                phoenix_settings = region_map[env_uuid].get('user_settings', {})
                if phoenix_settings:
                    add_compute_nodes(env_uuid, region, phoenix_settings)
                else:
                    stdout("No phoenix_settings found for {}. Unable to add nodes.".format(env_uuid))

        if swarm_worker_nodes:
            if (region.container_manager != const.CONTAINER_MANAGER_SWARM or
                    region.state == const.STATE_CRITICAL):
                add_critical_node(swarm_worker_nodes, region, const.NODE_TYPE_SWARM_WORKER)
            else:
                phoenix_settings = region_map[env_uuid].get('user_settings', {})
                if phoenix_settings:
                    add_swarm_worker_nodes(env_uuid, region, phoenix_settings)
                else:
                    stdout("No phoenix_settings found for {}. Unable to add nodes.".format(env_uuid))

        if k8s_compute_nodes:
            if (region.container_manager != const.CONTAINER_MANAGER_K8S or
                    region.state == const.STATE_CRITICAL):
                add_critical_node(k8s_compute_nodes, region, const.NODE_TYPE_K8S_SLAVE)
            else:
                phoenix_settings = region_map[env_uuid].get('user_settings', {})
                if phoenix_settings:
                    add_k8s_nodes(env_uuid, region, phoenix_settings)
                else:
                    stdout("No phoenix_settings found for {}. Unable to add nodes.".format(
                        env_uuid))


def add_nodes():
    """
    This method identifies new nodes in Phoenix and adds them to Furion
    and sets goal state on these nodes.
    """
    if is_task_running('Azzinoth', 120):
        stdout("Another add_nodes task running. Try later.")
        return
    stdout("Starting an add_nodes task.")

    # Get nodes of all the types that can be created by user using script.
    # The method will only return nodes with initial incarnation so that
    # we do not iterate over nodes that already have a goal state set.
    empty_nodes = get_phoenix_nodes_by_type(const.PHOENIX_NODE_TYPE_EMPTY)
    compute_nodes = get_phoenix_nodes_by_type(const.PHOENIX_NODE_TYPE_COMPUTE)
    attribute_nodes = get_phoenix_nodes_by_type(const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE)
    swarm_worker_nodes = get_phoenix_nodes_by_type(const.PHOENIX_NODE_TYPE_SWARM_WORKER)
    k8s_compute_nodes = get_phoenix_nodes_by_type(const.PHOENIX_NODE_TYPE_K8S_SLAVE)

    region_map = {}
    add_nodes_to_map(region_map, empty_nodes, const.PHOENIX_NODE_TYPE_EMPTY)
    add_nodes_to_map(region_map, compute_nodes, const.PHOENIX_NODE_TYPE_COMPUTE)
    add_nodes_to_map(region_map, attribute_nodes, const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE)
    add_nodes_to_map(region_map, swarm_worker_nodes, const.PHOENIX_NODE_TYPE_SWARM_WORKER)
    add_nodes_to_map(region_map, k8s_compute_nodes, const.PHOENIX_NODE_TYPE_K8S_SLAVE)
    update_region_settings(region_map)
    add_region_nodes(region_map)
    stdout("Completed add_nodes task.")
