from django.http import Http404
from controls.region import RegionControl
from controls.node import NodeControl
from util import const
from util.cron import stdout, is_task_running
import logging
from app.models import Node

LOG = logging.getLogger(__name__)


def add_vmware_node():
    if is_task_running('Vmware_In', 120):
        stdout("Another add_vmware_node task running. Try later.")
        return
    stdout("Starting an add_nodes task for Vmware instance.")
    nodes_to_check = []
    nodes = Node.objects.filter(private_ip=const.VMWARE_CLONING_STATE_IP)

    for node in nodes:
        region = RegionControl.get_object(id=node.region_id)
        cloud = RegionControl.get_cloud_data(region)
        if cloud['name'] == const.CLOUD_VMWARE:
            nodes_to_check.append(node)

    for node in nodes_to_check:
        stdout("i am going to check ip for node {}".format(node))
        region = RegionControl.get_object(id=node.region_id)
        cloud_obj = RegionControl.get_cloud(region)
        new_vm_ip = cloud_obj.check_clone_ip(node.instance_id)
        stdout("new ip is {}".format(new_vm_ip))

        if new_vm_ip is not None:
            node_type = const.PHOENIX_NODE_TYPE_COMPUTE
            if node.type == const.NODE_TYPE_EMPTY:
                node_type = const.PHOENIX_NODE_TYPE_EMPTY
            elif node.type == const.NODE_TYPE_COMPUTE:
                if region.container_manager == const.CONTAINER_MANAGER_SWARM:
                    node_type = const.PHOENIX_NODE_TYPE_SWARM_WORKER
                elif region.container_manager == const.CONTAINER_MANAGER_K8S:
                    node_type = const.PHOENIX_NODE_TYPE_K8S_SLAVE
            user_data = RegionControl.get_user_data(region, node_type)
            inst_attr = region.features.get('node', {}).get('instance', {})
            name_prefix = inst_attr.pop('name_tag', None)
            if not name_prefix:
                inst_attr['name_tag'] = 'alauda_{}'.format(region.name)
            else:
                inst_attr['name_tag'] = name_prefix
            NodeControl.update_node_ip(region.id, const.VMWARE_CLONING_STATE_IP, new_vm_ip)
            try:
                node = NodeControl.get_object(region=region, private_ip=new_vm_ip)
                if node is not None:
                    stdout("i am going to execute command for node {}".format(node.private_ip))
                else:
                    stdout("node is none")
            except Http404:
                continue
            cloud_obj.execute_command(node.instance_id, user_data,
                                      inst_attr['template_user'], inst_attr['template_password'])


def delete_vmware_node():
    if is_task_running('Vmware_Out', 120):
        stdout("Another delete_vmware_node task running. Try later.")
        return
    stdout("Starting an check delete task for Vmware instance.")
    nodes_to_check = []
    nodes = Node.objects.filter(state=const.STATE_DESTROYING)

    for node in nodes:
        region = RegionControl.get_object(id=node.region_id)
        cloud = RegionControl.get_cloud_data(region)
        if cloud['name'] == const.CLOUD_VMWARE:
            nodes_to_check.append(node)

    for node in nodes_to_check:
        stdout("i am going to check running state for node {}".format(node.private_ip))
        region = RegionControl.get_object(id=node.region_id)
        try:
            cloud_obj = RegionControl.get_cloud(region)
            if cloud_obj.is_deletable(node.instance_id):
                cloud_obj.destroy_instance(node.instance_id)
                node.delete()
        except Exception as e:
            stdout("exception happens when deleting node: {} Exception: {}".format(node.private_ip, e))
