#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.db import transaction
from app.models import Node
from controls.region import RegionControl
from django.db.models import Q
from util import const
import traceback
import datetime


def sync_private_ip():

    print "sync private ip start"

    nodes = Node.objects.filter(Q(private_ip__startswith='169.254.0.') |
                                Q(private_ip__startswith=const.KINGSOFT_PRIVATE_IP_PREFIX)).exclude(
        instance_id='').exclude(instance_id=None)

    print "try to sync {} ip".format(len(nodes))
    for node in nodes:
        try:
            manager = RegionControl.get_cloud(node.region)
            res = manager.get_instance_info(node.instance_id)
            with transaction.atomic():
                node.private_ip = res['private_ip']
                node.save()
        except Exception as e:
            print traceback.print_exc()
            print '[{}] [Error]__sync_private_ip__ | {}'.format(datetime.datetime.now(),
                                                                e.message)
    print "sync node finish"
