#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.utils import timezone
from django.db import transaction
from app.models import Node, Region
from controls.region import RegionControl
from controls.node import NodeControl
from util import const

import itertools
import traceback
import datetime

__author__ = 'Hang Yan, lgong <lgong@alauda.io>'


def delete_long_deploying_nodes():
    nodes = Node.objects.filter(state=const.STATE_DEPLOYING)
    for node in nodes:
        # just delete the auto-create node
        if not node.instance_id or node.type == const.MASTER_NODE_TYPE or node.type == const.MASTER_AND_SLAVE_NODE_TYPE:
            # instance_id is null/"" means this node is not created by API
            continue
        diff = (timezone.now() - node.updated_at).total_seconds()
        if diff > const.FAILED_NODE_INTERVAL:
            print "Preparing to delete long deployed node. | {}".format(node)
            NodeControl.delete(node.region, node.private_ip)


def sync_private_ip():

    nodes = Node.objects.filter(private_ip__startswith=const.QING_PRIVATE_IP_PREFIX
                                ).exclude(instance_id='').exclude(instance_id=None).values()

    data = sorted(nodes, key=lambda x: x['region_id'])

    node_dict = {
        k: [node['instance_id'] for node in v]
        for k, v in itertools.groupby(data, key=lambda x: x['region_id'])
    }

    for region_id, nodes in node_dict.iteritems():
        try:
            region = Region.objects.get(pk=region_id)
            qcloud = RegionControl.get_cloud(region)
            res = qcloud.session.describe_instances(instances=nodes)
            if 0 == res['ret_code']:
                instance_dict = {}
                for inst in res['instance_set']:
                    if inst['vxnets']:
                        instance_dict[inst['instance_id']] = inst['vxnets'][0]['private_ip']
                    else:
                        continue
                with transaction.atomic():
                    node_objects = Node.objects.select_for_update().filter(
                        instance_id__in=instance_dict)
                    for obj in node_objects:
                        obj.private_ip = instance_dict[obj.instance_id]
                        # avoid conflict with puck cron
                        puck_node = Node.objects.filter(private_ip=obj.private_ip,
                                                        region_id=obj.region_id).first()
                        if puck_node:
                            obj.attr.update(puck_node.attr)
                            obj.attr['source'] = const.NODE_SOURCE_API_CREATE
                            obj.resources.update(puck_node.resources)
                            obj.state = puck_node.state
                            puck_node.delete()
                        obj.save()
            else:
                continue
        except Exception as e:
            print traceback.print_exc()
            print '[{}] [Error]__sync_private_ip__ | {}'.format(datetime.datetime.now(), e.message)
