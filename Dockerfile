FROM  index.alauda.cn/alaudaorg/alpine:base
LABEL maintainer='hangyan@alauda.io,xdzhang@alauda.io,lgong@alauda.io,yiyuan@alauda.io'

RUN apk update && \
    apk add  linux-pam  && \
    mkdir -p /var/log/mathilde/ && chmod 755 /var/log/mathilde/ && \
    apk add openssh-client git 

WORKDIR /furion

RUN python -m pip install --upgrade pip

EXPOSE 8080

ENV TERM xterm
ENV UWSGI_CHEAPER 10
ENV UWSGI_CHEAPER_INITIAL 10

RUN apk add gcc g++ make libffi-dev 

COPY requirements.txt /
RUN pip install --no-cache-dir  -r /requirements.txt

RUN pip install --no-cache-dir supervisor-stdout

COPY . /furion
RUN pip install  --no-cache-dir  -r /furion/requirements-dev.txt
RUN pip install --no-cache-dir --trusted-host pypi.alauda.io --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ -r /furion/requirements-alauda.txt

RUN echo '' > /etc/nginx/conf.d/default.conf && echo "daemon off;" >> /etc/nginx/nginx.conf && \
    mkdir /run/nginx && \
    rm -rf /etc/supervisord.conf && \
    ln -s /furion/conf/furion_nginx.conf /etc/nginx/conf.d/furion_nginx.conf && \
    ln -s /furion/conf/supervisord.conf /etc/supervisord.conf && \
    chmod a+x /furion/*.sh

CMD ["/furion/run.sh"]
