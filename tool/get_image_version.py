#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: yfwang
# @Date:   2017-07-17 12:05:10
# @Email:  yfwang@alauda.io
# @Last Modified by:   yfwang
# @Last Modified time: 2017-12-18 11:13:54

import os
import importlib


def get_mod_name():
    dir = '/usr/lib/python2.7/site-packages/alauda/alchemist/templates/components'
    file = os.listdir(dir)
    mod_name = []
    for name in file:
        if name[-10:] == 'version.py':
            mod_name.append(name[:-3])

    if 'dd_server_version' in mod_name:
        mod_name.remove('dd_server_version')
    return mod_name


component_map = {
    'alb_haproxy': 'alb',
    'alb_nginx': 'alb',
    'alb_xlb': 'alb',
    'chronos': 'gondar',
    'coil_client': 'coil-client',
    'controller_haproxy': 'controller_haproxy',
    'dd_agent': 'dd_agent',
    'docker_daemon_proxy': 'docker-daemon-proxy',
    'doom': 'doom',
    'etcd': 'etcd',
    'glusterfs_control': 'glusterfs',
    'glusterfs_node': 'glusterfs',
    'haproxy': 'haproxy',
    'internal_haproxy': 'haproxy',
    'keepalived': 'keepalived',
    'kubernetes_compute': 'harbinger',
    'kubernetes_init': 'harbinger',
    'marathon': 'marathon',
    'meepo': 'meepo',
    'mesos_master': 'mesos-master',
    'mesos_slave': 'mesos-slave',
    'mesos_slave_ipvlan': 'mesos-slave',
    'mesos_slave_macvlan': 'mesos-slave',
    'monkey': 'monkey',
    'nevermore': 'nevermore',
    'puck': 'puck',
    'pudge': 'pudge',
    'registry': 'alauda-registry',
    'xin_agent': 'xin',
    'xin_driver': 'xin',
    'zeus': 'zeus',
    'zookeeper': 'zookeeper'
}

for component_name_version in get_mod_name():
    mod_name = 'alauda.alchemist.templates.components.{}'.format(component_name_version)
    mod = importlib.import_module(mod_name)
    version = mod.DEFAULT_VERSION
    component_name = mod.VERSION_TEMPLATE_MAP[mod.DEFAULT_VERSION]
    try:
        image_name = component_map[component_name]
    except KeyError:
        print "\n{} is not in component_map, please update component_map!!!!!!!!\n".format(component_name)
    print "claas/{}:{}".format(image_name, version)
