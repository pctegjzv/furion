#!/usr/bin/env python
import os

import django
from django.conf import settings

os.environ["DJANGO_SETTINGS_MODULE"] = "furion.settings"


if __name__ == "__main__":
    django.setup()
    print("django env setup succeeded.")
    from app.models import Region

    count = 0
    regions = Region.objects.all()
    for r in regions:
        if r.platform_version == "v4" and r.features and ("logs" in r.features):
            print("migrating region {}({}) ...".format(r.name, r.id))
            r.features = {}
            r.save()
            count += 1
            print("done.")
    
    print("successfully migrated {} regions".format(count))