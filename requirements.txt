Django==1.8.9
django-jsonfield==0.9.19
djangorestframework==3.3.2
psycopg2==2.7.3
pycrypto
pyvmomi
boto3
django-crontab
python-dateutil
redis
qingcloud-sdk
mysqlclient==1.3.9
Jinja2
django-extensions
django-log-request-id
sqlalchemy==1.1.10
alauda-redis-py-cluster==1.3.4
alauda-kombu==3.0.38
alauda-celery==3.1.25rc1
kubernetes==6.0.0
schema==0.6.6
shortuuid
semantic_version==2.6.0
