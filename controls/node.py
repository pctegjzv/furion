#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import logging
from collections import defaultdict
import yaml
import json
import copy

from django.conf import settings
from django.db import transaction
from django.forms import model_to_dict
from django.utils import timezone
from operator import itemgetter

from app.control import Control
from app.serializers import NodeSerializer
from controls.label import KubernetesLabelManager
from controls.region import RegionControl, RegionSettingControl

from app.models import Node, RegionSetting, Label
from django.shortcuts import get_object_or_404
from furion.exception import FurionException, InvalidRegionDataException, ErrCodes
from util import const
from util.events import Event
from alauda.clients.furion import rget
from util.phoenix import NodeManager
from util.phoenix import RegionManager
from util.phoenix import ResourceManager
from util.phoenix import EnvironmentManager
from util.phoenix import ResourceUtil

from alauda.common.collection import pop_keys
from alauda.common.util import format_resource_value

from dateutil import parser
from django.db.models import Q

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


def get_same_node(node_type, region, private_ip):
    def _get_phoenix_healthy_compute_nodes():
        result = {}
        nodes = NodeManager.list_nodes(region.env_uuid)
        for node in nodes:
            if node['node_type'] in ['controller', 'empty', '']:
                continue
            if node['health_state'] != 'HEALTHY':
                continue
            # 1 is init incarnation. so health goal stats can't be 1
            if node['goal_incarnation'] <= 1:
                continue
            result[node['ip_address']] = node
        return result

    def _get_furion_compute_nodes():
        nodes = Node.objects.filter(type=const.NODE_TYPE_COMPUTE, region=region)
        result = {}
        for node in nodes:
            result[node.private_ip] = node.state
        return result

    furion_nodes = _get_furion_compute_nodes()
    phoenix_nodes = _get_phoenix_healthy_compute_nodes()

    node_ips = list(set(furion_nodes.keys()) & set(phoenix_nodes.keys()))
    for ip in node_ips:
        if ip == private_ip:
            continue
        if node_type != phoenix_nodes[ip]['node_type']:
            continue
        return ip
    return None


def add_resources_to_node(region, ip, resource_names):
    node = NodeManager.retrieve(region.env_uuid, ip)
    resources = ResourceManager.list_resources(region.env_uuid)
    for resource in resources:
        if resource['name'] in resource_names or resource['type'] == 'REGISTRIES':
            ResourceManager.add_node(resource['resource_uuid'], node['node_uuid'])


def update_swarm_worker(region, ip):
    resources = ResourceManager.list_resources(region.env_uuid)
    for resource in resources:
        if resource['name'] == const.CONTAINER_MANAGER_SWARM.lower():
            resource['identifier']['swarm_workers'].append(ip)
            del resource['created_at']
            del resource['updated_at']
            ResourceManager.update(region.env_uuid, resource['resource_uuid'], resource)


class NodeControl(Control):
    model_name = 'Node'
    model = Node
    serializer = NodeSerializer

    @classmethod
    def _get_labels(cls, node):
        """Get node'labels.
        The non-editable items will be put at front.
        Args:
            node(Node):
        Return:
            list[dict]
        """
        label_objects = Label.objects.filter(node=node)
        labels = [model_to_dict(x, fields=['key', 'value', 'editable']) for x in label_objects]
        result = sorted(labels, key=itemgetter('editable'))
        keys = [x['key'] for x in result if not x['editable']]
        if 'ip' not in keys and 'node_tag' in node.attr:
            result.insert(0, Label.node_tag_to_label(node.attr['node_tag']))
        return result

    @classmethod
    def _update_public_ip_attr(cls, node):
        """Set public_ip in node.attr if in cloud env."""
        if node.instance_id and not node.attr.get('public_ip'):
            rc = RegionControl.get_region_lib(node.region)
            if 'OPENSTACK' != rc.get_cloud_name() and const.CLOUD_VMWARE != rc.get_cloud_name() \
                    and const.CLOUD_KINGSOFT != rc.get_cloud_name():
                cloud_manager = RegionControl.get_cloud(node.region)
                info = cloud_manager.get_instance_info(node.private_ip)
                node.attr['public_ip'] = info['attr']['public_ip']
                node.save(skip_update_time=True)

    @classmethod
    def _check_node_comps(cls, node, data):
        """Update node data to UNKNOWN state if no comps update.
        
        Args:
            node(Node): node object
            data(dict): origin node data
        Returns:
            dict: updated node data
        """

        def _gen_docker():
            return {'status': const.STATE_RUNNING, 'type': 'DOCKER'}

        if node.attr.get('components', []):
            updated_at = node.attr.get('comps_updated_at')
            if updated_at:
                expire = timezone.now() - parser.parse(updated_at) >= datetime.timedelta(minutes=1)
            else:
                expire = True
            comps = data['attr']['components']
            comps.append(_gen_docker())
            if expire:
                for comp in comps:
                    comp['status'] = const.STATE_UNKNOWN
        return data

    @classmethod
    def get_nodes(cls, region, node_type):
        """Get node list by type.
        
        Args:
            region(Region): region object
            node_type(str): EMPTY/SYS/SLAVE
            
        Returns:
            list[dict]: list of node info
        """
        nodes = Node.objects.filter(region=region, type=node_type)
        return [cls._format(x, detail=False) for x in nodes]

    @classmethod
    def _update_k8s_node_schedulable_status(cls, node, data):
        """Modify sys node type to SYSLAVE if this region have schedulable master feature.
        Also set the all the  node's scheduler status
        Args:
            node(node): list of controller node object
            data(dict): the origin node data
        Returns:
            data: updated node data
        """

        if node.type == const.NODE_TYPE_K8S_SLAVE:
            if 'schedulable' not in data['attr']:
                data['attr']['schedulable'] = True
            return data
        rc = RegionControl.get_region_lib(node.region)
        if rc.is_kubernetes_region():
            manager = rc.get_kubernetes_config()
            if manager.get('schedulable_master'):
                if node.type == const.NODE_TYPE_K8S_MASTER:
                    data['type'] = const.NODE_TYPE_K8S_SYSLAVE
                    if 'schedulable' not in data['attr']:
                        data['attr']['schedulable'] = True
                    return data
        if node.type == const.NODE_TYPE_K8S_MASTER:
            data['attr']['schedulable'] = False
            if 'node_tag' in data:
                del data['node_tag']
        return data

    @classmethod
    def _format(cls, obj, detail=True):
        """Serialized node data for retrieve/list node api.
        Note: This function will also maybe update node's data in db.
        Args:
            obj(Node): node object
            detail(bool): if need detailed information. Normally detail=true for node detail api.
        Returns:
            dict: node info dict
        """
        if detail:
            cls._update_public_ip_attr(obj)
        if obj.state == const.STATE_STOPPING:
            obj.state = const.STATE_STOPPED
            obj.save()
        data = super(NodeControl, cls)._format(obj)
        pop_keys(['region'], data)
        if detail:
            data = cls._check_node_comps(obj, data)
            data['attr'].update({'init_volume_size': obj.attr.get('init_volume_size', '')})
        if 'node_tag' in data['attr']:
            data['node_tag'] = data['attr']['node_tag']
        if obj.type != const.NODE_TYPE_EMPTY:
            data['labels'] = cls._get_labels(obj)
        if obj.region.container_manager == "KUBERNETES":
            data = cls._update_k8s_node_schedulable_status(obj, data)
        return data

    @classmethod
    def select_node_by_labels(cls, region_id, labels):
        """Check the matched node of labels
        Args:
            region_id(str):
            labels(str): format: k1:v1,k2:v2
        Return:
            dict: {
                "nodes": ["127.0.0.1"]
            }
        """
        result = {
            "nodes": []
        }
        if not labels:
            return result
        label_selector = {}
        kvs = labels.split(",")
        for kv in kvs:
            k, v = kv.split(":")
            label_selector[k] = v
        LOG.debug("Receive node label selector: {}".format(labels))

        node_ip_list = []
        nodes = Node.objects.filter(region__id=region_id, state=const.STATE_RUNNING)
        for node in nodes:
            node_labels = cls._get_labels(node)
            node_labels = {x['key']: x['value'] for x in node_labels}
            if label_selector.viewitems() <= node_labels.viewitems():
                node_ip_list.append(node.private_ip)

        result['nodes'] = node_ip_list
        return result

    @classmethod
    def update_node_ip(cls, region_id, private_ip, new_ip):
        nodes = cls.model.objects.filter(
            Q(private_ip=private_ip), region_id=region_id).select_related(
            'region__container_manager').only(
            'id', 'private_ip', 'instance_id', 'resources', 'type', 'state',
            'attr', 'created_at', 'updated_at', 'region__container_manager'
        )
        node = None
        for n in nodes:
            if n.private_ip == private_ip:
                node = n
        if not node:
            raise FurionException(
                'resource_not_exist', message='{} not found! | {}'.format(
                    cls.model_name, {
                        'region_id': region_id,
                        'private_ip': private_ip
                    })
            )
        node.private_ip = new_ip
        node.save(update_fields=['private_ip'])

    @classmethod
    def update(cls, region_id, private_ip, data):
        nodes = cls.model.objects.filter(
            Q(private_ip=private_ip) | Q(type=const.NODE_TYPE_CONTROLLER),
            region_id=region_id).select_related(
            'region__container_manager').only(
            'id', 'private_ip', 'instance_id', 'resources', 'type', 'state',
            'attr', 'created_at', 'updated_at', 'region__container_manager'
        )

        node, sys_com_nodes = None, []

        for n in nodes:
            if n.private_ip == private_ip:
                node = n
            if const.NODE_TYPE_CONTROLLER == n.type:
                sys_com_nodes.append(n)

        if not node:
            raise FurionException(
                'resource_not_exist', message='{} not found! | {}'.format(
                    cls.model_name, {
                        'region_id': region_id,
                        'private_ip': private_ip
                    })
            )

        max_disk_usage = data['attr'].pop('max_disk_usage', None)
        metrics_type = const.REGION_METRICS_MAP.get(node.region.container_manager,
                                                    const.STAT_MESOS_METRICS)
        stats = None
        if max_disk_usage is not None:
            from controls.stats import StatsControl
            stats = StatsControl.update_disk_usage(
                region_id, metrics_type, private_ip, max_disk_usage,
                commit=False
            )

        if 'attr' in data:
            node.attr = dict(node.attr.items() + data['attr'].items())
            if data.get('attr', {}).get('components', []):
                node.attr['comps_updated_at'] = str(timezone.now())

        with transaction.atomic():
            node.save(update_fields=['attr'])
            if stats:
                stats.save(update_fields=['data', 'updated_at'])

        cls.send_comps_metrics(region_id, cls.get_sys_comps(region_id, sys_com_nodes)['components'],
                               private_ip, node.attr['components'],
                               max_disk_usage=max_disk_usage)

    @classmethod
    def get_sys_comps(cls, region_id, nodes=None):
        if not isinstance(nodes, list):
            nodes = Node.objects.filter(region__id=region_id, type=const.NODE_TYPE_CONTROLLER)
        if not len(nodes):
            return {'components': []}
        comps_flags = dict.fromkeys(const.NODE_SYS_TYPES, const.STATE_RUNNING)
        hosts = defaultdict(list)

        def _process_node(n):
            for c in n['attr'].get('components', []):
                t = c['type']
                if t in const.NODE_SYS_TYPES:
                    hosts[t].append(n['private_ip'])
                    if c['status'] in [const.STATE_ERROR, const.STATE_UNKNOWN]:
                        comps_flags[t] = c['status']

        for node in nodes:
            _process_node(cls._format(node))

        LOG.debug("Get sys components hosts map. | {}".format(hosts))
        return {
            'components': [
                {
                    'type': x,
                    "status": comps_flags[x],
                    "hosts": hosts[x]
                }
                for x in hosts.keys() if hosts[x]
            ]
        }

    @classmethod
    def create_nodes(cls, region, data, user_data, source=const.NODE_SOURCE_API_CREATE):
        """ @param: iaas request body
            {
                "image_id"
                "instance_type"
                "user_data"
                "name_tag"
                "security_groups"
                "subnet_id"
                "ssh_key_name"
                "login_mode"
            }
        """
        # TODO:SS_Milestone2 - Currently this method creates a Slave (Compute) node only.
        # Ensure that in future, this is capable of creating an empty node too.
        support_args = ['image_id', 'instance_type', 'security_groups', 'subnet_id', 'login_mode', 'user_data']
        vmware_args = ['name_tag', 'cpu_num', 'mem_size', 'disk_size', 'node_type']
        support_args.extend(vmware_args)
        inst_attr = region.features.get('node', {}).get('instance', {})

        inst_attr.update({key: v for key, v in data.iteritems() if key in support_args})

        custom_user_data = inst_attr.pop('user_data', None)
        if custom_user_data:
            inst_attr['user_data'] = user_data + '\n' + custom_user_data
        else:
            inst_attr['user_data'] = user_data

        name_prefix = inst_attr.pop('name_tag', None)
        if not name_prefix:
            inst_attr['name_tag'] = 'alauda_{}'.format(region.name)
        else:
            inst_attr['name_tag'] = name_prefix

        if not inst_attr.get('image_id'):
            cloud = RegionControl.get_cloud_data(region)
            if 'region_id' in cloud and cloud['region_id']:
                inst_attr['image_id'] = settings.DEFAULT_IMAGE_IDS[cloud['name']][cloud['region_id']]

        if 'ssh_key_name' in data:
            inst_attr['ssh_key_name'] = data['ssh_key_name']

        iam_instance_profile = inst_attr.pop('iam_instance_profile', None)
        if iam_instance_profile:
            inst_attr['iam_instance_profile'] = {
                "Name": iam_instance_profile
            }
        cloud_driver = RegionControl.get_cloud(region)
        res = cloud_driver.create_instances(
            inst_attr,
            data.get('num'),
            data.get('volume_size')
        )
        # when use aws-vpc backend for flannel, instance should disable source check
        if iam_instance_profile:
            for ins in res:
                cloud_driver.disable_instance_source_check(ins['instance_id'])

        with transaction.atomic():
            for x in res:
                x['region'] = region
                x['attr']['source'] = source
                x['type'] = const.PHOENIX_NODE_TYPE_MAP[data['node_type']]
                node = Node.objects.create(**x)
                Event.node_add(region, node.private_ip, node.type, node.state)

    @classmethod
    def delete(cls, region, private_ip):
        """
        This method will remove a node from Furion. This method can be called on compute
        and empty nodes. Deleting a node from ACMP removes it from ACMP. Deleting an empty
        node deletes it from the region.
        
        If this node is a cloud instance, delete it from the cloud first.
        
        Raises:
            FurionException: resource_not_exist, not_support
        
        """
        node = Node.objects.filter(region=region, private_ip=private_ip).first()
        if not node:
            return
        try:
            if node.instance_id:
                # This deletes the node from the cloud service.
                RegionControl.get_cloud(copy.deepcopy(region)).delete_instance(node.instance_id)
        except FurionException as e:
            if e.code != 'resource_not_exist':
                raise e
        except Exception as e:
            LOG.error("Hit error when delete instance: {}. error: {}".format(private_ip, e))

        cloud = region.attr.get('cloud', {})
        if node.type == const.NODE_TYPE_COMPUTE:
            # Remove the resources from the node and remove the node from Phoenix and Furion.
            try:
                cls._remove_slave_node(region, private_ip)
            except Exception as e:
                LOG.error("Failed to remove Node {} from Region {}:{}. Exception - {}".format(
                    private_ip, region.name, region.env_uuid, e))
            cls._delete_phoenix_node(region, private_ip)
            Event.node_remove(region, private_ip, const.NODE_TYPE_COMPUTE)
            if cloud['name'] != const.CLOUD_VMWARE:
                node.delete()
            else:
                if node.instance_id:
                    node.state = const.STATE_DESTROYING
                    node.save(update_fields=['state'])
                else:
                    # delete the node not auto added.
                    node.delete()
        elif node.type == const.NODE_TYPE_EMPTY:
            # The node is empty so its resources have already been removed. Simply Remove
            # the node from Phoenix and then from the region.
            cls._delete_phoenix_node(region, private_ip)
            Event.node_remove(region, private_ip, const.NODE_TYPE_EMPTY)
            if cloud['name'] != const.CLOUD_VMWARE:
                node.delete()
            else:
                if node.instance_id:
                    node.state = const.STATE_DESTROYING
                    node.save(update_fields=['state'])
                else:
                    # delete the node not auto added.
                    node.delete()
        else:
            message = 'Cannot delete node {} since it is of type {}'.format(private_ip, node.type)
            raise FurionException('not_support', message_params=[message])
        LOG.info("Remove node. | {} {}".format(region, private_ip))

    @classmethod
    def _delete_phoenix_node(cls, region, private_ip):
        LOG.debug("Delete Phoenix Node {} from Region - {}:{}.".format(
            private_ip, region.name, region.env_uuid))
        if len(region.env_uuid) > 0:
            try:
                NodeManager.delete(region.env_uuid, private_ip)
            except Exception as e:
                LOG.error("Failed to delete Node {} from Region - {}:{}. Exception - {}".format(
                    private_ip, region.name, region.env_uuid, e))

    @classmethod
    def _update_region_settings(cls, region, private_ip):
        with transaction.atomic():
            region_setting = get_object_or_404(RegionSetting.objects.select_for_update(), region=region)
            if not region_setting:
                return
            if private_ip in region_setting.slave_node:
                region_setting.slave_node.remove(private_ip)
            if private_ip in region_setting.cluster_settings.get('MESOS_SLAVE_WITH_TAG', []):
                region_setting.cluster_settings['MESOS_SLAVE_WITH_TAG'].remove(private_ip)
            region_setting.save()

    @classmethod
    def get_nodes_info(cls, region):
        """
        Returns information about the nodes in a given region.
        
        Args:
            region(Region): region object
        Returns:
            list(dict): list of nodes info
        
        """
        nodes = []
        if region.container_manager in const.CONTAINER_MANAGER_NOT_NONE_LIST:
            # First fetch controller nodes for currently supported managers(k8s,mesos,swarm)
            # Then fetch compute nodes for both legacy and ACMP regions.
            # puck_nodes = cls.get_puck_nodes(region=region)
            # We check for compute nodes even in EMPTY region. This is to display any node that
            # was incorrectly added to Furion.
            nodes += sorted(cls.get_nodes(region, const.NODE_TYPE_CONTROLLER), key=itemgetter('private_ip'))
            nodes += sorted(cls.get_nodes(region, const.NODE_TYPE_COMPUTE), key=itemgetter('private_ip'))
        if region.container_manager != const.CONTAINER_MANAGER_LEGACY:
            # Only fetch empty nodes if region is not legacy.
            nodes += sorted(cls.get_nodes(region, const.NODE_TYPE_EMPTY), key=itemgetter('private_ip'))

        result = []
        ip_list = []
        for x in nodes:
            if x['private_ip'] not in ip_list:
                ip_list.append(x['private_ip'])
                result.append(x)
        return result

    @classmethod
    def update_compute_node_stat(cls, region, obj, stat):
        """Update a single node's stats from agent metrics.

        1. Only update COMPUTE nodes (include schedulable master node in kubernetes)
        2. If node'status is OFFLINE, change it to RUNNING. (because it's alive now)

        Args:
            region(Region): node's region
            obj(Node): object
            stat(dict):
                {
                    "private_ip": "172.31.0.0",
                    "hostname": "host-1",
                    "uptime":
                    "cpus_total": 2,
                    "mem_total": 1024 #(M)
                    "cpus_allocated": 0.5,
                    "mem_allocated": 256,
                    "cpus_utilization": 0.25,
                    "mem_utilization": 0.25,
                    "containers_count":5,
                    "pods_count": 4,
                    # kubernetes only data
                    "labels": {
                        "hostname": "host-1"
                     },
                     "schedulable": true/false
                    # mesos/swarm only data
                    "gpus_total": 2,
                    "gpus_allocated": 1,
                    "gpus_utilization": 0.5,
                    "ports_total":
                    "ports_allocated":
                    "attributes":  {
                        "ip": "172.31.0.0"
                    }

                }

        """
        LOG.debug('Preparing update node with stat data. | {} {}'.format(stat['private_ip'], stat))
        obj.resources = {
            'total_cpus': str(stat['cpus_total']),
            'available_cpus': str(stat['cpus_total'] - stat['cpus_allocated']),
            'total_mem': format_resource_value(stat['mem_total']),
            'available_mem': format_resource_value(stat['mem_total'] - stat['mem_allocated']),
        }
        if 'containers_count' in stat:
            obj.resources['containers_count'] = int(stat['containers_count'])
        if 'pods_count' in stat:
            obj.resources['pods_count'] = int(stat['pods_count'])
        if obj.state == const.STATE_OFFLINE:
            Event.node_state_update(region, obj, obj.state)
            obj.state = const.STATE_RUNNING
        tag = rget(stat, 'attributes.ip')
        if tag:
            obj.attr['node_tag'] = "ip:{}".format(tag)
        elif rget(stat, 'labels.ip'):
            obj.attr['node_tag'] = "ip:{}".format(stat['labels']['ip'])
        elif 'node_tag' in obj.attr:
            del obj.attr['node_tag']
        obj.attr['schedulable'] = stat.get('schedulable', True)
        obj.save(force_update=True)

    @classmethod
    def update_labels(cls, region, private_ip, data):
        """Update a node's labels.
        This function will directly call kubernetes api to create the labels(--force).
        Args:
            region(Region):
            private_ip(str): node's private ip
            data(dict): label dict
        Raises:
            Http404
            FurionException('not_support')
        """
        if region.container_manager != const.CONTAINER_MANAGER_K8S:
            raise FurionException('not_support',
                                  message_params=['Update labels is only supported in kubernetes.'])
        node = cls.get_object(region=region, private_ip=private_ip)
        manager = RegionControl.get_manager_info(region)
        lm = KubernetesLabelManager(endpoint=manager['endpoint'], token=manager['token'])
        lm.set_labels(data, node)

    @classmethod
    def update_compute_nodes_stats(cls, region, stats):
        """Update a region's nodes stats.
        Only update compute/controller nodes.
        """
        records = Node.objects.filter(region=region,
                                      type__in=[const.NODE_TYPE_COMPUTE, const.NODE_TYPE_CONTROLLER])
        for stat in stats:
            obj = next((x for x in records if x.private_ip == stat['private_ip']), None)
            if not obj:
                LOG.warning("Node {} for Region {} does not exist!".format(stat['private_ip'], region.name))
                continue
            cls.update_compute_node_stat(region, obj, stat)

        ips = [x['private_ip'] for x in stats]

        def _process_record(record):
            if record.type != const.NODE_TYPE_COMPUTE or record.private_ip in ips:
                return

            if record.state == const.STATE_STOPPING:
                Event.node_state_update(region, record, const.STATE_STOPPED)
                record.state = const.STATE_STOPPED

            delta = timezone.now() - record.updated_at
            if record.state == const.STATE_RUNNING and delta > datetime.timedelta(hours=1):
                # The node is running but not present in agent response. For the first hour,
                # continue to treat the node as Running.or set the node state to 'STATE_OFFLINE'
                record.state = const.STATE_OFFLINE
                Event.node_state_update(region, record, const.STATE_OFFLINE)
            record.save()

        for r in records:
            _process_record(r)

    @classmethod
    def send_comps_metrics(cls, region_id, region_comps, private_ip, node_comps,
                           max_disk_usage=None):

        def _gen_node_mt(comp):
            return {
                "name": "comp-{}-status".format(comp['type'].lower()),
                "dimensions": {
                    "resource_id": region_id,
                    "resource_type": "NODE",
                    "node_private_ip": private_ip
                },
                "value": 0 if comp['status'] == const.STATE_RUNNING else 1,
                "unit": "Count",
            }

        def _gen_region_mt(comp):
            return {
                "name": "region-comp-{}-status".format(comp['type'].lower()),
                "dimensions": {
                    "resource_id": region_id,
                    "resource_type": "REGION",
                },
                "value": 0 if comp["status"] == const.STATE_RUNNING else 1,
                "unit": "Count",
            }

        data = [_gen_region_mt(x) for x in region_comps] + [_gen_node_mt(x) for x in node_comps]

        if max_disk_usage is not None:
            disk_mt = {
                "name": "node-max-disk-percent",
                "dimensions": {
                    "resource_id": region_id,
                    "resource_type": "NODE",
                    "node_private_ip": private_ip
                },
                "value": max_disk_usage,
                "unit": "Percent",
                "base": 0.01
            }
            data.append(disk_mt)
        try:
            from .tasks import SendMetrics
            if settings.METHOD_NAME:
                SendMetrics().delay(**{'data': data})
            else:
                SendMetrics().run(**{'data': data})
        except Exception as e:
            LOG.error('Send node metrics error : {}'.format(e.message))

    @classmethod
    def _add_mesos_compute_node(cls, region, node, is_slave_attribute):
        if region.container_manager != const.CONTAINER_MANAGER_MESOS:
            raise FurionException('region_data_incorrect', 'Region must have a container manager deployed.')
        phoenix_settings = RegionSettingControl.get_settings(region)
        phoenix_settings = yaml.safe_load(json.dumps(phoenix_settings['user_settings']))
        if isinstance(phoenix_settings['MASTER_NODES'], dict):
            phoenix_settings['MASTER_NODES'] = [phoenix_settings['MASTER_NODES']['private_ip']]
        phoenix_settings['SLAVE_NODES'] = [node.private_ip]
        phoenix_settings['MESOS_SLAVE_WITH_TAG'] = []
        NodeManager.clean_up_one_node(region.env_uuid, node.private_ip)
        node_type = const.PHOENIX_NODE_TYPE_COMPUTE
        if is_slave_attribute:
            phoenix_settings['MESOS_SLAVE_WITH_TAG'].append(node.private_ip)
            node_type = const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE
        # New way to add node
        spectre_version = EnvironmentManager.get_spectre_version(region.env_uuid)
        if spectre_version < const.SPECTRE_OLD_VERSION:
            RegionManager.add_slave_to_region(region.env_uuid, phoenix_settings)
        else:
            ip = get_same_node(node_type, region, node.private_ip)
            if ip:
                NodeManager.copy_resources_to_node(region.env_uuid, ip, node.private_ip)
            else:
                resource_names = copy.deepcopy(const.COMPUTE_RESOURCE_MESOS)
                resource_names.append(node_type.replace('-', '_'))
                add_resources_to_node(region, node.private_ip, resource_names)

        region_setting = RegionSetting.objects.get(region=region)
        region_setting.slave_node.extend(phoenix_settings['SLAVE_NODES'])
        region_setting.slave_node = list(set(region_setting.slave_node))
        if is_slave_attribute:
            node_with_tags = region_setting.cluster_settings.get('MESOS_SLAVE_WITH_TAG', [])
            node_with_tags.extend(phoenix_settings['MESOS_SLAVE_WITH_TAG'])
            node_with_tags = list(set(node_with_tags))
            region_setting.cluster_settings['MESOS_SLAVE_WITH_TAG'] = node_with_tags
        region_setting.save()

    @classmethod
    def _add_swarm_worker_node(cls, region, node):
        if region.container_manager != const.CONTAINER_MANAGER_SWARM:
            raise InvalidRegionDataException('Region manager must have be SWARM to add worker.')
        phoenix_settings = RegionSettingControl.get_settings(region)
        phoenix_settings = yaml.safe_load(json.dumps(phoenix_settings['user_settings']))
        phoenix_settings['SWARM_WORKER_NODES'].append(node.private_ip)
        phoenix_settings['SWARM_WORKER_NODES'] = list(set(phoenix_settings['SWARM_WORKER_NODES']))
        NodeManager.clean_up_one_node(region.env_uuid, node.private_ip)
        spectre_version = EnvironmentManager.get_spectre_version(region.env_uuid)
        if spectre_version < const.SPECTRE_OLD_VERSION:
            RegionManager.add_slave_to_region(region.env_uuid, phoenix_settings)
            RegionManager.add_slave_to_region(region.env_uuid,
                                              template_type=const.CONTAINER_MANAGER_SWARM.lower(),
                                              phoenix_settings=phoenix_settings,
                                              update=True)
        else:
            node_type = const.PHOENIX_NODE_TYPE_SWARM_WORKER
            ip = get_same_node(node_type, region, node.private_ip)
            if ip:
                NodeManager.copy_resources_to_node(region.env_uuid, ip, node.private_ip)
            else:
                add_resources_to_node(region, node.private_ip, const.COMPUTE_RESOURCE_SWARM)
                update_swarm_worker(region, ip)

        region_settings = RegionSetting.objects.get(region=region)
        region_settings.slave_node = phoenix_settings['SWARM_WORKER_NODES']
        region_settings.save()

    @classmethod
    def _add_k8s_compute_node(cls, region, node):
        phoenix_settings = RegionSettingControl.get_settings(region)
        phoenix_settings = yaml.safe_load(json.dumps(phoenix_settings['user_settings']))
        nodes = phoenix_settings['KUBERNETES_COMPUTE_NODES']
        nodes.append(node.private_ip)
        phoenix_settings['KUBERNETES_COMPUTE_NODES'] = list(set(nodes))

        NodeManager.clean_up_one_node(region.env_uuid, node.private_ip)
        ip = get_same_node(const.PHOENIX_NODE_TYPE_K8S_SLAVE, region, node.private_ip)
        if ip:
            NodeManager.copy_resources_to_node(region.env_uuid, ip, node.private_ip)
        else:
            add_resources_to_node(region, node.private_ip, const.COMPUTE_RESOURCE_K8S)

        region_settings = RegionSetting.objects.get(region=region)
        region_settings.slave_node = phoenix_settings['KUBERNETES_COMPUTE_NODES']
        region_settings.save()

    @classmethod
    def _remove_slave_node(cls, region, private_ip):
        LOG.info("Removing Slave {} from region {}:{}.".format(private_ip, region.name, region.env_uuid))
        if region.container_manager == const.CONTAINER_MANAGER_K8S:
            nm = RegionControl.get_kubernetes_manager(region)
            n = nm.get_node(private_ip)
            if n:
                LOG.info("Remove node from kubernetes: {}. ip: {}".format(n.metadata.name, private_ip))
                nm.delete_node(n.metadata.name)
            # remove labels
            node = Node.objects.filter(region=region, private_ip=private_ip).first()
            if node:
                Label.objects.filter(node=node).delete()

        phoenix_node = NodeManager.retrieve(region.env_uuid, private_ip)
        NodeManager.delete_resources(region.env_uuid, phoenix_node)
        ResourceUtil.add_cleanup_resource(region.env_uuid, region.container_manager, [phoenix_node])

        cls._update_region_settings(region, private_ip)

    @classmethod
    def _update_phoenix_type(cls, region, private_ip, node_type):
        if len(region.env_uuid) > 0:
            try:
                NodeManager.update_type(region.env_uuid, private_ip, node_type)
            except Exception as ex:
                error = 'Update node type failed. Node: {}, Env: {}, Type: {}, Error: {}'.format(
                    private_ip, region.env_uuid, node_type, ex.message)
                raise FurionException('alchemist_error', message_params=[error])

    @classmethod
    def update_type(cls, region, private_ip, phoenix_type):
        """
        Update a type for a node. The following type changes are permitted:
        mesos-slave           --> empty
        mesos-slave-attribute --> empty
        empty                 --> mesos-slave
        empty                 --> mesos-slave-attribute
        empty                 --> swarm_worker
        swarm_worker          --> empty
        empty                 --> k8s-slave
        """
        LOG.info("Update node: {}, type: {}".format(private_ip, phoenix_type))
        furion_type = const.PHOENIX_NODE_TYPE_MAP[phoenix_type]
        if furion_type == const.NODE_TYPE_CONTROLLER:
            message = "Changing node to Controller not permitted."
            raise FurionException('invalid_node_type', message_params=[message])

        node = cls.get_object(region=region, private_ip=private_ip)
        if node.type == const.NODE_TYPE_CONTROLLER:
            message = "Changing type of a Controller node not permitted."
            raise FurionException('invalid_node_type', message_params=[message])

        if node.type == const.NODE_TYPE_COMPUTE and furion_type == const.NODE_TYPE_EMPTY:
            # Remove node resources & change type from COMPUTE to EMPTY
            try:
                cls._remove_slave_node(region, node.private_ip)
                source = node.attr.get('source', {})
                node.attr = {}
                if source:
                    node.attr['source'] = source
                node.resources = {}
                cls._update_phoenix_type(region, node.private_ip, phoenix_type)
                node.type = furion_type
                node.state = const.STATE_DEPLOYING
                LOG.info("Updated type for node {}, region {} from {} to {}({}).".format(
                    private_ip, region.name, node.type, furion_type, phoenix_type))
            except Exception as e:
                LOG.error("Updating node {} to EMPTY failed with {}.".format(private_ip, e))
                node.state = const.STATE_CRITICAL
        elif node.type == const.NODE_TYPE_EMPTY and furion_type == const.NODE_TYPE_COMPUTE:
            try:
                # Add slave resources to change type from EMPTY to COMPUTE
                is_slave_attribute = (phoenix_type == const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE)
                if region.container_manager == const.CONTAINER_MANAGER_SWARM:
                    cls._add_swarm_worker_node(region, node)
                elif region.container_manager == const.CONTAINER_MANAGER_MESOS:
                    cls._add_mesos_compute_node(region, node, is_slave_attribute)
                elif region.container_manager == const.CONTAINER_MANAGER_K8S:
                    cls._add_k8s_compute_node(region, node)
                cls._update_phoenix_type(region, node.private_ip, phoenix_type)
                old_type = node.type
                node.type = furion_type
                node.state = const.STATE_DEPLOYING
                LOG.info("Updated type for node {}, region {} from {} to {}({}).".format(
                    private_ip, region.name, old_type, furion_type, phoenix_type))
            except Exception as e:
                LOG.error("Updating node {} to COMPUTE failed with {}.".format(private_ip, e))
                node.state = const.STATE_CRITICAL
        else:
            message = "Change from type {} to {} is not permitted.".format(node.type, phoenix_type)
            raise FurionException('invalid_node_type', message_params=[message])
        node.updated_at = timezone.now()
        Event.node_state_update(region, node, node.state)
        node.save()


class NodeActionControl(NodeControl):
    @classmethod
    def do_actions(cls, region, private_ip, action):
        """Do action on node.
        Note the difference between this function and start/stop actions. The are both actions, but they require 
        different user permissions,.
        Currently only support kubernetes regions.
        Args:
            region(Region): region object
            private_ip(str): node's private_ip
            action(str): support actions. see const.NODE_ACTION*. If it's a unknown action, do nothing.
        Raises:
            Http404: if node not found.
            FurionException: kubernetes_resource_error,not_support
        """
        node = cls.get_object(region=region, private_ip=private_ip)
        km = RegionControl.get_kubernetes_manager(node.region)
        if not km:
            raise FurionException(ErrCodes.NOT_SUPPORT,
                                  message_params=['{} a node in non kubernetes regions'.format(action)])
        if action == const.NODE_ACTION_CORDON:
            km.update_node_schedulable_state(private_ip, schedulable=False)
            node.attr['schedulable'] = False
        if action == const.NODE_ACTION_UNCORDON:
            km.update_node_schedulable_state(private_ip, schedulable=True)
            node.attr['schedulable'] = True
        if action == const.NODE_ACTION_DRAIN:
            if node.type != const.NODE_TYPE_COMPUTE:
                return FurionException(ErrCodes.NOT_SUPPORT,
                                       message_params=['Cannot drain non-slave node! | {}'.format(private_ip)])
            node_name = km.get_node(private_ip, raise_ex=True).metadata.name
            cls.drain(region, private_ip, node_name)
            node.attr['schedulable'] = False
            node.state = const.STATE_DRAINING
        node.save()

    @classmethod
    def drain(cls, region, private_ip, node_name):
        """Drain a node in kubernetes region.
        This is an async task, spectre/dirge will execute this task in the background.
        
        Args:
            region(Region): region object
            private_ip(str): target node's private_ip.
            node_name(str): target node's name in kubernetes
        """
        phoenix_node = NodeManager.retrieve(region.env_uuid, private_ip)
        ResourceUtil.add_drain_node_resource(region.env_uuid, node_name, phoenix_node)

    @classmethod
    def stop(cls, region, private_ip):
        cls._node_act('stop', region, private_ip)
        NodeManager.activate(region.env_uuid, private_ip, False)

    @classmethod
    def start(cls, region, private_ip):
        cls._node_act('start', region, private_ip)
        NodeManager.activate(region.env_uuid, private_ip, True)

    @classmethod
    def _node_act(cls, action, region, private_ip):
        """Start or start a node (IAAS).
        
        Args:
            action(str): start/stop
            region(Region): region object
            private_ip(str): node's private ip
        Raises:
            FurionException        
        """
        node = cls.get_object(region=region, private_ip=private_ip)
        if not node.instance_id:
            msg = '{} node: {} {}'.format(action.capitalize(), region, private_ip)
            raise FurionException('not_support', message_params=[msg])
        getattr(RegionControl.get_cloud(region), '{}_instance'.format(action))(node.instance_id)
        node.state = (const.STATE_DEPLOYING, const.STATE_STOPPED)[action == 'stop']
        Event.node_state_update(region, node, node.state)
        node.save()
        LOG.info("{} node. | {} {}".format(action.capitalize(), region, private_ip))
