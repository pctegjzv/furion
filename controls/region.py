#!/usr/bin/python
# -*- coding: utf-8 -*-
from operator import itemgetter

from alauda.clients.furion import FurionClient
from django.db import transaction
from django.forms import model_to_dict
from django.utils import timezone
from schema import Schema

from app.control import Control
import logging
from django.conf import settings

from app.serializers import RegionSerializer
from controls.self_service.ss_settings import SELF_SERVICE
from app.models import Node, Region, Label
from controls.component_version import ComponentVersionControl
from controls.region_settings import RegionSettingControl
from controls.port_pool import generate_tunnel_info, recycle_tunnel
from cloud import driver
from util import const
from util.func import check_keys, wrapped_encode_aes, wrapped_decode_aes

from furion.exception import FurionException, InvalidRegionDataException
from app import legacy
import copy
from Crypto.Cipher import AES
from alauda.common.func import pad_str, decode_aes
from util import phoenix
from util.events import Event
from controls.self_service.transverter import transverter
from controls.self_service.service_register import ServiceRegister
from controls.self_service.validation import Validation
import traceback
import requests

from util.kubernetes_client import KubernetesManager
from util.validation import validate
from gateway.database import MirrorDBGateway
from gateway.cache import CacheGateway
from app_v2.mirror.core import get_mirror

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


def _get_cloud_settings(features, attr):
    new_feature = {"service": {"features": []}}
    service = features.get('service', {})
    node = features.get('node', {})
    lb_info = list(set(service.get('features', {})) & set(service) & set(const.LB_TYPE))
    cloud_info = attr.get('cloud', {})
    docker_settings = attr.get('docker', {})
    network_interface = attr.get("nic")
    new_attr = {}
    if cloud_info:
        new_attr['cloud'] = cloud_info
    if docker_settings:
        new_attr['docker'] = docker_settings
    if network_interface:
        new_attr['nic'] = network_interface
    # auto create node will add node info in features
    if node:
        new_feature['node'] = node

    if not lb_info:
        return new_feature, new_attr
    lb = lb_info[0]
    new_feature['service'] = {
        'features': [lb],
        lb: service[lb]
    }

    return new_feature, new_attr


def _xcode_region_info(func_type, features, attr):
    assert isinstance(features, dict)
    assert isinstance(attr, dict)
    assert func_type in ['encode', 'decode']
    if not features:
        return

    _aec_func = wrapped_encode_aes if 'encode' == func_type else wrapped_decode_aes

    service = features.get('service', {})
    node = features.get('node', {})

    lb_info = set(service.get('features', {})) & set(service) & set(const.LB_TYPE)
    cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
    if 'elb' in lb_info:
        elb = service['elb']
        elb['access_key'] = _aec_func(cipher, elb['access_key'])
        elb['secret_access_key'] = _aec_func(cipher, elb['secret_access_key'])
        if node:
            node['instance']['access_key'] = elb['access_key']
            node['instance']['secret_access_key'] = elb['secret_access_key']
    elif 'ulb' in lb_info:
        elb = service['ulb']
        elb['public_key'] = _aec_func(cipher, elb['public_key'])
        elb['private_key'] = _aec_func(cipher, elb['private_key'])
    elif 'slb' in lb_info:
        slb = service['slb']
        slb['access_key'] = _aec_func(cipher, slb['access_key'])
        slb['secret_access_key'] = _aec_func(cipher, slb['secret_access_key'])
    elif 'blb' in lb_info:
        blb = service['blb']
        blb['access_key'] = _aec_func(cipher, blb['access_key'])
        blb['secret_access_key'] = _aec_func(cipher, blb['secret_access_key'])
    elif 'clb' in lb_info:
        clb = service['clb']
        clb['secret_key'] = _aec_func(cipher, clb['secret_key'])
        clb['secret_id'] = _aec_func(cipher, clb['secret_id'])
    else:
        pass

    if service.get('manager', None):
        manager = service['manager']
        if 'password' in manager:
            manager['password'] = _aec_func(cipher, manager['password'])
        if 'token' in manager:
            manager['token'] = _aec_func(cipher, manager['token'])

    mesos = attr.get('mesos')
    if mesos and mesos.get('auth'):
        mesos_auth = mesos['auth']
        mesos_auth['token'] = _aec_func(cipher, mesos_auth['token'])
        if mesos_auth.get('log_token'):
            mesos_auth['log_token'] = _aec_func(cipher, mesos_auth['log_token'])

    swarm = attr.get('swarm')
    if swarm and swarm.get('auth'):
        swarm_auth = swarm['auth']
        swarm_auth['token'] = _aec_func(cipher, swarm_auth['token'])
        if swarm_auth.get('log_token'):
            swarm_auth['log_token'] = _aec_func(cipher, swarm_auth['log_token'])


def get_docker_version():
    return {
        'versions': SELF_SERVICE['SUPPORT_DOCKER_VERSION'],
        'default': SELF_SERVICE['DEFAULT_DOCKER_VERSION']
    }


class RegionControl(Control):
    model_name = 'Region'
    model = Region
    serializer = RegionSerializer

    @classmethod
    def _get_list(cls, **args):
        LOG.debug("Get list regions' query params. | {}".format(args))
        ft = {k: args[k][0] for k in cls.model._meta.get_all_field_names() & args.viewkeys()}
        LOG.debug("Formalized list regions query. | {}".format(ft))
        return cls.model.objects.filter(**ft)

    @classmethod
    def _create_empty_region(cls, data):
        data['attr'] = data.get('attr', {})
        data['features'] = data.get('features', {})
        Validation.valid_empty_region_data(data)
        query = {'namespace': data['namespace'], 'name': data['name']}
        if super(RegionControl, cls)._get_list(**query):
            raise FurionException('region_data_incorrect',
                                  message='{} has been existed in {}'.format(data['name'], data['namespace']))
        # data['features']['tunnel'] = generate_tunnel_info(data['name'])
        _xcode_region_info('encode', data['features'], data['attr'])
        if 'name' in data.get('attr', {}).get('cloud', {}).keys():
            data['attr']['cloud']['name'] = str(data['attr']['cloud']['name']).upper()
        if 'node' in data['features'] and 'features' in data['features']['node']:
            data['features']['node']['features'].append('auto-scaling')
        data['env_uuid'] = phoenix.EnvironmentManager.create(data['name'])
        return data

    @classmethod
    def _create_acmp_region(cls, original_data, **args):

        def _create_update_data(region, data):
            LOG.debug("Create update data data: {}".format(data))
            update_data = {}
            rs_rep = None
            try:
                # TODO: db atomic?
                region_settings = phoenix.ClusterTemplateManger.get_region_settings(data, data['template_type'])
                phoenix_setting = region_settings.get('attr', {}).pop('phoenix_settings', {})
                versions = region_settings.get('attr', {}).pop('component_versions', {})
                phoenix_setting['region_id'] = args['id']
                if data['container_manager'] == const.CONTAINER_MANAGER_SWARM:
                    phoenix_setting['MASTER_NODES'] = [phoenix_setting['SWARM_INIT_NODE']]
                    phoenix_setting['SLAVE_NODES'] = phoenix_setting['SWARM_WORKER_NODES']
                rs_rep = RegionSettingControl.save(phoenix_setting)
                for k, v in versions.items():
                    ComponentVersionControl.save(v, **{'name': k, 'region_settings_id': rs_rep['id']})
                tunnel = region.features.get('tunnel')
                if tunnel:
                    region_settings['features']['tunnel'] = tunnel
                region_settings['attr']['nic'] = region.attr.get('nic', 'eth0')
                region_settings['attr']['cloud'].update(region.attr.get('cloud', {}))
                region_settings['container_manager'] = data['container_manager']
                LOG.debug("Data for region_settings pre_save: {}".format(region_settings))
                update_data = cls._pre_save(region_settings, **args)
                # keep AUTO create node
                if region.features.get('node'):
                    update_data['features']['node'] = region.features.get('node')
                # update_data['container_manager'] = data['container_manager']
                return update_data
            except Exception as e:
                # Error creating Furion data for region update. Delete any created objects.
                LOG.error("Creating ACMP Furion data failed. Region {}:{}. stack: {} {}.".format(
                    region.name, region.env_uuid, e, traceback.format_exc()))
                if rs_rep:
                    ComponentVersionControl.delete(**{'region_settings_id': rs_rep['id']})
                    RegionSettingControl.delete(**{'pk': rs_rep['id']})
                message = "Failed to trigger deployment of Container Management platform."
                Event.region_state_update(region, region.state, message)
                raise FurionException('region_data_incorrect', 'Failed to create region / component settings.')

        region = cls.model.objects.filter(**args).first()
        # Not support k8s when spectre < 2.8
        spectre_version = phoenix.EnvironmentManager.get_spectre_version(region.env_uuid)
        if spectre_version < const.SPECTRE_OLD_VERSION and \
                original_data['container_manager'] == const.CONTAINER_MANAGER_K8S:
            raise FurionException('not_support', 'Not support deploy {} at spectre{}'.format(
                original_data['container_manager'], spectre_version))

        gc_region = copy.deepcopy(region)
        if not region:
            raise FurionException('region_not_exist', 'region not exist!')
        if region.container_manager != const.CONTAINER_MANAGER_NONE:
            message = "Container Management Platform can only be deployed on empty region."
            Event.region_state_update(region, region.state, message)
            raise FurionException('region_data_incorrect', 'original region must be an empty region')
        try:
            LOG.debug("Before trans: {}".format(original_data))
            transfer = transverter(original_data, cls._format(region, False))
            data = transfer.get_result()
            LOG.debug("After trans: {}".format(data))
        except Exception as e:
            message = 'Validate data failed.'
            if isinstance(e, FurionException):
                message += "Error: {}".format(e.message)
            Event.region_state_update(region, const.STATE_ERROR, message)
            cls.model.objects.filter(**args).update(state=const.STATE_ERROR)
            LOG.error("Transfer data failed. stack: {} {}".format(e, traceback.format_exc()))
            original_data['container_manager'] = const.CONTAINER_MANAGER_NONE
            gc = transverter(original_data, cls._format(gc_region))
            gc.cleanup()
            LOG.error("Cleanup region for gc success!")
            raise FurionException('region_data_incorrect', e.message)
        update_data = _create_update_data(region, copy.deepcopy(data))
        try:
            all_ips = phoenix.RegionManager.create(region.env_uuid,
                                                   data,
                                                   data['template_type'])
            with transaction.atomic():
                for node_type, ips in all_ips.items():
                    for ip in ips:
                        LOG.debug('SET {} to {}'.format(ip, node_type))
                        node_update = {
                            'type': const.PHOENIX_NODE_TYPE_MAP[node_type],
                            'state': const.STATE_DEPLOYING
                        }
                        # In production, the Furion node gets created in the cron job and the
                        # create logic here will not be executed. However, in unit test, where
                        # we test the region, this logic will trigger node creation.
                        node = Node.objects.get_or_create(defaults=node_update,
                                                          region=region,
                                                          private_ip=ip)
                        if not node[1]:
                            node[0].type = const.PHOENIX_NODE_TYPE_MAP[node_type]
                            node[0].state = const.STATE_DEPLOYING
                            node[0].save()
                        Event.node_state_update(region, node[0], const.STATE_DEPLOYING)
                        phoenix.NodeManager.update_type(region.env_uuid, ip, node_type)
            update_data['state'] = const.STATE_DEPLOYING
            message = "Triggered Container Management Platform deployment to the Region."
        except Exception as e:
            # Error deploying the region. Update the Region state & state for all
            # nodes to Critical.
            LOG.error("Failed to deploy ACMP to Region - {}:{}. stack: {} {}.".format(
                region.name, region.env_uuid, e, traceback.format_exc()))
            update_data.clear()
            update_data['state'] = const.STATE_CRITICAL
            message = "Failed to deploy Container Management Platform to the Region."
            Node.objects.filter(region=region).update(state=const.STATE_CRITICAL, updated_at=timezone.now())
        update_data['container_manager'] = data['container_manager']
        if update_data['container_manager'] == const.CONTAINER_MANAGER_K8S:
            update_data['platform_version'] = original_data.get('platform_version',
                                                                const.KUBERNETES_PLATFORM_VERSION_V2)
        update_data['updated_at'] = timezone.now()
        features = update_data['features']
        if 'logs' not in features:
            features['logs'] = {}
        if 'storage' not in features['logs']:
            features['logs']['storage'] = {}
        if 'logs' in original_data and \
           'storage' in original_data['logs'] and \
           'read_log_source' in original_data['logs']['storage'] and \
           'write_log_source' in original_data['logs']['storage']:
            features['logs']['storage']['read_log_source'] = original_data['logs']['storage']['read_log_source']
            features['logs']['storage']['write_log_source'] = original_data['logs']['storage']['write_log_source']
        Event.region_state_update(region, update_data['state'], message)
        return super(RegionControl, cls)._pre_update(update_data)

    @classmethod
    def _cleanup_region(cls, data, **args):
        with transaction.atomic():
            region = cls.model.objects.filter(**args).first()
            if region.container_manager == const.CONTAINER_MANAGER_LEGACY:
                raise FurionException('region_data_incorrect', 'Invalid operation for a legacy region')
            try:
                region_dict = copy.deepcopy(region)
                transfer = transverter(data, cls._format(region_dict))
                transfer.cleanup()
            except Exception as e:
                LOG.error("Transfer data fail. stack: {} {}".format(e, traceback.format_exc()))
                raise e
            if region.container_manager == const.CONTAINER_MANAGER_NONE and region.state == const.STATE_RUNNING:
                return super(RegionControl, cls)._pre_update(data)
            env_uuid = region.env_uuid
            features, attr = _get_cloud_settings(region.features, region.attr)
            data['attr'] = attr
            data['features'] = features
            container_manager = region.container_manager
            data['state'] = const.STATE_DEPLOYING
            query_params = {'region_id': region.id}
            try:
                region_settings = RegionSettingControl._get_list(**query_params)
                ComponentVersionControl.delete(**{'region_settings_id': region_settings[0].id})
                region_settings.delete()
            except Exception as ex:
                LOG.error('Remove related table fail on updating! error: {}'.format(ex.message))
            try:
                # Remove resources from all Phoenix Nodes & set type to EMPTY
                phoenix_nodes = phoenix.NodeManager.list_nodes(env_uuid)
                for node in phoenix_nodes:
                    phoenix.NodeManager.delete_resources(env_uuid, node)
                    phoenix.NodeManager.update_type(env_uuid, node['ip_address'], const.PHOENIX_NODE_TYPE_EMPTY)
                    furion_node = Node.objects.filter(region=region, private_ip=node['ip_address']).first()
                    if furion_node:
                        furion_node.type = const.NODE_TYPE_EMPTY
                        if furion_node.state != const.STATE_STOPPED:
                            furion_node.state = const.STATE_DEPLOYING
                        if 'node_tag' in furion_node.attr:
                            del furion_node.attr['node_tag']
                        if 'components' in furion_node.attr:
                            del furion_node.attr['components']
                        furion_node.resources = {}
                        furion_node.updated_at = timezone.now()
                        furion_node.save()
                    else:
                        LOG.error("Failed to find Furion Node {} in region {}:{}.".format(
                            node['ip_address'], region.name, region.env_uuid))
                phoenix.ResourceManager.clean_up(env_uuid)
                phoenix.ResourceUtil.add_cleanup_resource(env_uuid, region.container_manager)
                container_manager = const.CONTAINER_MANAGER_NONE
                recycle_tunnel(region.name, region.namespace)
            except Exception as e:
                LOG.error('Failed to Empty Region {}:{}. stack: {} {}.'.format(
                    region.name, region.env_uuid, e, traceback.format_exc()))
                data.clear()
                data['state'] = const.STATE_CRITICAL
                Node.objects.filter(region=region).update(state=const.STATE_CRITICAL, updated_at=timezone.now())
            Label.objects.filter(node__region_id=region.id).delete()
            data['container_manager'] = container_manager
            data['updated_at'] = timezone.now()
            data['platform_version'] = const.KUBERNETES_PLATFORM_VERSION_V2
            return super(RegionControl, cls)._pre_update(data)

    @classmethod
    def _pre_save(cls, data, **args):
        check_keys(['name'], data)
        # If container manager is NONE, the pre-save was called when creating an empty region.
        # Create an empty region & return region data.
        container_manager = data.get('container_manager')
        if container_manager == const.CONTAINER_MANAGER_NONE:
            return cls._create_empty_region(data)

        features = data['features']['service']['features']
        if 'tunnel' in features:
            if data['features'].get('tunnel') is None:
                data['features']['tunnel'] = generate_tunnel_info(data['name'], data['namespace'])

            if container_manager == const.CONTAINER_MANAGER_SWARM:
                data['features']['service']['manager']['endpoint'] = \
                    'http://{}'.format(data['features']['tunnel']['port_mapping']['puck'])
                if 'volume' in features:
                    data['features']['service']['volume']['agent']['endpoint'] = \
                        'http://{}/volume_driver'.format(
                            data['features']['tunnel']['port_mapping']['puck'])
            elif container_manager == const.CONTAINER_MANAGER_MESOS:
                data['features']['service']['manager']['endpoint'] = \
                    'http://{}/marathon'.format(data['features']['tunnel']['port_mapping']['puck'])
                if 'volume' in features:
                    data['features']['service']['volume']['agent']['endpoint'] = \
                        'http://{}/volume_driver'.format(
                            data['features']['tunnel']['port_mapping']['puck'])
            elif container_manager == const.CONTAINER_MANAGER_K8S:
                data['features']['service']['manager']['endpoint'] = \
                    'https://{}'.format(data['features']['tunnel']['port_mapping']['tiny'])
                if 'volume' in features:
                    data['features']['service']['volume']['agent']['endpoint'] = \
                        'http://{}'.format(
                            data['features']['tunnel']['port_mapping']['puck'])
            if data['features'].get("cd"):
                data['features']['cd']['temporary']['ip'] = \
                    'http://{}'.format(data['features']['tunnel']['port_mapping']['doom'])
        _xcode_region_info('encode', data['features'], data['attr'])
        return data

    @classmethod
    def _post_save(cls, data):
        _xcode_region_info('decode', data['features'], data['attr'])
        return super(RegionControl, cls)._post_save(data)

    @classmethod
    def save(cls, data, **args):
        phoenix_setting = data.get('attr', {}).pop('phoenix_settings', {})
        versions = data.get('attr', {}).pop('component_versions', {})
        resp = super(RegionControl, cls).save(data, **args)
        if phoenix_setting and versions:
            phoenix_setting['region_id'] = resp['id']
            rs_rep = RegionSettingControl.save(phoenix_setting)
            for k, v in versions.items():
                ComponentVersionControl.save(v, **{'name': k, 'region_settings_id': rs_rep['id']})
        return resp

    @classmethod
    def _pre_update(cls, data, **args):
        region_type = data.get('container_manager', '')
        type_list = [x[0] for x in const.CONTAINER_MANAGER]
        if region_type and region_type not in type_list:
            raise InvalidRegionDataException(message='container manager must be in {} or empty, '
                                                     'found {}'.format(type_list, region_type))
        if region_type == const.CONTAINER_MANAGER_NONE:
            return cls._cleanup_region(data, **args)
        if region_type in const.CONTAINER_MANAGER_NOT_NONE_LIST:
            return cls._create_acmp_region(data, **args)

    @classmethod
    def delete(cls, **args):
        region = cls.get_object(**args)
        if region.container_manager != const.CONTAINER_MANAGER_NONE:
            raise FurionException('not_support', message_params=["Please empty this region first!"])
        try:
            nodes = phoenix.NodeManager.list_nodes(region.env_uuid)
            for node in nodes:
                phoenix.NodeManager.delete(region.env_uuid, node['ip_address'])
            phoenix.NodeManager.clean_up(region.env_uuid)
            phoenix.ResourceManager.clean_up(region.env_uuid)
            phoenix.EnvironmentManager.delete(region.env_uuid)
        except Exception as ex:
            region.state = const.STATE_CRITICAL
            region.save()
            error = 'Failed to delete Region {}:{}. Exception: {}'.format(
                region.name, region.env_uuid, ex.message)
            LOG.error(error)
            raise FurionException('alchemist_error', message_params=[error])
        recycle_tunnel(region.name, region.namespace)
        super(RegionControl, cls).delete(**args)

    @classmethod
    def _format(cls, obj, duplicated=True):
        new_obj = copy.deepcopy(obj) if duplicated else obj
        tunnel = new_obj.features.get('tunnel')
        if tunnel:
            cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
            tunnel['private_key'] = decode_aes(cipher, tunnel['private_key'])
            tunnel['public_key'] = decode_aes(cipher, tunnel['public_key'])
        _xcode_region_info('decode', new_obj.features, new_obj.attr)
        data = super(RegionControl, cls)._format(new_obj)
        if not data['mirror']:
            data['mirror'] = {}
            return data
        data['mirror'] = get_mirror(MirrorDBGateway, CacheGateway, data['mirror'])
        return data

    @classmethod
    def get_labels(cls, region_id):
        """Retrieve all labels in a region.
        Args:
            region_id(str)
        Return:
            dict: {
                'labels': [<label data>]
            }
        """
        labels = Label.objects.filter(node__region_id=region_id)
        result = [model_to_dict(x, fields=['key', 'value', 'editable']) for x in labels
                  if x.node.is_schedulable()]

        nodes = Node.objects.filter(region__id=region_id)
        node_tags = []
        for node in nodes:
            if not node.is_schedulable():
                continue
            if 'node_tag' in node.attr:
                node_tags.append(Label.node_tag_to_label(node.attr['node_tag']))
        result += node_tags
        result = [i for n, i in enumerate(result) if i not in result[n + 1:]]
        result = sorted(result, key=itemgetter('key'))
        return {"labels": result}

    @classmethod
    def get_region_lib(cls, obj):
        """Get a region client library.

        Args:
            obj(Region): region object
        Returns:
            FurionClient
        """
        return FurionClient(region=cls._format(obj))

    @classmethod
    def get_manager_info(cls, obj):
        """Get a region's container manager info.
        Args:
            obj(Region): region object
        Returns:
            dict: {
                'endpoint':
                'token': (optional)
                'password': (optional)
                'user': (optional)
            }
            None
        """
        return cls.get_region_lib(obj).get_manager_config()

    @classmethod
    def get_kubernetes_manager(cls, region):
        """"Get a KubernetesManager object

        Args:
            region(Region): region object
        Returns:
            KubernetesManager:
            None: not a kubernetes region
        """
        if region.container_manager != const.CONTAINER_MANAGER_K8S:
            return
        manager = cls.get_manager_info(region)
        return KubernetesManager(endpoint=manager['endpoint'], token=manager['token'])

    @classmethod
    def update_node_scale_settings(cls, region, data):
        def _check():
            check_keys(['enable_scale'], data, request_data=True)
            if data['enable_scale']:
                check_keys(['scale_up', 'scale_down'], data, request_data=True)

        _check()
        region.attr['node_scaling'] = data
        region.save()

    @classmethod
    def get_cloud_data(cls, region):
        cloud = region.attr.get('cloud', {})
        check_keys(['name'], cloud)
        if cloud['name'] == "OPENSTACK" or cloud['name'] == const.CLOUD_VMWARE or cloud['name'] == const.CLOUD_KINGSOFT:
            return cloud
        check_keys(['region_id'], cloud)
        return cloud

    @classmethod
    def get_cloud(cls, region):
        cloud = cls.get_cloud_data(region)
        _xcode_region_info('decode', region.features, region.attr)
        inst = region.features.get('node', {}).get('instance', {})
        if cloud['name'] == 'OPENSTACK':
            args = {
                'name': cloud['name'],
                # 'region_id': cloud['region_id'],
                'loginurl': inst['login_url'],
                'apiurl': inst['api_url'],
                'uname': inst['user_name'],
                'passwd': inst['password'],
                'domain_name': inst['domain_name']
            }
            return driver.get_cloud(args)
        elif cloud['name'] == const.CLOUD_KINGSOFT:
            args = {
                'name': cloud['name'],
                'login_url': inst['login_url'],
                'nova_url': inst['nova_url'],
                'user_name': inst['user_name'],
                'password': inst['password'],
                'tenant_name': inst['tenant_name']
            }
            return driver.get_cloud(args)
        elif cloud['name'] == const.CLOUD_VMWARE:
            args = {
                'region_id': region.id,
                'name': cloud['name'],
                'address': cloud['address'],
                'user': inst['user'],
                'password': inst['password'],
                'template_name': inst['template_name'],
                'template_user': inst['template_user'],
                'template_password': inst['template_password']
            }
            return driver.get_cloud(args)
        check_keys(['access_key', 'secret_access_key'], inst)
        args = {
            'name': cloud['name'],
            'region_id': cloud['region_id'],
            'access_key': inst['access_key'],
            'secret_access_key': inst['secret_access_key']
        }
        return driver.get_cloud(args)

    @classmethod
    def get_key_pairs(cls, region):
        return cls.get_cloud(region).list_key_pairs()

    @classmethod
    def get_user_data(cls, region, node_type):
        """For aws to pass a script when ec2 instance is launched"""
        # TODO:SS_Milestone2: If no type is specified, we default to slave to stay consistent
        # with the earlier behavior. Will remove this in Milestone 2.
        if not node_type:
            node_type = const.PHOENIX_NODE_TYPE_EMPTY
        # params = {'node_type': node_type}
        reg = ServiceRegister(None)
        rep = reg.get_command(region.namespace, region.name, node_type)
        return '#!/bin/bash\n{}'.format(rep['commands']['install'])

    @classmethod
    def get_node_scripts(cls, region, params=None):
        # node type is mandatory for region created via self service (ACMP / EMPTY). Thy method
        # will fail if no node type is provided for a new region. For legacy regions, if no node
        # type is provided, it defaults to 'mesos-slave'.
        def _attr(comp_name, attr, old_value=''):
            return legacy.right_attr(region, comp_name, attr, old_value)

        if region.env_uuid:
            LOG.debug('node script params| {}'.format(params))
            node_type = None
            auto_create_node = False
            if params:
                node_type = params.get('node_type', None)
                auto_create_node = params.get('auto_create_node', False)

            if region.container_manager == const.CONTAINER_MANAGER_LEGACY:
                if not node_type:
                    # For legacy regions, if no node_type is specified, default to mesos-slave.
                    node_type = const.PHOENIX_NODE_TYPE_COMPUTE

            valid_types = [
                const.PHOENIX_NODE_TYPE_COMPUTE,
                const.PHOENIX_NODE_TYPE_COMPUTE_ATTRIBUTE,
                const.PHOENIX_NODE_TYPE_EMPTY,
                const.PHOENIX_NODE_TYPE_SWARM_WORKER,
                const.PHOENIX_NODE_TYPE_K8S_SLAVE
            ]

            if node_type not in valid_types:
                message = "Invalid type {}. Valid Types: {}.".format(node_type, valid_types)
                raise FurionException('invalid_node_type', message_params=[message])

            install_command = phoenix.EnvironmentManager.get_command(region.env_uuid)
            install_command = '{} --node-type={}'.format(install_command, node_type)
            docker_info = region.attr.get('docker', {})
            install_command = '{} --docker-path={} --docker-version={}'.format(
                install_command,
                docker_info.get('path', '/var/lib/docker'),
                docker_info.get('version', settings.DEFAULT_DOCKER_VERSION)
            )
            if 'nic' in region.attr:
                install_command = '{} --network-interface={} '.format(install_command,
                                                                      region.attr['nic'])
            cloud_name = str(region.attr.get('cloud', {}).get("name", 'PRIVATE')).upper()
            if cloud_name in SELF_SERVICE['SUPPORT_STATIC_IP_CLOUDS']:
                install_command = '{} --experimental=1'.format(install_command)
            remove_cmd = 'curl -fsSL http://get.alauda.cn/remove/spectre/ | sudo bash'

            if cloud_name == 'AWS' and auto_create_node:
                install_command = install_command.replace('| sudo bash -s --', '| bash -s --')

            if settings.PRIVATE_DEPLOY_SETTINGS['PRIVATE_DEPLOY_ENABLED']:
                install_command += ' --manager_registry_address={}'
                install_command += ' --registry_address={}'
                install_command += ' --dns_address={}'
                install_command += ' --download_endpoint={}'
                install_command = install_command.format(
                    settings.PRIVATE_DEPLOY_SETTINGS['MANAGER_REGISTRY_ADDRESS'],
                    settings.PRIVATE_DEPLOY_SETTINGS['REGISTRY_ADDRESS'],
                    settings.PRIVATE_DEPLOY_SETTINGS['DNS_ADDRESS'],
                    settings.PRIVATE_DEPLOY_SETTINGS['DOWNLOAD_ENDPOINT']
                )
                if cloud_name == 'AWS' and auto_create_node:
                    install_command = install_command.replace('| bash -s --',
                                                              '>/tmp/dd.sh ;bash /tmp/dd.sh')
                elif cloud_name == 'OPENSTACK':
                    install_command = install_command.replace('| sudo bash -s --',
                                                              '>/tmp/dd.sh ;bash /tmp/dd.sh')
                else:
                    install_command = install_command.replace('| sudo bash -s --',
                                                              '>/tmp/dd.sh ;sudo bash /tmp/dd.sh')

                remove_cmd = 'curl -fsSL {} | sudo bash'.format(
                    settings.PRIVATE_DEPLOY_SETTINGS['REMOVE_SCRIPT_ENDPOINT'])
            suffix = settings.DNS_REGISTER['SUFFIX']
            index_name = settings.DNS_REGISTER['INDEX']
            if suffix:
                install_command = '{} --insecure-registry-list {}-{}.{}.{}:5000'.format(
                    install_command, region.name.replace('_', '-'),
                    region.namespace, index_name, suffix)
            command = {
                'commands': {
                    'install': install_command,
                    'uninstall': remove_cmd
                }
            }
            return command

        # TODO: Once all regions move to using Phoenix, remove the below code. It is deprecated
        # for the new regions.
        _xcode_region_info('decode', region.features, region.attr)
        auth = region.attr.get("mesos", {}).get("auth", {})
        versions = region.attr.get("mesos", {}).get("versions", {})
        instance = region.features.get("node", {}).get("instance", {})
        marathon_url = _attr('marathon', 'endpoint',
                             region.attr.get("mesos", {}).get('marathon_endpoint', ''))
        if not marathon_url:
            raise FurionException('region_data_incorrect', [region.id, 'attr.components.marathon.endpoint'])
        else:
            if marathon_url[-1] == '/':
                marathon_url += "v2/info"
            else:
                marathon_url += "/v2/info"

        if settings.DEPLOY_ENV == 'INT':
            cmd_head = 'curl http://get.alauda.cn/deploy/slave/int/ | sudo sh -s'
        else:
            cmd_head = 'curl http://get.alauda.cn/deploy/slave/ | sudo sh -s'

        cmd_list = [
            cmd_head, "--",
            "--mesos-version", _attr('mesos', 'version', versions.get("mesos")),
            "--marathon-url", marathon_url,
            "--username", _attr('api', 'username', auth.get("username")),
            "--token", _attr('api', 'token', auth.get("token")),
            "--zeus-version", _attr('zeus', 'version', versions.get("zeus")),
            "--nevermore-version", _attr('nevermore', 'version', versions.get("nevermore"))
        ]
        if '' in cmd_list or None in cmd_list:
            raise InvalidRegionDataException(message_params=[region.id, 'attr.components'])

        log_token = _attr('zeus', 'token', auth.get("log_token"))
        if log_token:
            cmd_list.extend(["--log-token", log_token])

        insecure_registry = region.attr.get("insecure_registry")
        if insecure_registry:
            insecure_registry = ','.join(insecure_registry)
            cmd_list.extend(["--insecure-registry", insecure_registry])

        cloud_name = region.attr.get("cloud", {}).get("name")
        if cloud_name:
            cmd_list.extend(["--cloud-name", str(cloud_name).upper()])

        meepo_address = _attr('meepo', 'endpoint')
        if meepo_address:
            cmd_list.extend(["--meepo-address", meepo_address])

        cmd_list.extend(["--region-name", region.name])
        ssh_key = instance.get('ssh_key', '')
        if ssh_key:
            cmd_list.extend(["--ssh-key", ssh_key])

        deploy_cmd = ' '.join(cmd_list)
        remove_cmd = 'curl http://get.alauda.cn/deploy/remove/ | sudo sh'

        command = {
            "commands": {
                "install": deploy_cmd,
                "uninstall": remove_cmd
            }
        }
        return command

    @classmethod
    def check_region_features(cls, region_id, features):

        """Check whether region contains the features in request body

        Args:
            region_id: region id
            features(list): ['f1', 'f2' ..]

        Returns:
            None

        Raises:
            FurionException(code=invalid_input)
            FurionException(code=feature_error)

        """
        validate(Schema([basestring]), features,
                 message="Invalid input: \'features\' needs to be list of strings !")

        features = set(features)

        client = FurionClient(region=cls.get_info(id=region_id))

        available_features = client.get_supported_features()
        if 'alb' in available_features and 'haproxy' in features and 'alb' not in features:
            raise FurionException('feature_error', message_params=['haproxy', region_id])

        delta = features - set(available_features)
        if delta:
            raise FurionException('feature_error', message_params=[list(delta), region_id])

    @classmethod
    def get_upgrade(cls, region_id):
        region = RegionControl.get_object(pk=region_id)
        LOG.debug("region upgrade. | {}".format(region_id))
        temp = {
            'components_upgrade': {},
            'unrecognizable_components': [],
            'spectre_upgrade': {}
        }
        spectre_current_version = phoenix.EnvironmentManager.get_spectre_version(region.env_uuid)
        spectre_latest_version = phoenix.EnvironmentManager.get_spectre_latest_version()
        if spectre_current_version < spectre_latest_version:
            temp['spectre_upgrade'] = {
                'current_version': spectre_current_version,
                'latest_version': spectre_latest_version
            }
        resources = phoenix.ResourceManager.list_resources(region.env_uuid)
        for res in resources:
            if res.get('type') == 'CONTAINERS':
                comp_name = res.get('name')
                image = res.get('identifier').get('image')
                current_version = image.split(':')[-1]
                try:
                    latest_version = phoenix.ComponentVersionManager.get_default_version(comp_name).get('version')
                except Exception:
                    temp['unrecognizable_components'].append(comp_name)
                    continue
                if current_version != latest_version:
                    add_env_list = []
                    del_env_list = []
                    if isinstance(res.get('configuration').get('environment'), dict):
                        current_env_key_list = res.get('configuration').get('environment').keys()
                        latest_env_key_list = phoenix.ComponentManager.get_component_envs(comp_name)
                        add_env_list = list(set(latest_env_key_list).difference(current_env_key_list))
                        del_env_list = list(set(current_env_key_list).difference(latest_env_key_list))
                    temp['components_upgrade'][comp_name] = {
                        'current_version': current_version,
                        'latest_version': latest_version,
                        'add_env_list': add_env_list,
                        'del_env_list': del_env_list
                    }
        return temp

    @classmethod
    def get_log_source(cls, features):
        def merge_uuids(write_uuids, read_uuids):
            ret = set()
            for uuid in "{},{}".format(write_uuids, read_uuids).split(","):
                if uuid == 'default':
                    continue
                ret.add(uuid.strip())
            return ",".join(ret)

        def request_log_source(uuids):
            davion_endpoint = settings.DAVION_ENDPOINT.strip('/')
            url = "{}/{}".format(davion_endpoint, "v1/integrations")
            payload = {"uuids": uuids}
            rep = requests.get(url, params=payload)
            if rep.status_code != 200:
                LOG.error("Request {} failed. code {} . content {}.".format(url, rep.status_code, rep.text))
            return rep.json()

        def factory(uuids):
            ret = []
            for uuid in uuids.split(','):
                uuid = uuid.strip()
                temp = {}
                if uuid == 'default':
                    temp['id'] = 'default'
                    temp['name'] = 'default'
                    temp['type'] = 'default'
                else:
                    if uuid in log_source_map:
                        temp = log_source_map[uuid]
                if temp:
                    ret.append(temp)
            return ret

        if not (features and 'logs' in features and 'storage' in features['logs'] and
                'write_log_source' in features['logs']['storage'] and
                'read_log_source' in features['logs']['storage']):
            LOG.error("Region features not contains logs or logs.storage")
            default_log_source = {"type": "default", "id": "default", "name": "default"}
            return {"write_log_source": [default_log_source], "read_log_source": [default_log_source]}

        write_uuids = features['logs']['storage']['write_log_source']
        read_uuids = features['logs']['storage']['read_log_source']
        if not write_uuids and not read_uuids:
            return {"write_log_source": [], "read_log_source": []}

        uuids = merge_uuids(write_uuids, read_uuids)
        log_source_map = {}
        log_sources = request_log_source(uuids)
        for log_source in log_sources:
            log_source['fields']['id'] = log_source['id']
            log_source['fields']['type'] = log_source['type']
            log_source['fields']['enabled'] = log_source['enabled']
            log_source_map[log_source['id']] = log_source['fields']

        ret = {}
        ret['write_log_source'] = factory(write_uuids)
        ret['read_log_source'] = factory(read_uuids)
        return ret
