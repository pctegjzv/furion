from __future__ import absolute_import, unicode_literals
from celery.utils.log import get_task_logger
from django.db import transaction
from controls.node import NodeControl
from controls.region import RegionControl
from util import const
from celery.task import Task
import socket
from django.utils import timezone
from django.conf import settings
import requests
import json
from furion.exception import FurionException
from alauda.common import collection
from util.misc import TinyRequest
from controls.self_service.transverter import transverter
import traceback
import copy


LOG = get_task_logger(__name__)


def create_alarm_message(task):
    message = '\n'
    current_time = timezone.now()
    alarm_time = '{}-{}-{}T{}:{}:{}.0Z'.format(
        current_time.year, current_time.month, current_time.day,
        current_time.hour, current_time.minute, current_time.second
    )
    message += 'Execute task: {} fail!\n'.format(task['id'])
    message += 'region: {}\n'.format(task['region_id'])
    message += 'action: {}\n'.format(task['action'])
    message += 'error: {}\n'.format(task['error'])
    message += 'detail: {}\n'.format(task['detail'])
    message += 'node_ip: {}\n'.format(task['node'])
    result = {
        'template_type': 'Task',
        'data': {
            'subject': 'Task fail alarm',
            'content': message,
            'time': alarm_time
        }
    }
    return result


def send_alarm(task):
    msg = create_alarm_message(task)
    url = '{}/v1/notifications/{}/messages'.format(
        settings.ALARM_SETTINGS['ALARM_ENDPOINT'],
        settings.ALARM_SETTINGS['NOTIFICATION_ID'])
    ret = requests.post(url,
                        data=json.dumps(msg),
                        headers={'Content-Type': 'application/json'})
    if ret.status_code != 204:
        LOG.error('Create alarm to {} fail!. code: {} error: {} msg: {}'.format(
            url, ret.status_code, ret.content, msg
        ))
    LOG.error('Create alarm to {} success. Msg: {}'.format(url, msg))


class LuciferTask(Task):
    max_retries = 0

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        LOG.info('Run task fail!')
        LOG.info('exc: {},\ntask_id: {},\nargs: {},\nkwargs: {},\neinfo: {}'.format(
            exc, task_id, args, kwargs, einfo))
        task_info = {
            'id': task_id,
            'region_id': kwargs.get('region'),
            'action': self.__name__,
            'error': exc,
            'detail': einfo,
            'node': socket.gethostbyname(socket.gethostname())
        }
        send_alarm(task_info)
        super(LuciferTask, self).on_failure(exc, task_id, args, kwargs, einfo)


class RegionControlCelery(RegionControl):
    @classmethod
    def _pre_update(cls, data, **args):
        region_type = data.get('container_manager', '')
        if region_type == const.CONTAINER_MANAGER_NONE:
            return cls._cleanup_region(data, **args)
        if region_type in const.CONTAINER_MANAGER_NOT_NONE_LIST:
            return cls._create_acmp_region(data, **args)

    @classmethod
    def _create_acmp_region(cls, data, **args):
        return super(RegionControlCelery, cls)._create_acmp_region(data, **args)


class RegionControlTask(LuciferTask):
    def run(self, *args, **kwargs):
        LOG.info("Start update a region!")
        RegionControlCelery.update(*args, **kwargs)
        LOG.info('Finish update a region!')


class RegionControlAsync(RegionControl):

    @classmethod
    def update(cls, data, **args):
        LOG.debug('This is region async update!')
        # TODO: use control.get_object?
        region = cls.model.objects.filter(**args).first()
        if not region:
            raise FurionException('region_not_exist', 'region not exist!')
        region_type = data.get('container_manager', '')
        if region_type in const.CONTAINER_MANAGER_NOT_NONE_LIST:
            if region.container_manager != const.CONTAINER_MANAGER_NONE:
                raise FurionException('region_data_incorrect',
                                      'original region must be an empty region')
            try:
                LOG.debug("Before trans: {}".format(data))
                trans_region = copy.deepcopy(region)
                transfer = transverter(data, cls._format(trans_region))
                transfer._pre_transfer()
                transfer.valid()
                LOG.debug("After trans: {}".format(transfer.data))
            except Exception as e:
                LOG.error("Transfer data fail. stack: {} {}".format(e, traceback.format_exc()))
                raise e
            region.state = const.STATE_PREPARING
            region.save()
            task = RegionControlTask()
            task.delay(data, **args)
            LOG.debug('Send task to Queue')
            return
        super(RegionControlAsync, cls).update(data, **args)


class NodeControlCelery(NodeControl):
    @classmethod
    def update_type(cls, region, private_ip, phoenix_type):
        super(NodeControlCelery, cls).update_type(region, private_ip, phoenix_type)


class NodeControlTask(LuciferTask):
    def run(self, *args, **kwargs):
        region_id = kwargs.get('region')
        region = RegionControl.get_object(**{'id': region_id})
        private_ip = kwargs.get('private_ip')
        phoenix_type = kwargs.get('phoenix_type')
        LOG.debug('region: {} private_ip: {}, phoenix_type: {}'.format(
            region.id, private_ip, phoenix_type))
        LOG.info('Start update a node!')
        NodeControlCelery.update_type(region, private_ip, phoenix_type)
        LOG.info('Finish update a node!')


class NodeControlAsync(NodeControl):

    @classmethod
    def update_type(cls, region, private_ip, phoenix_type):
        LOG.debug('This is node type async update!')
        furion_type = const.PHOENIX_NODE_TYPE_MAP[phoenix_type]
        if furion_type == const.NODE_TYPE_CONTROLLER:
            message = "Changing node to Controller not permitted."
            raise FurionException('invalid_node_type', message_params=[message])

        with transaction.atomic():
            node = cls.get_object(region=region, private_ip=private_ip)
            if node.type == const.NODE_TYPE_CONTROLLER:
                message = "Changing type of a Controller node not permitted."
                raise FurionException('invalid_node_type', message_params=[message])
            if node.type == const.NODE_TYPE_EMPTY and furion_type == const.NODE_TYPE_COMPUTE:
                try:
                    LOG.debug('Update node: {} | {} type from {} to {}!'.format(
                        node.id, node.private_ip, node.type, furion_type))
                    node.state = const.STATE_PREPARING
                    node.save()
                    task = NodeControlTask()
                    task.delay(region=region.id, private_ip=private_ip, phoenix_type=phoenix_type)
                except Exception as es:
                    LOG.error('Change node {} | {} type fail! From {} to {}. error: {}'.format(
                        node.id, node.private_ip, node.type, furion_type, es))
                    node.state = const.STATE_CRITICAL
                    node.save()
                return
        super(NodeControlAsync, cls).update_type(region, private_ip, phoenix_type)


class SendMetrics(LuciferTask):

    soft_time_limit = 5
    ignore_result = True

    def run(self, *args, **kwargs):
        data = kwargs['data']
        for chunk in collection.chunks(data, 20):
            try:
                TinyRequest.send('/metrics-data/', method='POST', data=chunk)
            except Exception as e:
                LOG.error('Send metrics to tiny error: {}'.format(e))
                self.retry(exc=e, countdown=1, max_retries=2)
