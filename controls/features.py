from __future__ import absolute_import, unicode_literals
from celery.utils.log import get_task_logger
from app.models import Node, Region, ComponentVersion
from app.serializers import RegionSerializer
from controls.region_settings import RegionSettingControl
from controls.component_version import ComponentVersionControl
from controls.self_service.service_register import ServiceRegister
from controls.self_service.get_info import GetInfo
from controls.self_service.validation import Validation
from furion.exception import FurionException, InvalidArgsException
from util.phoenix import RegionManager, NodeManager, ClusterTemplateManger
from django.db import transaction
from util import const
from util.events import Event
from django.utils import timezone
from django.conf import settings
import traceback
import yaml
import json

__author__ = 'Xiaodong Zhang'

LOG = get_task_logger(__name__)


FEATURE_COMPONENT_MAP = {
    'exec': {
        'components': ['doom'],
        'relay_on': [],
        'relay_by': []
    },
    'haproxy': {
        'components': ['haproxy'],
        'relay_on': [],
        'relay_by': ['internal-haproxy']
    },
    'internal-haproxy': {
        'components': ['internal_haproxy'],
        'relay_on': ['haproxy'],
        'relay_by': []
    },
    'registry': {
        'components': ['registry'],
        'relay_on': [],
        'relay_by': ['chronos']
    },
    'chronos': {
        'components': ['chronos'],
        'relay_on': ['registry'],
        'relay_by': []
    },
}

AVAILABLE_FEATURES = {
    'poc': ['internal-haproxy', 'chronos', 'registry'],
    'prod': ['haproxy', 'exec', 'internal-haproxy', 'chronos', 'registry']
}


def get_class(action, feature):
    if feature == const.FEATURE_EXEC:
        if action == 'Disable':
            return DisableExec
        return EnableExec
    if feature == const.FEATURE_HAPROXY:
        if action == 'Disable':
            return DisableHaproxy
        return EnableHaproxy
    if feature == const.FEATURE_INTERNAL_HAPROXY:
        if action == 'Disable':
            return DisableInternalhaproxy
        return EnableInternalhaproxy
    if feature == const.FEATURE_REGISTRY:
        if action == 'Disable':
            return DisableRegistry
        return EnableRegistry
    if feature == const.FEATURE_CHRONOS:
        if action == 'Disable':
            return DisableChronos
        return EnableChronos

    raise FurionException('not_support',
                          message='Not support enable/disable feature: {} or feature not exist!'.format(feature))


def get_settings(obj):
    user_settings = {
        'MASTER_NODES': obj.master_node,
        'SLAVE_NODES': obj.slave_node,
        'USER_SETTINGS': obj.user_settings,
        'USER_REGISTRIES': obj.user_registries,
        'REGION_LOCATION': obj.region_location,
        'IMAGE_LOCATION': obj.image_location
    }
    user_settings.update(obj.cluster_settings)
    user_settings = yaml.safe_load(json.dumps(user_settings))
    return user_settings, obj.cluster_template_type


class DisableFeature(object):

    def __init__(self, region, data):
        self.region = region
        self.region_setting = RegionSettingControl.get_object(region=self.region)
        self.nodes = Node.objects.filter(region=self.region)
        self.feature = 'feature'
        self.components = ComponentVersion.objects.filter(region_settings=self.region_setting)
        self.component_map = {}
        for component in self.components:
            self.component_map[component.name] = component
        self.remove_component = {}
        self.data = data
        self.region_info = RegionSerializer(self.region).data

    def is_valid(self):
        if self.region.container_manager != const.CONTAINER_MANAGER_MESOS:
            raise FurionException('not_support', message='Region must be MESOS if you want enable/disable feature')
        if self.region.state != const.STATE_RUNNING:
            raise FurionException(
                'not_support',
                message='Region must be at running state if you want enable/disable feature'
            )
        if self.feature not in self.region.features['service']['features']:
            raise FurionException(
                'invalid_input',
                message='Region {} does not have feature: {}'.format(self.region.name,
                                                                     self.feature)
            )
        available_features = AVAILABLE_FEATURES.get(self.region_setting.cluster_template_type, [])
        if self.feature not in available_features:
            raise FurionException(
                'not_support',
                message='Region: {} not support disable feature: {}'.format(self.region.name,
                                                                            self.feature))
        relay_features = FEATURE_COMPONENT_MAP[self.feature]['relay_by']
        for relay_feature in relay_features:
            if relay_feature in self.region.features['service']['features']:
                raise FurionException(
                    'not_support',
                    message='{} rely on this feature. You must disable them first.'.format(
                        relay_feature))

    def _pre_disable(self):
        self.is_valid()

    def _post_disable(self):
        self.region_setting.save()
        self.region.save()
        remove_list = ', '.join(self.remove_component.keys())
        for k, v in self.remove_component.items():
            LOG.info('Remove component: {} from region: {}'.format(k, self.region.name))
            v.delete()
        detail = 'Removing components {} from Region.'.format(remove_list)
        Event.region_state_update(self.region, const.STATE_DEPLOYING, detail)

    def disable_from_regionsetting(self):
        if self.region.container_manager != const.CONTAINER_MANAGER_MESOS:
            raise FurionException(
                'not_support',
                message='Region must be MESOS if you want enable/disable feature'
            )
        if self.feature in self.region_setting.user_settings['REGION']['FEATURES']:
            self.region_setting.user_settings['REGION']['FEATURES'].remove(self.feature)
        for component in FEATURE_COMPONENT_MAP[self.feature]['components']:
            if component in self.region_setting.cluster_settings.get('COMPONENT', []):
                self.region_setting.cluster_settings['COMPONENT'].remove(component)

    def disable_from_phoenix(self):
        user_settings, template_type = get_settings(self.region_setting)
        return RegionManager.remove_feature(self.region.env_uuid,
                                            user_settings,
                                            template_type,
                                            FEATURE_COMPONENT_MAP[self.feature]['components'])

    def disable_from_region(self):
        self.region.features['service']['features'].remove(self.feature)
        self.region.state = const.STATE_DEPLOYING

    def disable_from_node(self, empty_ips, other_ips):
        all_ips = []
        all_ips.extend(empty_ips)
        all_ips.extend(other_ips)
        with transaction.atomic():
            for node in self.nodes:
                if node.private_ip in all_ips:
                    node.state = const.STATE_DEPLOYING
                    node.attr = {}
                    node.resources = {}
                    node.updated_at = timezone.now()
                    if node.private_ip in empty_ips:
                        node.type = const.NODE_TYPE_EMPTY
                    node.save()
                    Event.node_state_update(self.region, node, const.STATE_DEPLOYING)

    def disable_from_component_version(self):
        for component in FEATURE_COMPONENT_MAP[self.feature]['components']:
            if component in self.component_map.keys():
                self.remove_component[component] = self.component_map[component]

    def disable(self):
        self._pre_disable()
        try:
            empty_ips, other_ips = self.disable_from_phoenix()
        except Exception as e:
            LOG.error('Disable feature: {} in region: {} fail. error: {}, stack: {}'.format(
                self.feature, self.region.id, e, traceback.format_exc()))
            self.handle_exception()
            raise FurionException(
                'feature_error',
                message='Disable feature: {} fail. error: {}'.format(self.feature, e.message))
        self.disable_from_node(empty_ips, other_ips)
        self.disable_from_region()
        self.disable_from_regionsetting()
        self.disable_from_component_version()
        self._post_disable()

    def handle_exception(self):
        Region.objects.filter(id=self.region.id).update(updated_at=timezone.now(), state=const.STATE_CRITICAL)


class EnableFeature(object):

    def __init__(self, region, data):
        self.region = region
        self.region_setting = RegionSettingControl.get_object(region=self.region)
        self.nodes = Node.objects.filter(region=self.region)
        self.node_map = {}
        for node in self.nodes:
            self.node_map[node.private_ip] = node
        self.feature = 'feature'
        self.data = data
        self.components = ComponentVersion.objects.filter(region_settings=self.region_setting)
        self.component_map = {}
        for component in self.components:
            self.component_map[component.name] = component
        self.add_component = {}
        self.region_info = RegionSerializer(self.region).data

    def valid_node(self, check_list):
        for ip in check_list:
            if ip not in self.node_map.keys():
                raise InvalidArgsException(message='{} not exist!'.format(ip))
            if self.node_map[ip].type == const.NODE_TYPE_COMPUTE:
                raise FurionException(
                    'invalid_input',
                    message="Can't add feature in a computer node {}.".format(ip))
            if self.node_map[ip].state != const.STATE_RUNNING:
                raise FurionException(
                    'invalid_input',
                    message="Node: {} must be running.".format(ip))

    def is_valid(self):
        if self.region.state != const.STATE_RUNNING:
            raise FurionException(
                'not_support',
                message='Region must be at running state if you want enable/disable feature'
            )
        if self.feature in self.region.features['service']['features']:
            raise FurionException('invalid_input',
                                  message="Region: {} have already had this feature!".format(
                                          self.region.name))
        available_features = AVAILABLE_FEATURES.get(self.region_setting.cluster_template_type, [])
        if self.feature not in available_features:
            raise FurionException(
                'not_support',
                message='Region: {} not support enable feature: {}'.format(self.region.id,
                                                                           self.feature))
        relay_features = FEATURE_COMPONENT_MAP[self.feature]['relay_on']
        for relay_feature in relay_features:
            if relay_feature not in self.region.features['service']['features']:
                raise FurionException(
                    'not_support',
                    message='This feature relay on feature: {}. Please enable them first.'.format(
                        relay_feature)
                )

    def _pre_enable(self):
        self.is_valid()

    def _post_enable(self):
        self.region.save()
        self.region_setting.save()
        add_list = ', '.join(self.add_component.keys())
        for k, v in self.add_component.items():
            LOG.info('Add component: {} in region: {}'.format(k, self.region.name))
            ComponentVersionControl.save(v, **{'name': k, 'region_settings_id': self.region_setting.id})
        detail = 'Adding components {} to Region.'.format(add_list)
        Event.region_state_update(self.region, const.STATE_DEPLOYING, detail)

    def enable_from_node(self, all_ips):
        for node_type, ips in all_ips.items():
            for ip in ips:
                node_update = {'type': const.PHOENIX_NODE_TYPE_MAP[node_type],
                               'state': const.STATE_DEPLOYING
                               }
                # In production, the Furion node gets created in the cron job and the
                # create logic here will not be executed. However, in unit test, where
                # we test the region, this logic will trigger node creation.
                node = Node.objects.get_or_create(defaults=node_update, region=self.region, private_ip=ip)
                if not node[1]:
                    node[0].type = const.PHOENIX_NODE_TYPE_MAP[node_type]
                    node[0].state = const.STATE_DEPLOYING
                    node[0].save()
                NodeManager.update_type(self.region.env_uuid, ip, node_type)
                Event.node_state_update(self.region, node[0], const.STATE_DEPLOYING)

    def enable_from_region(self):
        if self.feature not in self.region.features['service']['features']:
            self.region.features['service']['features'].append(self.feature)
        self.region.state = const.STATE_DEPLOYING

    def enable_from_region_settings(self):
        if self.feature not in self.region_setting.user_settings['REGION']['FEATURES']:
            self.region_setting.user_settings['REGION']['FEATURES'].append(self.feature)
        for component in FEATURE_COMPONENT_MAP[self.feature]['components']:
            if component not in self.region_setting.cluster_settings.get('COMPONENT', []):
                self.region_setting.cluster_settings.setdefault('COMPONENT', []).append(component)

    def enable_from_phoenix(self):
        user_settings, template_type = get_settings(self.region_setting)
        return RegionManager.create(self.region.env_uuid,
                                    user_settings,
                                    template_type,
                                    FEATURE_COMPONENT_MAP[self.feature]['components'])

    def enable_from_component_version(self):
        user_settings, template_type = get_settings(self.region_setting)
        region_settings = ClusterTemplateManger.get_region_settings(
            user_settings,
            template_type,
            FEATURE_COMPONENT_MAP[self.feature]['components'])
        versions = region_settings.get('attr', {}).pop('component_versions', {})
        for component in FEATURE_COMPONENT_MAP[self.feature]['components']:
            obj = versions.get(component)
            if not obj:
                LOG.error('Component: {} not in region_settings result'.format(component))
                continue
            if component not in self.component_map.keys() and component not in self.add_component.keys():
                self.add_component[component] = obj

    def enable(self):
        self._pre_enable()
        self.enable_from_region_settings()
        self.enable_from_region()
        self.enable_from_component_version()
        try:
            ips = self.enable_from_phoenix()
            self.enable_from_node(ips)
        except Exception as e:
            LOG.error('Enable feature: {} in region: {} fail. error: {}. stack: {}'.format(
                self.feature, self.region.id, e, traceback.format_exc()))
            self.handle_exception()
            raise FurionException(
                'feature_error',
                message='Enable feature: {} fail. error: {}'.format(self.feature, e.message))
        self._post_enable()

    def handle_exception(self):
        Region.objects.filter(id=self.region.id).update(updated_at=timezone.now(), state=const.STATE_CRITICAL)


class DisableExec(DisableFeature):

    def __init__(self, region, data):
        super(DisableExec, self).__init__(region, data)
        self.feature = 'exec'

    def disable_from_regionsetting(self):
        super(DisableExec, self).disable_from_regionsetting()
        self.region_setting.cluster_settings['DOOM_NODES'] = []
        self.region_setting.cluster_settings['DOOM_LB'] = ""

    def disable_from_region(self):
        super(DisableExec, self).disable_from_region()
        del self.region.features['service']['exec']


class EnableExec(EnableFeature):
    def __init__(self, region, data):
        super(EnableExec, self).__init__(region, data)
        self.feature = 'exec'

    def is_valid(self):
        super(EnableExec, self).is_valid()
        if 'node' not in self.data.keys():
            raise FurionException('insufficient_data', message='node field is needed')
        if not (isinstance(self.data['node'], list) or isinstance(self.data['node'], dict)):
            raise FurionException('invalid_input', message='node field must be list or dict')
        check_list = self.data['node']
        if isinstance(self.data['node'], dict):
            check_list = [self.data['node']['private_ip']]
        self.valid_node(check_list)

    def enable_from_region_settings(self):
        super(EnableExec, self).enable_from_region_settings()
        self.region_setting.cluster_settings['DOOM_NODES'] = self.data['node']
        if 'lb' in self.data.keys():
            self.region_setting.cluster_settings['DOOM_LB'] = self.data['lb']

    def enable_from_region(self):
        super(EnableExec, self).enable_from_region()
        endpoint = self.data.get('lb') if self.data.get('lb') else self.data['node']['public_ip']
        self.region.features['service']['exec'] = {'endpoint': endpoint}


class DisableHaproxy(DisableFeature):

    def __init__(self, region, data):
        super(DisableHaproxy, self).__init__(region, data)
        self.feature = 'haproxy'

    def disable_from_regionsetting(self):
        super(DisableHaproxy, self).disable_from_regionsetting()
        del self.region_setting.cluster_settings['HAPROXY_NODE']
        if 'HAPROXY_IP_SETTINGS' in self.region_setting.cluster_settings:
            del self.region_setting.cluster_settings['HAPROXY_IP_SETTINGS']

    def disable_from_region(self):
        super(DisableHaproxy, self).disable_from_region()
        del self.region.features['service']['haproxy']


class EnableHaproxy(EnableFeature):
    def __init__(self, region, data):
        super(EnableHaproxy, self).__init__(region, data)
        self.feature = 'haproxy'

    def is_valid(self):
        super(EnableHaproxy, self).is_valid()
        if not isinstance(self.data, dict):
            raise FurionException('invalid_input', message='data field must be dict')
        if 'ip' not in self.data.keys():
            raise FurionException('insufficient_data', message='ip field is needed')

        if set(['public_ip', 'private_ip']) != set(self.data['ip'].keys()):
            raise FurionException('insufficient_data', message='public_ip, private_ip must be spec.')
        check_list = [self.data['ip']['private_ip']]
        self.valid_node(check_list)

    def enable_from_region_settings(self):
        super(EnableHaproxy, self).enable_from_region_settings()
        self.region_setting.cluster_settings['HAPROXY_IP_SETTINGS'] = {
            'IS_PUBLIC': self.data.get('is_public', 'true')
        }
        self.region_setting.cluster_settings['HAPROXY_NODE'] = self.data['ip']

    def enable_from_region(self):
        super(EnableHaproxy, self).enable_from_region()
        self.region.features['service']['haproxy'] = {
            'region_name': self.region.name,
            'cloud_name': self.region.attr.get('cloud', {}).get('name', 'private'),
            'lb_tag': 'ha',
            'ipaddress': self.data['ip']['public_ip'],
            'private_ip': self.data['ip']['private_ip']
        }


class DisableInternalhaproxy(DisableFeature):

    def __init__(self, region, data):
        super(DisableInternalhaproxy, self).__init__(region, data)
        self.feature = 'internal-haproxy'

    def disable_from_regionsetting(self):
        super(DisableInternalhaproxy, self).disable_from_regionsetting()
        del self.region_setting.cluster_settings['INTERNAL_HAPROXY_NODE']

    def disable_from_region(self):
        super(DisableInternalhaproxy, self).disable_from_region()
        del self.region.features['service']['internal_haproxy']


class EnableInternalhaproxy(EnableFeature):

    def __init__(self, region, data):
        super(EnableInternalhaproxy, self).__init__(region, data)
        self.feature = 'internal-haproxy'

    def is_valid(self):
        super(EnableInternalhaproxy, self).is_valid()
        if not isinstance(self.data, dict):
            raise FurionException('invalid_input', message='data field must be dict')
        if 'ip' not in self.data.keys():
            raise FurionException('insufficient_data', message='ip field is needed')
        check_list = [self.data['ip']]
        self.valid_node(check_list)

    def enable_from_region_settings(self):
        super(EnableInternalhaproxy, self).enable_from_region_settings()
        self.region_setting.cluster_settings['INTERNAL_HAPROXY_NODE'] = self.data['ip']

    def enable_from_region(self):
        super(EnableInternalhaproxy, self).enable_from_region()
        self.region.features['service']['internal_haproxy'] = {
            'region_name': self.region.name,
            'cloud_name': self.region.attr.get('cloud', {}).get('name', 'private'),
            'lb_tag': 'ha',
            'private_ip': self.data['ip']
        }


class DisableRegistry(DisableFeature):
    def __init__(self, region, data):
        super(DisableRegistry, self).__init__(region, data)
        self.feature = 'registry'
        self.token = self.data['root_token']
        self.register = ServiceRegister(self.token)

    def disable_from_regionsetting(self):
        super(DisableRegistry, self).disable_from_regionsetting()
        index = ''
        if 'REGISTRY_NODE' in self.region_setting.cluster_settings:
            index = '{}:5000'.format(self.region_setting.cluster_settings['REGISTRY_NODE'])
            del self.region_setting.cluster_settings['REGISTRY_NODE']
        if 'REGISTRY_SETTINGS' in self.region_setting.cluster_settings:
            del self.region_setting.cluster_settings['REGISTRY_SETTINGS']
        if index and index in self.region_setting.user_registries.keys():
            del self.region_setting.user_registries[index]

    def _post_disable(self):
        super(DisableRegistry, self)._post_disable()
        self.register.remove_registry(self.region_info)


class EnableRegistry(EnableFeature):
    def __init__(self, region, data):
        super(EnableRegistry, self).__init__(region, data)
        self.feature = 'registry'
        self.token = self.data['root_token']
        self.register = ServiceRegister(self.token)

    def _pre_enable(self):
        registry = {'registry': self.data}
        Validation.valid_registry(registry)
        node, registry_setting = GetInfo.get_private_registry(self.region_info, registry)
        self.data = {
            'REGISTRY_NODE': node,
            'REGISTRY_SETTINGS': registry_setting
        }
        super(EnableRegistry, self)._pre_enable()

    def _post_enable(self):
        self.data['USER_SETTINGS'] = self.region_setting.user_settings
        self.register.create_registry(self.region_info, self.data)
        super(EnableRegistry, self)._post_enable()

    def is_valid(self):
        super(EnableRegistry, self).is_valid()
        if not set(['REGISTRY_NODE', 'REGISTRY_SETTINGS']).issubset(self.data.keys()):
            raise FurionException('insufficient_data',
                                  message='REGISTRY_NODE and REGISTRY_SETTINGS field are needed.')
        check_list = [self.data['REGISTRY_NODE']]
        self.valid_node(check_list)

    def enable_from_region_settings(self):
        super(EnableRegistry, self).enable_from_region_settings()
        self.region_setting.cluster_settings['REGISTRY_NODE'] = self.data['REGISTRY_NODE']
        self.region_setting.cluster_settings['REGISTRY_SETTINGS'] = self.data['REGISTRY_SETTINGS']
        index = self.data['REGISTRY_NODE']
        suffix = settings.DNS_REGISTER['SUFFIX']
        index_name = settings.DNS_REGISTER['INDEX']
        if suffix:
            index = '{}-{}.{}.{}'.format(self.region.name.replace('_', '-'),
                                         self.region.namespace,
                                         index_name, suffix)
        index = '{}:5000'.format(index)
        self.region_setting.user_registries[index] = {
            'token': self.region_setting.user_settings['JAKIRO_AUTH']['TOKEN'],
            'username': self.region_setting.user_settings['JAKIRO_AUTH']['USERNAME']
        }

    def enable_from_phoenix(self):
        user_settings, template_type = get_settings(self.region_setting)
        LOG.debug('User settings is: {}'.format(user_settings))
        components = []
        components.extend(FEATURE_COMPONENT_MAP[self.feature]['components'])
        components.append('coil_client')
        components.append('mesos_slave')
        return RegionManager.create(self.region.env_uuid,
                                    user_settings,
                                    template_type,
                                    components,
                                    True)


class EnableChronos(EnableFeature):
    def __init__(self, region, data):
        super(EnableChronos, self).__init__(region, data)
        self.feature = 'chronos'
        self.token = self.data['root_token']
        self.register = ServiceRegister(self.token)

    def _post_enable(self):
        data = {'USER_SETTINGS': self.region_setting.user_settings}
        data.update(self.region_setting.cluster_settings)
        self.register.create_private_build(self.region_info, data)
        super(EnableChronos, self)._post_enable()


class DisableChronos(DisableFeature):
    def __init__(self, region, data):
        super(DisableChronos, self).__init__(region, data)
        self.feature = 'chronos'
        self.token = self.data['root_token']
        self.register = ServiceRegister(self.token)

    def _post_disable(self):
        self.register.remove_private_build(self.region_info)
        super(DisableChronos, self)._post_disable()
