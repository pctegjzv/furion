#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

from django.conf import settings
from django.utils import timezone
from dateutil import parser

from app.models import Node
from controls.node import NodeControl
from controls.region import RegionControl
from util import const

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


class SCALE_CHOICE:  # noqa
    SCALE_UP, SCALE_DOWN, NONE = range(3)


def scale_check(region, data):
    if not _config_check(region):
        return SCALE_CHOICE.NONE
    if not _time_check(region):
        return SCALE_CHOICE.NONE
    if _scale_up_check(data, region):
        _update_time_point(region)
        return SCALE_CHOICE.SCALE_UP
    if _scale_down_check(data, region):
        _update_time_point(region)
        return SCALE_CHOICE.SCALE_DOWN
    return SCALE_CHOICE.NONE


def scale(choice, region, nodes):
    nodes_count = len(nodes)
    cloud_name = region.attr['cloud']['name'].upper()
    config = region.attr['node_scaling']

    def _scale_up():
        up_config = config['scale_up']
        delta = up_config['increase_delta'] if up_config['increase_delta'] is not None else 1
        if up_config['max_scale_num'] is not None:
            deploying_nodes_count = _get_deploying_count(region)
            delta = min(up_config['max_scale_num'] - nodes_count - deploying_nodes_count, delta)
            if delta <= 0:
                LOG.warning("Scale up check error, existing nodes >= 'max_scale_num'")
                return
        if const.CLOUD_VMWARE != cloud_name:
            instance_type = settings.CLOUD_SPEC_CONFIGS[cloud_name]['default_scale_type']
            if 'instance_type' in up_config:
                instance_type = up_config['instance_type']
            args = {
                'num': delta,
                'instance_type': instance_type
            }
            key = 'ssh_key_name'
            if key in up_config and up_config[key]:
                args[key] = up_config[key]
            LOG.info("Preparing to scale up. | {} / {} / {}".format(region, delta, instance_type))
        else:
            # add vmware keys
            args = {}
            vmware_scale_key = ['cpu_num', 'mem_size']
            for x in vmware_scale_key:
                if up_config[x]:
                    args[x] = up_config[x]

        # Note that we currently only support auto-scale for nodes of type mesos-slave.
        # mesos-slave-attribute auto-scale is not supported.
        node_type = const.PHOENIX_NODE_TYPE_COMPUTE
        if region.container_manager == const.CONTAINER_MANAGER_SWARM:
            node_type = const.PHOENIX_NODE_TYPE_SWARM_WORKER
        if region.container_manager == const.CONTAINER_MANAGER_K8S:
            node_type = const.PHOENIX_NODE_TYPE_K8S_SLAVE
        args['node_type'] = node_type
        user_data = RegionControl.get_user_data(region, node_type)
        NodeControl.create_nodes(region, args, user_data=user_data, source=const.NODE_SOURCE_SCALE_UP)

    def _scale_down():
        down_config = config['scale_down']
        delta = down_config['decrease_delta'] if down_config['decrease_delta'] is not None else 1
        if down_config['min_scale_num'] is not None:
            delta = min(nodes_count - down_config['min_scale_num'], delta)
            if delta <= 0:
                LOG.warning("Scale down check error, existing nodes <= 'min_scale_num'")
                return
        LOG.info("Preparing to scale down. | {} / {}".format(region, delta))
        res_list = sorted(nodes, key=lambda k: k['containers_count'])
        key = 'forbid_delete_occupied_node'
        if key not in down_config or down_config[key]:
            res_list = [x for x in nodes if x['containers_count'] == 0]
        count = 0
        for node in res_list:
            if count == delta:
                return
            ip = node['private_ip']
            obj = Node.objects.filter(region=region, private_ip=ip).first()
            if obj and obj.type == const.NODE_TYPE_COMPUTE:
                if obj.attr.get('source') in [const.NODE_SOURCE_API_CREATE, const.NODE_SOURCE_SCALE_UP]:
                    LOG.info("Delete node due to scale down. | {} / {}".format(region, ip))
                    NodeControl.delete(region, ip)
                    count += 1
        if count == 0:
            LOG.info("No suitable nodes found for scale down!")

    (_scale_up if choice == SCALE_CHOICE.SCALE_UP else _scale_down)()


def _scale_up_check(metrics, region):
    if _get_deploying_count(region):
        return False
    config = region.attr['node_scaling']['scale_up']
    key = 'max_scale_num'
    deploying_nodes_count = _get_deploying_count(region)
    if config[key] is not None and metrics['nodes_count'] + deploying_nodes_count >= config[key]:
        return False
    for key in ['cpus_utilization', 'mem_utilization']:
        m_key = 'max_{}'.format(key)
        if config[m_key] is not None and metrics[key] >= config[m_key]:
            LOG.info("Scale up check passed. | {}={}".format(key, metrics[key]))
            return True
    if region.container_manager != const.CONTAINER_MANAGER_K8S:
        key = 'available_ports_count'
        m_key = 'min_{}'.format(key)
        if m_key in config and config[m_key] is not None and key in metrics and metrics[key] <= config[m_key]:
            LOG.info("Scale up check passed. | {}={}".format(key, metrics[key]))
            return True
    return False


def _scale_down_check(metrics, region):
    config = region.attr['node_scaling']['scale_down']
    key = 'min_scale_num'
    if config[key] is not None and metrics['nodes_count'] <= config[key]:
        return False
    key = 'min_cpus_utilization'
    cpus_flag = config[key] is None or config[key] > metrics[key[4:]]
    key = 'min_mem_utilization'
    mem_flag = config[key] is None or config[key] > metrics[key[4:]]
    ports_flag = True
    if region.container_manager != const.CONTAINER_MANAGER_K8S:
        key = 'max_available_ports_count'
        if key in config and key[4:] in metrics:
            ports_flag = config[key] is None or config[key] < metrics[key[4:]]
    result = True if cpus_flag and mem_flag and ports_flag else False
    if result:
        LOG.info("Scale down check passed. | {} {} {}".format(metrics['cpus_utilization'],
                                                              metrics['mem_utilization'],
                                                              metrics.get('available_ports_count', -1)))
    return result


def _config_check(region):
    attr = region.attr
    if 'node_scaling' not in attr:
        return False
    config = region.attr['node_scaling']
    if not config['enable_scale']:
        return False
    return True


def _time_check(region):
    key = 'last_node_scaling_at'
    attr = region.attr
    if key not in attr:
        return True
    if int((timezone.now() - parser.parse(attr[key])).seconds) > int(settings.NODE_SCALE_INTERVAL):
        return True
    return False


def _get_deploying_count(region):
    return len(Node.objects.filter(region=region, state=const.STATE_DEPLOYING))


def _update_time_point(region):
    region.attr['last_node_scaling_at'] = str(timezone.now())
    region.save()
