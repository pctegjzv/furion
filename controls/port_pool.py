#!/usr/bin/python
# -*- coding: utf-8 -*-
from django.conf import settings
from Crypto.Cipher import AES
from alauda.common.func import encode_aes, pad_str
from django.db import transaction
from furion.exception import FurionException
from util import const
from app.models import PortPool


def generate_tunnel_info(region_name, namespace):
    def _generate_ssh_keys():
        from Crypto.PublicKey import RSA
        key = RSA.generate(2048)
        private_key, pub_key = key.exportKey('PEM'), key.publickey().exportKey('OpenSSH')
        cipher = AES.new(pad_str(settings.SECRET_KEY)[:32])
        return encode_aes(cipher, private_key), encode_aes(cipher, pub_key)

    def _allocate_ports():
        with transaction.atomic():
            ssh_port = PortPool.objects.filter(type=const.SSH, used=False).first()
            if not ssh_port:
                raise FurionException('not_enough_port', message_params=['SSH port'])
            mapping_ports = PortPool.objects.filter(
                type=const.MAPPING, used=False,
                public_ip=ssh_port.public_ip).order_by('port')[:5]
            if not mapping_ports or len(mapping_ports) < 5:
                raise FurionException('not_enough_port', message_params=[ssh_port.public_ip])

            ssh_port.used = True
            ssh_port.region_name = region_name
            ssh_port.namespace = namespace
            ssh_port.save()

            for port in mapping_ports:
                port.used = True
                port.region_name = region_name
                port.namespace = namespace
                port.save()

            return {
                'ssh': ssh_port.to_address(),
                'puck': mapping_ports[0].to_address(),
                'tiny': mapping_ports[1].to_address(),
                'doom': mapping_ports[2].to_address(),
                'registry': mapping_ports[3].to_address(),
                'reserved': mapping_ports[4].to_address(),
            }

    private_key, pub_key = _generate_ssh_keys()
    return {
        'private_key': private_key,
        'public_key': pub_key,
        'port_mapping': _allocate_ports()
    }


def recycle_tunnel(region_name, namespace):
    with transaction.atomic():
        port_mapping = PortPool.objects.filter(region_name=region_name, namespace=namespace)
        if port_mapping.exists():
            for port in port_mapping:
                port.used = False
                port.region_name = ''
                port.namespace = ''
                port.save()
