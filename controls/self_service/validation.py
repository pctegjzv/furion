from furion.exception import InvalidArgsException
from util import const
import ss_settings
import copy
import re

VALID_PATH_PATTERN = '/[/A-Za-z0-9_]*'
VALID_NAME_PATTERN = '[A-Za-z0-9_-]*'
VALID_CIDR = 16
VALID_K8S_VERSION_PATTERN = re.compile(r'^(\d)\.(\d+)\.(\d+)$')
VALID_K8S_MAJOR_VERSION_PATTERN = re.compile(r'^(\d)\.(\d+)$')


class CheckFeature(object):
    @classmethod
    def check_feature(cls, data, feature):
        return feature in data.get('features', [])

    @classmethod
    def check_region_feature(cls, region, feature):
        return feature in region['features']['service']['features']


class BasicValidation(object):
    @classmethod
    def valid_dict(cls, obj, name):
        if not isinstance(obj, dict):
            raise InvalidArgsException(message='{} expect: dict, actual: {}'.format(name, type(obj)))

    @classmethod
    def valid_list(cls, obj, name):
        if not isinstance(obj, list):
            raise InvalidArgsException(message='{} expect: list, actual: {}'.format(name, type(obj)))

    @classmethod
    def valid_string(cls, obj, name):
        if not (isinstance(obj, str) or isinstance(obj, unicode)):
            raise InvalidArgsException(message='{} expect: str, actual: {}'.format(name, type(obj)))

    @classmethod
    def valid_contain_keys(cls, obj, keys, name):
        if not set(keys).issubset(set(obj)):
            raise InvalidArgsException(message='{} should contain: {}, actual: {}'.format(name, keys, set(obj)))

    @classmethod
    def valid_unique(cls, obj, keys, name):
        length = len(set(obj) & set(keys))
        if length != 1:
            raise InvalidArgsException(message='{} can only contain one of {}, actual: {}'.format(name, keys, set(obj)))

    @classmethod
    def valid_startwith(cls, obj, target, name):
        if not str(obj).startswith(target):
            raise InvalidArgsException(message='{} should starts with {}, actual: {}'.format(name, target, obj))

    @classmethod
    def valid_length_equal(cls, obj, target, name):
        if len(obj) != target:
            raise InvalidArgsException(message="{}'s size should be {}, actual: {}".format(name, target, len(obj)))

    @classmethod
    def valid_greater(cls, obj, target, name):
        if len(obj) < target:
            raise InvalidArgsException(
                "{}'s size should greater or equal {}, actual: {}".format(name, target, len(obj)))

    @classmethod
    def valid_less(cls, obj, target, name):
        if len(obj) > target:
            raise InvalidArgsException("{}'s size should less than {}, actual: {}".format(name, target, len(obj)))

    @classmethod
    def valid_in(cls, obj, target, name):
        if obj not in target:
            raise InvalidArgsException(message="{}'s {} should be one of {}.".format(name, obj, target))

    @classmethod
    def valid_subset(cls, obj, target, obj_name, target_name):
        if not set(obj).issubset(set(target)):
            raise InvalidArgsException("{} {} should be {}'s subset {}.".format(obj_name, obj, target_name, target))

    @classmethod
    def valid_not_equal(cls, src, dest, name):
        if src == dest:
            raise InvalidArgsException(message='{} should not same as {} in {}'.format(src, dest, name))

    @classmethod
    def valid_equal(cls, src, dest, name):
        if src != dest:
            raise InvalidArgsException(message='{} should same as {} in {}'.format(src, dest, name))

    @classmethod
    def valid_re(cls, src, pattern, name):
        p = re.compile(pattern)
        ret = p.search(src)
        if not ret or ret.group() != src:
            raise InvalidArgsException(message='{} is invalid input for {}. pattern: {}'.format(src, pattern, name))

    @classmethod
    def valid_ip(cls, obj, name):
        cls.valid_string(obj, name)
        p = re.compile('^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
        if p.match(obj) is None:
            raise InvalidArgsException("{} is not an valid ip in {}.".format(obj, name))

    @classmethod
    def valid_ascii(cls, obj, name):
        cls.valid_string(obj, name)
        if not all(ord(c) < 128 for c in obj):
            raise InvalidArgsException("{} is not ascii.".format(name))


class Validation(BasicValidation):
    @classmethod
    def valid_container_manager(cls, region, data):
        cm = data.get('container_manager', '').upper()
        if not cm:
            raise InvalidArgsException(message='Please specify container_mananger')
        if region['container_manager'] == const.CONTAINER_MANAGER_NONE:
            if cm not in const.CONTAINER_MANAGER_NOT_NONE_LIST:
                lis = const.CONTAINER_MANAGER_NOT_NONE_LIST
                raise InvalidArgsException(message='container_manager must in: {}'.format(lis))
        if region['container_manager'] in const.CONTAINER_MANAGER_NOT_NONE_LIST:
            if cm != const.CONTAINER_MANAGER_NONE:
                raise InvalidArgsException(message='Only support EMPTY an UN-EMPTY region')

    @classmethod
    def valid_registry(cls, data):
        Validation.valid_contain_keys(data, ['registry'], 'data')
        registry = data['registry']
        keys = ['registry_settings', 'node']
        Validation.valid_contain_keys(registry, keys, 'registry in data')
        Validation.valid_dict(registry['registry_settings'], 'registry_settings in registry')
        Validation.valid_contain_keys(registry['registry_settings'], ['name', 'storage'], 'registry_settings')
        reg_settings = registry['registry_settings']
        Validation.valid_ascii(reg_settings['name'], 'name in registry')
        Validation.valid_re(reg_settings['name'], VALID_NAME_PATTERN, 'name in registry')
        if 'local_hostdir' in reg_settings:
            Validation.valid_re(reg_settings['local_hostdir'], VALID_PATH_PATTERN, 'local_hostdir in registry')

    @classmethod
    def valid_pipeline(cls, data):
        features = data.get('features', [])
        if 'registry' not in features or 'chronos' not in features:
            raise InvalidArgsException('Please enable registry and chronos before pipeline')

    @classmethod
    def valid_zone(cls, zone, aws_region_id):
        if not re.match('^' + aws_region_id + '[a-z]$', zone):
            raise InvalidArgsException('AWS zone is wrong')

    @classmethod
    def valid_volume(cls, data, region_data):
        volume = data.get('volume')
        if volume is None:
            raise InvalidArgsException('Must spec volume')

        if not isinstance(volume, dict):
            raise InvalidArgsException('volume must be a dict')

        volume_settings = volume.get('volume_settings', {})
        if not volume_settings:
            return

        if volume_settings is None:
            raise InvalidArgsException('Must spec volume settings')

        if not isinstance(volume_settings, dict):
            raise InvalidArgsException('volume_settings must be a dict')

        drivers = volume_settings.keys()
        if CheckFeature.check_feature(data, ss_settings.VOLUME_GLUSTERFS):
            if ss_settings.VOLUME_GLUSTERFS not in drivers:
                raise InvalidArgsException('glusterfs settings not exist')

        if drivers:
            for driver in drivers:
                if driver not in ss_settings.VOLUME_DRIVERS:
                    raise InvalidArgsException('driver not support')

            if ss_settings.VOLUME_EBS in drivers:
                need_keys = ['aws_access_key', 'aws_secret_key']
                if data['container_manager'] == const.CONTAINER_MANAGER_K8S:
                    need_keys.append('zone')
                Validation.valid_contain_keys(volume_settings[ss_settings.VOLUME_EBS], need_keys, 'EBS in volume')
                zone_data = volume_settings[ss_settings.VOLUME_EBS].get('zone')
                aws_region_id = region_data.get('attr', {}).get('cloud', {}).get('region_id')
                if aws_region_id is None:
                    raise InvalidArgsException('Only aws region can specify EBS')
                if zone_data:
                    Validation.valid_zone(zone=zone_data, aws_region_id=aws_region_id)

            if ss_settings.VOLUME_GLUSTERFS in drivers:
                # if not CheckFeature.check_feature(data, ss_settings.VOLUME_GLUSTERFS):
                #     raise FurionException('invalid_args',
                #                           message="glusterfs volume need spec glusterfs feature")
                Validation.valid_contain_keys(volume_settings[ss_settings.VOLUME_GLUSTERFS],
                                              ['replica', 'nodes'],
                                              'glusterfs in volume')
                # path = volume_settings[ss_settings.VOLUME_GLUSTERFS]['path']
                # Validation.valid_re(path, VALID_PATH_PATTERN, 'path in volume_settings')

                nodes = volume_settings[ss_settings.VOLUME_GLUSTERFS]['nodes']
                replica = int(volume_settings[ss_settings.VOLUME_GLUSTERFS]['replica'])
                Validation.valid_dict(nodes, 'nodes in glusterfs')
                Validation.valid_greater(nodes, 1, 'nodes in glusterfs')
                if len(nodes) % replica != 0:
                    raise InvalidArgsException('nodes number should be a multiple of replica')

    @classmethod
    def valid_alb(cls, data, lb_type):
        Validation.valid_contain_keys(data, [lb_type], 'data')
        lb = data[lb_type]
        Validation.valid_dict(lb, '{} in data'.format(lb_type))
        Validation.valid_contain_keys(lb, ['public_ip', 'private_ip'], '{} in data'.format(lb_type))

    @classmethod
    def valid_internal_haproxy(cls, data):
        Validation.valid_contain_keys(data, ['internal-haproxy'], 'data')
        in_haproxy = data['internal-haproxy']
        if isinstance(in_haproxy, dict):
            Validation.valid_contain_keys(in_haproxy, ['floating_ip', 'nodes'], 'internal-haproxy in data')
            Validation.valid_list(in_haproxy['nodes'], 'nodes in internal-haproxy')

    @classmethod
    def valid_exec(cls, data):
        pass

    @classmethod
    def valid_tunnel(cls, data):
        pass

    @classmethod
    def valid_static_ip_docker_version(cls, region):
        docker_version = region['attr']['docker']['version']
        if docker_version not in ss_settings.SELF_SERVICE['SUPPORT_STATIC_IP_DOCKER_VERSION']:
            return False
        return True

    @classmethod
    def get_all_lb_name(cls):
        return [v['lb'] for _, v in ss_settings.SELF_SERVICE['CLOUDS'].items()]

    @classmethod
    def _valid_cloud_setting(cls, data):
        cloud_name = data.get('attr', {}).get('cloud', {}).get('name', 'private')
        cloud_name = str(cloud_name).upper()
        if cloud_name == 'PRIVATE':
            return True
        if cloud_name not in ss_settings.SELF_SERVICE['CLOUDS'].keys():
            raise InvalidArgsException('Cloud: {} not support'.format(cloud_name))
        lb = ss_settings.SELF_SERVICE['CLOUDS'][cloud_name]['lb']
        if not lb:
            return True
        keys = ss_settings.SELF_SERVICE['CLOUDS'][cloud_name]['keys']
        setting = copy.deepcopy(data.get('features', {}).get('service', {}).get(lb))
        if setting is None:
            raise InvalidArgsException('{} field is needed'.format(lb))
        setting.update(data.get('attr', {}).get('cloud', {}))
        if not (set(keys).issubset(set(setting.keys()))):
            raise InvalidArgsException('{} in {} field are needed'.format(keys, lb))
        service = data.get('features', {}).get('service', {})
        lb_info = set(service.keys()) & set(cls.get_all_lb_name())
        if len(lb_info) > 1:
            raise InvalidArgsException('Only one Load Balancer type is supported. Now: {}'.format(lb_info))
        features = service.get('features', [])
        if len(features) > 1:
            raise InvalidArgsException('Only one Load Balancer type is supported. Now: {}'.format(features))

    @classmethod
    def _valid_docker(cls, data):
        default_version = ss_settings.SELF_SERVICE['DEFAULT_DOCKER_VERSION']
        docker_version = data.get('attr', {}).get('docker', {}).get('version', default_version)
        path = data.get('attr', {}).get('docker', {}).get('path', '/var/lib/docker')
        support_version = ss_settings.SELF_SERVICE['SUPPORT_DOCKER_VERSION']
        if docker_version not in support_version:
            raise InvalidArgsException('docker version: {} is not support.'.format(docker_version))
        Validation.valid_re(path, VALID_PATH_PATTERN, 'path in docker')

    @classmethod
    def valid_empty_region_data(cls, data):
        cls._valid_cloud_setting(data)
        cls._valid_docker(data)

    @classmethod
    def valid_controller_haproxy(cls, obj):
        cls.valid_contain_keys(obj, ['nodes', 'floating_ip', 'monitor_ip'], 'controller_haproxy')
        cls.valid_list(obj['nodes'], 'nodes in controller_haproxy')
        cls.valid_greater(obj['nodes'], 1, 'nodes in controller_haproxy')
        cls.valid_string(obj['floating_ip'], 'floating_ip in controller_haproxy')
        cls.valid_string(obj['monitor_ip'], 'floating_ip in controller_haproxy')

    @classmethod
    def valid_k8s_cni(cls, cni, cidr):
        cls.valid_dict(cni, 'cni in data')
        cls.valid_contain_keys(cni, ['type'], 'type in cni')
        if cni['type'] == 'flannel':
            cls.valid_k8s_flannel(cni, cidr)
        elif cni['type'] == 'macvlan':
            cls.valid_k8s_macvlan(cni)
        elif cni['type'] == 'calico':
            cls.valid_k8s_calico(cni, cidr)
        else:
            raise InvalidArgsException("We don't support CNI: {} yet!".format(cni['type']))

    @classmethod
    def valid_k8s_macvlan(cls, obj):
        pass

    @classmethod
    def valid_k8s_flannel_alivpc(cls, flannel_settings):
        keys = ['access_key', 'secret_access_key']
        cls.valid_contain_keys(flannel_settings, keys, 'ali-vpc')
        cls.valid_string(flannel_settings['access_key'], 'access_key in ali-vpc')
        cls.valid_string(flannel_settings['secret_access_key'], 'secret_access_key in ali-vpc')

    @classmethod
    def valid_k8s_flannel(cls, obj, cidr):
        support_type = ['vxlan', 'host-gw', 'aws-vpc', 'ali-vpc']
        cls.valid_contain_keys(obj, ['flannel'], 'cni')
        cls.valid_dict(obj['flannel'], 'flannel in cni')
        cls.valid_contain_keys(obj['flannel'], ['type'], 'flannel in cni')
        cls.valid_in(obj['flannel']['type'], support_type, 'flannel type')
        # It seems flannel cidr must same as kubernetes cidr.
        # cls.valid_not_equal(obj['flannel']['cidr'], cidr, 'cidr in flannel')

    @classmethod
    def valid_k8s_calico(cls, obj, cidr):
        pass

    @classmethod
    def valid_cidr(cls, cidr):
        ip_cidr = cidr.split('/')
        cls.valid_equal(len(ip_cidr), 2, 'cidr in data')
        cls.valid_ip(ip_cidr[0], 'cidr in data')
        cls.valid_equal(int(ip_cidr[1]), VALID_CIDR, 'cidr in data')

    @classmethod
    def valid_k8s_basic_setting(cls, data, cni_types):
        cls.valid_contain_keys(data, ['master', 'cni'], 'data')
        cls.valid_unique(data['features'], cni_types, 'kubernetes')
        cls.valid_k8s_cni(data['cni'], data.get('cidr'))
        if data['cni']['type'] in ['flannel', 'calico']:
            cls.valid_contain_keys(data, ['cidr'], 'data')
            cls.valid_cidr(data['cidr'])

    @classmethod
    def valid_k8s_poc_init(cls, master):
        cls.valid_dict(master, 'master in data')
        cls.valid_contain_keys(master, ['public_ip', 'private_ip'], 'master in data')

    @classmethod
    def valid_k8s_prod_init(cls, data):
        cls.valid_list(data['master'], 'master in data')
        cls.valid_length_equal(data['master'], 3, 'master in data')
        cls.valid_unique(data, ['customer_lbs', 'controller_haproxy'], 'data')

    @classmethod
    def valid_external_lb(cls, data, keys):
        cls.valid_contain_keys(data, keys, 'data')

    @classmethod
    def valid_k8s_lb(cls, data, lbs):
        Validation.valid_unique(data, ['controller_haproxy', 'customer_lbs'], 'data')
        if 'controller_haproxy' in data.keys():
            cls.valid_controller_haproxy(data['controller_haproxy'])
            return
        cls.valid_dict(data['customer_lbs'], 'customer_lbs in data')
        cls.valid_contain_keys(data['customer_lbs'], lbs, 'customer_lbs in data')

    @classmethod
    def valid_mesos_lb(cls, data, lbs):
        Validation.valid_unique(data, ['controller_haproxy', 'customer_lbs'], 'data')
        if 'controller_haproxy' in data.keys():
            cls.valid_controller_haproxy(data['controller_haproxy'])
            return
        cls.valid_dict(data['customer_lbs'], 'customer_lbs in data')
        cls.valid_contain_keys(data['customer_lbs'], lbs, 'customer_lbs in data')

    @classmethod
    def valid_k8s_version(cls, data):
        version = data.get('version', ss_settings.SELF_SERVICE['KUBERNETES_DEFAULT_VERSION'])
        if VALID_K8S_MAJOR_VERSION_PATTERN.match(version):
            Validation.valid_in(
                version,
                ss_settings.SELF_SERVICE['KUBERNETES_SUPPORT_VERSION'],
                'KUBERNETES VERSION'
            )
        elif VALID_K8S_VERSION_PATTERN.match(version):
            major_version = '.'.join(version.split('.')[0:2])
            Validation.valid_in(
                major_version,
                ss_settings.SELF_SERVICE['KUBERNETES_SUPPORT_VERSION'],
                'KUBERNETES MAJOR VERSION'
            )
            Validation.valid_in(
                version,
                ss_settings.SELF_SERVICE['KUBERNETES_SUPPORT_VERSION'][major_version]['support'],
                'KUBERNETES VERSION'
            )
        else:
            raise InvalidArgsException(message="Invalid kubernetes version. Not Support")
