from validation import CheckFeature
import uuid
import copy
import ss_settings
import json
import base64

from util.func import gen_rand


class GetInfo(object):
    @classmethod
    def get_username(cls, region, data):
        username = data.get('username')
        if username != region['namespace'] and username.find('/') == -1:
            username = '{}/{}'.format(region['namespace'], username)
        return username

    @classmethod
    def generate_marathon_auth(cls):
        return {
            "USERNAME": 'alauda_marathon',
            "PASSWORD": gen_rand()
        }

    @classmethod
    def get_user_settings(cls, region, data):
        def _generate_puck_info():
            return {
                'USERNAME': 'puck',
                'PASSWORD': gen_rand()
            }

        over_commit = data.get('over_commit', {})
        if not isinstance(over_commit, dict):
            over_commit = {
                'cpu': 1,
                'memory': 1
            }
            data['over_commit'] = over_commit

        jakiro_auth = {
            'USERNAME': region['namespace'],
            'TOKEN': data.get('root_token')
        }
        feature_name = '{}_BASIC_FEATURES'.format(data['container_manager'])
        features = [x for x in ss_settings.SELF_SERVICE[feature_name]]
        features.extend(data.get('features', []))
        features.extend(region['features'].get('service', {}).get('features', []))
        region_info = {
            'CLOUD_NAME': str(region['attr'].get('cloud', {}).get('name', 'PRIVATE')).upper(),
            'NODE_LOCATION': region['attr'].get('cloud', {}).get('region_id', None),
            'NAMESPACE': region['namespace'],
            'SLAVE_USERNAME': data.get('slave_username', 'ubuntu'),
            'SLAVE_SSH_PORT': data.get("slave_ssh_port", "22"),
            'DISPLAY_NAME': region['display_name'],
            'NAME': region['name'],
            'OVER_COMMIT': {
                'CPU': data.get('over_commit', {}).get("cpu", 1),
                'MEMORY': data.get('over_commit', {}).get("memory", 1)
            },
            'FEATURES': list(set(features)),
            'ID': region['id']
        }
        return {
            'JAKIRO_AUTH': jakiro_auth,
            'REGION': region_info,
            'PUCK_AUTH': _generate_puck_info()
        }

    @classmethod
    def get_basic_registry(cls, region, data):
        result = {
            ss_settings.INDEX_URL: {
                'username': region['namespace'],
                'token': data.get('root_token')
            },
            '{}.{}'.format(region['namespace'], ss_settings.INDEX_URL): {
                'username': region['namespace'],
                'token': data.get('root_token')
            }
        }
        return result

    @classmethod
    def get_lb_settings(cls, region):
        lbs = region['features'].get('service', {}).get('features', None)
        if not lbs:
            return {}
        lbs = region['features']['service']['features']
        if not lbs:
            return {}
        lb = lbs[0]
        return {
            lb: region['features']['service'][lb]
        }

    @classmethod
    def get_private_registry(cls, region, data):
        registry = data['registry']
        node = registry['node']
        registry_settings = {str(k).upper(): v for k, v in registry['registry_settings'].items()}
        registry_settings_template = {
            'NAME': '',
            'ISSUER': '',
            'SERVICE': '',
            'STORAGE': '',
            'LOCAL_HOSTDIR': '',
            'LOCAL_ROOTDIR': '',
            'OSS_ACCESS_KEY': '',
            'OSS_SERECT_KEY': '',
            'OSS_REGION': '',
            'OSS_BUCKET': '',
            'OSS_ROOTDIR': '',
            'S3_ACCESS_KEY': '',
            'S3_SERECT_KEY': '',
            'S3_REGION': '',
            'S3_BUCKET': '',
            'S3_ROOTDIR': ''
        }
        registry_settings_template['ISSUER'] = str(uuid.uuid4())
        registry_settings_template['SERVICE'] = 'service'
        registry_settings_template.update(registry_settings)
        return node, registry_settings_template

    @classmethod
    def get_volume_settings(cls, data):

        """
        data(json): {
            'volume': {
                'volume_settings': {
                    "glusterfs": { ... },
                    "ebs": { ... },
                    "storageclasses": [ ... ]
                }
            }
        }

        Returns:
        {
            "AWS_SECRET_KEY": "",
            "AWS_ACCESS_KEY": "",
            "GFS_SERVER": "192.168.243.107,192.168.243.108",
            "DRIVERS": [{
                "disp_name": "glusterfs",
                "driver_name": "glusterfs",
                "desc": "glusterfs"
            }]
        }

        {
            "TOPO": "[\"192.168.243.107\", \"192.168.243.108\"]",
            "CLUSTERS": "192.168.243.107,192.168.243.108",
            "REPLICA": 1
        }

        """
        volume = data['volume']
        vs = volume['volume_settings']

        # xin settings
        volume_settings_template = {
            'AWS_ACCESS_KEY': '',
            'AWS_SECRET_KEY': '',
            'GFS_SERVER': '',
            'DRIVERS': [
            ]
        }

        driver_map = {
            'ebs': {
                'driver_name': 'ebs',
                'disp_name': 'AWS EBS',
                'desc': 'AWS EBS'
            },
            'glusterfs': {
                'driver_name': 'glusterfs',
                'disp_name': 'glusterfs',
                'desc': 'glusterfs'
            }
        }
        for driver in vs.keys():
            if driver == ss_settings.VOLUME_EBS:
                volume_settings_template['AWS_ACCESS_KEY'] = vs[driver]['aws_access_key']
                volume_settings_template['AWS_SECRET_KEY'] = vs[driver]['aws_secret_key']
                if 'zone' in vs[driver]:
                    volume_settings_template['AWS_ZONE'] = vs[driver]['zone']
            elif driver == ss_settings.VOLUME_GLUSTERFS:
                volume_settings_template['GFS_SERVER'] = ','.join(vs[driver]['nodes'])
            else:
                continue
            volume_settings_template['DRIVERS'].append(driver_map.get(driver, {}))

        gluster_settings = {}
        if 'glusterfs' in vs.keys():
            replica = vs[ss_settings.VOLUME_GLUSTERFS].get('replica', '1')
            gluster_settings = {
                'TOPO': base64.b64encode(
                    json.dumps(vs[ss_settings.VOLUME_GLUSTERFS]['nodes'])
                ),
                'CLUSTERS': ','.join((vs[ss_settings.VOLUME_GLUSTERFS]['nodes'])),
                'REPLICA': replica
            }

        return volume_settings_template, gluster_settings

    @classmethod
    def get_alb_settings(cls, data, region):
        result = {}
        nodes = data['haproxy'].get('nodes', [])
        if not nodes:
            nodes = [data['haproxy']['private_ip']]
        result['ALB_HAPROXY_NODES'] = nodes
        result['ALB_SETTINGS'] = {
            'HAPROXY_PUBLIC_IP': data.get('haproxy', {}).get('public_ip')
        }
        if CheckFeature.check_region_feature(region, 'slb') or CheckFeature.check_region_feature(region, 'elb'):
            result['ALB_XLB_NODES'] = [data.get('haproxy', {}).get('private_ip')]
        return result

    @classmethod
    def get_k8s_token(cls):
        return "{}.{}".format(gen_rand(3), gen_rand(8))

    @classmethod
    def get_k8s_cni(cls, data, region, cni_type):
        if cni_type == 'flannel':
            r = copy.deepcopy(data['cni'][cni_type])
            return {
                'CIDR': data['cidr'],
                'TYPE': r.pop('type'),
                'METADATA': r
            }
        elif cni_type == 'calico':
            return {
                'CIDR': data['cidr'],
                'TYPE': 'calico',
            }
        elif cni_type in ['macvlan', 'ipvlan']:
            retval = {
                'NIC': region['attr'].get('nic', 'eth0')
            }
            if region['attr'].get('cloud', {}).get('name', 'PRIVATE').upper() == 'QCLOUD':
                retval['DRIVER'] = 'qcloud'
                retval['REGION'] = region['attr']['cloud']['region_id']
                keypair = region['features']['service']['clb']
                retval['SECRET_ID'] = keypair['secret_id']
                retval['SECRET_KEY'] = keypair['secret_key']

            return retval
