from get_info import GetInfo
from validation import CheckFeature, Validation
from django.conf import settings
import ss_settings
from service_register import ServiceRegister
from controls.port_pool import generate_tunnel_info
import logging

LOG = logging.getLogger(__name__)


class DataTransverter(object):
    def __init__(self, data, region):
        self.region = region
        self.data = data
        self.result = {}
        self.token = self.data['root_token']
        self.register = ServiceRegister(self.token)

    def valid(self):
        Validation.valid_container_manager(self.region, self.data)
        if CheckFeature.check_feature(self.data, 'volume'):
            Validation.valid_volume(self.data, self.region)
        if CheckFeature.check_feature(self.data, 'haproxy'):
            Validation.valid_alb(self.data, 'haproxy')
        if CheckFeature.check_feature(self.data, 'nginx'):
            Validation.valid_alb(self.data, 'nginx')
        if CheckFeature.check_feature(self.data, 'internal-haproxy'):
            Validation.valid_internal_haproxy(self.data)
        if CheckFeature.check_feature(self.data, 'exec'):
            Validation.valid_exec(self.data)
        if CheckFeature.check_feature(self.data, 'registry'):
            Validation.valid_registry(self.data)

    def _pre_transfer(self):
        self.result['IPVLAN_SETTINGS'] = {}
        self.result['OTHERS_NODES'] = []
        self.result['XIN_SETTINGS'] = {"DRIVERS": []}
        self.result['COMPONENT'] = []

    def _post_transfer(self):
        cloud_name = self.region['attr']['cloud']['name'].upper()
        if cloud_name in ss_settings.SELF_SERVICE['SUPPORT_STATIC_IP_CLOUDS']:
            if Validation.valid_static_ip_docker_version(self.region):
                self.trans_static_ip(cloud_name)
        self.result['MANAGEMENT_REGISTRY'] = ss_settings.MANAGEMENT_REGISTRY
        if settings.PRIVATE_DEPLOY_SETTINGS['PRIVATE_DEPLOY_ENABLED']:
            self.result['LOCATION_SETTINGS'] = ss_settings.LOCATION_SETTINGS
        if CheckFeature.check_feature(self.data, 'alb') and 'haproxy' in self.result['COMPONENT']:
            self.result['COMPONENT'].remove('haproxy')
        self.trans_external_info()

    def get_result(self):
        self._pre_transfer()
        self.valid()
        self.transfer()
        self._post_transfer()
        self.result['TRANSVERTER'] = True
        return self.result

    def transfer(self):
        self.trans_location()
        self.trans_lb()
        self.trans_user_info()
        if CheckFeature.check_feature(self.data, 'tunnel'):
            Validation.valid_tunnel(self.data)
            self.trans_tunnel()
        if CheckFeature.check_feature(self.data, 'volume'):
            Validation.valid_volume(self.data, self.region)
            self.trans_volume()
        if CheckFeature.check_feature(self.data, 'haproxy'):
            Validation.valid_alb(self.data, 'haproxy')
            self.trans_haproxy()
            self.trans_alb(self.data, 'external', 'haproxy')
        if CheckFeature.check_feature(self.data, 'nginx'):
            Validation.valid_alb(self.data, 'nginx')
            self.trans_nginx()
            self.trans_alb(self.data, 'external', 'nginx')
        if CheckFeature.check_feature(self.data, 'internal-haproxy'):
            Validation.valid_internal_haproxy(self.data)
            self.trans_internal_haproxy()
        if CheckFeature.check_feature(self.data, 'exec'):
            Validation.valid_exec(self.data)
            self.trans_exec()
        if CheckFeature.check_feature(self.data, 'registry'):
            Validation.valid_registry(self.data)
            self.register.check(self.region,
                                self.data['registry']['registry_settings']['name'],
                                ss_settings.RES_PRIVATE_REGISTRY)
            self.trans_registry()
        if CheckFeature.check_feature(self.data, 'chronos'):
            self.register.create_private_build(self.region, self.result)
        self.trans_feature_to_component()
        self.trans_other_settings()

    def trans_other_settings(self):
        self.result['container_manager'] = self.data['container_manager']
        self.result['template_type'] = self.data['template_type']
        self.result['DOCKER'] = {
            'PATH': self.region['attr']['docker']['path'],
            'version': self.region['attr']['docker']['version']
        }
        self.result['IS_COMPUTE'] = self.data.get('is_compute', False)
        self.result['NIC'] = self.region['attr'].get('nic', 'eth0')

    def trans_feature_to_component(self):
        features = self.result['USER_SETTINGS']['REGION']['FEATURES']
        component = [x for x in ss_settings.SELF_SERVICE['MESOS_BASIC_COMPONENT']]
        for feat in features:
            if feat in ss_settings.SELF_SERVICE['MESOS_BASIC_FEATURES']:
                continue
            if feat not in ss_settings.SELF_SERVICE['COMPONENT_FEATURE_MAP'].keys():
                continue
            if feat in ss_settings.SELF_SERVICE['COMPONENT_MULTI_MAP'].keys():
                for f in ss_settings.SELF_SERVICE['COMPONENT_MULTI_MAP'][feat]:
                    if f in features:
                        component.extend(ss_settings.SELF_SERVICE['COMPONENT_MULTI_MAP'][feat][f])
            component.extend(ss_settings.SELF_SERVICE['COMPONENT_FEATURE_MAP'][feat])
        component.extend(ss_settings.SELF_SERVICE['MESOS_COMPUTE_COMPONENT'])
        self.result['COMPONENT'].extend(list(set(component)))

    def trans_location(self):
        self.result['IMAGE_LOCATION'] = ss_settings.IMAGE_LOCATION
        self.result['REGION_LOCATION'] = ss_settings.REGION_LOCATION

    def trans_user_info(self):
        self.result['USER_SETTINGS'] = GetInfo.get_user_settings(self.region, self.data)
        self.result['USER_REGISTRIES'] = GetInfo.get_basic_registry(self.region, self.data)

    def trans_lb(self):
        self.result['CLOUD_LB_SETTINGS'] = GetInfo.get_lb_settings(self.region)

    def trans_tunnel(self):
        self.region['features']['tunnel'] = generate_tunnel_info(self.region['name'],
                                                                 self.region['namespace'])

    def trans_registry(self):
        def _parse_register_ip(node):
            if 'lb' in self.data['registry']:
                return self.data['registry']['lb']
            if 'controller_haproxy' in self.data:
                return self.data['controller_haproxy']['floating_ip']
            if isinstance(node, list):
                return node[0]
            return node

        def _parse_registry_endpoint():
            node = self.result['REGISTRY_LB']
            endpoint = '{}:5000'.format(node)
            if settings.DNS_REGISTER['SUFFIX']:
                endpoint = '{}-{}.{}.{}:5000'.format(self.region['name'].replace('_', '-'),
                                                     self.region['namespace'],
                                                     settings.DNS_REGISTER['INDEX'],
                                                     settings.DNS_REGISTER['SUFFIX'])
            return endpoint

        node, registry_settings = GetInfo.get_private_registry(self.region, self.data)

        self.result['REGISTRY_LB'] = _parse_register_ip(node)
        self.result['REGISTRY_SETTINGS'] = registry_settings
        endpoint = _parse_registry_endpoint()
        self.result['USER_REGISTRIES'][endpoint] = {
            'username': self.region['namespace'],
            'token': self.data.get('root_token')
        }
        self.register.create_registry(self.region, self.result, endpoint, self.result['REGISTRY_LB'])
        self.result['REGISTRY_NODE'] = node

    def trans_volume(self):
        """ Translate Volume structure from user input to alchemist style

        Args:
            data(json):  {
                "volume": {
                    "volume_settings": {
                        "glusterfs": {
                            "path": "/data",
                            "nodes": [
                                "10.1.0.20",
                                "10.1.0.21",
                                "10.1.0.22"
                            ],
                            "replica": "1"
                        },
                        "ebs": {
                                "aws_secret_key": "bbbbb",
                                "aws_access_key": "aaaaa",
                                "zone": "cn-north1a"
                        }
                    },
                    "storageclasses": [
                        {
                            "name": "ebs",
                            "default": true,
                        },
                        {
                            "name": "glusterfs",
                            "default": false,
                        }
                    ]
                },
            }
        """

        xin_settings, gfs_settings = GetInfo.get_volume_settings(self.data)

        self.result['XIN_SETTINGS'] = xin_settings
        if gfs_settings:
            self.result['GLUSTERFS_SETTINGS'] = gfs_settings
            self.result['GLUSTERFS_NODES'] = gfs_settings['CLUSTERS']
            if not CheckFeature.check_feature(self.data, 'glusterfs'):
                self.result['USER_SETTINGS']['REGION']['FEATURES'].append('glusterfs')

    def trans_alb(self, data, address_type, lb_type):
        # self.result.update(GetInfo.get_alb_settings(data, self.region))
        name = self.register.create_alb(self.region, self.result, address_type, lb_type)
        self.result['LOADBALANCE_NAME'] = name

    def trans_static_ip(self, cloud_name):
        self.result['USER_SETTINGS']['REGION']['FEATURES'].append('static-ip')
        if cloud_name != 'PRIVATE':
            lb_name = ss_settings.SELF_SERVICE['CLOUDS'][cloud_name]['lb']
            self.result['IPVLAN_SETTINGS'] = {
                'IPVLAN_ACCESS_KEY':
                    self.result['CLOUD_LB_SETTINGS'][lb_name]['access_key'],
                'IPVLAN_SECRET_KEY':
                    self.result['CLOUD_LB_SETTINGS'][lb_name]['secret_access_key'],
                'IPVLAN_NETWORK_TYPE': self.data.get('static-ip', 'ipvlan')
            }
        else:
            self.result['IPVLAN_SETTINGS'] = {
                'IPVLAN_NETWORK_TYPE': 'macvlan'
            }

    def trans_haproxy(self):
        self.result['HAPROXY_NODE'] = self.data.get('haproxy')
        self.result['HAPROXY_IAAS_LB'] = self.data.get('haproxy', {}).get("iaas_lb", False)

    def trans_nginx(self):
        self.result['NGINX_NODE'] = self.data.get('nginx')

    def trans_internal_haproxy(self):
        in_haproxy = self.data['internal-haproxy']
        if isinstance(in_haproxy, dict):
            self.result['INTERNAL_HAPROXY_NODE'] = in_haproxy['nodes']
            self.result['INTERNAL_HAPROXY_PRIVATE_IP'] = in_haproxy['floating_ip']
            self.result['INTERNAL_HAPROXY_IAAS_LB'] = in_haproxy.get('iaas_lb', False)
        else:
            self.result['INTERNAL_HAPROXY_NODE'] = in_haproxy
            self.result['INTERNAL_HAPROXY_PRIVATE_IP'] = in_haproxy

    def trans_exec(self):
        pass

    def trans_external_info(self):
        pass

    def trans_controller_haproxy(self, obj):
        self.result['CONTROLLER_HAPROXY_NODES'] = obj['nodes']
        self.result['CONTROLLER_HAPROXY_FLOATING_IP'] = obj['floating_ip']
        self.result['COMPONENT'].append('controller_haproxy')
        self.result['CONTROLLER_HAPROXY_MONITOR_IP'] = obj['monitor_ip']
        self.result['CONTROLLER_HAPROXY_IAAS_LB'] = obj.get('iaas_lb', False)


class Cleanup(object):
    def __init__(self, region, token):
        self.region = region
        self.register = ServiceRegister(token)

    def cleanup(self):
        self.register.remove_registry(self.region)
        self.register.remove_private_build(self.region)
        self.register.remove_alb(self.region)
        self.register.remove_namespaces(self.region)
