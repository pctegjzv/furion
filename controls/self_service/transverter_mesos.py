from get_info import GetInfo
from validation import CheckFeature, Validation
import logging
from controls.self_service.transverter_base import DataTransverter

LOG = logging.getLogger(__name__)


class MesosPocDataTransverter(DataTransverter):
    def _pre_transfer(self):
        if 'haproxy' not in self.data.keys():
            self.data['haproxy'] = self.data['master']
        super(MesosPocDataTransverter, self)._pre_transfer()

    def trans_mesos_lb(self):
        master_nodes = self.data.get('master')
        self.result['CHRONOS_PUBLIC_LB'] = 'http://{}:4400'.format(master_nodes['public_ip'])

    def trans_mesos(self):
        master_nodes = self.data.get('master')
        self.result['MASTER_NODES'] = master_nodes
        slaves = self.data.get('slave', [])
        self.result['MESOS_SLAVE_WITH_TAG'] = self.data.get('slave_with_tag', [])
        slaves.extend(self.result['MESOS_SLAVE_WITH_TAG'])
        self.result['SLAVE_NODES'] = list(set(slaves))
        marathon_auth = GetInfo.generate_marathon_auth()
        self.result['USER_SETTINGS']['MARATHON_AUTH'] = marathon_auth
        self.trans_mesos_lb()

    def transfer(self):
        super(MesosPocDataTransverter, self).transfer()
        self.trans_mesos()

    def valid(self):
        Validation.valid_contain_keys(self.data, ['master'], 'master')
        Validation.valid_dict(self.data['master'], 'master in data')
        Validation.valid_contain_keys(self.data['master'], ['public_ip', 'private_ip'], 'master')
        super(MesosPocDataTransverter, self).valid()


class MesosProdDataTransverter(MesosPocDataTransverter):
    def __init__(self, data, region):
        super(MesosProdDataTransverter, self).__init__(data, region)
        self.lbs = ['puck_public_lb', 'puck_private_lb', 'marathon_lb']

    def _pre_transfer(self):
        # Call grandfather class
        super(MesosPocDataTransverter, self)._pre_transfer()

    def trans_customer_lbs(self, obj):
        self.result['PUCK_PUBLIC_LB'] = obj['puck_public_lb']
        self.result['PUCK_INTERNAL_LB'] = obj['puck_private_lb']
        self.result['MARATHON_LB'] = obj['marathon_lb']
        if CheckFeature.check_feature(self.data, 'exec'):
            self.result['DOOM_LB'] = obj['doom_lb']
        if CheckFeature.check_feature(self.data, 'registry'):
            self.result['REGISTRY_LB'] = obj['registry_lb']
        if CheckFeature.check_feature(self.data, 'chronos'):
            self.result['CHRONOS_PUBLIC_LB'] = 'http://{}:4400'.format(obj['chronos_public_lb'])
            self.result['CHRONOS_PRIVATE_LB'] = 'http://{}:4400'.format(obj['chronos_private_lb'])
        if CheckFeature.check_feature(self.data, 'volume'):
            self.result['XIN_AGENT_LB'] = obj['xin_agent_lb']

    def trans_mesos_lb(self):
        if 'controller_haproxy' in self.data:
            self.trans_controller_haproxy(self.data['controller_haproxy'])
            return
        if 'customer_lbs' in self.data:
            self.trans_customer_lbs(self.data['customer_lbs'])

    def valid(self):
        Validation.valid_mesos_lb(self.data, self.lbs)
        Validation.valid_contain_keys(self.data, ['master'], 'master')
        Validation.valid_list(self.data['master'], 'master in data')
        super(MesosPocDataTransverter, self).valid()
