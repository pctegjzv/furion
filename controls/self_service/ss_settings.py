import os

INDEX_URL = os.getenv('SS_INDEX_URL', 'index.alauda.cn')

IMAGE_LOCATION = os.getenv('SS_IMAGE_LOCATION', 'CN')
REGION_LOCATION = os.getenv('SS_REGION_LOCATION', 'INT')

KALDR_URL = os.getenv('KALDR_URL')

RES_PRIVATE_REGISTRY = 'PRIV_REGISTRY'
RES_PRIVATE_BUILD_CONFIG = 'BUILDS_CONFIG'

VOLUME_EBS = 'ebs'
VOLUME_GLUSTERFS = 'glusterfs'
STORAGECLASSES = 'storageclasses'
STORAGECLASSES_GFS_NAME = 'alauda-system-gfs'
STORAGECLASSES_EBS_NAME = 'alauda-system-ebs'
STORAGECLASSES_PROVISIONER_GFS = 'kubernetes.io/glusterfs'
STORAGECLASSES_PROVISIONER_EBS = 'kubernetes.io/aws-ebs'

K8S_LIST = {
    "apiVersion": "v1",
    "items": [],
    "kind": "List",
}

STORAGECLASSE_TMP = {
    "apiVersion": "storage.k8s.io/v1",
    "kind": "StorageClass",
    "metadata": {
        "annotations": {
            "storageclass.kubernetes.io/is-default-class": "false"
        },
        "name": "",
    },
    "mountOptions": [
    ],
    "parameters": {
    },
    "provisioner": "",
    "reclaimPolicy": "Delete"
}

VOLUME_DRIVERS = [
    VOLUME_EBS,
    VOLUME_GLUSTERFS,
]

SELF_SERVICE = {
    'COMPONENT_FEATURE_MAP': {
        'haproxy': ['haproxy'],
        'internal-haproxy': ['internal_haproxy'],
        'registry': ['registry'],
        'volume': ['xin_agent', 'xin_driver'],
        # 'glusterfs': ['glusterfs'],
        'crawler': [],
        'static-ip': [],
        'exec': ['doom'],
        'pipeline': [],
        'elb': [],
        'slb': [],
        'blb': [],
        'ulb': [],
        'clb': [],
        'qinglb': [],
        'alb': [],
        'nginx': [],
        'tunnel': ['coil_client'],
    },

    'COMPONENT_MULTI_MAP': {
        'alb': {
            'slb': ['alb_xlb'],
            'elb': ['alb_xlb'],
            'clb': ['alb_xlb'],
            'nginx': ['alb_nginx'],
            'haproxy': ['alb_haproxy']
        }
    },

    'MESOS_BASIC_FEATURES': ['raw-container', 'file-log', 'tunnel',
                             'application', 'host-network', 'mipn', 'chronos',
                             'mount-points'],
    'MESOS_BASIC_COMPONENT': ['mesos_master', 'marathon', 'zookeeper',
                              'puck', 'coil_client', 'xin_agent', 'chronos'],
    'MESOS_COMPUTE_COMPONENT': ['mesos_slave', 'nevermore', 'docker_daemon_proxy',
                                'xin_driver'],
    'SWARM_BASIC_FEATURES': ['raw-container', 'file-log', 'tunnel',
                             'application', 'host-network', 'mount-points'],
    'SWARM_BASIC_COMPONENT': ['meepo', 'puck', 'coil_client', 'swarm',
                              'xin_agent'],
    'SWARM_COMPUTE_COMPONENT': ['nevermore', 'docker_daemon_proxy', 'xin_driver'],
    'KUBERNETES_BASIC_FEATURES': ['raw-container', 'application', 'host-network',
                                  'mount-points', 'file-log'],
    'KUBERNETES_BASIC_COMPONENT': ['kubernetes_init', 'etcd'],
    'KUBERNETES_COMPUTE_COMPONENT': ['kubernetes_compute', 'nevermore'],
    'SUPPORT_CLOUD': [''],
    'CLOUDS': {
        'AWS': {
            'lb': 'elb',
            'keys': ['access_key', 'secret_access_key', 'region_id']
        },
        'UCLOUD': {
            'lb': 'ulb',
            'keys': ['public_key', 'private_key', 'project_id']
        },
        'ALI': {
            'lb': 'slb',
            'keys': ['access_key', 'secret_access_key', 'region_id']
        },
        'QCLOUD': {
            'lb': 'clb',
            'keys': ['secret_key', 'secret_id', 'region_id'],
        },
        'BAIDU': {
            'lb': 'blb',
            'keys': ['access_key', 'secret_access_key', 'region_id']
        },
        'QING': {
            'lb': 'qinglb',
            'keys': ['access_key', 'secret_access_key', 'region_id']
        },
        'OPENSTACK': {
            'lb': '',
            'keys': []
        },
        'VMWARE': {
            'lb': '',
            'keys': []
        },
        'KINGSOFT': {
            'lb': '',
            'keys': []
        }
    },
    'SUPPORT_ENABLE_DISABLE_FEATURES': ['haproxy', 'internal-haproxy',
                                        'exec', 'chronos', 'registry'],
    'SUPPORT_STATIC_IP_CLOUDS': ['AWS', 'BAIDU', 'PRIVATE'],
    'SUPPORT_DOCKER_VERSION': ['1.12.6', '17.03', '17.12'],
    'DEFAULT_DOCKER_VERSION': '1.12.6',
    'SUPPORT_STATIC_IP_DOCKER_VERSION': ['1.12.6'],
    'KUBERNETES_DEFAULT_VERSION': os.getenv('KUBERNETES_DEFAULT_VERSION', '1.7.3'),
    'KUBERNETES_SUPPORT_VERSION': {
        '1.5': {
             'default': '1.5.1',
             'support': ['1.5.1']
         },
        '1.6': {
             'default': '1.6.7',
             'support': ['1.6.7']
         },
        '1.7': {
             'default': '1.7.3',
             'support': ['1.7.3']
         },
        '1.8': {
             'default': '1.8.4',
             'support': ['1.8.4']
         },
        '1.9': {
             'default': '1.9.6',
             'support': ['1.9.1', '1.9.4', '1.9.6']
         },
        '1.10': {
             'default': '1.10.0',
             'support': ['1.10.0']
         }
    },
    'KUBERNETES_PORT_RANGE': os.getenv("KUBERNETES_PORT_RANGE", "60000-62000")
}

LOCATION_SETTINGS = {
    'PRIVATE': {
        'INDEX': os.getenv('PRIVATE_INDEX', 'index.alauda.cn'),
        'JAKIRO_ENDPOINT': os.getenv('PRIVATE_JAKIRO_ENDPOINT', 'https://api.alauda.cn'),
        'JAKIRO_INTERNAL_ENDPOINT': os.getenv('PRIVATE_JAKIRO_INTERNAL_ENDPOINT',
                                              'https://innerapi.alauda.club:8443'),
        'NAIX_ENDPOINT': os.getenv('PRIVATE_NAIX_ENDPOINT', 'https://naix.alauda.cn'),
        'MIRANA_URL': os.getenv('PRIVATE_MIRANA_URL', 'https://mirana.alauda.club:8443/api/v1'),
        'MIRANA_TOKEN': os.getenv('PRIVATE_MIRANA_TOKEN', 'c3lzX2FkbWluOkRrNiQqa0tVWVY='),
    }
}

MANAGEMENT_REGISTRY = {
    os.getenv('PRIVATE_INDEX', 'index.alauda.cn'): {
        'username': os.getenv('PRIVATE_INDEX_USERNAME', 'claas'),
        'password': os.getenv('PRIVATE_INDEX_PASSWORD', 'YWxhdWRhCg')
    }
}
