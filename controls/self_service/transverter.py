from furion.exception import FurionException
from util.const import CONTAINER_MANAGER_SWARM, CONTAINER_MANAGER_MESOS, CONTAINER_MANAGER_NONE, \
    CONTAINER_MANAGER_K8S
from controls.self_service.transverter_base import Cleanup
from controls.self_service.transverter_k8s import K8sPocDataTransverter, K8sProdDataTransverter
from controls.self_service.transverter_mesos import MesosPocDataTransverter, \
    MesosProdDataTransverter
from controls.self_service.transverter_swarm import SwarmDataTransverter


def transverter(data, region):
    container_manager = data.get('container_manager')
    acmp_type = data.get('template_type')
    if container_manager == CONTAINER_MANAGER_MESOS:
        if acmp_type == 'poc':
            return MesosPocDataTransverter(data, region)
        if acmp_type == 'prod':
            return MesosProdDataTransverter(data, region)
        raise FurionException('invalid_input', message='{} is invalid type.'.format(acmp_type))
    if container_manager == CONTAINER_MANAGER_SWARM:
        return SwarmDataTransverter(data, region)
    if container_manager == CONTAINER_MANAGER_K8S:
        if acmp_type == 'kubernetes_poc':
            return K8sPocDataTransverter(data, region)
        if acmp_type == 'kubernetes_prod':
            return K8sProdDataTransverter(data, region)
    if container_manager == CONTAINER_MANAGER_NONE:
        return Cleanup(region, data.pop('root_token'))
    raise FurionException('invalid_input', message='{} is invalid container_manager'.format(container_manager))
