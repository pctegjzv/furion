from validation import CheckFeature
from django.conf import settings
from furion.exception import FurionException
import ss_settings
from controls.self_service.transverter_base import DataTransverter


class SwarmDataTransverter(DataTransverter):
    def __init__(self, data, region):
        super(SwarmDataTransverter, self).__init__(data, region)
        self.not_support_feature = ['chronos', 'pipeline', 'exec', 'alb']
        self.remove_feature = ['chronos']

    def _pre_transfer(self):
        if self.data['template_type'] == 'poc':
            if not CheckFeature.check_feature(self.data, 'haproxy'):
                self.data['features'].append('haproxy')
                if 'haproxy' not in self.data.keys():
                    self.data['haproxy'] = self.data['master']
        self.data['template_type'] = 'swarm'
        if CheckFeature.check_feature(self.data, 'exec'):
            self.data['features'].remove('exec')
        super(SwarmDataTransverter, self)._pre_transfer()

    def _post_transfer(self):
        for feature in self.remove_feature:
            if feature in self.result['USER_SETTINGS']['REGION']['FEATURES']:
                self.result['USER_SETTINGS']['REGION']['FEATURES'].remove(feature)
        self.result['MANAGEMENT_REGISTRY'] = ss_settings.MANAGEMENT_REGISTRY
        if settings.PRIVATE_DEPLOY_SETTINGS['PRIVATE_DEPLOY_ENABLED']:
            self.result['LOCATION_SETTINGS'] = ss_settings.LOCATION_SETTINGS

    def valid(self):
        for feature in self.not_support_feature:
            if CheckFeature.check_feature(self.data, feature):
                raise FurionException('invalid_input', message="Swarm doesn't support {} yet".format(feature))

    def trans_feature_to_component(self):
        features = self.result['USER_SETTINGS']['REGION']['FEATURES']
        component = [x for x in ss_settings.SELF_SERVICE['SWARM_BASIC_COMPONENT']]
        for feature in features:
            if feature in ss_settings.SELF_SERVICE['SWARM_BASIC_FEATURES']:
                continue
            if feature not in ss_settings.SELF_SERVICE['COMPONENT_FEATURE_MAP'].keys():
                continue
            component.extend(ss_settings.SELF_SERVICE['COMPONENT_FEATURE_MAP'][feature])

        component.extend(ss_settings.SELF_SERVICE['SWARM_COMPUTE_COMPONENT'])
        self.result['COMPONENT'] = list(set(component))

    def transfer(self):

        super(SwarmDataTransverter, self).transfer()
        master_nodes = self.data.get('master')
        manage_node = []
        if isinstance(master_nodes, dict):
            init_node = master_nodes.get('private_ip')
        else:
            init_node = master_nodes[0]
            manage_node = master_nodes[1:]

        self.result['SWARM_INIT_NODE'] = init_node
        self.result['SWARM_MANAGER_NODES'] = manage_node
        self.result['SWARM_WORKER_NODES'] = self.data.get('slave', [])
