from get_info import GetInfo
from validation import CheckFeature, Validation
from django.conf import settings
from furion.exception import FurionException
import ss_settings
import logging
from controls.self_service.transverter_base import DataTransverter
from util.const import KUBERNETES_PLATFORM_VERSION_V3
import six
import copy
import base64
import json


LOG = logging.getLogger(__name__)


class K8sPocDataTransverter(DataTransverter):
    def __init__(self, data, region):
        super(K8sPocDataTransverter, self).__init__(data, region)
        self.not_support_feature = []
        self.remove_feature = ['chronos']
        self.cni_types = ['macvlan', 'flannel', 'ipvlan', 'calico']
        self.cni_type = ''

    def _pre_transfer(self):
        pass

    def trans_static_ip(self, cloud_name):
        # Not support in K8s. So Override it
        pass

    # def trans_tunnel(self):
    #     self.result['USER_SETTINGS']['REGION']['FEATURES'].remove('tunnel')

    def trans_feature_to_component(self):
        features = self.result['USER_SETTINGS']['REGION']['FEATURES']
        component = [x for x in ss_settings.SELF_SERVICE['KUBERNETES_BASIC_COMPONENT']]
        for feature in features:
            if feature in ss_settings.SELF_SERVICE['KUBERNETES_BASIC_FEATURES']:
                continue
            if feature not in ss_settings.SELF_SERVICE['COMPONENT_FEATURE_MAP'].keys():
                continue
            multi_map = ss_settings.SELF_SERVICE['COMPONENT_MULTI_MAP']
            if feature in multi_map.keys():
                for f in multi_map[feature]:
                    if f in features:
                        component.extend(multi_map[feature][f])
            component.extend(ss_settings.SELF_SERVICE['COMPONENT_FEATURE_MAP'][feature])

        component.extend(ss_settings.SELF_SERVICE['KUBERNETES_COMPUTE_COMPONENT'])
        self.result['COMPONENT'] = list(set(component))

    def valid(self):
        for feature in self.not_support_feature:
            if CheckFeature.check_feature(self.data, feature):
                raise FurionException('invalid_input',
                                      message="Kubernetes doesn't support {} yet".format(feature))
        Validation.valid_k8s_basic_setting(self.data, self.cni_types)
        Validation.valid_k8s_poc_init(self.data['master'])
        Validation.valid_k8s_version(self.data)
        super(K8sPocDataTransverter, self).valid()

    def get_heketi_endpoint(self, master_address, port_range):
        if (
            not isinstance(master_address, six.string_types) or
            not isinstance(port_range, six.string_types) or
            not master_address or
            not port_range
        ):
            return

        _, _, max_port = port_range.partition("-")

        if not max_port:
            return

        return "http://{}:{}".format(master_address, max_port)

    def get_storageclasses(self, heketi_endpoint):

        if not heketi_endpoint:
            return

        if (
            'volume' not in self.data or
            not isinstance(self.data['volume'], dict)
        ):
            return

        volume = self.data["volume"]

        volume_settings = volume["volume_settings"]

        scs = volume.get("storageclasses")
        if not scs or not isinstance(scs, list):
            return

        default_sc = False
        storageclasses = copy.copy(ss_settings.K8S_LIST)
        for sc in scs:
            if (
                not isinstance(sc, dict) or
                sc.get("name") not in ss_settings.VOLUME_DRIVERS or
                sc.get("name") not in volume_settings
            ):
                continue
            name = sc["name"]
            storageclass_dict = copy.deepcopy(ss_settings.STORAGECLASSE_TMP)
            if ss_settings.VOLUME_GLUSTERFS == name:
                storageclass_dict["metadata"]["name"] = \
                    ss_settings.STORAGECLASSES_GFS_NAME

                replica = volume_settings[ss_settings.VOLUME_GLUSTERFS]['replica']

                volume_type = "none"

                if int(replica) > 1:
                    volume_type = "replicate:%s" % replica

                storageclass_dict.update({
                    "provisioner": ss_settings.STORAGECLASSES_PROVISIONER_GFS,
                    "parameters": {
                        "resturl": heketi_endpoint,
                        "volumetype": volume_type
                    }
                })
            elif ss_settings.VOLUME_EBS == name:
                storageclass_dict["metadata"]["name"] = \
                    ss_settings.STORAGECLASSES_EBS_NAME
                update_dict = {
                    "provisioner": ss_settings.STORAGECLASSES_PROVISIONER_EBS,
                    "parameters": {}
                }
                zone = volume_settings.get(name, {}).get('zone')
                if zone:
                    update_dict['parameters']["zone"] = zone
                storageclass_dict.update(update_dict)
            else:
                # someone modify the ss_settings.VOLUME_DRIVERS
                continue
            if not default_sc and True is sc.get("default"):
                storageclass_dict["metadata"]["annotations"].update({
                    "storageclass.kubernetes.io/is-default-class": "true"
                })
                default_sc = True
            storageclasses["items"].append(storageclass_dict)

        return base64.b64encode(json.dumps(storageclasses))

    def trans_k8s(self):
        master_nodes = self.data['master']
        self.result['KUBERNETES_INIT_NODES'] = [master_nodes['private_ip']]
        self.result['KUBERNETES_COMPUTE_NODES'] = self.data.get('slave', [])
        self.result['KUBERNETES_TOKEN'] = GetInfo.get_k8s_token()
        self.result['KUBERNETES_CIDR'] = self.data.get('cidr', '')
        self.cni_type = (list(set(self.cni_types) & set(self.data['features'])))[0]
        self.result['KUBERNETES_VERSION'] = self.data.get(
            'version', ss_settings.SELF_SERVICE.get('KUBERNETES_DEFAULT_VERSION'))
        self.result['KUBERNETES_PORT_RANGE'] = ss_settings.SELF_SERVICE['KUBERNETES_PORT_RANGE']
        self.result['ADVERTISE_ADDRESS'] = master_nodes['private_ip']
        self.result['ADVERTISE_PUBLIC_ADDRESS'] = master_nodes['public_ip']

        heketi_endpoint = self.get_heketi_endpoint(
            self.result["ADVERTISE_ADDRESS"], self.result["KUBERNETES_PORT_RANGE"]
        )
        if heketi_endpoint:
            self.result['KUBERNETES_HEKETI_ENDPOINT'] = heketi_endpoint

        storageclasses = self.get_storageclasses(
            self.result.get('KUBERNETES_HEKETI_ENDPOINT')
        )
        if storageclasses:
            self.result['KUBERNETES_STORAGECLASSES'] = storageclasses

        self.trans_newk8s()
        self.trans_k8s_version()
        self.trans_cni()
        self.result['KUBERNETES_CNI_TYPE'] = self.cni_type

    def trans_newk8s(self):
        is_new_k8s = self.data.get('platform_version') == KUBERNETES_PLATFORM_VERSION_V3
        if is_new_k8s:
            self.result['USER_SETTINGS']['REGION']['FEATURES'].append('service-catalog')
        self.result['IS_NEW_K8S'] = str(is_new_k8s)

    def trans_k8s_version(self):
        o_version = self.data.get(
            'version', ss_settings.SELF_SERVICE.get('KUBERNETES_DEFAULT_VERSION'))
        if o_version not in ss_settings.SELF_SERVICE['KUBERNETES_SUPPORT_VERSION']:
            self.result['KUBERNETES_VERSION'] = o_version
        else:
            self.result['KUBERNETES_VERSION'] = \
                ss_settings.SELF_SERVICE['KUBERNETES_SUPPORT_VERSION'][o_version]['default']

        if ss_settings.KALDR_URL:
            self.result['KALDR_URL'] = ss_settings.KALDR_URL

    def trans_cni(self):
        if self.cni_type == 'macvlan':
            if self.data.get('platform_version') != KUBERNETES_PLATFORM_VERSION_V3:
                self.result['COMPONENT'].append('monkey')
            else:
                # Qcloud only support ipvlan now, turn macvlan into ipvlan
                if self.region['attr']['cloud']['name'].upper() == 'QCLOUD':
                    self.cni_type == 'ipvlan'

            self.result['USER_SETTINGS']['REGION']['FEATURES'].append("subnet")

        self.result[self.cni_type.upper()] = GetInfo.get_k8s_cni(self.data,
                                                                 self.region,
                                                                 self.cni_type)
        self.result['KUBERNETES_NETWORK_POLICY'] = self.data.get('cni', {}).get('network_policy', '')

    def transfer(self):
        super(K8sPocDataTransverter, self).transfer()
        self.trans_k8s()

    def _post_transfer(self):
        for feature in self.remove_feature:
            if feature in self.result['USER_SETTINGS']['REGION']['FEATURES']:
                self.result['USER_SETTINGS']['REGION']['FEATURES'].remove(feature)
        self.result['MANAGEMENT_REGISTRY'] = ss_settings.MANAGEMENT_REGISTRY
        if settings.PRIVATE_DEPLOY_SETTINGS['PRIVATE_DEPLOY_ENABLED']:
            self.result['LOCATION_SETTINGS'] = ss_settings.LOCATION_SETTINGS
        if CheckFeature.check_feature(self.data, 'alb') and 'haproxy' in self.result['COMPONENT']:
            self.result['COMPONENT'].remove('haproxy')


class K8sProdDataTransverter(K8sPocDataTransverter):
    def __init__(self, data, region):
        super(K8sProdDataTransverter, self).__init__(data, region)
        self.lbs = ['puck_public_lb', 'puck_private_lb', "advertise_address",
                    'advertise_public_address']

    def valid(self):
        Validation.valid_k8s_basic_setting(self.data, self.cni_types)
        Validation.valid_k8s_prod_init(self.data)
        if CheckFeature.check_feature(self.data, 'exec'):
            self.lbs.append('doom_lb')
        if CheckFeature.check_feature(self.data, 'registry'):
            self.lbs.append('registry_lb')
        if CheckFeature.check_feature(self.data, 'volume'):
            self.lbs.append('xin_agent_lb')
        Validation.valid_k8s_lb(self.data, self.lbs)
        super(K8sPocDataTransverter, self).valid()

    def trans_k8s(self):
        master_nodes = self.data['master']
        self.result['KUBERNETES_INIT_NODES'] = master_nodes
        self.result['KUBERNETES_COMPUTE_NODES'] = self.data.get('slave', [])
        self.result['KUBERNETES_TOKEN'] = GetInfo.get_k8s_token()
        self.result['KUBERNETES_CIDR'] = self.data.get('cidr', '')
        self.cni_type = (list(set(self.cni_types) & set(self.data['features'])))[0]
        self.trans_newk8s()
        self.trans_k8s_version()
        self.trans_cni()
        self.trans_k8s_lb()
        self.result['KUBERNETES_CNI_TYPE'] = self.cni_type

    def trans_customer_lbs(self, obj):
        self.result['PUCK_PUBLIC_LB'] = obj['puck_public_lb']
        self.result['PUCK_INTERNAL_LB'] = obj['puck_private_lb']
        self.result['ADVERTISE_ADDRESS'] = obj['advertise_address'].lower()
        self.result['ADVERTISE_PUBLIC_ADDRESS'] = obj['advertise_public_address'].lower()
        # kubeadm not support change port yet. When it support, we need support user input this.
        self.result['KUBERNETES_APISERVER_BACKEND_PORT'] = '6443'
        self.result['KUBERNETES_APISERVER_FRONTEND_PORT'] = '6443'
        self.result['KUBERNETES_DISCOVERY_BACKEND_PORT'] = '9898'
        self.result['KUBERNETES_DISCOVERY_FRONTEND_PORT'] = '9898'
        if CheckFeature.check_feature(self.data, 'exec'):
            self.result['DOOM_LB'] = obj['doom_lb']
        if CheckFeature.check_feature(self.data, 'registry'):
            self.result['REGISTRY_LB'] = obj['registry_lb']
        if CheckFeature.check_feature(self.data, 'volume'):
            self.result['XIN_AGENT_LB'] = obj['xin_agent_lb']

    def trans_k8s_lb(self):
        if 'controller_haproxy' in self.data:
            self.trans_controller_haproxy(self.data['controller_haproxy'])
            return
        if 'customer_lbs' in self.data:
            self.trans_customer_lbs(self.data['customer_lbs'])
