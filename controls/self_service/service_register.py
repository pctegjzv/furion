import requests
import json
import logging
import base64
import hashlib
import re
from util.func import get_stack
from util import error_code, const
from furion.exception import FurionException
from django.conf import settings
import time

LOG = logging.getLogger(__name__)


class ServiceRegister(object):
    def __init__(self, token, timeout=5):
        self.jakiro_endpoint = settings.JAKIRO['ENDPOINT'].strip('/')
        self.dns_endpoint = settings.DNS_REGISTER['ENDPOINT']
        self.timeout = timeout
        self.token = token
        self.username = settings.JAKIRO['USERNAME']
        self.password = settings.JAKIRO['PASSWORD']
        self.request = requests.session()
        self.headers = {'content-type': 'application/json', 'Authorization': 'Token {}'.format(self.token)}
        if self.username and self.password:
            auth = base64.encodestring('{}:{}'.format(self.username, self.password))
            self.headers['Authorization'] = 'Basic {}'.format(auth.strip())

    def do_request(self, path, method, data, code=200, **kwargs):
        endpoint = self.jakiro_endpoint
        if 'endpoint' in kwargs.keys():
            endpoint = kwargs['endpoint']
        url = '{}{}'.format(endpoint, path)
        payload = data if data is None else json.dumps(data)
        rep = self.request.request(method, url, data=payload, headers=self.headers, timeout=self.timeout)
        if code == 0:
            return rep
        if rep.status_code != code:
            stack = get_stack()
            LOG.error("Request: {} fail. code: {}. expect: {}. content: {}".format(url,
                                                                                   rep.status_code,
                                                                                   code,
                                                                                   rep.text))
            if rep.status_code < 500:
                raise FurionException('request_error', message=rep.json()['errors'][0]['message'])
            fg = int(time.time() * 1000000)
            raise FurionException(
                'request_error',
                message='Register error! Type: {} code: {}-{} id: {}'.format(
                    error_code.INTERNAL_ERROR_REGISTER,
                    stack[0].replace('create_', '').replace('remove_', ''),
                    rep.status_code,
                    fg))
        return rep

    def check(self, region, name, resource_type):
        path = '/v1/has_permission/{}/{}/{}/R'.format(region['namespace'], resource_type, name)
        rep = self.do_request(path, 'GET', None, 0)
        if rep.status_code == 404:
            return True
        if rep.status_code >= 500:
            stack = get_stack()
            fg = int(time.time() * 1000000)
            raise FurionException(
                'request_error',
                message='Register error! Type: {} code: {}-{} id: {}'.format(
                    error_code.INTERNAL_ERROR_CHECK,
                    stack[0].replace('create_', '').replace('remove_', ''),
                    rep.status_code,
                    fg))
        raise FurionException(
            'invalid_input', message='Resource {} {} already existed.'.format(resource_type, name)
        )

    def get_command(self, namespace, region_name, node_type):
        path = '/v1/regions/{}/{}/node-scripts?node_type={}&auto_create_node=True'.format(
            namespace, region_name, node_type)
        rep = self.do_request(path, 'GET', None, 200)
        return rep.json()

    def create_dns(self, region, ip):
        if not settings.DNS_REGISTER['SUFFIX']:
            return
        payload = {
            'record': '{}-{}.{}'.format(region['name'].replace('_', '-'),
                                        region['namespace'],
                                        settings.DNS_REGISTER['INDEX']),
            'domain': '{}'.format(settings.DNS_REGISTER['SUFFIX']),
            'type': 'A',
            'value': ip
        }
        p = re.compile('^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
        if p.match(ip) is None:
            payload['type'] = 'CNAME'
        extra_data = {
            'endpoint': settings.DNS_REGISTER['ENDPOINT']
        }
        self.do_request('/v1/domains', 'POST', payload, 201, **extra_data)

    def create_registry(self, region, data, endpoint, node):
        channel = ''
        if 'tunnel' in data['USER_SETTINGS']['REGION']['FEATURES']:
            channel = region['features']['tunnel']['port_mapping']['registry']
        payload = {
            'description': "{}-{}-{}-registry".format(region['namespace'], region['name'], node),
            'endpoint': endpoint,
            'protocol': 'http',
            'channel': 'http://{}'.format(channel) if channel else '',
            'issuer': data.get('REGISTRY_SETTINGS', {}).get('ISSUER'),
            'name': data.get('REGISTRY_SETTINGS', {}).get('NAME'),
            'region_id': region['id'],
            'audience': data.get('REGISTRY_SETTINGS', {}).get('SERVICE'),
        }
        path = '/v1/registries/{}/'.format(region['namespace'])
        self.do_request(path, 'POST', payload, 201)
        self.create_dns(region, node)

    def create_private_build(self, region, data):
        payload = {
            'endpoint': 'http://fake-build-endpoint',
            'region_id': region['id']
        }
        if 'CHRONOS_PUBLIC_LB' in data:
            chronos = data['CHRONOS_PUBLIC_LB']
            if 'tunnel' in data['USER_SETTINGS']['REGION']['FEATURES']:
                chronos = 'http://{}'.format(region['features']['tunnel']['port_mapping']['doom'])
            payload['endpoint'] = chronos

        path = '/v1/private-build-endpoints/{}/'.format(region['namespace'])
        self.do_request(path, 'POST', payload, 201)

    def create_alb(self, region, data, address_type, lb_type):
        ip = data['{}_NODE'.format(lb_type.upper())]['public_ip']
        if address_type == 'internal':
            ip = data['INTERNAL_HAPROXY_PRIVATE_IP']
        name = '{}-{}'.format(lb_type, ip.replace('.', '-'))
        p = re.compile('^((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)$')
        if p.match(ip) is None:
            ip_md5 = hashlib.md5(ip.encode('utf-8')).hexdigest()[8:-8]
            name = '{}-{}'.format(lb_type, ip_md5)
        payload = {
            'region_name': region['name'],
            'create_type': 'import',
            'type': lb_type,
            'address_type': address_type,
            'name': name,
            'address': ip,
            'bind_address': '*'
        }
        path = '/v1/load_balancers/{}/'.format(region['namespace'])
        self.do_request(path, 'POST', payload, 201)
        return name

    # TODO: remove
    def init_namespaces(self, region, data):
        if data['platform_version'] != const.KUBERNETES_PLATFORM_VERSION_V3:
            return
        payload = {
            "resource": {
                "name": "default"
            },
            "cluster": {
                "uuid": region['id'],
                "name": region['name']
            }
        }
        path = '/v2/namespaces?org_name={}'.format(region['namespace'])
        LOG.debug("try to init cluster namespaces with: {}".format(payload))
        self.do_request(path, 'POST', payload, 201)

    def remove_namespaces(self, region):
        if region['container_manager'] != const.CONTAINER_MANAGER_K8S:
            return
        if region['platform_version'] != const.KUBERNETES_PLATFORM_VERSION_V3:
            return
        query = "cluster={}&org_name={}&only_resources=true".format(region['name'],
                                                                    region['namespace'])
        path = "/v2/namespaces?{}".format(query)

        result = self.do_request(path, 'GET', None, 200)
        data = result.json()['results']
        LOG.debug("trying to remove cluster namespaces: {}".format(data))
        for item in data:
            if item['cluster']['uuid'] == region['id']:
                path = '/v2/namespaces/{}'.format(item['resource']['uuid'])
                self.do_request(path, 'DELETE', None, 204)

    def remove_alb(self, region):
        path = '/v1/load_balancers/{}?region_name={}'.format(region['namespace'], region['name'])
        rep = self.do_request(path, 'GET', None, 200)
        lbs = rep.json()
        for lb in lbs:
            name = lb['name']
            path = '/v1/load_balancers/{}/{}'.format(region['namespace'], name)
            self.do_request(path, 'DELETE', None, 204)

    def remove_registry(self, region):
        path = '/v1/registries/{}?region_id={}'.format(region['namespace'], region['id'])
        self.do_request(path, 'DELETE', None, 204)
        self.remove_dns(region)

    def remove_private_build(self, region):
        path = '/v1/private-build-endpoints/{}/?region_id={}'.format(region['namespace'],
                                                                     region['id'])
        self.do_request(path, 'DELETE', None, 204)

    def remove_dns(self, region):
        if not settings.DNS_REGISTER['SUFFIX']:
            return
        payload = {
            'record': '{}-{}.{}'.format(region['name'].replace('_', '-'),
                                        region['namespace'],
                                        settings.DNS_REGISTER['INDEX']),
            'domain': '{}'.format(settings.DNS_REGISTER['SUFFIX']),
        }
        extra_data = {
            'endpoint': settings.DNS_REGISTER['ENDPOINT']
        }
        self.do_request('/v1/domains', 'DELETE', payload, 204, **extra_data)
