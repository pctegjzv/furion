#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Node labels module

Operation about node labels, current only support kubernetes cluster.

"""
import logging

from django.db import transaction
from util.kubernetes_client import KubernetesManager

from app.models import Label

from furion.exception import FurionException, ErrCodes

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)

NON_EDITABLE_LABELS = [
    "ip",
]


class KubernetesLabelManager(KubernetesManager):
    def set_labels(self, data, node):
        """Set labels to a specific node.
        Use replace node resource to update labels.
        Args:
            data(dict):  key-value dict
            node(Node): node object
        Raises:
            FurionException
        """
        exist_labels = Label.objects.filter(node=node)
        for label in exist_labels:
            if not label.editable and label.key in data:
                raise FurionException(ErrCodes.NOT_ALLOWED,
                                      message_params='Label {} is not editable!'.format(label))
        k_node = self.get_node(node.private_ip, raise_ex=True)
        meta = k_node.metadata
        meta.labels.update(data)
        for label in exist_labels:
            if not label.editable:
                meta.labels[label.key] = label.value
            else:
                if label.key not in data:
                    meta.labels.pop(label.key, None)
        try:
            self.core_client.replace_node(name=k_node.metadata.name, body=k_node)
        except Exception as e:
            LOG.error("Update kubernetes labels error: {}".format(e))
            raise FurionException(ErrCodes.KUBERNETES_RESOURCE_ERROR, message=e)

        with transaction.atomic():
            obj_list = []
            for k, v in data.items():
                obj_list.append(Label(key=k, value=v, node=node, editable=k not in NON_EDITABLE_LABELS))
            Label.objects.filter(node=node, editable=True).delete()
            Label.objects.bulk_create(obj_list)
