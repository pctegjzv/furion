#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import transaction

from app.control import Control
from app.models import Stats
from app.serializers import StatsSerializer
from controls import scale
from controls.node import NodeControl
from furion.exception import FurionException
from util import const
from util.misc import send_metrics
import logging

__author__ = 'Hang Yan'

LOG = logging.getLogger(__name__)


class StatsControl(Control):
    model_name = 'Stats'
    model = Stats
    serializer = StatsSerializer

    @classmethod
    def create_or_update(cls, region, ac_type, data):
        """ Receive latest metrics from agent and update target objects'state.

        other actions may be taken:
            1. scale node
            2. send metrics to tiny

        Args:
            region(object):
            ac_type(str): metrics type,
                * KUBERNETES_METRICS
                * MESOS_METRICS
                * SWARM_METRICS
            data(dict):
                {
                    "type": "<ac_type>",
                    "nodes": [
                        <node info dict>
                    ],
                    "region": {
                        <region metrics dict>
                    }
                }
        Raise:
            FurionException:
                * invalid_input

        """
        if ac_type not in dict(const.STATS_TYPES).keys():
            raise FurionException('invalid_input', message='Not supported metrics type: {}!'.format(ac_type))
        if not data.get('region'):
            LOG.warning("Receive empty region metrics. | {} {}".format(ac_type, data))
            return
        cls.model.objects.update_or_create(region=region, type=ac_type, defaults={'data': data})
        with transaction.atomic():
            NodeControl.update_compute_nodes_stats(region, data['nodes'])
#        if ac_type != const.STAT_K8S_METRICS:
        cls._scale_node(region, data)
        try:
            send_metrics(cls._format_metrics(region, data))
        except Exception as e:
            LOG.error('Send region metrics error : {}'.format(e.message))

    @classmethod
    def _scale_node(cls, region, data):
        choice = scale.scale_check(region, data['region'])
        if choice == scale.SCALE_CHOICE.NONE:
            return
        scale.scale(choice, region, data['nodes'])

    @classmethod
    def update_disk_usage(cls, region_id, ac_type, private_ip, max_disk_usage, commit=True):
        try:
            stats = cls.model.objects.get(region__id=region_id, type=ac_type)
        except cls.model.DoesNotExist:
            return
        nodes = stats.data.get('nodes', [])
        for node in nodes:
            if node.get('private_ip') == private_ip:
                node['max_disk_usage'] = max_disk_usage
        if commit:
            stats.save(update_fields=['data', 'updated_at'])
        else:
            return stats

    @classmethod
    def _format_metrics(cls, region, data):
        dims = {
            'resource_id': region.id,
            'resource_type': 'REGION'
        }

        def _format_region(_data):
            _data = _data['region']

            metrics = [{
                'name': 'region-{}-percent'.format(x),
                'dimensions': dims,
                'value': _data['{}_utilization'.format(x)],
                'unit': 'Percent'

            } for x in ['cpus', 'mem']]
            counts = [{
                'name': 'region-{}'.format(x.replace('_', '-')),
                'dimensions': dims,
                'value': _data[x],
                'unit': 'Count'

            } for x in ['services_count', 'nodes_count', 'containers_count']]
            return metrics + counts

        def _format_node(node):
            node_dims = {
                'resource_id': region.id,
                'resource_type': 'NODE',
                'node_private_ip': node['private_ip']
            }
            metrics = [{
                'name': 'node-{}-percent'.format(x),
                'dimensions': node_dims,
                'value': node['{}_utilization'.format(x)],
                'unit': 'Percent'

            } for x in ['cpus', 'mem']]

            counts = [{
                'name': 'node-containers-count',
                'dimensions': node_dims,
                'value': node['containers_count'],
                'unit': 'Count'
            }]

            return metrics + counts

        return _format_region(data) + sum([_format_node(x) for x in data['nodes']], [])
