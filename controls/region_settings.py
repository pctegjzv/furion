#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from app.control import Control
from app.models import RegionSetting
from app.serializers import RegionSettingSerializer
from furion.exception import FurionException
from util import const


__author__ = 'Xiaodong Zhang'

LOG = logging.getLogger(__name__)


class RegionSettingControl(Control):

    model_name = 'RegionSetting'
    model = RegionSetting
    serializer = RegionSettingSerializer

    @classmethod
    def get_settings(cls, region):
        LOG.info('Get region settings | {}'.format(region.id))
        obj = RegionSetting.objects.get(region=region)
        user_settings = {
            'USER_SETTINGS': obj.user_settings,
            'USER_REGISTRIES': obj.user_registries,
            'REGION_LOCATION': obj.region_location,
            'IMAGE_LOCATION': obj.image_location
        }
        if region.container_manager == const.CONTAINER_MANAGER_K8S:
            user_settings['KUBERNETES_INIT_NODES'] = obj.master_node
            user_settings['KUBERNETES_COMPUTE_NODES'] = obj.slave_node
        else:
            user_settings['MASTER_NODES'] = obj.master_node
            user_settings['SLAVE_NODES'] = obj.slave_node

        user_settings.update(obj.cluster_settings)
        LOG.debug('user_setting: {}. template_type: {}'.format(user_settings, obj.cluster_template_type))
        return {
            'user_settings': user_settings,
            'template_type': obj.cluster_template_type
        }

    @classmethod
    def _post_save(cls, data):
        return super(RegionSettingControl, cls)._post_save(data)

    @classmethod
    def _pre_save(cls, data, **args):
        def _parse_controller_node():
            if data.get('MASTER_NODES') is None:
                master = data.pop('KUBERNETES_INIT_NODES')
                slave = data.pop('KUBERNETES_COMPUTE_NODES')
                return master, slave
            master = data.pop('MASTER_NODES')
            slave = data.pop('SLAVE_NODES')
            return master, slave

        master, slave = _parse_controller_node()
        insert_data = {
            'region': data.pop('region_id'),
            'master_node': master,
            'slave_node': slave,
            'region_location': data.pop('REGION_LOCATION'),
            'image_location': data.pop('IMAGE_LOCATION'),
            'user_settings': data.pop('USER_SETTINGS'),
            'user_registries': data.pop('USER_REGISTRIES'),
            'cluster_template_type': data.pop('cluster_template_type'),
            'cluster_settings': {}
        }
        insert_data['cluster_settings'].update(data)
        return insert_data

    @classmethod
    def _pre_update(cls, data, **args):
        data = cls._pre_save(data)
        data.pop('region')
        # data.pop('slave_node', None)
        return data

    @classmethod
    def update(cls, data, **args):
        try:
            data = cls._pre_update(data, **args)
            rs = cls.model.objects.get_or_create(defaults=data, **args)
            if not rs[1]:
                data.pop('slave_node', None)
                LOG.debug("Get update data for {} | {}".format(cls.model_name, data))
                cls.model.objects.filter(**args).update(**data)
            return rs[0]
        except Exception as e:
            raise FurionException('invalid_args', message=str(e))
