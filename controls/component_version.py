#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from app.control import Control
from app.models import ComponentVersion
from app.serializers import ComponentVersionSerializer
from furion.exception import FurionException
from util import phoenix
import traceback

__author__ = 'Xiaodong Zhang'

LOG = logging.getLogger(__name__)


class ComponentVersionControl(Control):

    model_name = 'ComponentVersion'
    model = ComponentVersion
    serializer = ComponentVersionSerializer

    @classmethod
    def get_default_version(cls, region_settings):
        LOG.info('Get region default versions | {}'.format(region_settings.region.id))
        components = ComponentVersionControl._get_list(**{'region_settings_id': region_settings.id})
        result = {}
        for component in components:
            result[component.name] = phoenix.ComponentVersionManager.get_default_version(
                component.name)
        return result

    @classmethod
    def get_current_version(cls, region_settings):
        LOG.info('Get region current versions | {}'.format(region_settings.region.id))
        result = {}
        components = ComponentVersionControl._get_list(**{'region_settings_id': region_settings.id})
        for component in components:
            result[component.name] = {
                'version': component.version,
                'incarnation': component.incarnation,
                'description': component.description
            }
        return result

    @classmethod
    def filter_components(cls, current, default):
        # TODO: Support add and remove components
        result = {}
        for name, version in current.items():
            if version['version'] == default[name]['version'] and \
                    version['incarnation'] == default[name]['incarnation']:
                continue
            result[name] = default[name]
        return result

    @classmethod
    def upgrade_component(cls, region, comp_name, current_version, latest_version, add_env, del_env):
        try:
            resource = phoenix.ResourceManager.retrieve_by_name(region.env_uuid, comp_name)
        except Exception:
            raise FurionException('upgrade_error',
                                  message_params=['get resource {} from phoenix error'.format(comp_name)])
        if not resource or len(resource) != 1:
            raise FurionException('upgrade_error', message_params=['wrong component name {}'.format(comp_name)])
        comp_data = resource[0]
        image = comp_data.get('identifier').get('image')
        # Adjust for private deploy 
        current_version_from_resource = image.split(':')[-1]
        if current_version_from_resource != current_version:
            message = 'component {} current_version not match {}'.format(comp_name, current_version_from_resource)
            raise FurionException('upgrade_error', message_params=[message])
        latest_version_from_alchimest = phoenix.ComponentVersionManager.get_default_version(comp_name).get('version')
        if latest_version_from_alchimest != latest_version:
            message = 'component {} only support upgrade to latest_version [{}]'.format(comp_name,
                                                                                        latest_version_from_alchimest)
            raise FurionException('upgrade_error', message_params=[message])

        comp_data['identifier']['image'] = image.replace(current_version_from_resource, latest_version)

        if del_env:
            for env in del_env:
                comp_data['configuration']['environment'].pop(env)
        if add_env:
            for env in add_env:
                vals = env.split('=')
                if len(vals) != 2:
                    raise FurionException('upgrade_error', message_params=['[{}] is wrong, use [ENV=xxxx]'.format(env)])
                comp_data['configuration']['environment'][vals[0]] = vals[1]
        result_env_set = set(comp_data['configuration']['environment'].keys())
        expected_env_set = set(phoenix.ComponentManager.get_component_envs(comp_name))
        if result_env_set != expected_env_set:
            message = 'component {} env list not match, result:{}, expected:{}'.format(comp_name, result_env_set,
                                                                                       expected_env_set)
            raise FurionException('upgrade_error', message_params=[message])
        try:
            phoenix.ResourceManager.update(region.env_uuid, comp_data.get('resource_uuid'), comp_data)
        except Exception as ex:
            LOG.error('Upgrade region {} : component {} fail. version: {}. error: {}. stack: {}'.format(
                region.id, comp_name, latest_version, ex.message, traceback.format_exc()
            ))
            raise FurionException('upgrade_error', message_params=['Upgrade component {} fail'.format(comp_name)])

    @classmethod
    def upgrade_spectre(cls, region, rs_obj, spectre):
        if rs_obj.is_update_blocked:
            raise FurionException('upgrade_error', message_params=['This region has been locked. Please contact us!'])
        latest_version = spectre.get('latest_version')
        latest_version_from_phoenix = phoenix.EnvironmentManager.get_spectre_latest_version()
        if latest_version != latest_version_from_phoenix:
            message = 'spectre only support upgrade to latest_version [{}]'.format(latest_version_from_phoenix)
            raise FurionException('upgrade_error', message_params=[message])
        phoenix.EnvironmentManager.upgrade_spectre_to_latest_version(region.env_uuid)

    @classmethod
    def _post_save(cls, data):
        return super(ComponentVersionControl, cls)._post_save(data)

    @classmethod
    def _pre_save(cls, data, **args):
        data['name'] = args.get('name')
        data['region_settings'] = args.get('region_settings_id')
        return data

    @classmethod
    def _pre_update(cls, data, **args):
        data.pop('filename', '')
        return data

    @classmethod
    def update_version(cls, data, region_setting, component_name):
        super(ComponentVersionControl, cls).update(data,
                                                   **{'region_settings_id': region_setting.id, 'name': component_name})
        if not region_setting.is_update_blocked:
            region_setting.is_update_blocked = True
            region_setting.save()

    @classmethod
    def upgrade_components(cls, region, rs_obj, components):
        if rs_obj.is_update_blocked:
            raise FurionException('upgrade_error', message_params=['This region has been locked. Please contact us!'])
        for comp_name, comp_upgrade in components.items():
            cls.upgrade_component(region, comp_name,
                                  comp_upgrade.get('current_version'), comp_upgrade.get('latest_version'),
                                  comp_upgrade.get('add_env_list'), comp_upgrade.get('del_env_list'))

    @classmethod
    def update(cls, data, **args):
        cls._pre_update(data, **args)
        rs = cls.model.objects.get_or_create(defaults=data, **args)
        if not rs[1]:
            super(ComponentVersionControl, cls).update(data, **args)

    @classmethod
    def delete(cls, **args):
        objs = cls._get_list(**args)
        for obj in objs:
            obj.delete()
